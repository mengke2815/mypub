﻿using CommonUtils;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Math;
using Org.BouncyCastle.Utilities.Encoders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleGMSM4
{
    public static class Tester01
    {
        public static void TestSM4_CBC()
        {
            String content = "1234泰酷拉";
            Console.WriteLine("待加密字符串:" + content);

            String key = "9814548961710661";
            byte[] byteKey = Encoding.UTF8.GetBytes(key);
            String civ = "1234567890123456"; // SM4 IV 长度16
            byte[] byteIV = Encoding.UTF8.GetBytes(civ);

            //string algo = "SM4/CBC/PKCS7Padding";
            string algo = "SM4/CBC/PKCS5Padding"; // 与JAVA 保持一致

            byte[] sourceData = Encoding.UTF8.GetBytes(content);
            byte[] encryptedData = GmUtil.Sm4EncryptCBC(byteKey, sourceData, byteIV, algo);
            string encryptedStr = BitConverter.ToString(encryptedData).Replace("-", "").ToLower();
            Console.WriteLine(".NET SM4 CBC 加密后:" + encryptedStr);
            
            byte[] byJavaEncrypted = Hex.Decode(encryptedStr);
            byte[] decryptedData = GmUtil.Sm4DecryptCBC(byteKey, byJavaEncrypted, byteIV, algo);
            string sm4DecryptedStr = Encoding.UTF8.GetString(decryptedData);
            Console.WriteLine(".NET SM4 CBC 解密结果:" + sm4DecryptedStr);

            //JAVA 加 .NET  解
            encryptedStr = "c5311f14ef3e80735c7a1ca7f81230412022273d815bd33c45f6f141975873f0";
            Console.WriteLine("JAVA程序 SM4 CBC 加密后的串:" + encryptedStr);
             byJavaEncrypted = Hex.Decode(encryptedStr);
              decryptedData = GmUtil.Sm4DecryptCBC(byteKey, byJavaEncrypted, byteIV, algo);
              sm4DecryptedStr = Encoding.UTF8.GetString(decryptedData);
            Console.WriteLine("JAVA 加 .NET  解，结果:" + sm4DecryptedStr);
        }


        public static void TestSM2Sign()
        {
            string userId = "1234567812345678";
            byte[] byUserId = Encoding.UTF8.GetBytes(userId);
            String privateKeyHex = "FAB8BBE670FAE338C9E9382B9FB6485225C11A3ECB84C938F10F20A93B6215F0";
            string pubKeyHex = "049EF573019D9A03B16B0BE44FC8A5B4E8E098F56034C97B312282DD0B4810AFC3CC759673ED0FC9B9DC7E6FA38F0E2B121E02654BF37EA6B63FAF2A0D6013EADF";
            //如果是130位公钥，.NET 使用的话，把开头的04截取掉。
            if (pubKeyHex.Length == 130)
            {
                pubKeyHex = pubKeyHex.Substring(2, 128);
            }
            //公钥X，前64位
            String x = pubKeyHex.Substring(0, 64);
            //公钥Y，后64位
            String y = pubKeyHex.Substring(64);
            //获取公钥对象
            AsymmetricKeyParameter publicKey1 = GmUtil.GetPublickeyFromXY(new BigInteger(x, 16), new BigInteger(y, 16));
            BigInteger d = new BigInteger(privateKeyHex, 16);
            //获取私钥对象，用ECPrivateKeyParameters 或 AsymmetricKeyParameter 都可以
            //ECPrivateKeyParameters bcecPrivateKey = CommonUtils.GmUtil.GetPrivatekeyFromD(d);
            AsymmetricKeyParameter bcecPrivateKey = CommonUtils.GmUtil.GetPrivatekeyFromD(d);

            String content = "1234泰酷拉NET";
            Console.WriteLine("待处理字符串：" + content);
            //SignSm3WithSm2 是RS，SignSm3WithSm2Asn1Rs 是 asn1
            byte[] digestByte = GmUtil.SignSm3WithSm2(Encoding.UTF8.GetBytes(content), byUserId, bcecPrivateKey);
            string strSM2 = Convert.ToBase64String(digestByte);
            Console.WriteLine("SM2加签后：" + strSM2);

            //.NET 验签    
            byte[] byToProc = Convert.FromBase64String(strSM2);
            //顺序：报文，userId，签名值，公钥。
            bool verifySign = GmUtil.VerifySm3WithSm2(Encoding.UTF8.GetBytes(content), byUserId, byToProc, publicKey1);

            Console.WriteLine("SM2 验签：" + verifySign.ToString());

            //JAVA 签名 .NET验签
            string javaContent = "1234泰酷拉JJ"; //注意：报文要和JAVA一致
            Console.WriteLine("javaContent：" + javaContent);
            string javaSM2 = "MEUCIF5PXxIlF0NmQaUtfIGLbZm4JuYT4bkYyoFMA/eIqVaUAiEAkRT3GkrtY2YtUSF9Ya0jOLRMcMUuHNLiWPTy591vnco=";

            Console.WriteLine("javaSM2签名结果：" + javaSM2);
            byToProc = Convert.FromBase64String(javaSM2);
            //注意：JAVA HUTOOL - sm2.sign 结果格式是 asn1 的，我们得用 VerifySm3WithSm2Asn1Rs。
            verifySign = GmUtil.VerifySm3WithSm2Asn1Rs(Encoding.UTF8.GetBytes(javaContent), byUserId, byToProc, publicKey1);

            Console.WriteLine("JAVA SM2 验签：" + verifySign.ToString());
        }


        public static void TestSM2Enc()
        {
            String privateKeyHex = "FAB8BBE670FAE338C9E9382B9FB6485225C11A3ECB84C938F10F20A93B6215F0";
            //完整公钥128位“9EF573019D9A03B16B0BE44FC8A5B4E8E098F56034C97B312282DD0B4810AFC3CC759673ED0FC9B9DC7E6FA38F0E2B121E02654BF37EA6B63FAF2A0D6013EADF”
            //完整公钥130位，比128位开头多了04“049EF573019D9A03B16B0BE44FC8A5B4E8E098F56034C97B312282DD0B4810AFC3CC759673ED0FC9B9DC7E6FA38F0E2B121E02654BF37EA6B63FAF2A0D6013EADF”
            string pubKeyHex = "049EF573019D9A03B16B0BE44FC8A5B4E8E098F56034C97B312282DD0B4810AFC3CC759673ED0FC9B9DC7E6FA38F0E2B121E02654BF37EA6B63FAF2A0D6013EADF";
            //如果是130位公钥，.NET 使用的话，把开头的04截取掉。
            if (pubKeyHex.Length == 130)
            {
                pubKeyHex = pubKeyHex.Substring(2, 128);
            }
            //公钥X，前64位
            String x = pubKeyHex.Substring(0, 64);
            //公钥Y，后64位
            String y = pubKeyHex.Substring(64);
            //获取公钥对象
            AsymmetricKeyParameter publicKey1 = GmUtil.GetPublickeyFromXY(new BigInteger(x, 16), new BigInteger(y, 16));
            String content = "1234泰酷拉NET";
            Console.WriteLine("待处理字符串：" + content);
            //GmUtil.Sm2Encrypt 对应  C1C3C2
            // GmUtil.Sm2EncryptOld ：C1C2C3
            byte[] digestByte = GmUtil.Sm2Encrypt(Encoding.UTF8.GetBytes(content), publicKey1);
            string strSM2 = Hex.ToHexString(digestByte);
            Console.WriteLine("SM2加密后：" + strSM2);
            // 4. .NET BC库SM2加密结果会带04，如果JAVA 那边报 Invalid point encoding 错误，删除加密结果前的04。如果对方要的是BASE64的加密结果，我们可以先转16进制字符串，裁掉04，再转BASE64。
            string newCipherText = Hex.ToHexString(digestByte);
            if (newCipherText.StartsWith("04"))
            {
                newCipherText = newCipherText.Substring(2);
            }
            Console.WriteLine("截取04后，加密结果：" + newCipherText);

            //.NET 自加自解
            BigInteger d = new BigInteger(privateKeyHex, 16);
            //先拿到私钥对象，用ECPrivateKeyParameters 或 AsymmetricKeyParameter 都可以
            //ECPrivateKeyParameters bcecPrivateKey = CommonUtils.GmUtil.GetPrivatekeyFromD(d);
            AsymmetricKeyParameter bcecPrivateKey = CommonUtils.GmUtil.GetPrivatekeyFromD(d);
            byte[] byToDecrypt = Hex.Decode(strSM2);
            byte[] byDecrypted = GmUtil.Sm2Decrypt(byToDecrypt, bcecPrivateKey);
            String strDecrypted = Encoding.UTF8.GetString(byDecrypted);
            Console.WriteLine("SM2解密后：" + strDecrypted);

            //JAVA 结果，.NET来解
            string javaSM2 = "04a7aaa9fd91aea6f99787ef431e19cb9feecc5bfb97fb445ce529c78c04676f1792e06b3a2814d1bda80bd3f63e530c149fc03911f1b81007dc86cef2c03f30c7fecc8b256272f881a8f2f4e71351c45d5bb27e8531f1e2ea6d55150c88f5026b8783ccef867a510a313178cfd26177";
            //.NET BC库解密，密文前要加 “04”，否则会报 Invalid point encoding XX
            //如果加密结果是BASE64的，把BASE64转16进制字符串，再判断是否04开头。 
            //如果对方源码，固定截取的头2位，那么就不用判断是否04开头了，直接写死：javaSM2 = "04" + javaSM2;
            if (!javaSM2.StartsWith("04"))
            {
                javaSM2 = "04" + javaSM2;
            }
            Console.WriteLine("javaSM2加密结果：" + javaSM2);
            byToDecrypt = Hex.Decode(javaSM2);
            byDecrypted = GmUtil.Sm2Decrypt(byToDecrypt, bcecPrivateKey);
            strDecrypted = Encoding.UTF8.GetString(byDecrypted);
            Console.WriteLine("java SM2解密后：" + strDecrypted);
        }

        public static void TestSM3()
        {
            String content = "1234泰酷拉";
            Console.WriteLine("待处理字符串：" + content);

            byte[] digestByte = GmUtil.Sm3(Encoding.UTF8.GetBytes(content));
            string strSM3 = Hex.ToHexString(digestByte);
            Console.WriteLine("SM3 HASH后：" + strSM3);
            string javaSM3 = "40f9b63398b72bee91f5e59c3c3d7fe9f66c4fb1637ec7adb9babb880e6489ec";
            Console.WriteLine("javaSM3：" + javaSM3);
            if (strSM3 == javaSM3)
            {
                Console.WriteLine("相等");
            }
        }


        public static void TestSM4()
        {
            String content = "1234泰酷拉";
            String key = "9814548961710661";
            byte[] byteKey = Encoding.UTF8.GetBytes(key);
            string algo = "SM4/ECB/PKCS7Padding";
            byte[] sourceData = Encoding.UTF8.GetBytes(content);
            byte[] encryptedData = GmUtil.Sm4EncryptECB(byteKey, sourceData, algo);
            string encryptedStr = BitConverter.ToString(encryptedData).Replace("-", "").ToLower();
            Console.WriteLine("encryptedStr:" + encryptedStr);

            string javaEncryptedStr = "02d225c9ff6bb99be6a67421aae4f3aa";
            Console.WriteLine("JAVA程序加密后的串:" + javaEncryptedStr);
            byte[] byJavaEncrypted = Hex.Decode(javaEncryptedStr);
            byte[] decryptedData = GmUtil.Sm4DecryptECB(byteKey, byJavaEncrypted, algo);
            string sm4DecryptedStr = Encoding.UTF8.GetString(decryptedData);
            Console.WriteLine("解密结果:" + sm4DecryptedStr);
        }


    }
}
