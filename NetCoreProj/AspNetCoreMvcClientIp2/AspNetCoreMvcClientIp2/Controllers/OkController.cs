﻿using CommonUtils;
using CommonUtils.Extensions;
using Microsoft.AspNetCore.Mvc;

namespace AspNetCoreMvcClientIp2.Controllers
{
    public class OkController : Controller
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public OkController(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }
        // GET: OkController
        public ActionResult Index()
        {
            var ip = CoreMvcClientIpUtil.GetClientIP(_httpContextAccessor.HttpContext);
            ViewBag.myip1 = "ip:" + ip;

            //直接实例化HttpContextAccessor
            IHttpContextAccessor ih = new HttpContextAccessor();
            string ip2 = CoreMvcClientIpUtil.GetClientIP(ih.HttpContext);
            ViewBag.myip2 = "ip2:" + ip2;

            //使用ControllerBase的HttpContext
            var ip3 = CoreMvcClientIpUtil.GetClientIP(HttpContext);
            ViewBag.myip3 = "ip3:" + ip3;

            //扩展方法
            var ip4 = HttpContext.GetClientIP();
            ViewBag.myip4 = "ip4:" + ip4;

            return View();
        }

         
    }
}
