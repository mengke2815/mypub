﻿using AspNetCoreMvcClientIp2.Models;
using CommonUtils;
using CommonUtils.Extensions;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using System.Text;

namespace AspNetCoreMvcClientIp2.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IHttpContextAccessor _httpContextAccessor;
        public HomeController(ILogger<HomeController> logger, IHttpContextAccessor httpContextAccessor)
        {
            _logger = logger;
            _httpContextAccessor = httpContextAccessor;
        }

        public IActionResult Index()
        {
            //注入实例化
            var ip = CoreMvcClientIpUtil.GetClientIP(_httpContextAccessor.HttpContext);
            ViewBag.myip1 = "ip:" + ip;

            //直接实例化HttpContextAccessor
            IHttpContextAccessor ih = new HttpContextAccessor();
            string ip2 = CoreMvcClientIpUtil.GetClientIP(ih.HttpContext);
            ViewBag.myip2 = "ip2:" + ip2;

            //使用ControllerBase的HttpContext
            var ip3 = CoreMvcClientIpUtil.GetClientIP(HttpContext);
            ViewBag.myip3 = "ip3:" + ip3;

            //扩展方法
            var ip4 = HttpContext.GetClientIP();
            ViewBag.myip4 = "ip4:" + ip4;

            #region MyRegion
            StringBuilder sc = new StringBuilder();

            var tmp = HttpContext.Request.Headers["Cdn-Src-Ip"].FirstOrDefault();
            
            if (!string.IsNullOrWhiteSpace(tmp))
                sc.AppendLine("Cdn-Src-Ip:" + tmp);

            tmp = HttpContext.Request.Headers["X-Forwarded-For"].FirstOrDefault();
            if (!string.IsNullOrWhiteSpace(tmp))
                sc.AppendLine("X-Forwarded-For:" + tmp);

            tmp = HttpContext.Connection.RemoteIpAddress.ToString();
            if (!string.IsNullOrWhiteSpace(tmp))
                sc.AppendLine("RemoteIpAddress:" + tmp);

            ViewBag.myip5 = "ip5:" + sc.ToString();
            #endregion

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}