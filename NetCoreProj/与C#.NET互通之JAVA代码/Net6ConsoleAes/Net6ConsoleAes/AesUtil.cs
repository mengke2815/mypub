﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Net6ConsoleAes
{
    public static class AesUtil
    {
        public static byte[] EncryptCbc(byte[] key, byte[] iv, byte[] ciphertext)
        {
            using (Aes aes = Aes.Create())
            {
                aes.Key = key;
                return aes.EncryptCbc(ciphertext, iv, PaddingMode.PKCS7);
            }
        }

        public static byte[] DecryptCbc(byte[] key, byte[] iv, byte[] ciphertext)
        {
            using (Aes aes = Aes.Create())
            {
                aes.Key = key;
                return aes.DecryptCbc(ciphertext, iv, PaddingMode.PKCS7);
            }
        }
    }
}
