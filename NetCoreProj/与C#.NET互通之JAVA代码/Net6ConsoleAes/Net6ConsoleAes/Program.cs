﻿// See https://aka.ms/new-console-template for more information
using Net6ConsoleAes;
using System.Text;

Console.WriteLine("Hello, World!");

try
{
    // AES/CBC/PKCS7 对应JAVA AES/CBC/PKCS5
    //KEY 16字符对应AES128,32字符对应AES256。 32 * 8 = 256 。
    string key = "1234567890123456";
    //IV 长度只能是 16。
    string iv = "1234567890123456";
    string myText = "中华人民共和国，OK";

    Console.WriteLine(" 待加密串：" + myText);

    var tmp1 = AesUtil.EncryptCbc(Encoding.UTF8.GetBytes(key), Encoding.UTF8.GetBytes(iv), Encoding.UTF8.GetBytes(myText));
    var base641 = Convert.ToBase64String(tmp1);
    Console.WriteLine(" 加密后转BASE64：" + base641);

    var toDecrypt = Convert.FromBase64String(base641);
    var decryptByte = AesUtil.DecryptCbc(Encoding.UTF8.GetBytes(key), Encoding.UTF8.GetBytes(iv), toDecrypt);
    var decryptStr = Encoding.UTF8.GetString(decryptByte);
    Console.WriteLine(" 解密后字符串：" + decryptStr);

    HashUtil.TestMd5();
}
catch (Exception ex)
{
    Console.WriteLine(" ex：" + ex.Message);
}
Console.ReadKey();