﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Net6ConsoleAes
{
    public static class HashUtil
    {
        public static void TestMd5()
        {
            string strOrg = "中华人民共和国";
            Console.WriteLine("md5 hash 16进制小写："+ Md5Hash(strOrg));
            Console.WriteLine("md5 hash 16进制大写：" + Md5HashUpper(strOrg));
            Console.WriteLine("Md5HashBitConvert：" + Md5HashBitConvert(strOrg));
            Console.WriteLine("md5 hash 16进制大写2 ：" + Md5Hash(strOrg).ToUpper());

        }

        public static String Md5Hash(String str)
        {
            MD5 md = MD5.Create();
            byte[] bytes = Encoding.UTF8.GetBytes(str);
            byte[] buffer2 = md.ComputeHash(bytes);
            string rst = "";
            //小写的 x2 是16进制小写。大写 X2 是转16进制大写。
            for (int i = 0; i < buffer2.Length; i++)
            {
                rst = rst + buffer2[i].ToString("x2");
            }
            return rst;
        }


        public static String Md5HashUpper(String str)
        {
            MD5 md = MD5.Create();
            byte[] bytes = Encoding.UTF8.GetBytes(str);
            byte[] buffer2 = md.ComputeHash(bytes);
            string rst = "";
            //小写的 x2 是16进制小写。大写 X2 是转16进制大写。
            for (int i = 0; i < buffer2.Length; i++)
            {
                rst = rst + buffer2[i].ToString("X2");
            }
            return rst;
        }

        public static String Md5HashBitConvert(String str)
        {
            MD5 md = MD5.Create();
            byte[] bytes = Encoding.UTF8.GetBytes(str);
            byte[] buffer2 = md.ComputeHash(bytes);
            string rst = BitConverter.ToString(buffer2);
            Console.WriteLine("BitConverter.ToString()之后原始值：" + rst);
            string rst2 = rst.Replace("-", "");
            Console.WriteLine("去掉减号‘-’后值：" + rst2);
            return rst2;
        }

    }
}
