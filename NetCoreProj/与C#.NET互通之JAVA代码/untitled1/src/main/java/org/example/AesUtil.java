package org.example;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class AesUtil {
    private static final String CBC_PKCS5_PADDING = "AES/CBC/PKCS5Padding";

    public static void Test()
    {
        try     {
            // 这里仅作 AES CBC 的演示，AES ECB 的搜索下就有了
            //注意点：
            //1。 AES的密钥（KEY），一般是16或32个字符，对应AES128 或 AES256。32  * 8=256 。
            //2。 Padding:JAVA PKCS5 对应 C# PKCS7。
            //3。 如果AES的KEY长度不为16或32个字符，那AES的KEY一定是做过演算，演算后才能充当AES的KEY(byte[])；
            // 比如：截断字符串，或MD5哈希后作为AES的KEY(MD5哈希后是16个byte，等于128bit，刚好做为AES的密钥)。
            //4。KEY和IV字符串转byte数组时，一般是UTF8编码。
            //5。明文转byte数组时，一般是UTF8编码，但双方也可以约定成其它编码，如果：GB2312，但GB2312仅支持常见汉字。
            //6。加密后得到byte数组，byte数组没法儿传输，需要转成字符串给其它程序。可能是历史原因，一般都是转16进制字符串，但也有的是转BASE64。
            //7。解密时，要看对方的加密结果是什么类型的字符串，好做对应的decode，得到byte数组后，作为解密方法的输入参数。
            //8。解密后，得到byte数组，需要转为字符串，编码要与加密时一致，如：UTF8。
            String key="12345678901234561234567890123456";//AES的KEY，一般是16或32长度，对应AES128 或 AES 256。
            String iv="1234567890123456";//AES的IV，一般是16长度。
            String md5HashInput="中华人民共和国";
            System.out.println( " 加密 输入：" +md5HashInput);
            String md5HashOutput=AesCbcEncrypt(md5HashInput,key,iv);
            System.out.println( " 加密 输出：" +md5HashOutput);

            System.out.println( " 解密 输入：" +md5HashOutput);
            String strDecrypt=AesCbcDecrypt(md5HashOutput,key,iv);
            System.out.println( " 解密 输出：" +strDecrypt);

        }catch (Exception ex)
        {
            System.out.println( "ex:"+ex.getMessage() );
        }
    }

    //AES CBC 加密，结果输出为：16进制字符串
    public static String AesCbcEncrypt(String content, String key, String iv)throws Exception{

        Cipher cipher = Cipher.getInstance(CBC_PKCS5_PADDING);
        //使用加密秘钥,key和IV转 byte[] 一般用UTF8编码
        SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes(), "AES");
        //偏移量
        IvParameterSpec zeroIv = new IvParameterSpec(iv.getBytes());
        //明文转byte[]，编码双方要统一，如：UTF8。
        byte[] byteContent = content.getBytes();

        cipher.init(Cipher.ENCRYPT_MODE, skeySpec, zeroIv);// 初始化为加密模式的密码器
        byte[] result = cipher.doFinal(byteContent);// 加密
        //AES 加密后 byte[]，一般转16进制字符串，也有的转 BASE64的。双方统一。
        String hexRst=bytesToHexString(result);
        return hexRst;

    }

    //AES CBC 解密，输入为：16进制字符串
    public static String AesCbcDecrypt(String content, String key, String iv)throws Exception{

        //把加密串，从16进制字符串转为 byte数组。
        //要看对方加密后果是什么类型的字符串，如果是BASE64的，这里要 Base64.decode，而不是 hexStringToBytes
        byte[] byteContent = hexStringToBytes(content);

        Cipher cipher = Cipher.getInstance(CBC_PKCS5_PADDING);
        //使用加密秘钥,key和IV转 byte[] 一般用UTF8编码
        SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes(), "AES");
        //偏移量
        IvParameterSpec zeroIv = new IvParameterSpec(iv.getBytes());

        cipher.init(Cipher.DECRYPT_MODE, skeySpec, zeroIv);// 初始化
        byte[] result = cipher.doFinal(byteContent);// 解密
        //解密后，用UTF8转成string。
        String Rst= new String(result);
        return Rst;

    }

    //byte[] 转16进制字符串
    public static String bytesToHexString(byte[] src){
        StringBuilder stringBuilder = new StringBuilder("");
        if(src==null||src.length<=0){
            return null;
        }
        for (int i = 0; i < src.length; i++) {
            int v = src[i] & 0xFF;
            String hv = Integer.toHexString(v);
            if (hv.length() < 2) {
                stringBuilder.append(0);
            }
            stringBuilder.append(hv);
        }
        return stringBuilder.toString();
    }

    //16进制字符串 转 byte[]
    public static byte[] hexStringToBytes(String hexString) {
        if (hexString == null || hexString.equals("")) {
            return null;
        }
        hexString = hexString.toUpperCase();
        int length = hexString.length() / 2;
        char[] hexChars = hexString.toCharArray();
        byte[] d = new byte[length];
        for (int i = 0; i < length; i++) {
            int pos = i * 2;
            d[i] = (byte) (charToByte(hexChars[pos]) << 4 | charToByte(hexChars[pos + 1]));
        }
        return d;
    }

    private static byte charToByte(char c) {
        return (byte) "0123456789ABCDEF".indexOf(c);
    }
}
