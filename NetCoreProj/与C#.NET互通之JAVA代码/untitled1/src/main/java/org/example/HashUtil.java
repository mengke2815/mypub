package org.example;

import java.security.MessageDigest;

public class HashUtil {
    public static String Md5Hash(String str) throws Exception
    {
        //默认UTF8
        byte[] byStr=str.getBytes();
        String rst="";
        // MessageDigest instance for MD5
        MessageDigest md = MessageDigest.getInstance("MD5");
        // Update MessageDigest with input text in bytes
        md.update(byStr);
        // Get the hashbytes
        byte[] hashBytes = md.digest();
        // Convert hash bytes to hex format
        StringBuilder sc = new StringBuilder();
        //小写的 x 是转16进制小写。
        for (byte b : hashBytes) {
            sc.append(String.format("%02x", b));
        }
        rst=sc.toString();
        return rst;
    }

    public static String Md5HashUpper(String str) throws Exception
    {
        //默认UTF8
        byte[] byStr=str.getBytes();
        String rst="";
        // MessageDigest instance for MD5
        MessageDigest md = MessageDigest.getInstance("MD5");
        // Update MessageDigest with input text in bytes
        md.update(byStr);
        // Get the hashbytes
        byte[] hashBytes = md.digest();
        // Convert hash bytes to hex format
        StringBuilder sc = new StringBuilder();
        //小写的 x 是转16进制小写。大写的 X 转为大写。
        for (byte b : hashBytes) {
            sc.append(String.format("%02X", b));
        }
        rst=sc.toString();
        return rst;
    }
}
