﻿// See https://aka.ms/new-console-template for more information
using CommonUtils;
using System.Data;

Console.WriteLine("Hello, World!");


try
{
    string dir = AppContext.BaseDirectory;
    //2003
    string fullName = Path.Combine(dir, "测试excel.xls");
    DataTable dt = ExcelHelper.GetDataTable(fullName);

    Console.WriteLine("Hello, World!" + dir);
    //2007
    string fullName2 = Path.Combine(dir, "测试excel.xlsx");
    //dt = ExcelHelper.GetDataTable(fullName);
    //DataTable dt2 = ExcelHelper.GetDataTable(fullName2, "sheetf");
    DataTable dt2 = ExcelHelper.GetDataTable(fullName2);

    string saveFullName = Path.Combine(dir, "save_excel.xls");
    //ExcelHelper2.ExportExcelByMemoryStream(saveFullName, dt2);
    string saveFullName2 = Path.Combine(dir, "save_excel2.xls");
    ExcelHelper.GenerateExcel(dt2, saveFullName2);

    Console.WriteLine("Hello, World!" + dir);
}
catch (Exception ex)
{
    Console.WriteLine("ex:" + ex.Message);
}


Console.ReadKey();