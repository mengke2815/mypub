﻿using System.Data;

namespace CommonUtils
{
    public static class ExcelHelper2
    {
        public static void ExportExcelByMemoryStream(string fileName, DataTable dt)
        {

            NPOI.SS.UserModel.IWorkbook workbook = new NPOI.XSSF.UserModel.XSSFWorkbook();

            NPOI.SS.UserModel.ISheet sheet = workbook.CreateSheet();

            NPOI.SS.UserModel.IRow headerRow = sheet.CreateRow(0);
            // handling header.
            int hi = 0;
            foreach (DataColumn column in dt.Columns)
            {
                headerRow.CreateCell(hi).SetCellValue(column.ColumnName);//If Caption not set, returns the ColumnName value     
                hi += 1;
            }
            // handling value.                   
            int rowIndex = 1;
            foreach (DataRow row in dt.Rows)
            {
                NPOI.SS.UserModel.IRow dataRow = sheet.CreateRow(rowIndex);

                hi = 0;
                foreach (DataColumn cell in dt.Columns)
                {
                    dataRow.CreateCell(hi).SetCellValue(TryStr(row[hi]));
                    hi += 1;
                }
                rowIndex++;
            }
            using (FileStream fs = new FileStream(fileName, FileMode.Create, FileAccess.Write))
            {
                workbook.Write(fs);
            }
        }

        /// <summary>
        /// 转换成string
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string TryStr(object obj)
        {
            return TryStr(obj, string.Empty);
        }

        /// <summary>
        /// 转换成string
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="defInt">默认值</param>
        /// <returns></returns>
        public static string TryStr(object obj, string defstr)
        {
            if (Object.Equals(obj, DBNull.Value) || obj == null)
                return defstr;
            else
                return obj.ToString().Trim();
        }

    }
}
