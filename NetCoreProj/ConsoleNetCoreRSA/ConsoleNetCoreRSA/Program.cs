﻿// See https://aka.ms/new-console-template for more information


using CommonUtils;
using System.Security.Cryptography;

Console.WriteLine("Hello, World!");
try
{
    // 加载PKCS8  公钥

    string privateKeyDir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "私钥对-1024");
    string privateKeyFullName = Path.Combine(privateKeyDir, "应用私钥1024-PKCS8.txt");
    string privateKeyPemPkcs8 = File.ReadAllText(privateKeyFullName);

    //加载公钥
    string pubKeyFullName = Path.Combine(privateKeyDir, "应用公钥1024.txt");
    string publicKeyPem = File.ReadAllText(pubKeyFullName);

    // 明文长度小于 私钥长度/1024-8 ，不分段。
    //string toEncryptStr = "123中华人民共和国";
    // 明文长度大于 私钥长度/1024-8 ， 分段。
    string toEncryptStr = "123中华人民共和国。123中华人民共和国。123中华人民共和国。123中华人民共和国。123中华人民共和国。123中华人民共和国。123中华人民共和国。123中华人民共和国。123中华人民共和国。123中华人民共和国。123中华人民共和国。123中华人民共和国。123中华人民共和国。";
    Console.WriteLine("明文：" + toEncryptStr);
    Console.WriteLine("明文 Length：" + toEncryptStr.Length);
    string EncryptedStr = CoreRsaEncryptUtil.RSAEncrypt(toEncryptStr, "UTF-8", publicKeyPem);
    Console.WriteLine("密文：" + EncryptedStr);
    string DecryptedStr = CoreRsaEncryptUtil.RSADecrypt(EncryptedStr, "UTF-8", privateKeyPemPkcs8, "PKCS8");
    Console.WriteLine("解密后：" + DecryptedStr);
}
catch (Exception ex)
{
    Console.WriteLine("Exception：" + ex.Message);
    if (ex.InnerException != null)
    {
        Console.WriteLine("InnerException：" + ex.InnerException.Message);
    }
}



Console.WriteLine("按任意键退出。");
Console.ReadKey();