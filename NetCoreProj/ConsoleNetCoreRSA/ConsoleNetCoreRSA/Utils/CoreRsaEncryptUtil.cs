﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace CommonUtils
{
    public static class CoreRsaEncryptUtil
    {
        #region 标准的-公钥加密-私钥解密

        /** 默认编码字符集 */
        private static string DEFAULT_CHARSET = "UTF-8";

        /// <summary>
        /// 公钥加密（超过 私钥长度 / 8 - 11，分段加密）
        /// </summary>
        /// <param name="content">明文</param>
        /// <param name="charset">编码</param>
        /// <param name="publicKeyPem">公钥</param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public static string RSAEncrypt(string content, string charset, string publicKeyPem)
        {
            try
            {
                //假设私钥长度为1024， 1024/8-11=117。
                //如果明文的长度小于117，直接全加密，然后转base64。(data.Length <= maxBlockSize)
                //如果明文长度大于117，则每117分一段加密，写入到另一个Stream中，最后转base64。while (blockSize > 0)

                RSA rsa = CoreRsaUtil.LoadPublicKey(publicKeyPem);
                 
                if (string.IsNullOrEmpty(charset))
                {
                    charset = DEFAULT_CHARSET;
                }
                byte[] data = Encoding.GetEncoding(charset).GetBytes(content);
                int maxBlockSize = rsa.KeySize / 8 - 11; //加密块最大长度限制
                if (data.Length <= maxBlockSize)
                {
                    byte[] cipherbytes = rsa.Encrypt(data, RSAEncryptionPadding.Pkcs1);
                    return Convert.ToBase64String(cipherbytes);
                }
                MemoryStream plaiStream = new MemoryStream(data);
                MemoryStream crypStream = new MemoryStream();
                Byte[] buffer = new Byte[maxBlockSize];
                int blockSize = plaiStream.Read(buffer, 0, maxBlockSize);
                while (blockSize > 0)
                {
                    Byte[] toEncrypt = new Byte[blockSize];
                    Array.Copy(buffer, 0, toEncrypt, 0, blockSize);
                    Byte[] cryptograph = rsa.Encrypt(toEncrypt, RSAEncryptionPadding.Pkcs1);
                    crypStream.Write(cryptograph, 0, cryptograph.Length);
                    blockSize = plaiStream.Read(buffer, 0, maxBlockSize);
                }

                return Convert.ToBase64String(crypStream.ToArray(), Base64FormattingOptions.None);
            }
            catch (Exception ex)
            {
                throw new Exception("EncryptContent = " + content + ",charset = " + charset, ex);
            }
        }

        /// <summary>
        /// 私钥解密（超过 私钥长度 / 8 - 11，分段加密）
        /// </summary>
        /// <param name="content">密文</param>
        /// <param name="charset">编码</param>
        /// <param name="privateKeyPem">私钥</param>
        /// <param name="keyFormat">私钥格式 PKCS1,PKCS8</param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public static string RSADecrypt(string content, string charset, string privateKeyPem, string keyFormat)
        {
            try
            {
                //假设私钥长度为1024， 1024/8 =128。
                //如果明文的长度小于 128，直接全解密。(data.Length <= maxBlockSize)
                //如果明文长度大于 128，则每 128 分一段解密，写入到另一个Stream中，最后 GetString。while (blockSize > 0)

                RSA  rsaCsp = CoreRsaUtil.LoadPrivateKey(privateKeyPem, keyFormat);

                //RSACryptoServiceProvider rsaCsp = LoadCertificateFile(privateKeyPem, signType);
                if (string.IsNullOrEmpty(charset))
                {
                    charset = DEFAULT_CHARSET;
                }
                byte[] data = Convert.FromBase64String(content);
                int maxBlockSize = rsaCsp.KeySize / 8; //解密块最大长度限制
                if (data.Length <= maxBlockSize)
                {
                    byte[] cipherbytes = rsaCsp.Decrypt(data, RSAEncryptionPadding.Pkcs1);
                    return Encoding.GetEncoding(charset).GetString(cipherbytes);
                }
                MemoryStream crypStream = new MemoryStream(data);
                MemoryStream plaiStream = new MemoryStream();
                Byte[] buffer = new Byte[maxBlockSize];
                int blockSize = crypStream.Read(buffer, 0, maxBlockSize);
                while (blockSize > 0)
                {
                    Byte[] toDecrypt = new Byte[blockSize];
                    Array.Copy(buffer, 0, toDecrypt, 0, blockSize);
                    Byte[] cryptograph = rsaCsp.Decrypt(toDecrypt, RSAEncryptionPadding.Pkcs1);
                    plaiStream.Write(cryptograph, 0, cryptograph.Length);
                    blockSize = crypStream.Read(buffer, 0, maxBlockSize);
                }

                return Encoding.GetEncoding(charset).GetString(plaiStream.ToArray());
            }
            catch (Exception ex)
            {
                throw new Exception("DecryptContent = " + content + ",charset = " + charset, ex);
            }
        }


        #endregion
    }
}
