﻿using System.Security.Cryptography;

namespace CommonUtils
{
    /// <summary>
    /// .netcore 把私钥/公钥文本转换为RSA
    /// </summary>
    public static class CoreRsaUtil
    {
        /// <summary>
        /// 转换私钥字符串为 RSA 
        /// </summary>
        /// <param name="privateKeyStr">私钥字符串</param>
        /// <param name="keyFormat">PKCS8,PKCS1</param>
        /// <param name="signType">RSA 私钥长度1024 ,RSA2 私钥长度2048</param>
        /// <returns></returns>
        public static RSA LoadPrivateKey(string privateKeyStr, string keyFormat)
        {
            privateKeyStr = privateKeyStr.Replace("-----BEGIN RSA PRIVATE KEY-----", "").Replace("-----END RSA PRIVATE KEY-----", "").Replace("\r", "").Replace("\n", "").Trim();
            privateKeyStr = privateKeyStr.Replace("-----BEGIN PRIVATE KEY-----", "").Replace("-----END PRIVATE KEY-----", "").Replace("\r", "").Replace("\n", "").Trim();

            RSA rsaPrivate = RSA.Create();
            //PKCS8,PKCS1
            if (keyFormat == "PKCS1")
            {
                rsaPrivate.ImportRSAPrivateKey(Convert.FromBase64String(privateKeyStr), out _);
            }
            else if (keyFormat == "PKCS8")
            {
                rsaPrivate.ImportPkcs8PrivateKey(Convert.FromBase64String(privateKeyStr), out _);
            }
            else
            {
                throw new Exception("私钥格式不支持");
            }
            return rsaPrivate;
        }
        /// <summary>
        /// pem 公钥文本 转  .NET RSA 
        /// </summary>
        /// <param name="publicKeyPem"></param>
        /// <returns></returns>
        public static RSA LoadPublicKey(string publicKeyPem)
        {
            publicKeyPem = publicKeyPem.Replace("-----BEGIN PUBLIC KEY-----", "").Replace("-----END PUBLIC KEY-----", "").Replace("\r", "").Replace("\n", "").Trim();

            RSA rsaPub = RSA.Create();
            try
            {
                //ASN.1-DER 编码中的 X.509 SubjectPublicKeyInfo 结构的字节。
                rsaPub.ImportSubjectPublicKeyInfo(Convert.FromBase64String(publicKeyPem), out _);
            }
            catch (Exception exPub)
            {
                //ASN.1-BER 编码中 PKCS#1 RSAPublicKey 结构的字节。
                rsaPub.ImportRSAPublicKey(Convert.FromBase64String(publicKeyPem), out _);
            }
            return rsaPub;
        }
    }
}
