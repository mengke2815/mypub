package CommonUtils;

import java.security.MessageDigest;


public class HashUtil {

    //SHA256 HASH 转16进制字符串
    public static String sha256hex(String base) throws Exception {
        StringBuffer hexString = new StringBuffer();
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        byte[] hash = digest.digest(base.getBytes("UTF-8"));
        String rst=byteToHex(hash);
        return rst;
    }

    //byte[] 转16进制字符串
    public static String byteToHex(byte[] byIn){
        StringBuffer hexString = new StringBuffer();
        for (int i = 0; i < byIn.length; i++) {
            String hex = Integer.toHexString(0xff & byIn[i]);
            if (hex.length() == 1) {
                hexString.append('0');
            }
            hexString.append(hex);
        }
        return hexString.toString();
    }

    //MD5 HASH 转16进制字符串
    public static String md5hex(String base) throws Exception {
        StringBuffer hexString = new StringBuffer();
        MessageDigest digest = MessageDigest.getInstance("MD5");
        byte[] hash = digest.digest(base.getBytes("UTF-8"));
        String rst=byteToHex(hash);
        return rst;
    }

}
