package CommonUtils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpUtil {

    public static String HttpPostJson(String reqUrl, String body, int timeOut) throws Exception {

        //timeOut 单位是秒，转 毫秒 。
        int timeOut2 = timeOut * 1000;

        String resp = null;

        URL url = new URL(reqUrl); //url地址

        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setDoInput(true);
        connection.setDoOutput(true);
        connection.setRequestMethod("POST");
        connection.setUseCaches(false);
        connection.setInstanceFollowRedirects(false);
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setConnectTimeout(timeOut2);
        connection.setReadTimeout(timeOut2);

        connection.connect();

        //OutputStream 、InputStream 和 C#.NET 意义相反。
        //请求报文,OutputStream() 代表向外输出的流。
        OutputStream os = connection.getOutputStream();
        os.write(body.getBytes("UTF-8"));

        //响应报文, InputStream() 接收到的输入流。
        BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        String lines;
        StringBuffer sbf = new StringBuffer();

        while ((lines = reader.readLine()) != null) {
            lines = new String(lines.getBytes(), "utf-8");
            sbf.append(lines);
        }

        resp = sbf.toString();

        //清理
        os.close();
        os.flush();

        reader.close();
        // 断开连接
        connection.disconnect();

        return resp;
    }
}

