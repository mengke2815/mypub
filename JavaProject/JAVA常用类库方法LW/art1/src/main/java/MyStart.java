import CommonUtils.HashUtil;
import CommonUtils.HttpUtil;
import HttpReqModel.TestJsonModel;
import com.alibaba.fastjson.JSON;

public class MyStart {

    public static void main(String[] args) {
        try {

            System.out.println("Hello Kitty !");
            String msg = "123";
            System.out.println("msg:" + msg);

            String sha256hex = HashUtil.sha256hex(msg).toUpperCase();
            System.out.println("sha256hex:" + sha256hex + " 长度：" + sha256hex.length());

            String md5hex = HashUtil.md5hex(msg).toUpperCase();
            System.out.println("md5hex:" + md5hex + " 长度：" + md5hex.length());

            TestJsonModel tjm = new TestJsonModel();
            tjm.mch_id = "1";
            tjm.out_trade_no = "10086";

            String userJson = JSON.toJSONString(tjm);
            System.out.println("userJson:" + userJson);
            String url = "http://localhost:55349/HOME/KJSON";
            System.out.println("准备发起HTTP"  );
            String rst = HttpUtil.HttpPostJson(url, userJson, 10);
            System.out.println("rst:" + rst);

            TestJsonModel siteInfoBean = JSON.parseObject(JSON.toJSONString(rst), TestJsonModel.class);
            if(siteInfoBean!=null)
            {
                System.out.println("siteInfoBean:" + siteInfoBean.mch_id);
            }
        }
        catch (java.net.ConnectException connEx){
            System.out.println("ConnectException:" + connEx.getMessage());
        }
        catch (java.net.SocketTimeoutException connEx){
            System.out.println("SocketTimeoutException:" + connEx.getMessage());
        }
        catch (Exception ex) {
            System.out.println("ex:" + ex.getMessage());
        }

    }
}
