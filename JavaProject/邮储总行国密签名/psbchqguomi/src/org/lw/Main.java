package org.lw;

public class Main {

    public static void main(String[] args) {
	// write your code here

        try {

            System.out.println("hello");

            SM2 clz = SM2.getInstance();
            byte [] sourceData = "userData".getBytes();

            /* 密管密钥测试 */
            String x = "E0E2E37F11901483CDFBC47F489D87D5D78C55DD7F919B73DEA83007748668B7";
            String y = "871A1BA9608F156E25B7D64C7821379BAC1E2C591D5A50FF311D1AAE026C1DAE";
            String d = "D25595C27BC0E4C678533F06D9D7BA66EECDBCED47268112B48E5CEA4563EC00";

            {   // 密管密钥自签自验
                byte[] sign = clz.SM2Sign(hexStringToBytes(d), sourceData);
                System.out.println("SignData：" + new String(sign));
                boolean verify = clz.SM2Verify(hexStringToBytes(x), hexStringToBytes(y), sourceData, sign);
                System.out.println("VerifyResult：" + verify);
            }

            {   // 验证密管签名数据
                //MEQCIB2kTQSQDycjwJegufB9rPnKh7uF8sLg2AFa31JKQ+FJAiAOt2ERQqRdaK9jA/cL9rIzkZcfmELAIS5PzXIeuE+LdQ==
                //MEYCIQDQkU1OuQMEXTiu/mRz7Lzq8/vdVYB+zI4ymBsl5KzW7AIhAKpn19GiOgZpLTCH4IvonROL5Yj3YtUjhIDXYzQGjRpW
                byte[] sign = "MEYCIQDV4XW/s5+JS2RBb1hV6b7yk6ye0UN9c6PgDJ8Q3qsU6wIhAIubdggjv5cem2J/E3JinmrXJ4iUrhWfRhzXpFES6B5K".getBytes();
                boolean verify = clz.SM2Verify(hexStringToBytes(x), hexStringToBytes(y), sourceData, sign);
                System.out.println("VerifyResult2：" + verify);
            }

            System.out.println("===========================================================");

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

    }

    public static byte charToByte(char c) {
        return (byte) "0123456789ABCDEF".indexOf(c);
    }

    public static byte[] hexStringToBytes(String hexString) {
        if (hexString == null || hexString.equals("")) {
            return null;
        }

        hexString = hexString.toUpperCase();
        int length = hexString.length() / 2;
        char[] hexChars = hexString.toCharArray();
        byte[] d = new byte[length];
        for (int i = 0; i < length; i++) {
            int pos = i * 2;
            d[i] = (byte) (charToByte(hexChars[pos]) << 4 | charToByte(hexChars[pos + 1]));
        }
        return d;
    }


}
