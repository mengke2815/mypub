package com.example.multidbdemo.dsconfig;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;

@Configuration
@MapperScan(basePackages = MchInfoDataSourceConfig.PACKAGE, sqlSessionFactoryRef = "mchinfoSqlSessionFactory")
public class MchInfoDataSourceConfig {
    // mapper精确到 cluster 目录，以便跟其他数据源隔离
    static final String PACKAGE = "com.example.multidbdemo.mapper.mchinfo";
    static final String MAPPER_LOCATION = "classpath:mapper/mchinfo/*.xml";

    @Bean(name = "mchinfoDataSource")
    @ConfigurationProperties(prefix = "mchinfo.datasource")
    public DataSource clusterDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "mchinfoTransactionManager")
    public DataSourceTransactionManager clusterTransactionManager() {
        return new DataSourceTransactionManager(clusterDataSource());
    }

    @Bean(name = "mchinfoSqlSessionFactory")
    public SqlSessionFactory clusterSqlSessionFactory(@Qualifier("mchinfoDataSource") DataSource clusterDataSource)
            throws Exception {
        final SqlSessionFactoryBean sessionFactory = new SqlSessionFactoryBean();
        sessionFactory.setDataSource(clusterDataSource);
        sessionFactory.setMapperLocations(new PathMatchingResourcePatternResolver()
                .getResources(MchInfoDataSourceConfig.MAPPER_LOCATION));
        return sessionFactory.getObject();

    }

}
