package com.example.multidbdemo.controller;

import com.example.multidbdemo.mapper.master.UserDao;
import com.example.multidbdemo.mapper.mchinfo.MchInfoUserDao;
import com.example.multidbdemo.mapper.second.SchoolDao;
import com.example.multidbdemo.model.master.UserVo;
import com.example.multidbdemo.model.mchinfo.MchInfoUserVo;
import com.example.multidbdemo.model.second.SchoolVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;

@RestController
public class HomeController {

    @Autowired
    private UserDao userDao;

    @Autowired
    private SchoolDao schoolDao;

    @Autowired
    private MchInfoUserDao mchInfoUserDao;

    @RequestMapping(value = "/findData")
    public HashMap<String, Object> findMultipleData() {

        HashMap<String, Object> hashMap = new HashMap<>();

        UserVo userVo= userDao.findById(1L);
        if(userVo!=null)
        {
            hashMap.put("userm 1", userVo);
        }
        SchoolVo schoolVo= schoolDao.findById(1L);
        if(schoolVo!=null)
        {
            hashMap.put("SchoolVo 1", schoolVo);
        }

        List<MchInfoUserVo> mchInfoUserVo = mchInfoUserDao.getAllUsers();

        if (!mchInfoUserVo .isEmpty() && mchInfoUserVo.size()>0) {
            System.out.println(mchInfoUserVo.get(0).getUserName());
            hashMap.put("mchInfoUserVo 1", mchInfoUserVo.get(0));
        }

        hashMap.put("end", "1");

        return hashMap;
    }

}
