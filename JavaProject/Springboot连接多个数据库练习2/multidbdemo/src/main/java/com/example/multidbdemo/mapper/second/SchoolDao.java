package com.example.multidbdemo.mapper.second;

import com.example.multidbdemo.model.second.SchoolVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface SchoolDao {
    SchoolVo findById(@Param(value = "id") Long id);
}
