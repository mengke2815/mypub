package com.example.multidbdemo.mapper.master;

import com.example.multidbdemo.model.master.UserVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserDao {
    UserVo findById(@Param(value = "id") Long id);
}
