package com.example.multidbdemo.mapper.mchinfo;

import com.example.multidbdemo.model.mchinfo.MchInfoUserVo;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MchInfoUserDao {
    List<MchInfoUserVo> getAllUsers();
    int addUser( MchInfoUserVo user );
    int deleteUser( MchInfoUserVo user );
}
