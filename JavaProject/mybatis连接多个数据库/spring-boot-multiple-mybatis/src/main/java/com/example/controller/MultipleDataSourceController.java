package com.example.controller;

import com.example.dao.master.UserDao;
import com.example.dao.mchinfo.MchInfoUserDao;
import com.example.dao.second.SchoolDao;
import com.example.domain.master.UserVo;
import com.example.domain.mchinfo.MchInfoUserVo;
import com.example.domain.second.SchoolVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;

@RestController
public class MultipleDataSourceController {

    @Autowired
    private UserDao userDao;

    @Autowired
    private SchoolDao schoolDao;

    @Autowired
    private MchInfoUserDao mchInfoUserDao;

    @RequestMapping(value = "/findData")
    public HashMap<String, Object> findMultipleData() {

        HashMap<String, Object> hashMap = new HashMap<>();

        List<MchInfoUserVo> mchInfoUserVo = mchInfoUserDao.getAllUsers();

        if (!mchInfoUserVo .isEmpty() && mchInfoUserVo.size()>0) {
            System.out.println(mchInfoUserVo.get(0).getUserName());
            hashMap.put("mchInfoUserVo 1", mchInfoUserVo.get(0));
        }

        hashMap.put("end", "1");

        return hashMap;
    }



}
