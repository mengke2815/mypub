package com.example.dao.mchinfo;

import com.example.domain.mchinfo.MchInfoUserVo;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MchInfoUserDao {
    List<MchInfoUserVo> getAllUsers();
    int addUser( MchInfoUserVo user );
    int deleteUser( MchInfoUserVo user );
}
