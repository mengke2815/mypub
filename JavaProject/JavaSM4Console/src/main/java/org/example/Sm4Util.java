package org.example;

import cn.hutool.crypto.symmetric.SymmetricCrypto;

import javax.crypto.spec.IvParameterSpec;

public class Sm4Util {

    //加密为16进制，也可以加密成base64/字节数组
    public static String encryptSm4ECB(String key,String plaintext) {
        String _Sm4EcbNoPaddingAlg ="SM4/ECB/PKCS5Padding";
        SymmetricCrypto sm4 = new SymmetricCrypto(_Sm4EcbNoPaddingAlg, key.getBytes());
        return sm4.encryptHex(plaintext);
    }

    //解密
    public static String decryptSm4ECB(String key,String ciphertext) {
        String _Sm4EcbNoPaddingAlg ="SM4/ECB/PKCS5Padding";
        SymmetricCrypto sm4 = new SymmetricCrypto(_Sm4EcbNoPaddingAlg, key.getBytes());
        return sm4.decryptStr(ciphertext);
    }

    public static String encryptSm4CBC(String key,String iv,String plaintext) {

        String _Sm4EcbNoPaddingAlg ="SM4/CBC/PKCS5Padding";
        SymmetricCrypto sm4 = new SymmetricCrypto(_Sm4EcbNoPaddingAlg, key.getBytes());
        sm4.setIv(iv.getBytes());
        return sm4.encryptHex(plaintext);
    }

    //解密
    public static String decryptSm4CBC(String key,String iv,String ciphertext) {
        String _Sm4EcbNoPaddingAlg ="SM4/CBC/PKCS5Padding";
        SymmetricCrypto sm4 = new SymmetricCrypto(_Sm4EcbNoPaddingAlg, key.getBytes());
        sm4.setIv(iv.getBytes());
        return sm4.decryptStr(ciphertext);
    }

}
