package org.example;

import cn.hutool.crypto.SmUtil;
import cn.hutool.crypto.asymmetric.KeyType;
import cn.hutool.crypto.asymmetric.SM2;
import org.bouncycastle.crypto.engines.SM2Engine;
import org.bouncycastle.util.encoders.Hex;
import java.util.Base64;

public class App {
    public static void main(String[] args) {
        System.out.println("Hello World!");
        try {
           // testSM2Sign();
//            testSM2Enc();
//            testSM3();
//            testSM4();
            testSM4CBC();
        } catch (Exception ex) {
            String msg = ex.getMessage();
            System.out.println("解密后：" + msg);
        }
    }


    static void testSM2Sign() {

        String content = "1234泰酷拉JJ";
        System.out.println("待处理字符串：" + content);
        String userId = "1234567812345678";
        byte[] byUserId = userId.getBytes();

        //私钥
        String privateKeyHex = "FAB8BBE670FAE338C9E9382B9FB6485225C11A3ECB84C938F10F20A93B6215F0";
        //公钥X
        String x = "9EF573019D9A03B16B0BE44FC8A5B4E8E098F56034C97B312282DD0B4810AFC3";
        //公钥Y
        String y = "CC759673ED0FC9B9DC7E6FA38F0E2B121E02654BF37EA6B63FAF2A0D6013EADF";
        //构造函数里，私钥或公钥，其中一方可为空 null
        SM2 sm2 = new SM2(privateKeyHex, x, y);
        byte[] byRst = sm2.sign(content.getBytes(), byUserId);
        String sm2Sign = Base64.getEncoder().encodeToString(byRst);
        System.out.println("sm2 BASE64 签名:" + sm2Sign);

        byte[] bySign = Base64.getDecoder().decode(sm2Sign);
        boolean verifySign = sm2.verify(content.getBytes(), bySign, byUserId);

        System.out.println("SM2 验签：" + verifySign);
    }

    static void testSM2Enc() {
        //sm2 加密模式:C1C3C2
        //私钥
        String privateKeyHex = "FAB8BBE670FAE338C9E9382B9FB6485225C11A3ECB84C938F10F20A93B6215F0";
        //完整公钥“9EF573019D9A03B16B0BE44FC8A5B4E8E098F56034C97B312282DD0B4810AFC3CC759673ED0FC9B9DC7E6FA38F0E2B121E02654BF37EA6B63FAF2A0D6013EADF”
        //公钥X
        String x = "9EF573019D9A03B16B0BE44FC8A5B4E8E098F56034C97B312282DD0B4810AFC3";
        //公钥Y
        String y = "CC759673ED0FC9B9DC7E6FA38F0E2B121E02654BF37EA6B63FAF2A0D6013EADF";

        // 数据和ID此处使用16进制表示
        String content = "1234泰酷拉JJ";
        System.out.println("待处理字符串：" + content);

        SM2 sm2 = new SM2(privateKeyHex, x, y);
        //默认是:C1C3C2
        sm2.setMode(SM2Engine.Mode.C1C3C2);
        //公钥加密
        byte[] byRst = sm2.encrypt(content.getBytes(), KeyType.PublicKey);

        String strRst = Hex.toHexString(byRst);
        System.out.println("加密后：" + strRst);

        byte[] byToDecrypt = Hex.decode(strRst);
        byte[] byDecrypted = sm2.decrypt(byToDecrypt, KeyType.PrivateKey);
        String strDecrypted = new String(byDecrypted);

        System.out.println("解密后：" + strDecrypted);
    }

    static void testSM3() {
        String content = "1234泰酷拉";
        System.out.println("待处理字符串：" + content);

        String digestHex = SmUtil.sm3(content);
        System.out.println("SM3 HASH后：" + digestHex);
    }

    static void testSM4() {

        String key = "9814548961710661";
        String content = "1234泰酷拉";
        System.out.println("待加密字符串：" + content);
        String plain = Sm4Util.encryptSm4ECB(key, content);
        System.out.println("加密后：" + plain);
        String cipher = Sm4Util.decryptSm4ECB(key, plain);
        System.out.println("解密后：" + cipher);
    }

    static void testSM4CBC() {
// I
        String key = "9814548961710661";
        String civ = "1234567890123456"; // SM4 IV 长度16
        String content = "1234泰酷拉from.java.";
        System.out.println("待加密字符串：" + content);
        String plain = Sm4Util.encryptSm4CBC(key,civ, content);
        System.out.println("加密后：" + plain);
        String cipher = Sm4Util.decryptSm4CBC(key,civ, plain);
        System.out.println("解密后：" + cipher);
    }
}
