package com.example.demosm4;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Demosm4Application {

	public static void main(String[] args) {
		SpringApplication.run(Demosm4Application.class, args);
	}

}
