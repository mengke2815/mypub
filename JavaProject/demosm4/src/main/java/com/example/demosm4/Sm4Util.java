package com.example.demosm4;

import cn.hutool.crypto.symmetric.SymmetricCrypto;

public  class Sm4Util {


    static String _Sm4EcbNoPaddingAlg ="SM4/ECB/PKCS5Padding";

    //加密为16进制，也可以加密成base64/字节数组
    public static String encryptSm4(String key,String plaintext) {
        SymmetricCrypto sm4 = new SymmetricCrypto(_Sm4EcbNoPaddingAlg, key.getBytes());
        return sm4.encryptHex(plaintext);
    }

    //解密
    public static String decryptSm4(String key,String ciphertext) {
        SymmetricCrypto sm4 = new SymmetricCrypto(_Sm4EcbNoPaddingAlg, key.getBytes());
        return sm4.decryptStr(ciphertext);
    }


}
