package com.example.demosm4;

import org.springframework.boot.system.ApplicationHome;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;

@RestController
public class HomeController {

    @RequestMapping("/test")
    String index() {

        String msg = "";

        try {

            String key = "9814548961710661";
            String content = "1234";
            String plain = Sm4Util.encryptSm4(key,content);
            System.out.println(plain + "\n" + plain);
            String cipher = Sm4Util.decryptSm4(key,plain);
            System.out.println(cipher + "\n" + cipher);


        } catch (Exception ex) {
            msg = ex.getMessage();
        }
        return "Hello Spring Boot  " + msg;
    }

    public String getJarRoot() {
        ApplicationHome home = new ApplicationHome(getClass());
        File jarFile = home.getSource();
        return jarFile.getParentFile().getPath();
    }

}
