package org.lw;

public class Main {

    public static void main(String[] args) {

        String privateKeyPKCS8 = "MIIEwAIBADANBgkqhkiG9w0BAQEFAASCBKowggSmAgEAAoIBAQDyNsCen1C5SKAy2qg7CUJRtrqgcP3f4eTv0oHcV7LIj3vj5oy+0SVk4USsKOSW+Ddg+Bn12bmBwxCA1aMcoSBzeLQFQT4OLA87I1+EP+b2Lrqf4xfU/77e+bY0MU6yatyb9rf99P5mDIn88JL89Z1rJYQrZnwJP1f8EAL0JcGY5bXO9+aYtDBP3qL7dDmuk+Wfc/57q233Lzr3zBCQ5EH3XuTgEr6b3IVduf4f/mrHWDvEldNLlj+xTjKnSZgR16yNYevaRLVsOzlIN+0ggUqeU+ZQDf9XnJEkd9yBbLfWnjOgGa4VZ5Y7nurEnBaOD3IG/vGfSZUEKtdw3uxbD+PZAgMBAAECggEBALdZrxHkM60uRuZ4EuUtqyBEHJ4bKnLxguXwChGL6XBc/UGVYnGHzLDCvcM86V1G5FTpOm2atQx/Zty/28tuRSxj8JIRwzHjNFxl+IYaAXHWCbvComXAevI7QSvdL19r+Teu2bTKYlFJqKLqUbpfCxzyt0xLNhWh9659SF8cvaJIuAblrKziXAyFYPDcLdmDPJI2rlsKKPiO5yXxOaj70pmULFBi4sKemabu23HtJkg48SdCcE4eZQhfIvloSDv4HYNT3l6sLezrHCemBIeTRk5V3wB96A/rjR1PgUIXttl6rUDp4nfMopTZpXnD7zPXhw5is7d/aiHc+E6CQvCqvQECgYEA+yADM23b53LNJs4shZHqC+Dc35j5DJjNswP0XA2Aq0SypY0fkUjI4qjiS2y4ktNwiFc9ljFOVyfQIczKcNXuhFswvbannBqqg7VYOCD4175URirfgtM4xU4TQiHm+5jyTz3GQiF7PdN2ISKGTiLs/fXrw3utaSgc7dukCYs6ZxECgYEA9up0gDZQ3/YhIjuEQBHiHhRQ9X3PriD8J4DJqTYSDG1OBZFPfO3YocJkCMTq9c2Jl2qPbLVP1BveAjIR6aExwQuolwisoM+QGurIcZY2m5dklYitRVg6+9AitCU/cqZK9U0vAPuDb3x1ZR/0+rYhQGcY+DFchtapTqzHSfXggEkCgYEA0PyXLVmjxD2Z1U2HZ7FC4ZfEuKAJwx33MZ984I6sIdwOABAt0S6NX3PEv5g/EpG7+PsBWdi2pXmQkFBpuPWQhb2OFpPHcPYQKYPlYvCtpn3SjIJpd+poOGr9Q/AK1h82qBN0xtwuQAmXKYQd2TDfoYnjJs/qRLUJPjmnjfm8JMECgYEAttoXnl8a81AdZ3F11dCoiCf5cGNUKhqJQWPRc3r0ULmdfugGWnj05Y3EcO4LJi6pBzXFsvZugKCGf0+/DinuY4yTtA2bcZdkm1plSCC6ney2czp9Po5BV/vhx1CSNQBLIG+hMHQR+LzNXy8UR5oa88ulpR9A6yYKyZWQHAh20ekCgYEAvgXQSX0GiXTMTTWUWtcrvHkATb3KKENcH8jmwa6sw/jU4FP2IxUmRwxRr/1mQ1RB/5RSYH/wbIfvoytBEVj40DMJzn0Aq/0+EwU3DP8ej6vkIK/U1/uxz/bVr7R7+dI2uw0THqXLo4hHTFElBZTREbnuWcqDPL+6ZU7FvpHP4iM=";
        String preStr = "Hello中国";

        String publicKeyStr="MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA8jbAnp9QuUigMtqoOwlCUba6oHD93+Hk79KB3FeyyI974+aMvtElZOFErCjklvg3YPgZ9dm5gcMQgNWjHKEgc3i0BUE+DiwPOyNfhD/m9i66n+MX1P++3vm2NDFOsmrcm/a3/fT+ZgyJ/PCS/PWdayWEK2Z8CT9X/BAC9CXBmOW1zvfmmLQwT96i+3Q5rpPln3P+e6tt9y8698wQkORB917k4BK+m9yFXbn+H/5qx1g7xJXTS5Y/sU4yp0mYEdesjWHr2kS1bDs5SDftIIFKnlPmUA3/V5yRJHfcgWy31p4zoBmuFWeWO57qxJwWjg9yBv7xn0mVBCrXcN7sWw/j2QIDAQAB";

        try {
            String sign = RsaUtil.getSign(preStr, privateKeyPKCS8);
            System.out.println("签名为：" + sign);

            boolean bRst=RsaUtil.verifySign(preStr, publicKeyStr,sign);
            System.out.println("验证签名结果：" + bRst);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

    }
}
