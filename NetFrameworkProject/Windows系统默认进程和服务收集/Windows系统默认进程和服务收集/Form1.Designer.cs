﻿namespace Windows系统默认进程和服务收集
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.btnColProcess = new System.Windows.Forms.Button();
            this.btnColService = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnColProcess
            // 
            this.btnColProcess.Location = new System.Drawing.Point(195, 226);
            this.btnColProcess.Name = "btnColProcess";
            this.btnColProcess.Size = new System.Drawing.Size(75, 23);
            this.btnColProcess.TabIndex = 0;
            this.btnColProcess.Text = "收集进程";
            this.btnColProcess.UseVisualStyleBackColor = true;
            this.btnColProcess.Click += new System.EventHandler(this.btnColProcess_Click);
            // 
            // btnColService
            // 
            this.btnColService.Location = new System.Drawing.Point(195, 308);
            this.btnColService.Name = "btnColService";
            this.btnColService.Size = new System.Drawing.Size(75, 23);
            this.btnColService.TabIndex = 1;
            this.btnColService.Text = "收集服务";
            this.btnColService.UseVisualStyleBackColor = true;
            this.btnColService.Click += new System.EventHandler(this.btnColService_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(811, 554);
            this.Controls.Add(this.btnColService);
            this.Controls.Add(this.btnColProcess);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnColProcess;
        private System.Windows.Forms.Button btnColService;
    }
}

