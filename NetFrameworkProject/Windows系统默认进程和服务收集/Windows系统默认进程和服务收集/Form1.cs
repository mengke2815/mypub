﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Windows系统默认进程和服务收集
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btnColProcess_Click(object sender, EventArgs e)
        {
            try
            {
                string OSVersion = Environment.OSVersion.ToString();
                var curPro = Process.GetProcesses();
                var v2 = curPro
                    .OrderBy(x => x.ProcessName)
                    .GroupBy(x => new { x.ProcessName })
                    .Select(x => x.First())
                    .ToList();

                StringBuilder sc = new StringBuilder();
                foreach (Process item in v2)
                {
                    sc.AppendLine(item.ProcessName);
                }
                string fullName = Path.Combine(Application.StartupPath, OSVersion + "默认进程.txt");
                string contents = sc.ToString();
                File.WriteAllText(fullName, contents);

                MessageBox.Show("ok");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnColService_Click(object sender, EventArgs e)
        {

        }
    }
}
