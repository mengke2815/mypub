﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PLUTUSPAY.Models
{

    public class RefundRsp
    {
        public string tran_state_msg { get; set; }
        public string order_status { get; set; }
        public object mer_memo { get; set; }
        public string done_refund_amount { get; set; }
        public string total_amount { get; set; }
        public string refund_amount { get; set; }
        public string tran_state { get; set; }
        public string currency { get; set; }
        public string tran_content { get; set; }
        public string tran_state_code { get; set; }
        public object pay_dsct_amount { get; set; }
    }

}
