﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PLUTUSPAY.Models
{
    public  class RefundQueryReq
    {
        /// <summary>
        /// 交易场景，支付交易上送的交易场景，如B2C-API-DISPLAYCODE等
        /// </summary>
        public string tran_scene { get; set; }

        /// <summary>
        /// 商户编号
        /// </summary>
        public string mer_ptc_id { get; set; }
        
         

        /// <summary>
        /// 商户退款的交易编号
        /// </summary>
        public string refund_mer_tran_no { get; set; }

         
        /// <summary>
        /// 商户侧退款日期 格式：yyyyMMdd
        /// </summary>
        public string mer_refund_date { get; set; }

         

        
    }
}
