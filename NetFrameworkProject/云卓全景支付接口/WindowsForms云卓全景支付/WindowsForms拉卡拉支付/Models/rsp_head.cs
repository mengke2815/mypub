﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PLUTUSPAY.Models
{

    public class rsp_head
    {
        public string trans_code { get; set; }
        public string response_code { get; set; }
        public string response_status { get; set; }
        public string response_time { get; set; }
        public string response_msg { get; set; }
    }

}
