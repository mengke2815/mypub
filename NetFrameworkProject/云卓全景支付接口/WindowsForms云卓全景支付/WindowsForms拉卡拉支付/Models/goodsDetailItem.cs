﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PLUTUSPAY.Models
{
    
    public class goodsDetailItem
    {

        public string goodsId { get; set; }
        public string goodsName { get; set; }
        public int quantity { get; set; }
        public int price { get; set; }
        public string body { get; set; }
        public string showUrl { get; set; }

    }
}
