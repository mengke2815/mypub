﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PLUTUSPAY.Models
{
    public class rsp_biz_content<T>
    {
        public string biz_state { get; set; }
        public string rsp_code { get; set; }
        public string rsp_msg { get; set; }


        public rsp_head rsp_head { get; set; }

        public T rsp_body { get; set; }

    }
}
