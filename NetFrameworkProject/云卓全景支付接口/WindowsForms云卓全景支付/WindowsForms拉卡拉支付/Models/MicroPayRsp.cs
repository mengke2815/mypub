﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PLUTUSPAY.Models
{
    public class MicroPayRsp
    {

        public string pay_mer_tran_no { get; set; }
        public string total_amount { get; set; }
        public string trd_dsct_amount { get; set; }
        public require_values require_values { get; set; }
        public object currency { get; set; }
        public object detail { get; set; }
        public string buyer_pay_amount { get; set; }
        public string sys_order_no { get; set; }
        public object pay_dsct_amount { get; set; }



    }
}
