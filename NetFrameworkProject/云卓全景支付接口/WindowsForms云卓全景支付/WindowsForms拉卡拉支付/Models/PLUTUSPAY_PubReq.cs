﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PLUTUSPAY.Models
{
    /// <summary>
    /// 报文主体
    /// </summary>
    public class PLUTUSPAY_PubReq
    {
         

        /// <summary>
        /// 开发者账号
        /// </summary>
        public string devId { get; set; }

        
        /// <summary>
        /// 经过加密的业务数据（base64）
        /// </summary>
        public string content { get; set; }
        /// <summary>
        /// 报文签名（base64）
        /// </summary>
        public string signature { get; set; }



    }
}
