﻿ 
namespace Common.Models2
{
    public enum SissPayReturnCode
    {
        SUCCESS,
        FAIL,
        WAIT_BUYER_PAY,
    }

    public enum SissPayTradeStatus
    {
        TRADE_SUCCESS,
        TRADE_FAIL,
        WAIT_BUYER_PAY,
        /// <summary>
        /// 对应IPS的状态P，一般申请退款后是这个状态。
        /// </summary>
        PROCESSING,
        UNKNOWN,
        /// <summary>
        /// 未付款交易超时关闭，或支付完成后全额退款
        /// </summary>
        TRADE_CLOSED,
        /// <summary>
        /// 未支付。微信：客户在手机上关闭了付款界面(密码输入界面点击了关闭)。
        /// </summary>
        NOTPAY,
        /// <summary>
        /// 已撤销
        /// </summary>
        REVOKED,
        /// <summary>
        /// 转入退款
        /// </summary>
        REFUND,
        /// <summary>
        /// 微信：用户输错密码。
        /// </summary>
        PAYERROR,
        /// <summary>
        /// 初始化
        /// </summary>
        INIT,
    }


}
