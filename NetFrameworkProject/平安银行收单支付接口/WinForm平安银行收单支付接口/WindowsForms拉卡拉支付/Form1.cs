﻿using CommonUtils;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LAKALA.Models;
using Org.BouncyCastle.Utilities.Encoders;
using Org.BouncyCastle.Math;
using Org.BouncyCastle.Crypto.Parameters;
using System.Net;
using Org.BouncyCastle.Crypto;

namespace WindowsForms拉卡拉支付
{
    public partial class Form1 : Form
    {
        System.Timers.Timer _tmrRemeber;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
            System.Net.ServicePointManager.ServerCertificateValidationCallback += (s, cert, chain, sslPolicyErrors) => true;

            RemForm.ReadRememberForm(this, null);

            _tmrRemeber = new System.Timers.Timer();
            _tmrRemeber.Elapsed += new System.Timers.ElapsedEventHandler(_tmrRemeber_Elapsed);
            _tmrRemeber.Interval = 10 * 1000;
            _tmrRemeber.Start();

        }

        void _tmrRemeber_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                RemForm.RememberForm(this, null);
            }
            catch (Exception ex)
            {
                //MessageBox.Show("_tmrRemeber_Elapsed" + ex.Message);
            }
        }

        private void btnReadCrt_Click(object sender, EventArgs e)
        {
            try
            {
                if (ofdCrt.ShowDialog() == DialogResult.OK)
                {
                    X509Certificate2 cert = new X509Certificate2(ofdCrt.FileName);
                    txtCertSN.Text = cert.SerialNumber;
                    //txtCertSN10.Text = cert.SerialNumber.ToLower();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        private void btnMicroPay_Click(object sender, EventArgs e)
        {
            try
            {
                JsonSerializerSettings jsetting = new JsonSerializerSettings();
                jsetting.DefaultValueHandling = DefaultValueHandling.Ignore;

                if (decimal.Parse(txtAmt.Text) >= 1M)
                {
                    DialogResult dr = MessageBox.Show("金额过大，是否继续？", "OO", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dr == DialogResult.No)
                    {
                        return;
                    }
                }
                if (string.IsNullOrWhiteSpace(txtAuthCode.Text))
                {
                    MessageBox.Show("付款码不能为空！");
                    return;
                }

                int total_fee = SIString.TryInt((SIString.TryDec(txtAmt.Text.Trim()) * 100));
                txtOutTradeNo.Text = "S000000000000" + DateTime.Now.ToString("yyyyMMddHHmmssfff");
                string sTermSSN = Guid.NewGuid().ToString("N").Substring(0, 6);
                string sTransmsnDateTime = DateTime.Now.ToString("yyyyMMddHHmmss");
                string sCardAccptrId = txtMchNo.Text.Trim();
                string sCardAccptrTermnlId = txtTermNo.Text.Trim();
                string sFwdInstIdCode = "63070000";
                //发送机构+商户号+终端号+商户流水号+交易时间(不补零或空格)
                string sTranLogTraceNo = sFwdInstIdCode + sCardAccptrId + sCardAccptrTermnlId + sTermSSN + sTransmsnDateTime;
                MicroPayReq req = new MicroPayReq();
                req.sTransClass = "9901";
                req.sServiceDistinctCode = "01";
                req.sTransmsnDateTime = sTransmsnDateTime;
                req.sTermSSN = sTermSSN;
                req.sFwdInstIdCode = sFwdInstIdCode;
                req.sCurrcyCodeTrans = "156";
                req.sMchtIp = "127.0.0.1";
                req.sTranLogTraceNo = sTranLogTraceNo;

                req.sCardAccptrId = sCardAccptrId;
                req.sCardAccptrTermnlId = sCardAccptrTermnlId;
                req.sAuthCode = txtAuthCode.Text.Trim();
                req.sAmtTrans = total_fee.ToString().PadLeft(12, '0');
                req.sOutOrderNo = txtOutTradeNo.Text;

                req.sEncryptFlag = "1";
                req.sSigncertID = txtCertSN.Text;

                string bodyJson = JsonConvert.SerializeObject(req, jsetting);

                #region 生成签名                
                string sign = GetSign(txtPrivateKey.Text, txtCertSN.Text, bodyJson);
                req.sSignature = sign;
                #endregion

                bodyJson = JsonConvert.SerializeObject(req, jsetting);

                string mainUrl = txtMainUrl.Text;
                string url = mainUrl;
                GLog.WLog("url:" + url);

                GLog.WLog("请求报文:" + bodyJson);

                var rstStr = HttpUtil.HttpPostJson(url, bodyJson, 30);
                GLog.WLog("响应报文:" + rstStr);
                txtRst.Text = rstStr;
                if (rstStr.Contains("sSignature"))
                {
                    if (!ValidSign(rstStr, txtPubCert.Text, txtCertSN.Text))
                    {
                        MessageBox.Show("签名验证失败");
                    }
                }
                txtAuthCode.Clear();

                var rspM = JsonConvert.DeserializeObject<MicroPayRsp>(rstStr);

                if (rspM.sRespCode == "00000000")
                {
                    lblPayStatus.Text = "支付成功";
                }
                else if ((!string.IsNullOrWhiteSpace(rspM.sRespCode) && (rspM.sRespCode.EndsWith("000025") || rspM.sRespCode.EndsWith("000096") || rspM.sRespCode.EndsWith("000098")))
                    || rspM.sRespCode == "AL000004" || rspM.sRespCode == "TX000004" || rspM.sRespCode == "YE000004"
                    )
                {
                    lblPayStatus.Text = "支付中";
                }
                else
                {
                    lblPayStatus.Text = "支付失败";
                }

                if (rspM != null)
                {
                    if (!string.IsNullOrWhiteSpace(rspM.sVoucherNum))
                        txtTradeNo.Text = rspM.sVoucherNum;
                    //if (!string.IsNullOrWhiteSpace(rspM.weOrderNo))
                    //    txtThirdTradeNo.Text = rspM.weOrderNo;

                    DateTime dtTimeEnd = DateTime.Now;
                    if (!string.IsNullOrWhiteSpace(rspM.sTransmsnDateTime))
                    {
                        try
                        {
                            dtTimeEnd = DateTime.ParseExact(rspM.sTransmsnDateTime, "yyyyMMddHHmmss", CultureInfo.InvariantCulture);
                        }
                        catch { }
                    }
                    txtPayTime.Text = dtTimeEnd.ToString("yyyy-MM-dd HH:mm:ss");
                    // 分转元：
                    if (!string.IsNullOrEmpty(rspM.sAmtTrans))
                    {
                        string tmpAmt = rspM.sAmtTrans.TrimStart('0');
                        string total_amount = (SIString.TryInt(tmpAmt) / 100M).ToString("F2");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public static string GetSign(string privateKey, string userId, string bodyJson)
        {
            //将签名串strSign用SM3摘要处理成对应16进制字符串
            //使用国密SM2签名Base64签名函数及商户国密私钥对摘要处理后的16进制待签名字符串进行签
            byte[] msg = System.Text.Encoding.UTF8.GetBytes(bodyJson);
            byte[] bytRst = GmUtil.Sm3(msg);
            string strRst = Hex.ToHexString(bytRst).ToUpper();
            // 由d生成私钥 ---------------------
            BigInteger d = new BigInteger(privateKey, 16);
            ECPrivateKeyParameters bcecPrivateKey = CommonUtils.GmUtil.GetPrivatekeyFromD(d);
            byte[] byteUserId = Encoding.UTF8.GetBytes(userId);//userid使用证书序列号
            msg = System.Text.Encoding.UTF8.GetBytes(strRst);
            byte[] sig = GmUtil.SignSm3WithSm2(msg, byteUserId, bcecPrivateKey);
            string sign = Convert.ToBase64String(sig);

            return sign;
        }

        /// <summary>
        /// 验证签名
        /// </summary>
        /// <param name="rspHeader">HTTP响应头</param>
        /// <param name="rspBody">响应报文</param>
        /// <param name="pubCert">公钥证书</param>
        /// <returns></returns>
        static bool ValidSign(string rspBody, string pubKey, string userId)
        {

            string rspSign = "";
            //如果用JsonConvert方式重新组装JSON，字段顺序会变，就必须用Substring和IndexOf来解析出待验证签名内容了。
            //还好JsonConvert与Dictionary 顺序不会变
            //取出 sign，
            Dictionary<string, string> dic = JsonConvert.DeserializeObject<Dictionary<string, string>>(rspBody);
            //sSignature
            rspSign = dic["sSignature"];
            //移除sign
            dic.Remove("sSignature");
            //重新制成JSON 
            string fnstr = JsonConvert.SerializeObject(dic);

            byte[] bytRst = GmUtil.Sm3(Encoding.UTF8.GetBytes(fnstr));
            string strRst = Hex.ToHexString(bytRst).ToUpper();

            //公钥128位，拆成2个64位。
            string pa = pubKey.Substring(0, 64);
            string pb = pubKey.Substring(64, 64);

            AsymmetricKeyParameter publicKey1 = GmUtil.GetPublickeyFromXY(new BigInteger(pa, 16), new BigInteger(pb, 16));

            byte[] byteuserId = Encoding.UTF8.GetBytes(userId);
            byte[] msg = System.Text.Encoding.UTF8.GetBytes(strRst);

            byte[] bytSig = Convert.FromBase64String(rspSign);
            bool bRst = GmUtil.VerifySm3WithSm2(msg, byteuserId, bytSig, publicKey1);

            return bRst;

        }

        private void txtAuthCode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnMicroPay_Click(sender, e);
            }
        }

        /// <summary>
        /// 核心查询方法
        /// </summary>
        /// <param name="mchNo">商户号</param>
        /// <param name="termId">终端号</param>
        /// <param name="outTradeNo">原商户单号</param>
        /// <param name="tradeTime">原支付发起时间：yyyy-MM-dd HH:mm:ss</param>
        /// <param name="privateKey">私钥</param>
        /// <param name="certSn">证书SN</param>
        /// <param name="pubKey">公钥</param>
        /// <returns></returns>
        OrderQueryRsp OrderQueryCore(string mchNo, string termId, string outTradeNo, string tradeTime, string privateKey, string certSn, string pubKey)
        {
            JsonSerializerSettings jsetting = new JsonSerializerSettings();
            jsetting.DefaultValueHandling = DefaultValueHandling.Ignore;

            string sTermSSN = Guid.NewGuid().ToString("N").Substring(0, 6);
            string sTransmsnDateTime = DateTime.Now.ToString("yyyyMMddHHmmss");
            string sCardAccptrId = mchNo;
            string sCardAccptrTermnlId = termId;
            string sFwdInstIdCode = "63070000";
            //发送机构+商户号+终端号+商户流水号+交易时间(不补零或空格)
            string sTranLogTraceNo = sFwdInstIdCode + sCardAccptrId + sCardAccptrTermnlId + sTermSSN + sTransmsnDateTime;
            var req = new OrderQueryReq();
            req.sTransClass = "9912";
            req.sServiceDistinctCode = "01";
            req.sTransmsnDateTime = sTransmsnDateTime;
            req.sTermSSN = sTermSSN;
            req.sFwdInstIdCode = sFwdInstIdCode;
            req.sCurrcyCodeTrans = "156";
            req.sMchtIp = "127.0.0.1";
            req.sTranLogTraceNo = sTranLogTraceNo;

            req.sCardAccptrId = sCardAccptrId;
            req.sCardAccptrTermnlId = sCardAccptrTermnlId;

            req.sOrgOutOrderNo = outTradeNo;//原商户单号
            req.sInquiryMod = "1";
            req.sOrigDataElemts = DateTime.Parse(tradeTime).ToString("yyyyMMdd");//原日期

            req.sEncryptFlag = "1";
            req.sSigncertID = certSn;

            string bodyJson = JsonConvert.SerializeObject(req, jsetting);

            #region 生成签名                
            string sign = GetSign(privateKey, certSn, bodyJson);
            req.sSignature = sign;
            #endregion

            bodyJson = JsonConvert.SerializeObject(req, jsetting);

            string mainUrl = txtMainUrl.Text;
            string url = mainUrl;
            GLog.WLog("url:" + url);

            GLog.WLog("请求报文:" + bodyJson);

            var rstStr = HttpUtil.HttpPostJson(url, bodyJson, 30);
            GLog.WLog("响应报文:" + rstStr);
            //txtRst.Text = rstStr;
            if (rstStr.Contains("sSignature"))
            {
                if (!ValidSign(rstStr, pubKey, certSn))
                {
                    MessageBox.Show("签名验证失败");
                }
            }

            var rspM = JsonConvert.DeserializeObject<OrderQueryRsp>(rstStr);

            return rspM;

        }

        private void btnOrderQuery_Click(object sender, EventArgs e)
        {
            try
            {
                JsonSerializerSettings jsetting = new JsonSerializerSettings();
                jsetting.DefaultValueHandling = DefaultValueHandling.Ignore;

                string sTermSSN = Guid.NewGuid().ToString("N").Substring(0, 6);
                string sTransmsnDateTime = DateTime.Now.ToString("yyyyMMddHHmmss");
                string sCardAccptrId = txtMchNo.Text.Trim();
                string sCardAccptrTermnlId = txtTermNo.Text.Trim();
                string sFwdInstIdCode = "63070000";
                //发送机构+商户号+终端号+商户流水号+交易时间(不补零或空格)
                string sTranLogTraceNo = sFwdInstIdCode + sCardAccptrId + sCardAccptrTermnlId + sTermSSN + sTransmsnDateTime;
                var req = new OrderQueryReq();
                req.sTransClass = "9912";
                req.sServiceDistinctCode = "01";
                req.sTransmsnDateTime = sTransmsnDateTime;
                req.sTermSSN = sTermSSN;
                req.sFwdInstIdCode = sFwdInstIdCode;
                req.sCurrcyCodeTrans = "156";
                req.sMchtIp = "127.0.0.1";
                req.sTranLogTraceNo = sTranLogTraceNo;

                req.sCardAccptrId = sCardAccptrId;
                req.sCardAccptrTermnlId = sCardAccptrTermnlId;

                req.sOrgOutOrderNo = txtOutTradeNo.Text;//原商户单号
                req.sInquiryMod = "1";
                req.sOrigDataElemts = DateTime.Parse(txtPayTime.Text).ToString("yyyyMMdd");//原日期

                req.sEncryptFlag = "1";
                req.sSigncertID = txtCertSN.Text;

                string bodyJson = JsonConvert.SerializeObject(req, jsetting);

                #region 生成签名                
                string sign = GetSign(txtPrivateKey.Text, txtCertSN.Text, bodyJson);
                req.sSignature = sign;
                #endregion

                bodyJson = JsonConvert.SerializeObject(req, jsetting);

                string mainUrl = txtMainUrl.Text;
                string url = mainUrl;
                GLog.WLog("url:" + url);

                GLog.WLog("请求报文:" + bodyJson);

                var rstStr = HttpUtil.HttpPostJson(url, bodyJson, 30);
                GLog.WLog("响应报文:" + rstStr);
                txtRst.Text = rstStr;
                if (rstStr.Contains("sSignature"))
                {
                    if (!ValidSign(rstStr, txtPubCert.Text, txtCertSN.Text))
                    {
                        MessageBox.Show("签名验证失败");
                    }
                }

                var rspM = JsonConvert.DeserializeObject<OrderQueryRsp>(rstStr);
                /*
0 — 处理中
1 — 交易成功
2 — 支付通道拒绝
4 — 受理方拒绝
5 — 已撤销
6 — 已关闭
7 — 转入退款
8 —交易结束，不可退款
a —已冲正
其他 —交易失败
                 */
                if (rspM != null)
                {
                    if (rspM.sOrgTransState == "1")
                    {
                        lblPayStatus.Text = "支付成功";
                    }
                    else if (rspM.sOrgTransState == "0")
                    {
                        lblPayStatus.Text = "支付中";
                    }
                    else
                    {
                        lblPayStatus.Text = "支付失败";
                    }

                    if (!string.IsNullOrWhiteSpace(rspM.sVoucherNum))
                        txtTradeNo.Text = rspM.sVoucherNum;

                    DateTime dtTimeEnd = DateTime.Now;

                    txtPayTime.Text = dtTimeEnd.ToString("yyyy-MM-dd HH:mm:ss");
                    // 分转元：
                    if (!string.IsNullOrEmpty(rspM.sAmtTrans))
                    {
                        string total_amount = PaAmtTrans(rspM.sAmtTrans);
                        txtAmt.Text = total_amount;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// 平安银行，分转元
        /// </summary>
        /// <param name="inputStr">输入：000000000001</param>
        /// <returns>输出：0.01</returns>
        static string PaAmtTrans(string inputStr)
        {
            if (string.IsNullOrWhiteSpace(inputStr))
            {
                throw new Exception("输入参数为空！");
            }
            //从左到右，第一个非0字符索引
            int not0index = 0;

            for (int i = 0; i < inputStr.Length; i++)
            {
                string tmp = inputStr[i].ToString();
                if (tmp != "0")
                {
                    not0index = i;
                    break;
                }
            }
            //取出金额：分。
            string newStr = inputStr.Substring(not0index);
            //转换为元
            string total_amount = (SIString.TryInt(newStr) / 100M).ToString("F2");
            return total_amount;
        }



        private void btnRefund_Click(object sender, EventArgs e)
        {
            try
            {
                txtOutRefundNo.Text = "R000000000000" + DateTime.Now.ToString("yyyyMMddHHmmssfff");
                int refund_fee = SIString.TryInt((SIString.TryDec(txtRefundAmt.Text.Trim()) * 100));
                string refundAmt = refund_fee.ToString().PadLeft(12, '0');

                OrderQueryRsp oqrModel = OrderQueryCore(txtMchNo.Text.Trim(), txtTermNo.Text.Trim(), txtOutTradeNo.Text.Trim(),
                    txtPayTime.Text
                    , txtPrivateKey.Text
                    , txtCertSN.Text, txtPubCert.Text);

                string sChnOrderNo = "";
                if (oqrModel != null && !string.IsNullOrWhiteSpace(oqrModel.sVoucherNum))
                {
                    sChnOrderNo = oqrModel.sVoucherNum;
                }

                JsonSerializerSettings jsetting = new JsonSerializerSettings();
                jsetting.DefaultValueHandling = DefaultValueHandling.Ignore;

                string sTermSSN = Guid.NewGuid().ToString("N").Substring(0, 6);
                string sTransmsnDateTime = DateTime.Now.ToString("yyyyMMddHHmmss");
                string sCardAccptrId = txtMchNo.Text.Trim();
                string sCardAccptrTermnlId = txtTermNo.Text.Trim();
                string sFwdInstIdCode = "63070000";
                //发送机构+商户号+终端号+商户流水号+交易时间(不补零或空格)
                string sTranLogTraceNo = sFwdInstIdCode + sCardAccptrId + sCardAccptrTermnlId + sTermSSN + sTransmsnDateTime;
                var req = new RefundReq();
                req.sTransClass = "9911";
                req.sServiceDistinctCode = "01";
                req.sTransmsnDateTime = sTransmsnDateTime;
                req.sTermSSN = sTermSSN;
                req.sFwdInstIdCode = sFwdInstIdCode;
                req.sCurrcyCodeTrans = "156";
                 
                req.sTranLogTraceNo = sTranLogTraceNo;

                req.sCardAccptrId = sCardAccptrId;
                req.sCardAccptrTermnlId = sCardAccptrTermnlId;

                req.sOutOrderNo = txtOutRefundNo.Text;//退款申请号
                req.sAmtTrans = refundAmt;//本次退款金额
                req.sInquiryMod = "1";//退款方式:0：使用原交易检索参考号退款                 1：使用原交易渠道订单号退款
                req.sChnOrderNo = sChnOrderNo;//原交易渠道订单号
                req.sOrigDataElemts= DateTime.Parse(txtPayTime.Text).ToString("yyyyMMdd");//原日期（YYYYMMDD）

                req.sEncryptFlag = "1";
                req.sSigncertID = txtCertSN.Text;

                string bodyJson = JsonConvert.SerializeObject(req, jsetting);

                #region 生成签名                
                string sign = GetSign(txtPrivateKey.Text, txtCertSN.Text, bodyJson);
                req.sSignature = sign;
                #endregion

                bodyJson = JsonConvert.SerializeObject(req, jsetting);

                string mainUrl = txtMainUrl.Text;
                string url = mainUrl;
                GLog.WLog("url:" + url);

                GLog.WLog("请求报文:" + bodyJson);

                var rstStr = HttpUtil.HttpPostJson(url, bodyJson, 30);
                GLog.WLog("响应报文:" + rstStr);
                txtRst.Text = rstStr;
                if (rstStr.Contains("sSignature"))
                {
                    if (!ValidSign(rstStr, txtPubCert.Text, txtCertSN.Text))
                    {
                        MessageBox.Show("签名验证失败");
                    }
                }

                var rspM = JsonConvert.DeserializeObject<RefundRsp>(rstStr);
                /*
0 — 处理中
1 — 交易成功
2 — 支付通道拒绝
4 — 受理方拒绝
5 — 已撤销
6 — 已关闭
7 — 转入退款
8 —交易结束，不可退款
a —已冲正
其他 —交易失败
                 */
                if (rspM != null)
                {
                    if (rspM.sRespCode == "00000000")
                    {
                        lblRefundStatus.Text = "退款成功";
                    }
                     
                    else
                    {
                        lblRefundStatus.Text = "退款失败";

                        string msg = string.Format("code:{0} .msg:{1}",rspM.sRespCode??"", rspM.sTransStateMsg ?? "");
                        lblRefundStatus.Text = "退款失败:"+ msg;
                    }

                     

                    
                     
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnRefundQuery_Click(object sender, EventArgs e)
        {
            try
            {
                JsonSerializerSettings jsetting = new JsonSerializerSettings();
                jsetting.DefaultValueHandling = DefaultValueHandling.Ignore;

                int total_fee = SIString.TryInt((SIString.TryDec(txtAmt.Text.Trim()) * 100));

                var req = new OrderQueryReq();
                //req.mercId = txtMchNo.Text.Trim();
                //req.termNo = txtTermNo.Text.Trim();
                //req.ornOrderId = txtOutRefundNo.Text;//传入退款申请号

                var pub = new LKL_PubReq<OrderQueryReq>();
                pub.reqData = req;

                string bodyJson = JsonConvert.SerializeObject(pub, jsetting);
                bodyJson = bodyJson.Substring(0, bodyJson.Length - 1);
                //空termExtInfo处理
                bodyJson = bodyJson + ",\"termExtInfo\":{}}";

                #region 生成签名

                string appid = txtAppId.Text;
                string serial_no = txtCertSN.Text;
                String Authorization = GetSign(appid, serial_no, bodyJson);

                #endregion

                Dictionary<string, string> dicHeader = new Dictionary<string, string>();
                dicHeader.Add("Authorization", Authorization);

                string mainUrl = txtMainUrl.Text;
                string url = mainUrl + "/labs_order_query";
                GLog.WLog("url:" + url);
                GLog.WLog("Authorization:" + Authorization);
                GLog.WLog("请求报文:" + bodyJson);
                Dictionary<string, string> rspHeader = new Dictionary<string, string>();

                var rstStr = HttpUtil.HttpPostJson(url, bodyJson, dicHeader, 30, rspHeader);
                GLog.WLog("响应报文:" + rstStr);
                txtRst.Text = rstStr;
                //if (!ValidSign(rspHeader, rstStr, txtPubCert.Text))
                //{
                //    MessageBox.Show("签名验证失败");
                //}

                var rspMA = JsonConvert.DeserializeObject<LKL_PubRsp<OrderQueryRsp>>(rstStr);
                var rspM = rspMA.respData;

                if (rspM != null)
                {
                    //除了退款成功，其它状态未能验证
                    //if (rspM.tradeState == "SUCCESS")
                    //{
                    //    lblRefundStatus.Text = "退款成功";
                    //}
                    //else if (rspM.tradeState == "DEAL")
                    //{
                    //    lblRefundStatus.Text = "退款中";
                    //}
                    //else
                    //{
                    //    lblRefundStatus.Text = "退款失败";
                    //}

                    //if (!string.IsNullOrWhiteSpace(rspM.lklOrderNo))
                    //    txtTradeNo.Text = rspM.lklOrderNo;

                    //if (!string.IsNullOrWhiteSpace(rspM.weOrderNo))
                    //    txtThirdTradeNo.Text = rspM.weOrderNo;


                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnTest_Click(object sender, EventArgs e)
        {
            string aa = "0000001122";
            aa = "0000000002";
            aa = "0000701122";
            string bb = PaAmtTrans(aa);

            MessageBox.Show(bb);
        }
    }
}
