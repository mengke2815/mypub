﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LAKALA.Models
{
    /// <summary>
    ///  响应
    /// </summary>
    public class RefundRsp
    {

         
            public string sOutOrderNo { get; set; }
            public string sCardAccptrId { get; set; }
            public string sEncryptFlag { get; set; }
            public string sSigncertID { get; set; }
            public string sDCFlag { get; set; }
            public string sTermSSN { get; set; }
            public string sAmtTrans { get; set; }
            public string sAcqInstIdCode { get; set; }
            public string sCurrcyCodeTrans { get; set; }
            public string sTransmsnDateTime { get; set; }
            public string sCardAccptrTermnlId { get; set; }
            public string sTransStateMsg { get; set; }
            public string sTransMedia { get; set; }
            public string sServiceDistinctCode { get; set; }
            public string sTransClass { get; set; }
            public string sRetrivlRefNum { get; set; }
            public string sRespCode { get; set; }
            public string sTranLogTraceNo { get; set; }
            public string sSignature { get; set; }
        




    }
}
