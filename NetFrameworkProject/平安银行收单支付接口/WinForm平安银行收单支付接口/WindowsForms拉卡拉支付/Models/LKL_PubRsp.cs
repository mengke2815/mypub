﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LAKALA.Models
{
    /// <summary>
    /// 响应报文
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public  class LKL_PubRsp<T>
    {
        public LKL_PubRsp()
        {
             
        }
        /// <summary>
        /// Unix时间戳。13位
        /// </summary>
        public long timestamp { get; set; }

        /// <summary>
        /// GLOBAL_SUCCESS,TRANS_USING_PAYING
        /// </summary>
        public string cmdRetCode { get; set; }
        public string retCode { get; set; }
        public string retMsg { get; set; }
        
        public T respData { get; set; }

        
    }
}
