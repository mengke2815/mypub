﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LAKALA.Models
{
    public class OrderQueryReq
    {
        /// <summary>
        /// 交易类 9901
        /// </summary>
        public string sTransClass { get; set; }

        /// <summary>
        /// 服务区别码 01：扫码
        /// </summary>
        public string sServiceDistinctCode { get; set; }

        /// <summary>
        /// 交易时间,yyyyMMddHHmmss
        /// </summary>
        public string sTransmsnDateTime { get; set; }
        /// <summary>
        /// 商户流水号
        /// </summary>
        public string sTermSSN { get; set; }
        /// <summary>
        /// 商户号
        /// </summary>
        public string sCardAccptrId { get; set; }
        /// <summary>
        /// 终端号
        /// </summary>
        public string sCardAccptrTermnlId { get; set; }
        /// <summary>
        /// 发送机构标识码
        /// </summary>
        public string sFwdInstIdCode { get; set; }
        /// <summary>
        /// 交易金额 以分为单位，左补零，补全12位
        /// </summary>
        public string sAmtTrans { get; set; }
        public string sCurrcyCodeTrans { get; set; }
        public string sMchtIp { get; set; }
        public string sAuthCode { get; set; }
        public string sTranLogTraceNo { get; set; }
        public string sOutOrderNo { get; set; }

        public string sEncryptFlag { get; set; }
        public string sSigncertID { get; set; }
        

        /// <summary>
        /// 填年月日（YYYYMMDD）
        /// </summary>
        public string sOrigDataElemts { get; set; }

        public string sInquiryMod { get; set; }
        /// <summary>
        /// 原交易商户订单号
        /// </summary>
        public string sOrgOutOrderNo { get; set; }

        public string sSignature { get; set; }
    }
}
