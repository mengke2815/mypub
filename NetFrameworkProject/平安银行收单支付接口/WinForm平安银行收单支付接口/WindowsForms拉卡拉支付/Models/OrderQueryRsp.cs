﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LAKALA.Models
{
    /// <summary>
    /// 订单查询，响应
    /// </summary>
    public class OrderQueryRsp
    {
        public string sDCFlag { get; set; }

        /// <summary>
        /// 渠道订单号，可用于退款
        /// </summary>
        public string sVoucherNum { get; set; }
        public string sTransClass { get; set; }
        public string sServiceDistinctCode { get; set; }
        public string sTransMedia { get; set; }
        public string sTermSSN { get; set; }
        public string sRetrivlRefNum { get; set; }
        public string sCurrcyCodeTrans { get; set; }
        public string sCardAccptrTermnlId { get; set; }
        public string sRespCode { get; set; }
        public string sCardAccptrId { get; set; }
        public string sTransStateMsg { get; set; }
        public string sAcqInstIdCode { get; set; }
        public string sOrgTransStateMsg { get; set; }
        public string sOrgTransState { get; set; }
        public string sOrgTermSSN { get; set; }
        public string sOrgRetrivlRefNum { get; set; }
        public string sTransmsnDateTime { get; set; }
        public string sTranLogTraceNo { get; set; }
        public string sEncryptFlag { get; set; }
        public string sSigncertID { get; set; }
        public string sSubOpenid { get; set; }
        public string sSignature { get; set; }

        /// <summary>
        /// 支付金额
        /// </summary>
        public string sAmtTrans { get; set; }

        

    }
}
