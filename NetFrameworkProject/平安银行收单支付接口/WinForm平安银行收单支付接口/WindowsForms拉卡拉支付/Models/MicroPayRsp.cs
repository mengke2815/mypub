﻿namespace LAKALA.Models
{
    public class MicroPayRsp
    {
        public string sTermSSN { get; set; }
        public string sEncryptFlag { get; set; }
        public string sAcqInstIdCode { get; set; }
        public string sCurrcyCodeTrans { get; set; }
        public string sTransmsnDateTime { get; set; }
        public string sTransStateMsg { get; set; }
        public string sServiceDistinctCode { get; set; }
        public string sCardAccptrTermnlId { get; set; }
        public string sTransMedia { get; set; }
        public string sTransClass { get; set; }
        public string sCardAccptrId { get; set; }
        public string sRetrivlRefNum { get; set; }
        public string sRespCode { get; set; }
        public string sAmtTrans { get; set; }

        /// <summary>
        /// 手机商-商户单号
        /// </summary>
        public string sVoucherNum { get; set; }
        public string sSigncertID { get; set; }
        public string sSignature { get; set; }

    }
}
