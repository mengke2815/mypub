﻿namespace 国密SM4加密
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSm4EcbEncrypt = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txt明文 = new System.Windows.Forms.TextBox();
            this.txt密文 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnSm4EcbDecrypt = new System.Windows.Forms.Button();
            this.txt解密后明文 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnSm4CbcEncrypt = new System.Windows.Forms.Button();
            this.btnSm4CbcDecrypt = new System.Windows.Forms.Button();
            this.txtKey = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnSm4EcbEncrypt
            // 
            this.btnSm4EcbEncrypt.Location = new System.Drawing.Point(490, 159);
            this.btnSm4EcbEncrypt.Name = "btnSm4EcbEncrypt";
            this.btnSm4EcbEncrypt.Size = new System.Drawing.Size(134, 23);
            this.btnSm4EcbEncrypt.TabIndex = 0;
            this.btnSm4EcbEncrypt.Text = "SM4 ECB 加密";
            this.btnSm4EcbEncrypt.UseVisualStyleBackColor = true;
            this.btnSm4EcbEncrypt.Click += new System.EventHandler(this.btnSm4EcbEncrypt_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(67, 162);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "明文";
            // 
            // txt明文
            // 
            this.txt明文.Location = new System.Drawing.Point(138, 159);
            this.txt明文.Name = "txt明文";
            this.txt明文.Size = new System.Drawing.Size(313, 21);
            this.txt明文.TabIndex = 2;
            this.txt明文.Text = "1234中华人民共和国";
            // 
            // txt密文
            // 
            this.txt密文.Location = new System.Drawing.Point(138, 198);
            this.txt密文.Multiline = true;
            this.txt密文.Name = "txt密文";
            this.txt密文.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txt密文.Size = new System.Drawing.Size(313, 64);
            this.txt密文.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(67, 201);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 12);
            this.label2.TabIndex = 4;
            this.label2.Text = "密文";
            // 
            // btnSm4EcbDecrypt
            // 
            this.btnSm4EcbDecrypt.Location = new System.Drawing.Point(490, 198);
            this.btnSm4EcbDecrypt.Name = "btnSm4EcbDecrypt";
            this.btnSm4EcbDecrypt.Size = new System.Drawing.Size(134, 23);
            this.btnSm4EcbDecrypt.TabIndex = 3;
            this.btnSm4EcbDecrypt.Text = "SM4 ECB 解密";
            this.btnSm4EcbDecrypt.UseVisualStyleBackColor = true;
            this.btnSm4EcbDecrypt.Click += new System.EventHandler(this.btnSm4EcbDecrypt_Click);
            // 
            // txt解密后明文
            // 
            this.txt解密后明文.Location = new System.Drawing.Point(138, 268);
            this.txt解密后明文.Name = "txt解密后明文";
            this.txt解密后明文.Size = new System.Drawing.Size(313, 21);
            this.txt解密后明文.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(67, 271);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 12);
            this.label3.TabIndex = 6;
            this.label3.Text = "解密后明文";
            // 
            // btnSm4CbcEncrypt
            // 
            this.btnSm4CbcEncrypt.Location = new System.Drawing.Point(641, 157);
            this.btnSm4CbcEncrypt.Name = "btnSm4CbcEncrypt";
            this.btnSm4CbcEncrypt.Size = new System.Drawing.Size(134, 23);
            this.btnSm4CbcEncrypt.TabIndex = 8;
            this.btnSm4CbcEncrypt.Text = "SM4 CBC 加密";
            this.btnSm4CbcEncrypt.UseVisualStyleBackColor = true;
            this.btnSm4CbcEncrypt.Click += new System.EventHandler(this.btnSm4CbcEncrypt_Click);
            // 
            // btnSm4CbcDecrypt
            // 
            this.btnSm4CbcDecrypt.Location = new System.Drawing.Point(641, 196);
            this.btnSm4CbcDecrypt.Name = "btnSm4CbcDecrypt";
            this.btnSm4CbcDecrypt.Size = new System.Drawing.Size(134, 23);
            this.btnSm4CbcDecrypt.TabIndex = 9;
            this.btnSm4CbcDecrypt.Text = "SM4 CBC 解密";
            this.btnSm4CbcDecrypt.UseVisualStyleBackColor = true;
            this.btnSm4CbcDecrypt.Click += new System.EventHandler(this.btnSm4CbcDecrypt_Click);
            // 
            // txtKey
            // 
            this.txtKey.Location = new System.Drawing.Point(129, 52);
            this.txtKey.Name = "txtKey";
            this.txtKey.Size = new System.Drawing.Size(313, 21);
            this.txtKey.TabIndex = 11;
            this.txtKey.Text = "9814548961710661";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(58, 55);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(23, 12);
            this.label4.TabIndex = 10;
            this.label4.Text = "Key";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.txtKey);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnSm4CbcDecrypt);
            this.Controls.Add(this.btnSm4CbcEncrypt);
            this.Controls.Add(this.txt解密后明文);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txt密文);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnSm4EcbDecrypt);
            this.Controls.Add(this.txt明文);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnSm4EcbEncrypt);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSm4EcbEncrypt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt明文;
        private System.Windows.Forms.TextBox txt密文;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnSm4EcbDecrypt;
        private System.Windows.Forms.TextBox txt解密后明文;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnSm4CbcEncrypt;
        private System.Windows.Forms.Button btnSm4CbcDecrypt;
        private System.Windows.Forms.TextBox txtKey;
        private System.Windows.Forms.Label label4;
    }
}

