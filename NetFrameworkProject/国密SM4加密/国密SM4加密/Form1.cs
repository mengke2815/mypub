﻿using CommonUtils;
using Org.BouncyCastle.Utilities.Encoders;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace 国密SM4加密
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        //string _Key = "9814548961710661";//密钥长度必须为16字节。 
        string _Iv = "0000000000000000";

        /// <summary>
        /// SM4 ECB 加密
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSm4EcbEncrypt_Click(object sender, EventArgs e)
        {
            string _Key = txtKey.Text;
            //string algo = "SM4/ECB/NoPadding";
            string algo = "SM4/ECB/PKCS5Padding";

            byte[] keyBytes = Encoding.UTF8.GetBytes(_Key);
            //SM4有一个小问题：字符串的长度需要满足是16的倍数（>=1）,所以要padding.
            //加密前需要padding
            //byte[] plain = MyPadding(Encoding.UTF8.GetBytes(txt明文.Text), 1);
            byte[] plain = Encoding.UTF8.GetBytes(txt明文.Text);

            byte[] byRst = GmUtil.Sm4EncryptECB(keyBytes, plain, algo);
            string result2 = Encoding.UTF8.GetString(Hex.Encode(byRst));

            txt密文.Text = result2;
        }

        /// <summary>
        /// SM4 ECB 解密
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSm4EcbDecrypt_Click(object sender, EventArgs e)
        {
            string _Key = txtKey.Text;
            //string algo = "SM4/ECB/NoPadding";
            string algo = "SM4/ECB/PKCS5Padding";

            byte[] keyBytes = Encoding.UTF8.GetBytes(_Key);
            byte[] plain = Hex.Decode(txt密文.Text);
            byte[] byRst = GmUtil.Sm4DecryptECB(keyBytes, plain, algo);
            //解密后需要移除padding
            //byte[] plain2 = MyPadding(byRst, 0);
            byte[] plain2 = byRst;
            string result2 = Encoding.UTF8.GetString(plain2);

            txt解密后明文.Text = result2;
        }

        /// <summary>
        /// 补足 16 进制字符串的 0 字符，返回不带 0x 的16进制字符串
        /// </summary>
        /// <param name="input"></param>
        /// <param name="mode">1表示加密，0表示解密</param>
        /// <returns></returns>
        private static byte[] MyPadding(byte[] input, int mode)
        {
            if (input == null)
            {
                return null;
            }
            byte[] ret = (byte[])null;
            if (mode == 1)
            {
                int p = 16 - input.Length % 16;
                ret = new byte[input.Length + p];
                Array.Copy(input, 0, ret, 0, input.Length);
                for (int i = 0; i < p; i++)
                {
                    ret[input.Length + i] = (byte)p;
                }
            }
            else
            {
                int p = input[input.Length - 1];
                ret = new byte[input.Length - p];
                Array.Copy(input, 0, ret, 0, input.Length - p);
            }
            return ret;
        }

        /// <summary>
        /// SM4 CBC 加密
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSm4CbcEncrypt_Click(object sender, EventArgs e)
        {
            string _Key = txtKey.Text;
            //algorithm
            string algo = "SM4/CBC/PKCS7Padding";             
            //algo = "SM4/CBC/NoPadding";

            byte[] keyBytes = Encoding.UTF8.GetBytes(_Key);
            byte[] ivBytes = Encoding.UTF8.GetBytes(_Iv);

            //加密前需要padding
            byte[] plain = MyPadding(Encoding.UTF8.GetBytes(txt明文.Text), 1);
            byte[] byRst = GmUtil.Sm4EncryptCBC(keyBytes, plain, ivBytes, algo);
            string result2 = Encoding.UTF8.GetString(Hex.Encode(byRst));

            txt密文.Text = result2;
        }

        /// <summary>
        /// SM4 CBC 解密
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSm4CbcDecrypt_Click(object sender, EventArgs e)
        {
            string _Key = txtKey.Text;
            string algo = "SM4/CBC/PKCS7Padding";
            //algo = "SM4/CBC/NoPadding";

            byte[] keyBytes = Encoding.UTF8.GetBytes(_Key);
            byte[] ivBytes = Encoding.UTF8.GetBytes(_Iv);

            byte[] plain = Hex.Decode(txt密文.Text);
            byte[] byRst = GmUtil.Sm4DecryptCBC(keyBytes, plain, ivBytes, algo);
            //解密后需要移除padding
            byte[] plain2 = MyPadding(byRst, 0);
            string result2 = Encoding.UTF8.GetString(plain2);

            txt解密后明文.Text = result2;
        }
    }
}
