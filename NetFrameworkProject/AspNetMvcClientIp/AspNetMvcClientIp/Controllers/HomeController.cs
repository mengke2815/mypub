﻿using CommonUtils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AspNetMvcClientIp.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            string ip = ClientIpUtil.GetClientIpMvc();
            string msg = "ip:" + ip;
            ViewBag.myip = msg;
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}