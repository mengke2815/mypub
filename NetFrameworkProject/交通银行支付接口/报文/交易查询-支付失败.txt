{
	"rsp_biz_content": {
		"biz_state": "S",
		"rsp_code": null,
		"rsp_msg": null,
		"rsp_body": {
			"order_status": "CLOSED",
			"mer_memo": null,
			"trd_dsct_amount": null,
			"require_values": null,
			"tran_state": "FAILURE",
			"currency": "CNY",
			"buyer_pay_amount": "0.01",
			"tran_state_code": null,
			"tran_state_msg": null,
			"pay_mer_tran_no": "S20220311170921348",
			"total_amount": "0.01",
			"refunded_amt": null,
			"tran_content": null,
			"pay_dsct_amount": ""
		},
		"rsp_head": {
			"trans_code": "MPNG020702",
			"response_code": "CIPP0004PY0000",
			"response_status": "S",
			"response_time": "20220311173943",
			"response_msg": "交易成功"
		}
	},
	"sign": "IjSOoyaKgWdScsUpxwokk4ZGIcYsi5NCSMb4ZPDBdgB0Uu18wdAMxNtTv4UgGjWByS2oOVYsCBJZPPtyJ5RGwQY+mH7hXeL5ShQVTvmISTeop3Dk1rX1OQJd5orwOdeYJgkuWMcnFOX3ah9bEGfjqax7qxLWkAYcqqWUTBVSHuROZU+KuX6gbtDJ58fo0qQ0E9jav06NPzcbGB5ib/qAAjXAOWAPz+bR3dDBsGsXEtgT9dllvJXfWpZZxkjiK5DiIoDL/PNsEBAMXhhyScH9esOCKu1WC3eehBrCNKglexLIDYt9lNZD1/iLrEby8/IcO3OGVUjrtN3aNIGIbPJFzQ=="
}