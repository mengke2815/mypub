﻿using CommonUtils;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BOCOM.Models;
using Aop.Api.Util;
using System.Net.Security;
using System.Net;
using Common.Models2;

namespace WindowsForms拉卡拉支付
{
    public partial class Form1 : Form
    {
        System.Timers.Timer _tmrRemeber;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            RemForm.ReadRememberForm(this, null);

            _tmrRemeber = new System.Timers.Timer();
            _tmrRemeber.Elapsed += new System.Timers.ElapsedEventHandler(_tmrRemeber_Elapsed);
            _tmrRemeber.Interval = 10 * 1000;
            _tmrRemeber.Start();

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
            System.Net.ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(CheckValidationResult);

        }


        private static bool CheckValidationResult(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors errors)
        {
            return true;
        }

        void _tmrRemeber_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                RemForm.RememberForm(this, null);
            }
            catch (Exception ex)
            {
                //MessageBox.Show("_tmrRemeber_Elapsed" + ex.Message);
            }
        }

        private void btnReadCrt_Click(object sender, EventArgs e)
        {
            try
            {
                if (ofdCrt.ShowDialog() == DialogResult.OK)
                {
                    X509Certificate2 cert = new X509Certificate2(ofdCrt.FileName);
                    txtCertSN.Text = cert.SerialNumber;
                    //txtCertSN10.Text = cert.SerialNumber.ToLower();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        private void btnMicroPay_Click(object sender, EventArgs e)
        {
            try
            {
                JsonSerializerSettings jsetting = new JsonSerializerSettings();
                jsetting.DefaultValueHandling = DefaultValueHandling.Ignore;

                if (decimal.Parse(txtAmt.Text) >= 1M)
                {
                    DialogResult dr = MessageBox.Show("金额过大，是否继续？", "OO", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dr == DialogResult.No)
                    {
                        return;
                    }
                }
                if (string.IsNullOrWhiteSpace(txtAuthCode.Text))
                {
                    MessageBox.Show("付款码不能为空！");
                    return;
                }
                //初始化付款完成时间。
                txtPayTime.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                int total_fee = VString.TryInt((VString.TryDec(txtAmt.Text.Trim()) * 100));
                txtOutTradeNo.Text = "S" + DateTime.Now.ToString("yyyyMMddHHmmssfff");

                List<Require_Fields> lstMicroPayRequire = new List<Require_Fields>();
                lstMicroPayRequire.Add(new Require_Fields { require_field = "bank_tran_no" });
                lstMicroPayRequire.Add(new Require_Fields { require_field = "third_party" });
                lstMicroPayRequire.Add(new Require_Fields { require_field = "third_party_tran_no" });

                MicroPayReq req = new MicroPayReq();
                req.mer_ptc_id = txtMchNo.Text.Trim();
                req.terminal_info = txtTermNo.Text.Trim();
                req.scan_code_text = txtAuthCode.Text.Trim();
                req.total_amount = txtAmt.Text;
                req.pay_mer_tran_no = txtOutTradeNo.Text;
                req.valid_period = DateTime.Now.AddMinutes(2).ToString("yyyyMMddHHmmss");
                req.currency = "CNY";
                req.tran_scene = "B2C-API-SCANCODE";
                req.ip = "182.119.117.128";
                req.mer_trade_date = DateTime.Now.ToString("yyyyMMdd");
                req.mer_trade_time = DateTime.Now.ToString("HHmmss");
                req.location = "OFFLINE";
                req.require_fields = lstMicroPayRequire.ToArray();
                BOCOM_PubBizReq<MicroPayReq> pub = new BOCOM_PubBizReq<MicroPayReq>();
                pub.req_body = req;

                //报文主体

                string bodyJson = JsonConvert.SerializeObject(pub, jsetting);

                string suffix = "/api/pmssMpng/MPNG210002/v1";

                #region 生成签名
                BOCOM_PubReq cont = new BOCOM_PubReq();
                cont.app_id = txtAppId.Text;
                cont.biz_content = bodyJson;

                var dic1 = HashUtil.ModelToDic(cont);

                string sign = GetSign(cont, suffix, txtPrivateKey.Text, "PKCS8");

                dic1.Add("sign", sign);


                #endregion

                WebUtils wu = new WebUtils();

                string mainUrl = txtMainUrl.Text;
                string url = mainUrl + suffix;
                GLog.WLog("url:" + url);

                GLog.WLog("请求报文:" + bodyJson);
                Dictionary<string, string> rspHeader = new Dictionary<string, string>();

                var rstStr = wu.DoPost(url, dic1, "UTF-8");
                GLog.WLog("响应报文:" + rstStr);
                txtRst.Text = rstStr;
                if (!ValidSign(rspHeader, rstStr, txtPubCert.Text))
                {
                    MessageBox.Show("签名验证失败");
                }
                txtAuthCode.Clear();

                #region 转义
                string _code = "";
                string _msg = "";


                var rspMA = JsonConvert.DeserializeObject<BOCOM_PubRsp<MicroPayRsp>>(rstStr);
                var rspHead = rspMA.rsp_biz_content.rsp_head;
                var rspBody = rspMA.rsp_biz_content.rsp_body;
                //response_status,交易状态 P-处理中  F-失败  S-成功
                if (rspHead.response_status == "S")
                {
                    lblPayStatus.Text = "支付成功";
                    _code = SissPayReturnCode.SUCCESS.ToString();
                }
                else if (rspHead.response_status == "P")
                {
                    lblPayStatus.Text = "支付中";
                    _code = SissPayReturnCode.WAIT_BUYER_PAY.ToString();

                }
                else
                {
                    lblPayStatus.Text = "支付失败";
                    _code = SissPayReturnCode.FAIL.ToString();
                    _msg = rspHead.response_msg;
                }
                if (rspBody != null)
                {
                    //交易单号
                    if (!string.IsNullOrWhiteSpace(rspBody.sys_order_no))
                        txtTradeNo.Text = rspBody.sys_order_no;

                    //三方交易号
                    if (rspBody.require_values != null && !string.IsNullOrWhiteSpace(rspBody.require_values.third_party_tran_no))
                        txtThirdTradeNo.Text = rspBody.require_values.third_party_tran_no;

                    txtPayTime.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                }
                #endregion


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public string GetSign(BOCOM_PubReq cont, string suffix, string PrivateKey, string PrivateKeyForm)
        {
            var dic1 = HashUtil.ModelToDic(cont);
            var dic2 = HashUtil.AsciiDictionary(dic1);//排序
            var toSignStr1 = HashUtil.BuildQueryString(dic2);//拼接

            string toSignStr2 = suffix + "?" + toSignStr1;//拼上URL后缀
            byte[] byToSign = Encoding.UTF8.GetBytes(toSignStr2);
            var rsa = RsaUtil.LoadPrivateKey(PrivateKey, PrivateKeyForm);
            byte[] byFnl = rsa.SignData(byToSign, "SHA256");
            string sign = Convert.ToBase64String(byFnl);

            return sign;
        }

        /// <summary>
        /// 验证签名
        /// </summary>
        /// <param name="rspHeader">HTTP响应头</param>
        /// <param name="rspBody">响应报文</param>
        /// <param name="pubCert">公钥证书</param>
        /// <returns></returns>
        bool ValidSign(IDictionary<string, string> rspHeader, string rspBody, string pubCert)
        {
            var dic = JsonConvert.DeserializeObject<BOCOM_PubRspSign>(rspBody);
            string fnstr = rspBody.Substring(0, rspBody.LastIndexOf(",\"sign"));
            fnstr = fnstr.Substring(fnstr.IndexOf("rsp_biz_content") + 17);
            string rspSign = dic.sign;

            byte[] byteCon = Encoding.UTF8.GetBytes(fnstr);

            byte[] byteRspSign = Convert.FromBase64String(rspSign);
            var rsaPub = RsaUtil.LoadPublicKey(pubCert);

            bool bRst = rsaPub.VerifyData(byteCon, "SHA256", byteRspSign);

            return bRst;

        }

        private void txtAuthCode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnMicroPay_Click(sender, e);
            }
        }

        private void btnOrderQuery_Click(object sender, EventArgs e)
        {
            try
            {
                List<Require_Fields> lstQueryRequire = new List<Require_Fields>();
                lstQueryRequire.Add(new Require_Fields { require_field = "bank_tran_no" });
                lstQueryRequire.Add(new Require_Fields { require_field = "third_party" });
                lstQueryRequire.Add(new Require_Fields { require_field = "third_party_tran_no" });
                lstQueryRequire.Add(new Require_Fields { require_field = "payment_info" });
                lstQueryRequire.Add(new Require_Fields { require_field = "refund_info" });


                JsonSerializerSettings jsetting = new JsonSerializerSettings();
                jsetting.DefaultValueHandling = DefaultValueHandling.Ignore;

                var req = new OrderQueryReq();
                req.mer_ptc_id = txtMchNo.Text.Trim();

                req.pay_mer_tran_no = txtOutTradeNo.Text;

                req.tran_scene = "B2C-API-SCANCODE";

                req.mer_trade_date = DateTime.Parse(txtPayTime.Text).ToString("yyyyMMdd");
                req.require_fields = lstQueryRequire.ToArray();
                var pub = new BOCOM_PubBizReq<OrderQueryReq>();
                pub.req_body = req;

                //报文主体

                string bodyJson = JsonConvert.SerializeObject(pub, jsetting);

                string suffix = "/api/pmssMpng/MPNG020702/v1";

                #region 生成签名
                BOCOM_PubReq cont = new BOCOM_PubReq();
                cont.app_id = txtAppId.Text;
                cont.biz_content = bodyJson;

                var dic1 = HashUtil.ModelToDic(cont);

                string sign = GetSign(cont, suffix, txtPrivateKey.Text, "PKCS8");

                dic1.Add("sign", sign);


                #endregion

                WebUtils wu = new WebUtils();

                string mainUrl = txtMainUrl.Text;
                string url = mainUrl + suffix;
                GLog.WLog("url:" + url);

                GLog.WLog("请求报文:" + bodyJson);
                Dictionary<string, string> rspHeader = new Dictionary<string, string>();

                var rstStr = wu.DoPost(url, dic1, "UTF-8");
                GLog.WLog("响应报文:" + rstStr);
                txtRst.Text = rstStr;
                if (!ValidSign(rspHeader, rstStr, txtPubCert.Text))
                {
                    MessageBox.Show("签名验证失败");
                }


                #region 转义
                string _code = "";
                string _msg = "";
                string _trade_status = "";

                var rspMA = JsonConvert.DeserializeObject<BOCOM_PubRsp<OrderQueryRsp>>(rstStr);
                var rspHead = rspMA.rsp_biz_content.rsp_head;
                var rspBody = rspMA.rsp_biz_content.rsp_body;

                _code = SissPayReturnCode.SUCCESS.ToString();

                //response_status,交易状态 P-处理中  F-失败  S-成功
                if (rspHead.response_status == "S")
                {
                    _code = SissPayReturnCode.SUCCESS.ToString();
                }
                else
                {
                    _code = SissPayReturnCode.FAIL.ToString();
                    _msg = rspHead.response_msg;
                }


                if (rspBody != null)
                {
                    //商户单号
                    if (!string.IsNullOrWhiteSpace(rspBody.pay_mer_tran_no))
                        txtOutTradeNo.Text = rspBody.pay_mer_tran_no;

                    //交易单号
                    if (rspBody.require_values!=null&&  !string.IsNullOrWhiteSpace(rspBody.require_values.bank_tran_no))
                        txtTradeNo.Text = rspBody.require_values.bank_tran_no;

                    //三方交易号
                    if (rspBody.require_values != null && !string.IsNullOrWhiteSpace(rspBody.require_values.third_party_tran_no))
                        txtThirdTradeNo.Text = rspBody.require_values.third_party_tran_no;

                    if (rspBody.order_status == "PAIED")
                    {
                        lblPayStatus.Text = "支付成功";
                        _trade_status = SissPayTradeStatus.TRADE_SUCCESS.ToString();
                    }
                    else if (rspBody.order_status == "WAITPAY")
                    {
                        lblPayStatus.Text = "支付中";
                        _trade_status = SissPayTradeStatus.WAIT_BUYER_PAY.ToString();

                    }
                    else
                    {
                        lblPayStatus.Text = "支付失败";
                        _trade_status = SissPayTradeStatus.TRADE_FAIL.ToString();

                    }

                    if (!string.IsNullOrEmpty(rspBody.total_amount))
                    {
                        string total_amount = rspBody.total_amount;
                    }
                }

                #endregion
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnRefund_Click(object sender, EventArgs e)
        {
            try
            {
                JsonSerializerSettings jsetting = new JsonSerializerSettings();
                jsetting.DefaultValueHandling = DefaultValueHandling.Ignore;

                int refund_fee = VString.TryInt((VString.TryDec(txtRefundAmt.Text) * 100));
                txtOutRefundNo.Text = "R" + DateTime.Now.ToString("yyyyMMddHHmmssfff");

                var req = new RefundReq();
                req.mer_ptc_id = txtMchNo.Text.Trim();//商户号

                req.amount = txtRefundAmt.Text;//退款金额。
                req.pay_mer_tran_no = txtOutTradeNo.Text;//原商户单号
                req.mer_trade_date = DateTime.Parse(txtPayTime.Text).ToString("yyyyMMdd");//原交易日期

                req.tran_scene = "B2C-API-SCANCODE";
                req.currency = "CNY";
                req.mer_refund_date = DateTime.Now.ToString("yyyyMMdd");//退款发起日期
                req.mer_refund_time = DateTime.Now.ToString("HHmmss");//退款发起时间
                req.refund_mer_tran_no = txtOutRefundNo.Text;//退款申请单号

                var pub = new BOCOM_PubBizReq<RefundReq>();
                pub.req_body = req;

                //报文主体

                string bodyJson = JsonConvert.SerializeObject(pub, jsetting);

                string suffix = "/api/pmssMpng/MPNG020701/v1";

                #region 生成签名
                BOCOM_PubReq cont = new BOCOM_PubReq();
                cont.app_id = txtAppId.Text;
                cont.biz_content = bodyJson;

                var dic1 = HashUtil.ModelToDic(cont);

                string sign = GetSign(cont, suffix, txtPrivateKey.Text, "PKCS8");

                dic1.Add("sign", sign);


                #endregion

                WebUtils wu = new WebUtils();

                string mainUrl = txtMainUrl.Text;
                string url = mainUrl + suffix;
                GLog.WLog("url:" + url);

                GLog.WLog("请求报文:" + bodyJson);
                Dictionary<string, string> rspHeader = new Dictionary<string, string>();

                var rstStr = wu.DoPost(url, dic1, "UTF-8");
                GLog.WLog("响应报文:" + rstStr);
                txtRst.Text = rstStr;
                if (!ValidSign(rspHeader, rstStr, txtPubCert.Text))
                {
                    MessageBox.Show("签名验证失败");
                }

                #region 转义
                string _code = "";
                string _msg = "";


                var rspMA = JsonConvert.DeserializeObject<BOCOM_PubRsp<RefundRsp>>(rstStr);
                var rspHead = rspMA.rsp_biz_content.rsp_head;
                var rspBody = rspMA.rsp_biz_content.rsp_body;
                //response_status,交易状态 P-处理中  F-失败  S-成功
                if (rspHead.response_status == "S" || rspHead.response_status == "P")
                {
                    lblRefundStatus.Text = "退款申请成功";
                    _code = SissPayReturnCode.SUCCESS.ToString();
                }
                else
                {
                    lblRefundStatus.Text = "退款失败";
                    _code = SissPayReturnCode.FAIL.ToString();
                    _msg = rspHead.response_msg ?? "";
                }

                #endregion

                //var rspMA = JsonConvert.DeserializeObject<LKL_PubRsp<OrderQueryRsp>>(rstStr);
                //var rspM = rspMA.respData;

                //if (rspMA.cmdRetCode == "GLOBAL_SUCCESS")
                //{
                //    lblRefundStatus.Text = "退款申请成功";
                //}
                //else
                //{
                //    lblRefundStatus.Text = "退款失败";
                //}

                //if (rspM != null)
                //{
                //}
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnRefundQuery_Click(object sender, EventArgs e)
        {
            try
            {
                JsonSerializerSettings jsetting = new JsonSerializerSettings();
                jsetting.DefaultValueHandling = DefaultValueHandling.Ignore;

                var req = new RefundQueryReq();
                req.mer_ptc_id = txtMchNo.Text.Trim();//商户号

                req.tran_scene = "B2C-API-SCANCODE";
                req.mer_refund_date = DateTime.Now.ToString("yyyyMMdd");//退款发起日期
                
                req.refund_mer_tran_no = txtOutRefundNo.Text;//退款申请单号

                var pub = new BOCOM_PubBizReq<RefundQueryReq>();
                pub.req_body = req;

                //报文主体

                string bodyJson = JsonConvert.SerializeObject(pub, jsetting);

                string suffix = "/api/pmssMpng/MPNG020703/v1";

                #region 生成签名
                BOCOM_PubReq cont = new BOCOM_PubReq();
                cont.app_id = txtAppId.Text;
                cont.biz_content = bodyJson;

                var dic1 = HashUtil.ModelToDic(cont);

                string sign = GetSign(cont, suffix, txtPrivateKey.Text, "PKCS8");

                dic1.Add("sign", sign);


                #endregion

                WebUtils wu = new WebUtils();

                string mainUrl = txtMainUrl.Text;
                string url = mainUrl + suffix;
                GLog.WLog("url:" + url);

                GLog.WLog("请求报文:" + bodyJson);
                Dictionary<string, string> rspHeader = new Dictionary<string, string>();

                var rstStr = wu.DoPost(url, dic1, "UTF-8");
                GLog.WLog("响应报文:" + rstStr);
                txtRst.Text = rstStr;
                if (!ValidSign(rspHeader, rstStr, txtPubCert.Text))
                {
                    MessageBox.Show("签名验证失败");
                }

                #region 转义
                string _code = "";
                string _msg = "";
                string _refund_status = "";

                var rspMA = JsonConvert.DeserializeObject<BOCOM_PubRsp<OrderQueryRsp>>(rstStr);
                var rspHead = rspMA.rsp_biz_content.rsp_head;
                var rspBody = rspMA.rsp_biz_content.rsp_body;

                _code = SissPayReturnCode.SUCCESS.ToString();

                //response_status,交易状态 P-处理中  F-失败  S-成功
                if (rspHead.response_status == "S")
                {                    
                    _code = SissPayReturnCode.SUCCESS.ToString();
                }                 
                else
                {                    
                    _code = SissPayReturnCode.FAIL.ToString();
                    _msg = rspHead.response_msg;
                }

                if (rspBody != null)
                {
                    /*
                    INITIAL 初始化
PAIED 交易成功
WAITPAY 等待支付
REFUNDED 部分退款
REFUNDALL 全部退款
CLOSED 订单关闭 
                    */

                    if (rspBody.order_status == "REFUNDALL" || rspBody.order_status == "REFUNDED")
                    {
                        lblRefundStatus.Text = "退款成功";
                        _refund_status = SissPayTradeStatus.TRADE_SUCCESS.ToString();
                    }
                    else
                    {
                        lblRefundStatus.Text = "退款失败";
                        _refund_status = SissPayTradeStatus.TRADE_FAIL.ToString();
                    }
                }

                #endregion


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
