﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace CommonUtils
{
    public static class HashUtil
    {

        public static string GetMd5(string src)
        {
            MD5 md =   MD5.Create();
            byte[] bytes = Encoding.UTF8.GetBytes(src);
            byte[] buffer2 = md.ComputeHash(bytes);
            string str = "";
            for (int i = 0; i < buffer2.Length; i++)
            {
                str = str + buffer2[i].ToString("x2");
            }
            return str;

        }

        public static IDictionary<string, string> ModelToDic<T1>(T1 cfgItem)
        {
            IDictionary<string, string> sdCfgItem = new Dictionary<string, string>();

            System.Reflection.PropertyInfo[] cfgItemProperties = cfgItem.GetType().GetProperties(System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public);
            foreach (System.Reflection.PropertyInfo item in cfgItemProperties)
            {
                string name = item.Name;
                object value = item.GetValue(cfgItem, null);
                if (value != null && (item.PropertyType.IsValueType || item.PropertyType.Name.StartsWith("String")) && !string.IsNullOrWhiteSpace(value.ToString()))
                {
                    sdCfgItem.Add(name, value.ToString());
                }
            }

            return sdCfgItem;
        }

        public static IDictionary<string, string> AsciiDictionary(IDictionary<string, string> sArray)
        {
            IDictionary<string, string> asciiDic = new Dictionary<string, string>();
            string[] arrKeys = sArray.Keys.ToArray();
            Array.Sort(arrKeys, string.CompareOrdinal);
            foreach (var key in arrKeys)
            {
                string value = sArray[key];
                asciiDic.Add(key, value);
            }
            return asciiDic;
        }

        public static string BuildQueryString(IDictionary<string, string> sArray)
        {

            //拼接 K=V&A=B&c=1 这种URL

            StringBuilder sc = new StringBuilder();

            foreach (var item in sArray)
            {
                string name = item.Key;
                string value = item.Value;
                if (!string.IsNullOrWhiteSpace(value))
                {
                    sc.AppendFormat("{0}={1}&", name, value);
                }

            }

            string fnlStr = sc.ToString();
            fnlStr = fnlStr.TrimEnd('&');

            return fnlStr;
        }

    }
}
