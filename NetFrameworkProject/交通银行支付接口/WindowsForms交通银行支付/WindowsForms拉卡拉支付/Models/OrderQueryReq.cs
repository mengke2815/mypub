﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOCOM.Models
{
    public class OrderQueryReq
    {

        /// <summary>
        /// 交易场景，支付交易上送的交易场景，如B2C-API-DISPLAYCODE等
        /// </summary>
        public string tran_scene { get; set; }

        /// <summary>
        /// 商户编号
        /// </summary>
        public string mer_ptc_id { get; set; }
        /// <summary>
        /// 原交易商户侧交易日期 yyyyMMdd
        /// </summary>
        public string mer_trade_date { get; set; }
        /// <summary>
        /// 商户交易编号，商户自定义的订单号，当日不可重复
        /// </summary>
        public string pay_mer_tran_no { get; set; }

        public Require_Fields[] require_fields { get; set; }
    }
}
