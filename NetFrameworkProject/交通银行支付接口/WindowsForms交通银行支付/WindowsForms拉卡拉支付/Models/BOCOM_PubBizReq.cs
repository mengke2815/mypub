﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOCOM.Models
{
    /// <summary>
    /// 业务主体
    /// </summary>
    public class BOCOM_PubBizReq<T>
    {
        public BOCOM_PubBizReq()
        {
            req_head = new req_head();            
        }
        public req_head req_head { get; set; }


        public T req_body { get; set; }

    }
}
