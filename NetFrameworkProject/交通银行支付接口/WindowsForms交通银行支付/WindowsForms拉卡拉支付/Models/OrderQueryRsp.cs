﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOCOM.Models
{
    public class OrderQueryRsp
    {
        public string pay_mer_tran_no { get; set; }
        public string total_amount { get; set; }
        public string trd_dsct_amount { get; set; }

        /// <summary>
        /// PROCESS 处理中        SUCCESS 交易成功 FAILURE 交易失败
        /// </summary>
        public string tran_state { get; set; }
       
        public string buyer_pay_amount { get; set; }
        public string sys_order_no { get; set; }

        /// <summary>
        /// INITIAL 初始化        PAIED 交易成功        WAITPAY 等待支付 REFUNDED 部分退款 REFUNDALL 全部退款 CLOSED订单关闭
        /// </summary>
        public string order_status { get; set; }

        public require_values require_values { get; set; }

    }
}
