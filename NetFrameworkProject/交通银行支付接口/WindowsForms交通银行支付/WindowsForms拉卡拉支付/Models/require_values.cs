﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOCOM.Models
{

    public class require_values
    {
        public string bank_tran_no { get; set; }
        public object open_id { get; set; }
        public string third_party { get; set; }
        public object sub_openid { get; set; }
        public string third_party_tran_no { get; set; }
    }

}
