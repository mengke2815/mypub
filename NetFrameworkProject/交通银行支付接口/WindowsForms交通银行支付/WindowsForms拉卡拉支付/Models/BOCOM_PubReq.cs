﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOCOM.Models
{
    /// <summary>
    /// 报文主体
    /// </summary>
    public class BOCOM_PubReq
    {
        public BOCOM_PubReq()
        {
            fmt_type = "json";
            charset = "UTF-8";
            msg_id = Guid.NewGuid().ToString("N");
            timestamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        }
         
        public string app_id { get; set; }

        public string charset { get; set; }
        public string fmt_type { get; set; }

        public string msg_id { get; set; }

        public string timestamp { get; set; }
        public string biz_content { get; set; }
        public string sign { get; set; }



    }
}
