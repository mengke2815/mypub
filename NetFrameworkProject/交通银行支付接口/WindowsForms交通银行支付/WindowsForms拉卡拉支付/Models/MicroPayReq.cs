﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOCOM.Models
{

    public class MicroPayReq
    {
        public string valid_period { get; set; }
        public string scan_code_text { get; set; }
        public string terminal_info { get; set; }
        public string partner_id { get; set; }
        public Require_Fields[] require_fields { get; set; }
        public string currency { get; set; }
        public string tran_scene { get; set; }
        public string ip { get; set; }
         
        public string mer_ptc_id { get; set; }
        public string mer_trade_time { get; set; }
        public string mer_trade_date { get; set; }
        public string pay_mer_tran_no { get; set; }
        public string total_amount { get; set; }
        public string location { get; set; }
    }

    

    



}
