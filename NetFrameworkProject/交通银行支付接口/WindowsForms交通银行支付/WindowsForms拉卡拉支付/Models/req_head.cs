﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOCOM.Models
{

    public class req_head
    {

        public req_head()
        {
            trans_time = DateTime.Now.ToString("yyyyMMddHHmmss");
            version = "V-1.0";
        }
        public string trans_time { get; set; }
        public string version { get; set; }
    }


}
