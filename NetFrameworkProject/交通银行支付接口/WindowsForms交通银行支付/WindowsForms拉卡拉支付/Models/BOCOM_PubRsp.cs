﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOCOM.Models
{
    /// <summary>
    /// 响应报文
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public  class BOCOM_PubRsp <T>
    {        
         
        public string sign { get; set; }
        
        public rsp_biz_content<T> rsp_biz_content { get; set; }

        
    }
}
