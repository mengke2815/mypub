﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MFE88.Models
{
    public class MFE88_PubRsp
    {
        /// <summary>
        /// 000000
        /// </summary>
        public string code { get; set; }
        public string message { get; set; }
        public string trace { get; set; }

        public string data { get; set; }

    }
}
