﻿using CommonUtils;
using MFE88.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Windows.Forms;

namespace MFE88TermBind
{
    public partial class Form1 : Form
    {
        System.Timers.Timer _tmrRemeber;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            RemForm.ReadRememberForm(this, null);

            _tmrRemeber = new System.Timers.Timer();
            _tmrRemeber.Elapsed += new System.Timers.ElapsedEventHandler(_tmrRemeber_Elapsed);
            _tmrRemeber.Interval = 10 * 1000;
            _tmrRemeber.Start();

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
            ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(CheckValidationResult);

            cbxChooseUrl.SelectedIndex = 0;
        }

        void _tmrRemeber_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                RemForm.RememberForm(this, null);
            }
            catch (Exception ex)
            {
                //MessageBox.Show("_tmrRemeber_Elapsed" + ex.Message);
            }
        }

        private static bool CheckValidationResult(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors errors)
        {
            return true;
        }

        private void btnTermBind_Click(object sender, EventArgs e)
        {
            try
            {
                JsonSerializerSettings jsetting = new JsonSerializerSettings();
                jsetting.DefaultValueHandling = DefaultValueHandling.Ignore;

                if (string.IsNullOrWhiteSpace(txtMchNo.Text))
                {
                    MessageBox.Show("txtMchNo 不能为空！");
                    return;
                }
                if (string.IsNullOrWhiteSpace(txtMchName.Text))
                {
                    MessageBox.Show("txtMchName 不能为空！");
                    return;
                }
                 

                string _TermDir = "已报备终端";
                string _TermFileName = "商户号" + txtMchNo.Text + "已报备终端.txt";
                string appPath = Application.StartupPath;
                string fileFullName = Path.Combine(appPath, _TermDir); //文件夹
                if (!Directory.Exists(fileFullName))
                {
                    Directory.CreateDirectory(fileFullName);
                }
                fileFullName = Path.Combine(fileFullName, _TermFileName);//文件全路径

                #region 检查有没有报备过
                if (File.Exists(fileFullName))
                {
                    string fileCon = File.ReadAllText(fileFullName);
                    if (!string.IsNullOrWhiteSpace(fileCon) && fileCon.Length >= 8)
                    {
                        try
                        {
                            ProcessStartInfo startInfo = new ProcessStartInfo();
                            startInfo.FileName = "notepad.exe";
                            startInfo.Arguments = fileFullName;
                            Process.Start(startInfo);

                            string tmpTermNO = fileCon;
                            if (fileCon.Contains(";"))
                            {
                                tmpTermNO = fileCon.Substring(0, fileCon.IndexOf(';'));
                            }
                            txtTermNo.Text = tmpTermNO;
                        }
                        catch (Exception exOpen)
                        {
                        }

                        string tipMsg = "已报备过终端号，是否继续报备？（不建议重复报备）,已报备的终端：" + fileCon;
                        DialogResult dr = MessageBox.Show(tipMsg, "OO", MessageBoxButtons.YesNo
                            , MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                        if (dr == DialogResult.No)
                        {
                            return;
                        }
                    }
                }
                #endregion

                TermBindReq req = new TermBindReq();
                req.merchantNo = txtMchNo.Text.Trim();
                req.merchantName = txtMchName.Text.Trim();
                req.terminalType = "11";
                //req.terminalNo=txtTermNo.Text.Trim();
                req.reportType = "00";

                string dataJson = JsonConvert.SerializeObject(req, jsetting);
                GLog.WLog("未加密：\r\n" + dataJson);
                //RSA加密
                var paramStr = RsaEncryptUtil.RSAEncrypt(dataJson, "UTF-8", txtPubCert.Text);
                //SHA256withRSA
                #region 签名

                //获取私钥对象
                var rsaPri = RsaUtil.LoadPrivateKey(txtPrivateKey.Text, "PKCS1");
                //待签名字符串 转为byte 数组
                byte[] byToSign = Encoding.UTF8.GetBytes(paramStr); // 编码要和其它语言一致，一般是：UTF8 
                byte[] bySigned = rsaPri.SignData(byToSign, "SHA256");//SHA256，对应JAVA，SHA256withRSA,签名结果是 byte 数组
                string strSigned = Convert.ToBase64String(bySigned);//将byte 数组转为字符串，方便传输，一般是base64字符串，其它类型需和对方协商
                //Console.WriteLine("签名值：" + strSigned);
                GLog.WLog("签名值：" + strSigned);


                #endregion
                MFE88_PubReq pubReq = new MFE88_PubReq();
                pubReq.param = paramStr;

                string paramJson = JsonConvert.SerializeObject(pubReq, jsetting);

                #region 准备HEADER

                Dictionary<string, string> dicHeaders = new Dictionary<string, string>();
                dicHeaders.Add("X-Security", "CFCA");
                dicHeaders.Add("X-Agency", txtAgency.Text);
                dicHeaders.Add("X-Sign", strSigned);
                dicHeaders.Add("X-Time", DateTime.Now.ToString("R"));
                dicHeaders.Add("X-Trace", Guid.NewGuid().ToString("N"));
                string headerLogJson = JsonConvert.SerializeObject(dicHeaders, jsetting);
                GLog.WLog("headerLogJson:" + headerLogJson);

                #endregion


                //报文主体

                string bodyJson = JsonConvert.SerializeObject(pubReq, jsetting);

                string suffix = "/terminal-report/api/open/terminal-report";

                //报备和进件不是同一网关
                string url = txtMchUrl.Text + suffix;
                GLog.WLog("url:" + url);

                GLog.WLog("请求报文:" + bodyJson);
                Dictionary<string, string> dicRspHeaders = new Dictionary<string, string>();
                var rstStr = HttpUtil.HttpPostJson(url, bodyJson, dicHeaders, 30, dicRspHeaders);
                GLog.WLog("响应报文:" + rstStr);

                //txtRst.Text = rstStr;

                string _code = "";
                string _msg = "";



                MFE88_PubRsp rspM = JsonConvert.DeserializeObject<MFE88_PubRsp>(rstStr);

                string decryptedStr = "";
                if (!string.IsNullOrWhiteSpace(rspM.data))
                {
                    //解密数据
                    decryptedStr = RsaEncryptUtil.RSADecrypt(rspM.data, "UTF-8", txtPrivateKey.Text, "PKCS1");
                    GLog.WLog("data解密后：\r\n" + decryptedStr);

                }

                if (rspM.code == "000000")
                {
                    TermBindRsp rspA = JsonConvert.DeserializeObject<TermBindRsp>(decryptedStr);
                    txtTermNo.Text = rspA.terminalNo;
                    //把终端号写入到本地,terminalNo.
                    string terminalNo = rspA.terminalNo + ";\r\n";

                    File.AppendAllText(fileFullName, terminalNo);

                    MessageBox.Show("成功");
                }
                else
                {
                    //_code = SissPayReturnCode.FAIL.ToString();
                    _msg = rspM.message;
                    MessageBox.Show("失败");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void cbxChooseUrl_SelectedIndexChanged(object sender, EventArgs e)
        {
            //MessageBox.Show(cbxChooseUrl.Text);
            try
            {
                string curText = cbxChooseUrl.Text;
                if (curText == "正式环境")
                {
                    txtMchUrl.Text = "https://scene.mfe88.com";
                }
                else {
                    txtMchUrl.Text = "https://test-scene.mfe88.com";

                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }
    }
}
