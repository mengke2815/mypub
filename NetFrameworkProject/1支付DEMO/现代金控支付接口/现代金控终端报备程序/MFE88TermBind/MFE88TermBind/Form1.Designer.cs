﻿namespace MFE88TermBind
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.btnTermBind = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.txtMchName = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtTermNo = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtMchNo = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.txtMchUrl = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtAgency = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtMainUrl = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtPrivateKey = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtPubCert = new System.Windows.Forms.TextBox();
            this.cbxChooseUrl = new System.Windows.Forms.ComboBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(311, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(125, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "现代金控终端报备程序";
            // 
            // btnTermBind
            // 
            this.btnTermBind.Location = new System.Drawing.Point(115, 518);
            this.btnTermBind.Name = "btnTermBind";
            this.btnTermBind.Size = new System.Drawing.Size(75, 23);
            this.btnTermBind.TabIndex = 7;
            this.btnTermBind.Text = "终端报备";
            this.btnTermBind.UseVisualStyleBackColor = true;
            this.btnTermBind.Click += new System.EventHandler(this.btnTermBind_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(44, 432);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(53, 12);
            this.label9.TabIndex = 58;
            this.label9.Text = "商户名称";
            // 
            // txtMchName
            // 
            this.txtMchName.Location = new System.Drawing.Point(115, 429);
            this.txtMchName.Name = "txtMchName";
            this.txtMchName.Size = new System.Drawing.Size(219, 21);
            this.txtMchName.TabIndex = 3;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(44, 622);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 12);
            this.label7.TabIndex = 56;
            this.label7.Text = "终端号";
            // 
            // txtTermNo
            // 
            this.txtTermNo.Location = new System.Drawing.Point(115, 619);
            this.txtTermNo.Name = "txtTermNo";
            this.txtTermNo.Size = new System.Drawing.Size(219, 21);
            this.txtTermNo.TabIndex = 8;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(44, 394);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 12);
            this.label6.TabIndex = 54;
            this.label6.Text = "商户号";
            // 
            // txtMchNo
            // 
            this.txtMchNo.Location = new System.Drawing.Point(115, 391);
            this.txtMchNo.Name = "txtMchNo";
            this.txtMchNo.Size = new System.Drawing.Size(219, 21);
            this.txtMchNo.TabIndex = 2;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(50, 294);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(47, 12);
            this.label23.TabIndex = 64;
            this.label23.Text = "进件Url";
            // 
            // txtMchUrl
            // 
            this.txtMchUrl.Location = new System.Drawing.Point(124, 291);
            this.txtMchUrl.Name = "txtMchUrl";
            this.txtMchUrl.Size = new System.Drawing.Size(247, 21);
            this.txtMchUrl.TabIndex = 0;
            this.txtMchUrl.Text = "https://test-scene.mfe88.com";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(44, 368);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(65, 12);
            this.label21.TabIndex = 62;
            this.label21.Text = "服务商编号";
            // 
            // txtAgency
            // 
            this.txtAgency.Location = new System.Drawing.Point(115, 364);
            this.txtAgency.Name = "txtAgency";
            this.txtAgency.Size = new System.Drawing.Size(219, 21);
            this.txtAgency.TabIndex = 1;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(536, 51);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(47, 12);
            this.label11.TabIndex = 60;
            this.label11.Text = "mainUrl";
            // 
            // txtMainUrl
            // 
            this.txtMainUrl.Location = new System.Drawing.Point(610, 48);
            this.txtMainUrl.Name = "txtMainUrl";
            this.txtMainUrl.Size = new System.Drawing.Size(247, 21);
            this.txtMainUrl.TabIndex = 59;
            this.txtMainUrl.Text = "https://test-dream.mfe88.com";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtPrivateKey);
            this.groupBox1.Controls.Add(this.label22);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Location = new System.Drawing.Point(415, 144);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(577, 280);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "商户或服务商或ISV";
            // 
            // txtPrivateKey
            // 
            this.txtPrivateKey.Location = new System.Drawing.Point(10, 42);
            this.txtPrivateKey.Multiline = true;
            this.txtPrivateKey.Name = "txtPrivateKey";
            this.txtPrivateKey.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtPrivateKey.Size = new System.Drawing.Size(561, 111);
            this.txtPrivateKey.TabIndex = 5;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(6, 156);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(161, 12);
            this.label22.TabIndex = 49;
            this.label22.Text = "接入方公钥证书（加密报文）";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(11, 20);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(101, 12);
            this.label10.TabIndex = 20;
            this.label10.Text = "接入方私钥(签名)";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(6, 185);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(565, 89);
            this.textBox1.TabIndex = 48;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(413, 438);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(125, 12);
            this.label13.TabIndex = 67;
            this.label13.Text = "平台公钥（验证签名）";
            // 
            // txtPubCert
            // 
            this.txtPubCert.Location = new System.Drawing.Point(415, 453);
            this.txtPubCert.Multiline = true;
            this.txtPubCert.Name = "txtPubCert";
            this.txtPubCert.Size = new System.Drawing.Size(565, 99);
            this.txtPubCert.TabIndex = 6;
            // 
            // cbxChooseUrl
            // 
            this.cbxChooseUrl.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxChooseUrl.Font = new System.Drawing.Font("宋体", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cbxChooseUrl.FormattingEnabled = true;
            this.cbxChooseUrl.Items.AddRange(new object[] {
            "正式环境",
            "测试环境"});
            this.cbxChooseUrl.Location = new System.Drawing.Point(124, 186);
            this.cbxChooseUrl.Name = "cbxChooseUrl";
            this.cbxChooseUrl.Size = new System.Drawing.Size(148, 23);
            this.cbxChooseUrl.TabIndex = 68;
            this.cbxChooseUrl.SelectedIndexChanged += new System.EventHandler(this.cbxChooseUrl_SelectedIndexChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1018, 753);
            this.Controls.Add(this.cbxChooseUrl);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.txtPubCert);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.txtMchUrl);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.txtAgency);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtMainUrl);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtMchName);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtTermNo);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtMchNo);
            this.Controls.Add(this.btnTermBind);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "现代金控终端报备程序";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnTermBind;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtMchName;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtTermNo;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtMchNo;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtMchUrl;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtAgency;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtMainUrl;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtPrivateKey;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtPubCert;
        private System.Windows.Forms.ComboBox cbxChooseUrl;
    }
}

