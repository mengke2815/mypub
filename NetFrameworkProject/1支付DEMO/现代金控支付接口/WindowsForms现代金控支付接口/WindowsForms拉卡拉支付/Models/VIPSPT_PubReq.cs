﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MFE88.Models
{
    /// <summary>
    /// 报文主体
    /// </summary>
    public class VIPSPT_PubReq<T>
    {
         

        /// <summary>
        /// 开发者账号
        /// </summary>
        public string appid { get; set; }

        /// <summary>
        /// 1573705768630
        /// </summary>
        public string timeStamp { get; set; }


        /// <summary>
        /// 经过加密的业务数据（base64）
        /// </summary>
        public T data { get; set; }
        /// <summary>
        /// 报文签名（base64）
        /// </summary>
        public string sign { get; set; }



    }
}
