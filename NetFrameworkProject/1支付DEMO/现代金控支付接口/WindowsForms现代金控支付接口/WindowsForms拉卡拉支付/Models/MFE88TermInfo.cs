﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MFE88.Models
{
    public class MFE88TermInfo
    {
        /// <summary>
        /// 11：条码支付辅助受理终端
        /// </summary>
        public string terminalType { get; set; }

        /// <summary>
        /// 机具编号
        /// </summary>
        public string terminalNo { get; set; }

    }
}
