﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MFE88.Models
{
     


    public class MicroPayRsp
    {
        public string merchantNo { get; set; }
        public string merOrderNo { get; set; }
        public string amount { get; set; }
        public string curType { get; set; }
        public string sendupTime { get; set; }
        public string version { get; set; }
        public string signType { get; set; }
        public object attach { get; set; }
        public object commodityName { get; set; }
        public string orderNo { get; set; }
        public string tradeStatus { get; set; }
        public string tradeDesc { get; set; }
        public string finishTime { get; set; }
        public object codeUrl { get; set; }
        public object receiptAmount { get; set; }
        public object transactionNo { get; set; }
        public object authCode { get; set; }
        public string channelOrderNo { get; set; }
        public string discountAmount { get; set; }
        public string discount { get; set; }
        public object payInfo { get; set; }
        public object cardType { get; set; }
    }

}
