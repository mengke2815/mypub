﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MFE88.Models
{
    public class RefundReq
    {


        public string merchantNo { get; set; }
        public string merOrderNo { get; set; }
        public string amount { get; set; }
        public string curType { get; set; }
        public string sendupTime { get; set; }
        public string version { get; set; }
        public string signType { get; set; }
        public string subProductCode { get; set; }
        public string sourceIp { get; set; }
        public string originalOrderNo { get; set; }
        public string refundDesc { get; set; }
        public string attach { get; set; }

    }
}
