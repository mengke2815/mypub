﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MFE88.Models
{

    public class VIPSPT_PubRsp<T>
    {
        public int ret { get; set; }

        public string msg { get; set; }


        public T data { get; set; }

        public string totalPage { get; set; }


    }
}
