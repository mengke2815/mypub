﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MFE88.Models
{

    public class RefundRsp
    {


        public string merchantNo { get; set; }
        public string merOrderNo { get; set; }
        public string amount { get; set; }
        public string curType { get; set; }
        public string sendupTime { get; set; }
        public string version { get; set; }
        public string signType { get; set; }
        public string orderNo { get; set; }
        public string tradeStatus { get; set; }
        public string tradeDesc { get; set; }
        public string finishTime { get; set; }
        public string attach { get; set; }


    }

}
