﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MFE88.Models
{
    public class TermBindRsp
    {
        public string merchantNo { get; set; }
        public string merchantName { get; set; }
        public string agencyNo { get; set; }

        public string terminalType { get; set; }
        public string reportType { get; set; }
        public string terminalNo { get; set; }
    }
}
