﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MFE88.Models
{

    public class MicroPayReq
    {

        public string merchantNo { get; set; }

        public string merOrderNo { get; set; }
        /// <summary>
        /// 上送交易金额（单位：分）
        /// </summary>
        public string amount { get; set; }

        public string curType { get; set; }
        public string sendupTime { get; set; }
        public string version { get; set; }
        public string signType { get; set; }
        /// <summary>
        /// 在金控开通的子产品
        /// </summary>
        public string subProductCode { get; set; }
        /// <summary>
        /// 订单有效期,必须为整数单位为分钟
        /// </summary>
        public string timeExpress { get; set; }

        /// <summary>
        /// 商品名称,string(128)
        /// </summary>
        public string commodityName { get; set; }

        public string orderSource { get; set; }

        public string sourceIp { get; set; }
        
        

        public string authCode { get; set; }
        /// <summary>
        /// 分账标识,00 不分账 01 分账
        /// </summary>
        public string splitFlag { get; set; }

        /// <summary>
        /// 优惠订单标识：0-非优惠 1-优惠
        /// </summary>
        public string discountFlag { get; set; }
        //MFE88TermInfo

        public MFE88TermInfo terminalInfo { get; set; }

    }







}
