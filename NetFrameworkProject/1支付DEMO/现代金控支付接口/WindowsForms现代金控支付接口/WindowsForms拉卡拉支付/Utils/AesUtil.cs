﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace CommonUtils
{
    public static  class AesUtil
    {
        public static string EncryptCBC(string encryptStr, string key, string iv)
        {


            byte[] byteIV = Encoding.UTF8.GetBytes(iv);

            byte[] byteKEY = Encoding.UTF8.GetBytes(key);

            byte[] byteContent = Encoding.UTF8.GetBytes(encryptStr);

            var _aes = new RijndaelManaged();


            _aes.Padding = PaddingMode.PKCS7;
            _aes.Mode = CipherMode.CBC;

            var _crypto = _aes.CreateEncryptor(byteKEY, byteIV);
            byte[] encrypted = _crypto.TransformFinalBlock(byteContent, 0, byteContent.Length);

            _crypto.Dispose();

            return System.Convert.ToBase64String(encrypted);
        }

        public static string DecryptCBC(string decryptStr, string key, string iv)
        {
            byte[] byteKEY = Encoding.UTF8.GetBytes(key);

            byte[] byteIV = Encoding.UTF8.GetBytes(iv);

            byte[] byteDecrypt = System.Convert.FromBase64String(decryptStr);

            var _aes = new RijndaelManaged();

            _aes.Padding = PaddingMode.PKCS7;
            _aes.Mode = CipherMode.CBC;

            var _crypto = _aes.CreateDecryptor(byteKEY, byteIV);
            byte[] decrypted = _crypto.TransformFinalBlock(
                byteDecrypt, 0, byteDecrypt.Length);

            _crypto.Dispose();

            return Encoding.UTF8.GetString(decrypted);
        }
    }
}
