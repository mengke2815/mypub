﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonUtils
{
    public static class GLog
    {
        static object _lock_log = new object();

        public static void WLog(string strLog)
        {
            lock (_lock_log)
            {
                try
                {
                    string apppath = AppDomain.CurrentDomain.BaseDirectory; ;
                    string strPath = apppath + "\\Logs\\Log_" + DateTime.Today.ToString("yyyyMMdd") + ".txt";
                    string directory;
                    directory = strPath.Substring(0, strPath.LastIndexOf("\\"));
                    if (!Directory.Exists(directory))
                    {
                        Directory.CreateDirectory(directory);
                    }

                    FileStream fs = new FileStream(strPath, FileMode.Append, FileAccess.Write);
                    StreamWriter sw = new StreamWriter(fs);

                    sw.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " " + strLog);

                    sw.Close();
                    fs.Close();
                }
                catch //(Exception e)
                {
                    //return 0; 暂不处理.
                }
            }
        }
    }
}
