﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonUtils
{
    public static  class VString
    {
        /// <summary>
        /// 字符串转换为Int
        /// </summary>
        /// <param name="obj">输入字符串</param>
        /// <returns></returns> 
        public static int TryInt(object obj)
        {
            return TryInt(obj, 0);
        }

        /// <summary>
        /// 字符串转换为Int
        /// </summary>
        /// <param name="obj">输入字符串</param>
        /// <param name="defInt">默认值</param>
        /// <returns></returns>
        public static int TryInt(object obj, int defInt)
        {
            int inttemp = 0;
            if (obj == null || obj == DBNull.Value || obj.Equals(string.Empty))
                return defInt;
            obj = obj.ToString().Replace("￥", "").Replace("$", "");
            if (obj.ToString().Contains("."))
            {
                obj = float.Parse(obj.ToString());
            }
            if (Int32.TryParse(obj.ToString(), out inttemp))
                return inttemp;
            else
                return defInt;
        }


        /// <summary>
        /// 字符串转换为decimal 
        /// </summary>
        /// <param name="obj">输入字符串</param>
        /// <returns></returns>
        public static decimal TryDec(object obj)
        {
            return TryDec(obj, 0M);
        }

        /// <summary>
        /// 字符串转换为decimal 
        /// </summary>
        /// <param name="obj">输入字符串</param>
        /// <param name="defInt">默认值</param>
        /// <returns></returns>
        public static decimal TryDec(object obj, decimal defDec)
        {
            decimal dectemp = 0M;
            if (obj == null || obj == DBNull.Value || obj.Equals(string.Empty))
                return defDec;
            obj = obj.ToString().Replace("￥", "").Replace("$", "");
            if (decimal.TryParse(obj.ToString(), out dectemp))
                return dectemp;
            else
                return defDec;
        }

    }
}
