﻿using CommonUtils;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MFE88.Models;
using Aop.Api.Util;
using System.Net.Security;
using System.Net;
using Common.Models2;
using System.Security.Cryptography;
using System.IO;
using System.Diagnostics;

namespace WindowsForms拉卡拉支付
{
    public partial class Form1 : Form
    {
        System.Timers.Timer _tmrRemeber;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            RemForm.ReadRememberForm(this, null);

            _tmrRemeber = new System.Timers.Timer();
            _tmrRemeber.Elapsed += new System.Timers.ElapsedEventHandler(_tmrRemeber_Elapsed);
            _tmrRemeber.Interval = 10 * 1000;
            _tmrRemeber.Start();

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
            ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(CheckValidationResult);

        }


        private static bool CheckValidationResult(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors errors)
        {
            return true;
        }

        void _tmrRemeber_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                RemForm.RememberForm(this, null);
            }
            catch (Exception ex)
            {
                //MessageBox.Show("_tmrRemeber_Elapsed" + ex.Message);
            }
        }

        private void btnReadCrt_Click(object sender, EventArgs e)
        {
            try
            {
                if (ofdCrt.ShowDialog() == DialogResult.OK)
                {
                    X509Certificate2 cert = new X509Certificate2(ofdCrt.FileName);
                    //txtCertSN.Text = cert.SerialNumber;
                    //txtCertSN10.Text = cert.SerialNumber.ToLower();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        private void btnMicroPay_Click(object sender, EventArgs e)
        {
            try
            {

                string tmpDate = DateTime.Now.ToString("R");

                JsonSerializerSettings jsetting = new JsonSerializerSettings();
                jsetting.DefaultValueHandling = DefaultValueHandling.Ignore;

                if (decimal.Parse(txtAmt.Text) >= 1M)
                {
                    DialogResult dr = MessageBox.Show("金额过大，是否继续？", "OO", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dr == DialogResult.No)
                    {
                        return;
                    }
                }
                if (string.IsNullOrWhiteSpace(txtAuthCode.Text))
                {
                    MessageBox.Show("付款码不能为空！");
                    return;
                }
                string amount = ((int)(decimal.Parse(txtAmt.Text) * 100)).ToString();


                //初始化付款完成时间。
                txtPayTime.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");


                txtOutTradeNo.Text = "S000000000000" + DateTime.Now.ToString("yyyyMMddHHmmssfff");
                /*
110101101	聚合-微信-B扫C支付
110102201	聚合-支付宝-B扫C支付
110103301	银联二维码-B扫C支付
                 */
                string subProductCode = "110101101";
                if (txtAuthCode.Text.StartsWith("2") || txtAuthCode.Text.StartsWith("3"))
                {
                    subProductCode = "110102201";
                }
                else if (txtAuthCode.Text.StartsWith("6"))
                {
                    subProductCode = "110103301";
                }

                MFE88TermInfo termInfo = new MFE88TermInfo();
                termInfo.terminalType = "11";
                termInfo.terminalNo = txtTermNo.Text;


                MicroPayReq req = new MicroPayReq();
                req.merchantNo = txtMchNo.Text.Trim();
                req.merOrderNo = txtOutTradeNo.Text;
                req.amount = amount;
                req.curType = "CNY";
                req.sendupTime = DateTime.Now.ToString("yyyyMMddHHmmss");
                req.version = "1.0";
                req.signType = "3";
                req.subProductCode = subProductCode;
                req.timeExpress = "1";
                req.commodityName = "条码支付-总部";//128

                req.orderSource = "API";
                req.sourceIp = "116.30.109.119";
                req.authCode = txtAuthCode.Text.Trim();
                req.discountFlag = "1";

                req.terminalInfo = termInfo;

                string dataJson = JsonConvert.SerializeObject(req, jsetting);
                GLog.WLog("未加密：\r\n" + dataJson);
                //RSA加密
                var paramStr = RsaEncryptUtil.RSAEncrypt(dataJson, "UTF-8", txtPubCert.Text);
                //SHA256withRSA
                #region 签名

                //获取私钥对象
                var rsaPri = RsaUtil.LoadPrivateKey(txtPrivateKey.Text, "PKCS1");
                //待签名字符串 转为byte 数组
                byte[] byToSign = Encoding.UTF8.GetBytes(paramStr); // 编码要和其它语言一致，一般是：UTF8 
                byte[] bySigned = rsaPri.SignData(byToSign, "SHA256");//SHA256，对应JAVA，SHA256withRSA,签名结果是 byte 数组
                string strSigned = Convert.ToBase64String(bySigned);//将byte 数组转为字符串，方便传输，一般是base64字符串，其它类型需和对方协商
                //Console.WriteLine("签名值：" + strSigned);
                GLog.WLog("签名值：" + strSigned);


                #endregion
                MFE88_PubReq pubReq = new MFE88_PubReq();
                pubReq.param = paramStr;

                string paramJson = JsonConvert.SerializeObject(pubReq, jsetting);

                #region 准备HEADER

                Dictionary<string, string> dicHeaders = new Dictionary<string, string>();
                dicHeaders.Add("X-Security", "CFCA");
                dicHeaders.Add("X-Agency", txtAgency.Text);
                dicHeaders.Add("X-Sign", strSigned);
                dicHeaders.Add("X-Time", DateTime.Now.ToString("R"));
                dicHeaders.Add("X-Trace", Guid.NewGuid().ToString("N"));
                string headerLogJson = JsonConvert.SerializeObject(dicHeaders, jsetting);
                GLog.WLog("headerLogJson:" + headerLogJson);

                #endregion


                //报文主体

                string bodyJson = JsonConvert.SerializeObject(pubReq, jsetting);

                string suffix = "/trade-connect/api/qrPayment";

                string url = txtMainUrl.Text + suffix;
                GLog.WLog("url:" + url);

                GLog.WLog("请求报文:" + bodyJson);
                Dictionary<string, string> dicRspHeaders = new Dictionary<string, string>();
                var rstStr = HttpUtil.HttpPostJson(url, bodyJson, dicHeaders, 30, dicRspHeaders);
                GLog.WLog("响应报文:" + rstStr);

                txtRst.Text = rstStr;

                string _code = "";
                string _msg = "";

                MFE88_PubRsp rspM = JsonConvert.DeserializeObject<MFE88_PubRsp>(rstStr);

                string decryptedStr = "";
                if (!string.IsNullOrWhiteSpace(rspM.data))
                {
                    //解密数据
                    decryptedStr = RsaEncryptUtil.RSADecrypt(rspM.data, "UTF-8", txtPrivateKey.Text, "PKCS1");
                    GLog.WLog("data解密后：\r\n" + decryptedStr);
                    txtRst.Text = decryptedStr;
                }

                if (rspM.code == "000000")
                {
                    var rspA = JsonConvert.DeserializeObject<MicroPayRsp>(decryptedStr);

                    if (rspA.tradeStatus == "02")
                    {
                        _code = SissPayReturnCode.SUCCESS.ToString();
                        lblPayStatus.Text = "成功";
                    }
                    else if (rspA.tradeStatus == "01" || rspA.tradeStatus == "99")
                    {
                        _code = SissPayReturnCode.WAIT_BUYER_PAY.ToString();
                        lblPayStatus.Text = "支付中";
                    }
                    else
                    {
                        _code = SissPayReturnCode.FAIL.ToString();
                        lblPayStatus.Text = "失败";
                    }

                    if (!string.IsNullOrWhiteSpace(rspA.channelOrderNo))
                    {
                        string trade_no = rspA.channelOrderNo;
                    }
                }
                else
                {
                    lblPayStatus.Text = "失败";
                    _code = SissPayReturnCode.FAIL.ToString();
                    _msg = rspM.message;
                    txtRst.Text = rstStr;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private static byte[] strToToHexByte(string hexString)
        {
            hexString = hexString.Replace(" ", "");
            if ((hexString.Length % 2) != 0)
                hexString += " ";
            byte[] returnBytes = new byte[hexString.Length / 2];
            for (int i = 0; i < returnBytes.Length; i++)
                returnBytes[i] = Convert.ToByte(hexString.Substring(i * 2, 2), 16);
            return returnBytes;
        }

        public static string AesEncryptCBC(string encryptStr, byte[] byteKEY, byte[] byteIV)
        {


            //byte[] byteIV = Encoding.UTF8.GetBytes(iv);

            //byte[] byteKEY = Encoding.UTF8.GetBytes(key);

            byte[] byteContent = Encoding.UTF8.GetBytes(encryptStr);

            var _aes = new RijndaelManaged();


            _aes.Padding = PaddingMode.PKCS7;
            _aes.Mode = CipherMode.CBC;

            var _crypto = _aes.CreateEncryptor(byteKEY, byteIV);
            byte[] encrypted = _crypto.TransformFinalBlock(byteContent, 0, byteContent.Length);

            _crypto.Dispose();

            return System.Convert.ToBase64String(encrypted);
        }

        public static string AesDecryptCBC(string decryptStr, byte[] byteKEY, byte[] byteIV)
        {
            //byte[] byteKEY = Encoding.UTF8.GetBytes(key);

            //byte[] byteIV = Encoding.UTF8.GetBytes(iv);

            byte[] byteDecrypt = System.Convert.FromBase64String(decryptStr);

            var _aes = new RijndaelManaged();

            _aes.Padding = PaddingMode.PKCS7;
            _aes.Mode = CipherMode.CBC;

            var _crypto = _aes.CreateDecryptor(byteKEY, byteIV);
            byte[] decrypted = _crypto.TransformFinalBlock(
                byteDecrypt, 0, byteDecrypt.Length);

            _crypto.Dispose();

            return Encoding.UTF8.GetString(decrypted);
        }

        //public string GetSign(BOCOM_PubReq cont, string suffix, string PrivateKey, string PrivateKeyForm)
        //{
        //    var dic1 = HashUtil.ModelToDic(cont);
        //    var dic2 = HashUtil.AsciiDictionary(dic1);//排序
        //    var toSignStr1 = HashUtil.BuildQueryString(dic2);//拼接

        //    string toSignStr2 = suffix + "?" + toSignStr1;//拼上URL后缀
        //    byte[] byToSign = Encoding.UTF8.GetBytes(toSignStr2);
        //    var rsa = RsaUtil.LoadPrivateKey(PrivateKey, PrivateKeyForm);
        //    byte[] byFnl = rsa.SignData(byToSign, "SHA256");
        //    string sign = Convert.ToBase64String(byFnl);

        //    return sign;
        //}

        /// <summary>
        /// 验证签名
        /// </summary>
        /// <param name="rspHeader">HTTP响应头</param>
        /// <param name="rspBody">响应报文</param>
        /// <param name="pubCert">公钥证书</param>
        /// <returns></returns>
        bool ValidSign(IDictionary<string, string> rspHeader, string rspBody, string pubCert)
        {
            //var dic = JsonConvert.DeserializeObject<BOCOM_PubRspSign>(rspBody);
            //string fnstr = rspBody.Substring(0, rspBody.LastIndexOf(",\"sign"));
            //fnstr = fnstr.Substring(fnstr.IndexOf("rsp_biz_content") + 17);
            //string rspSign = dic.sign;

            //byte[] byteCon = Encoding.UTF8.GetBytes(fnstr);

            //byte[] byteRspSign = Convert.FromBase64String(rspSign);
            //var rsaPub = RsaUtil.LoadPublicKey(pubCert);

            //bool bRst = rsaPub.VerifyData(byteCon, "SHA256", byteRspSign);

            return true;

        }

        private void txtAuthCode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnMicroPay_Click(sender, e);
            }
        }

        private void btnOrderQuery_Click(object sender, EventArgs e)
        {
            try
            {
                JsonSerializerSettings jsetting = new JsonSerializerSettings();
                jsetting.DefaultValueHandling = DefaultValueHandling.Ignore;

                string amount = ((int)(decimal.Parse(txtAmt.Text) * 100)).ToString();

                /*
110101101	聚合-微信-B扫C支付
110102201	聚合-支付宝-B扫C支付
110103301	银联二维码-B扫C支付
                 */
                string subProductCode = "110101101";
                if (txtAuthCode.Text.StartsWith("2") || txtAuthCode.Text.StartsWith("3"))
                {
                    subProductCode = "110102201";
                }
                else if (txtAuthCode.Text.StartsWith("6"))
                {
                    subProductCode = "110103301";
                }

                OrderQueryReq req = new OrderQueryReq();
                req.merchantNo = txtMchNo.Text.Trim();
                req.merOrderNo = txtOutTradeNo.Text;
                req.tradeDate = DateTime.Parse(txtPayTime.Text).ToString("yyyyMMdd");
                //req.amount = amount;
                req.curType = "CNY";
                req.sendupTime = DateTime.Now.ToString("yyyyMMddHHmmss");
                req.version = "1.0";
                req.signType = "3";
                req.subProductCode = subProductCode;

                string dataJson = JsonConvert.SerializeObject(req, jsetting);
                GLog.WLog("未加密：\r\n" + dataJson);
                //RSA加密
                var paramStr = RsaEncryptUtil.RSAEncrypt(dataJson, "UTF-8", txtPubCert.Text);
                //SHA256withRSA
                #region 签名

                //获取私钥对象
                var rsaPri = RsaUtil.LoadPrivateKey(txtPrivateKey.Text, "PKCS1");
                //待签名字符串 转为byte 数组
                byte[] byToSign = Encoding.UTF8.GetBytes(paramStr); // 编码要和其它语言一致，一般是：UTF8 
                byte[] bySigned = rsaPri.SignData(byToSign, "SHA256");//SHA256，对应JAVA，SHA256withRSA,签名结果是 byte 数组
                string strSigned = Convert.ToBase64String(bySigned);//将byte 数组转为字符串，方便传输，一般是base64字符串，其它类型需和对方协商
                //Console.WriteLine("签名值：" + strSigned);
                GLog.WLog("签名值：" + strSigned);

                #endregion
                MFE88_PubReq pubReq = new MFE88_PubReq();
                pubReq.param = paramStr;

                string paramJson = JsonConvert.SerializeObject(pubReq, jsetting);

                #region 准备HEADER

                Dictionary<string, string> dicHeaders = new Dictionary<string, string>();
                dicHeaders.Add("X-Security", "CFCA");
                dicHeaders.Add("X-Agency", txtAgency.Text);
                dicHeaders.Add("X-Sign", strSigned);
                dicHeaders.Add("X-Time", DateTime.Now.ToString("R"));
                dicHeaders.Add("X-Trace", Guid.NewGuid().ToString("N"));
                string headerLogJson = JsonConvert.SerializeObject(dicHeaders, jsetting);
                GLog.WLog("headerLogJson:" + headerLogJson);

                #endregion


                //报文主体

                string bodyJson = JsonConvert.SerializeObject(pubReq, jsetting);

                string suffix = "/trade-connect/api/qrQuery";

                string url = txtMainUrl.Text + suffix;
                GLog.WLog("url:" + url);

                GLog.WLog("请求报文:" + bodyJson);
                Dictionary<string, string> dicRspHeaders = new Dictionary<string, string>();
                var rstStr = HttpUtil.HttpPostJson(url, bodyJson, dicHeaders, 30, dicRspHeaders);
                GLog.WLog("响应报文:" + rstStr);

                txtRst.Text = rstStr;

                string _code = "";
                string _msg = "";


                MFE88_PubRsp rspM = JsonConvert.DeserializeObject<MFE88_PubRsp>(rstStr);

                string decryptedStr = "";
                if (!string.IsNullOrWhiteSpace(rspM.data))
                {
                    //解密数据
                    decryptedStr = RsaEncryptUtil.RSADecrypt(rspM.data, "UTF-8", txtPrivateKey.Text, "PKCS1");
                    GLog.WLog("data解密后：\r\n" + decryptedStr);
                    txtRst.Text = decryptedStr;
                }
                string _trade_status;
                if (rspM.code == "000000")
                {
                    var rspA = JsonConvert.DeserializeObject<OrderQueryRsp>(decryptedStr);
                    _code = SissPayReturnCode.SUCCESS.ToString();
                    /*
                     00-未支付 01-处理中 02-成功 03-失败 04-关单成功 99-状态未知
                     */
                    if (rspA.tradeStatus == "02")
                    {
                        _trade_status = SissPayTradeStatus.TRADE_SUCCESS.ToString();
                        lblPayStatus.Text = "成功";
                    }
                    else if (rspA.tradeStatus == "01" || rspA.tradeStatus == "99")
                    {
                        _trade_status = SissPayTradeStatus.WAIT_BUYER_PAY.ToString();
                        lblPayStatus.Text = "支付中";
                    }
                    else
                    {
                        _trade_status = SissPayTradeStatus.TRADE_FAIL.ToString();
                        lblPayStatus.Text = "失败";
                    }

                    if (!string.IsNullOrWhiteSpace(rspA.channelOrderNo))
                    {
                        string trade_no = rspA.channelOrderNo;
                    }

                    if (!string.IsNullOrEmpty(rspA.amount))
                    {
                        string total_amount = rspA.amount;
                    }

                    DateTime dtTimeEnd = DateTime.Now;
                    if (!string.IsNullOrWhiteSpace(rspA.finishTime))
                    {
                        try
                        {
                            dtTimeEnd = DateTime.ParseExact(rspA.finishTime, "yyyyMMddHHmmss", CultureInfo.InvariantCulture);
                        }
                        catch { }
                    }
                    string trade_time = dtTimeEnd.ToString("yyyy-MM-dd HH:mm:ss");
                }
                else
                {
                    lblPayStatus.Text = "失败";
                    _code = SissPayReturnCode.FAIL.ToString();
                    _msg = rspM.message;
                    txtRst.Text = rstStr;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }



        OrderQueryRsp QueryCore()
        {
            OrderQueryRsp orderQueryReturnModel = null;

            JsonSerializerSettings jsetting = new JsonSerializerSettings();
            jsetting.DefaultValueHandling = DefaultValueHandling.Ignore;

            string amount = ((int)(decimal.Parse(txtAmt.Text) * 100)).ToString();


            /*
110101101	聚合-微信-B扫C支付
110102201	聚合-支付宝-B扫C支付
110103301	银联二维码-B扫C支付
             */
            string subProductCode = "110101101";
            if (txtAuthCode.Text.StartsWith("2") || txtAuthCode.Text.StartsWith("3"))
            {
                subProductCode = "110102201";
            }
            else if (txtAuthCode.Text.StartsWith("6"))
            {
                subProductCode = "110103301";
            }

            OrderQueryReq req = new OrderQueryReq();
            req.merchantNo = txtMchNo.Text.Trim();
            req.merOrderNo = txtOutTradeNo.Text;
            req.tradeDate = DateTime.Parse(txtPayTime.Text).ToString("yyyyMMdd");
            //req.amount = amount;
            req.curType = "CNY";
            req.sendupTime = DateTime.Now.ToString("yyyyMMddHHmmss");
            req.version = "1.0";
            req.signType = "3";
            req.subProductCode = subProductCode;

            string dataJson = JsonConvert.SerializeObject(req, jsetting);
            GLog.WLog("未加密：\r\n" + dataJson);
            //RSA加密
            var paramStr = RsaEncryptUtil.RSAEncrypt(dataJson, "UTF-8", txtPubCert.Text);
            //SHA256withRSA
            #region 签名

            //获取私钥对象
            var rsaPri = RsaUtil.LoadPrivateKey(txtPrivateKey.Text, "PKCS1");
            //待签名字符串 转为byte 数组
            byte[] byToSign = Encoding.UTF8.GetBytes(paramStr); // 编码要和其它语言一致，一般是：UTF8 
            byte[] bySigned = rsaPri.SignData(byToSign, "SHA256");//SHA256，对应JAVA，SHA256withRSA,签名结果是 byte 数组
            string strSigned = Convert.ToBase64String(bySigned);//将byte 数组转为字符串，方便传输，一般是base64字符串，其它类型需和对方协商
                                                                //Console.WriteLine("签名值：" + strSigned);
            GLog.WLog("签名值：" + strSigned);

            #endregion
            MFE88_PubReq pubReq = new MFE88_PubReq();
            pubReq.param = paramStr;

            string paramJson = JsonConvert.SerializeObject(pubReq, jsetting);

            #region 准备HEADER

            Dictionary<string, string> dicHeaders = new Dictionary<string, string>();
            dicHeaders.Add("X-Security", "CFCA");
            dicHeaders.Add("X-Agency", txtAgency.Text);
            dicHeaders.Add("X-Sign", strSigned);
            dicHeaders.Add("X-Time", DateTime.Now.ToString("R"));
            dicHeaders.Add("X-Trace", Guid.NewGuid().ToString("N"));
            string headerLogJson = JsonConvert.SerializeObject(dicHeaders, jsetting);
            GLog.WLog("headerLogJson:" + headerLogJson);

            #endregion


            //报文主体

            string bodyJson = JsonConvert.SerializeObject(pubReq, jsetting);

            string suffix = "/trade-connect/api/qrQuery";

            string url = txtMainUrl.Text + suffix;
            GLog.WLog("url:" + url);

            GLog.WLog("请求报文:" + bodyJson);
            Dictionary<string, string> dicRspHeaders = new Dictionary<string, string>();
            var rstStr = HttpUtil.HttpPostJson(url, bodyJson, dicHeaders, 30, dicRspHeaders);
            GLog.WLog("响应报文:" + rstStr);

            txtRst.Text = rstStr;

            string _code = "";
            string _msg = "";


            MFE88_PubRsp rspM = JsonConvert.DeserializeObject<MFE88_PubRsp>(rstStr);

            string decryptedStr = "";
            if (!string.IsNullOrWhiteSpace(rspM.data))
            {
                //解密数据
                decryptedStr = RsaEncryptUtil.RSADecrypt(rspM.data, "UTF-8", txtPrivateKey.Text, "PKCS1");
                GLog.WLog("data解密后：\r\n" + decryptedStr);
                txtRst.Text = decryptedStr;
                orderQueryReturnModel = JsonConvert.DeserializeObject<OrderQueryRsp>(decryptedStr);
            }

            return orderQueryReturnModel;
        }

        private void btnRefund_Click(object sender, EventArgs e)
        {
            try
            {
                //初始化 Refund 时间。
                txtRefundTime.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                //查找原交易单号
                OrderQueryRsp queryModel = QueryCore();
                if (queryModel == null || string.IsNullOrWhiteSpace(queryModel.orderNo))
                {
                    throw new Exception("未查到原 交易单号 ，无法退款！");
                }
                string trade_no = "";
                if (queryModel != null)
                {
                    trade_no = queryModel.orderNo;
                }

                //return;

                /*
110101101	聚合-微信-B扫C支付
110102201	聚合-支付宝-B扫C支付
110103301	银联二维码-B扫C支付
                 */
                string subProductCode = "110101101";
                if (txtAuthCode.Text.StartsWith("2") || txtAuthCode.Text.StartsWith("3"))
                {
                    subProductCode = "110102201";
                }
                else if (txtAuthCode.Text.StartsWith("6"))
                {
                    subProductCode = "110103301";
                }

                JsonSerializerSettings jsetting = new JsonSerializerSettings();
                jsetting.DefaultValueHandling = DefaultValueHandling.Ignore;

                int refund_fee = (int)(decimal.Parse(txtRefundAmt.Text) * 100);
                txtOutRefundNo.Text = "R0000000000000" + DateTime.Now.ToString("yyyyMMddHHmmssfff");

                var req = new RefundReq();
                req.merchantNo = txtMchNo.Text;
                req.merOrderNo = txtOutRefundNo.Text;//商户退款单号
                req.amount = refund_fee.ToString();//退款金额
                req.curType = "CNY";
                req.sendupTime = DateTime.Now.ToString("yyyyMMddHHmmss");
                req.version = "1.0";
                req.signType = "3";
                req.sourceIp = "103.43.162.133";
                req.originalOrderNo = trade_no;//原交易单号
                req.subProductCode = subProductCode;//原支付类型

                string dataJson = JsonConvert.SerializeObject(req, jsetting);
                GLog.WLog("未加密：\r\n" + dataJson);
                //RSA加密
                var paramStr = RsaEncryptUtil.RSAEncrypt(dataJson, "UTF-8", txtPubCert.Text);
                //SHA256withRSA
                #region 签名

                //获取私钥对象
                var rsaPri = RsaUtil.LoadPrivateKey(txtPrivateKey.Text, "PKCS1");
                //待签名字符串 转为byte 数组
                byte[] byToSign = Encoding.UTF8.GetBytes(paramStr); // 编码要和其它语言一致，一般是：UTF8 
                byte[] bySigned = rsaPri.SignData(byToSign, "SHA256");//SHA256，对应JAVA，SHA256withRSA,签名结果是 byte 数组
                string strSigned = Convert.ToBase64String(bySigned);//将byte 数组转为字符串，方便传输，一般是base64字符串，其它类型需和对方协商
                //Console.WriteLine("签名值：" + strSigned);
                GLog.WLog("签名值：" + strSigned);


                #endregion
                MFE88_PubReq pubReq = new MFE88_PubReq();
                pubReq.param = paramStr;

                string paramJson = JsonConvert.SerializeObject(pubReq, jsetting);

                #region 准备HEADER

                Dictionary<string, string> dicHeaders = new Dictionary<string, string>();
                dicHeaders.Add("X-Security", "CFCA");
                dicHeaders.Add("X-Agency", txtAgency.Text);
                dicHeaders.Add("X-Sign", strSigned);
                dicHeaders.Add("X-Time", DateTime.Now.ToString("R"));
                dicHeaders.Add("X-Trace", Guid.NewGuid().ToString("N"));
                string headerLogJson = JsonConvert.SerializeObject(dicHeaders, jsetting);
                GLog.WLog("headerLogJson:" + headerLogJson);

                #endregion


                //报文主体

                string bodyJson = JsonConvert.SerializeObject(pubReq, jsetting);

                string suffix = "/trade-connect/api/qrRefund";

                string url = txtMainUrl.Text + suffix;
                GLog.WLog("url:" + url);

                GLog.WLog("请求报文:" + bodyJson);
                Dictionary<string, string> dicRspHeaders = new Dictionary<string, string>();
                var rstStr = HttpUtil.HttpPostJson(url, bodyJson, dicHeaders, 30, dicRspHeaders);
                GLog.WLog("响应报文:" + rstStr);

                txtRst.Text = rstStr;

                string _code = "";
                string _msg = "";

                MFE88_PubRsp rspM = JsonConvert.DeserializeObject<MFE88_PubRsp>(rstStr);

                string decryptedStr = "";
                if (!string.IsNullOrWhiteSpace(rspM.data))
                {
                    //解密数据
                    decryptedStr = RsaEncryptUtil.RSADecrypt(rspM.data, "UTF-8", txtPrivateKey.Text, "PKCS1");
                    GLog.WLog("data解密后：\r\n" + decryptedStr);
                    txtRst.Text = decryptedStr;
                }

                var rspA = JsonConvert.DeserializeObject<RefundRsp>(decryptedStr);

                if (rspM.code == "000000")
                {
                    /*
                     00-未支付 01-处理中 02-成功 03-失败 04-关单成功 99-状态未知
                     */
                    if (rspA.tradeStatus == "02" || rspA.tradeStatus == "01" || rspA.tradeStatus == "99")
                    {
                        _code = SissPayReturnCode.SUCCESS.ToString();
                        lblRefundStatus.Text = "成功";
                    }
                    else
                    {
                        _code = SissPayReturnCode.FAIL.ToString();
                        lblRefundStatus.Text = "失败";
                    }
                }
                else
                {
                    _code = SissPayReturnCode.FAIL.ToString();
                    _msg = rspM.message;
                    txtRst.Text = rstStr;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnRefundQuery_Click(object sender, EventArgs e)
        {
            try
            {
                //现代支付，退款查询使用的是交易查询接口。
                string _refund_status = "";

                OrderQueryRsp rspBody = RefundQueryCore();
                if (rspBody != null)
                {
                    /*
                    tradeStatus
00-未支付 01-处理中 02-成功 03-失败 04-关单成功 99-状态未知
                    */

                    if (rspBody.tradeStatus == "02"  )
                    {
                        lblRefundStatus.Text = "退款成功";
                        _refund_status = SissPayRefundStatus.SUCCESS.ToString();
                    }else if (  rspBody.tradeStatus == "01")
                    {
                        lblRefundStatus.Text = "退款中";
                        _refund_status = SissPayRefundStatus.PROCESSING.ToString();
                    }
                    else
                    {
                        lblRefundStatus.Text = "退款失败";
                        _refund_status = SissPayRefundStatus.FAIL.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnTermBind_Click(object sender, EventArgs e)
        {
            try
            {
                JsonSerializerSettings jsetting = new JsonSerializerSettings();
                jsetting.DefaultValueHandling = DefaultValueHandling.Ignore;

                if (string.IsNullOrWhiteSpace(txtMchNo.Text))
                {
                    MessageBox.Show("txtMchNo 不能为空！");
                    return;
                }
                if (string.IsNullOrWhiteSpace(txtMchName.Text))
                {
                    MessageBox.Show("txtMchName 不能为空！");
                    return;
                }
                //if (string.IsNullOrWhiteSpace(txtTermNo.Text))
                //{
                //    MessageBox.Show("txtTermNo 不能为空！");
                //    return;
                //}

                string _TermDir = "已报备终端";
                string _TermFileName = "商户号" + txtMchNo.Text + "已报备终端.txt";
                string appPath = Application.StartupPath;
                string fileFullName = Path.Combine(appPath, _TermDir); //文件夹
                if (!Directory.Exists(fileFullName))
                {
                    Directory.CreateDirectory(fileFullName);
                }
                fileFullName = Path.Combine(fileFullName, _TermFileName);//文件全路径

                #region 检查有没有报备过
                if (File.Exists(fileFullName))
                {
                    string fileCon = File.ReadAllText(fileFullName);
                    if (!string.IsNullOrWhiteSpace(fileCon) && fileCon.Length >= 8)
                    {
                        try
                        {
                            ProcessStartInfo startInfo = new ProcessStartInfo();
                            startInfo.FileName = "notepad.exe";
                            startInfo.Arguments = fileFullName;
                            Process.Start(startInfo);

                            string tmpTermNO = fileCon;
                            if (fileCon.Contains(";"))
                            {
                                tmpTermNO = fileCon.Substring(0, fileCon.IndexOf(';'));
                            }
                            txtTermNo.Text = tmpTermNO;
                        }
                        catch (Exception exOpen)
                        {
                        }

                        string tipMsg = "已报备过终端号，是否继续报备？（不建议重复报备）,已报备的终端：" + fileCon;
                        DialogResult dr = MessageBox.Show(tipMsg, "OO", MessageBoxButtons.YesNo
                            , MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                        if (dr == DialogResult.No)
                        {
                            return;
                        }
                    }
                }
                #endregion

                TermBindReq req = new TermBindReq();
                req.merchantNo = txtMchNo.Text.Trim();
                req.merchantName = txtMchName.Text.Trim();
                req.terminalType = "11";
                //req.terminalNo=txtTermNo.Text.Trim();
                req.reportType = "00";

                string dataJson = JsonConvert.SerializeObject(req, jsetting);
                GLog.WLog("未加密：\r\n" + dataJson);
                //RSA加密
                var paramStr = RsaEncryptUtil.RSAEncrypt(dataJson, "UTF-8", txtPubCert.Text);
                //SHA256withRSA
                #region 签名

                //获取私钥对象
                var rsaPri = RsaUtil.LoadPrivateKey(txtPrivateKey.Text, "PKCS1");
                //待签名字符串 转为byte 数组
                byte[] byToSign = Encoding.UTF8.GetBytes(paramStr); // 编码要和其它语言一致，一般是：UTF8 
                byte[] bySigned = rsaPri.SignData(byToSign, "SHA256");//SHA256，对应JAVA，SHA256withRSA,签名结果是 byte 数组
                string strSigned = Convert.ToBase64String(bySigned);//将byte 数组转为字符串，方便传输，一般是base64字符串，其它类型需和对方协商
                //Console.WriteLine("签名值：" + strSigned);
                GLog.WLog("签名值：" + strSigned);


                #endregion
                MFE88_PubReq pubReq = new MFE88_PubReq();
                pubReq.param = paramStr;

                string paramJson = JsonConvert.SerializeObject(pubReq, jsetting);

                #region 准备HEADER

                Dictionary<string, string> dicHeaders = new Dictionary<string, string>();
                dicHeaders.Add("X-Security", "CFCA");
                dicHeaders.Add("X-Agency", txtAgency.Text);
                dicHeaders.Add("X-Sign", strSigned);
                dicHeaders.Add("X-Time", DateTime.Now.ToString("R"));
                dicHeaders.Add("X-Trace", Guid.NewGuid().ToString("N"));
                string headerLogJson = JsonConvert.SerializeObject(dicHeaders, jsetting);
                GLog.WLog("headerLogJson:" + headerLogJson);

                #endregion


                //报文主体

                string bodyJson = JsonConvert.SerializeObject(pubReq, jsetting);

                string suffix = "/terminal-report/api/open/terminal-report";

                //报备和进件不是同一网关
                string url = txtMchUrl.Text + suffix;
                GLog.WLog("url:" + url);

                GLog.WLog("请求报文:" + bodyJson);
                Dictionary<string, string> dicRspHeaders = new Dictionary<string, string>();
                var rstStr = HttpUtil.HttpPostJson(url, bodyJson, dicHeaders, 30, dicRspHeaders);
                GLog.WLog("响应报文:" + rstStr);

                txtRst.Text = rstStr;

                string _code = "";
                string _msg = "";



                MFE88_PubRsp rspM = JsonConvert.DeserializeObject<MFE88_PubRsp>(rstStr);

                string decryptedStr = "";
                if (!string.IsNullOrWhiteSpace(rspM.data))
                {
                    //解密数据
                    decryptedStr = RsaEncryptUtil.RSADecrypt(rspM.data, "UTF-8", txtPrivateKey.Text, "PKCS1");
                    GLog.WLog("data解密后：\r\n" + decryptedStr);

                }

                if (rspM.code == "000000")
                {
                    TermBindRsp rspA = JsonConvert.DeserializeObject<TermBindRsp>(decryptedStr);
                    txtTermNo.Text = rspA.terminalNo;
                    //把终端号写入到本地,terminalNo.
                    string terminalNo = rspA.terminalNo + ";\r\n";

                    File.AppendAllText(fileFullName, terminalNo);
                }
                else
                {
                    _code = SissPayReturnCode.FAIL.ToString();
                    _msg = rspM.message;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnTermQuery_Click(object sender, EventArgs e)
        {
            try
            {
                JsonSerializerSettings jsetting = new JsonSerializerSettings();
                jsetting.DefaultValueHandling = DefaultValueHandling.Ignore;

                if (string.IsNullOrWhiteSpace(txtMchNo.Text))
                {
                    MessageBox.Show("txtMchNo 不能为空！");
                    return;
                }
                if (string.IsNullOrWhiteSpace(txtMchName.Text))
                {
                    MessageBox.Show("txtMchName 不能为空！");
                    return;
                }

                TermBindReq req = new TermBindReq();
                req.merchantNo = txtMchNo.Text.Trim();
                //req.merchantName = txtMchName.Text.Trim();
                //req.terminalType = "11";
                ////req.terminalNo=txtTermNo.Text.Trim();
                //req.reportType = "00";

                string dataJson = JsonConvert.SerializeObject(req, jsetting);
                GLog.WLog("未加密：\r\n" + dataJson);
                //RSA加密
                var paramStr = RsaEncryptUtil.RSAEncrypt(dataJson, "UTF-8", txtPubCert.Text);
                //SHA256withRSA
                #region 签名

                //获取私钥对象
                var rsaPri = RsaUtil.LoadPrivateKey(txtPrivateKey.Text, "PKCS1");
                //待签名字符串 转为byte 数组
                byte[] byToSign = Encoding.UTF8.GetBytes(paramStr); // 编码要和其它语言一致，一般是：UTF8 
                byte[] bySigned = rsaPri.SignData(byToSign, "SHA256");//SHA256，对应JAVA，SHA256withRSA,签名结果是 byte 数组
                string strSigned = Convert.ToBase64String(bySigned);//将byte 数组转为字符串，方便传输，一般是base64字符串，其它类型需和对方协商
                //Console.WriteLine("签名值：" + strSigned);
                GLog.WLog("签名值：" + strSigned);


                #endregion
                MFE88_PubReq pubReq = new MFE88_PubReq();
                pubReq.param = paramStr;

                string paramJson = JsonConvert.SerializeObject(pubReq, jsetting);

                #region 准备HEADER

                Dictionary<string, string> dicHeaders = new Dictionary<string, string>();
                dicHeaders.Add("X-Security", "CFCA");
                dicHeaders.Add("X-Agency", txtAgency.Text);
                dicHeaders.Add("X-Sign", strSigned);
                dicHeaders.Add("X-Time", DateTime.Now.ToString("R"));
                dicHeaders.Add("X-Trace", Guid.NewGuid().ToString("N"));
                string headerLogJson = JsonConvert.SerializeObject(dicHeaders, jsetting);
                GLog.WLog("headerLogJson:" + headerLogJson);

                #endregion


                //报文主体

                string bodyJson = JsonConvert.SerializeObject(pubReq, jsetting);

                string suffix = "/merchant-manager/api/query-merchant-report-api";

                //报备和进件不是同一网关
                string url = txtMchUrl.Text + suffix;
                GLog.WLog("url:" + url);

                GLog.WLog("请求报文:" + bodyJson);
                Dictionary<string, string> dicRspHeaders = new Dictionary<string, string>();
                var rstStr = HttpUtil.HttpPostJson(url, bodyJson, dicHeaders, 30, dicRspHeaders);
                GLog.WLog("响应报文:" + rstStr);

                txtRst.Text = rstStr;

                string _code = "";
                string _msg = "";


                MFE88_PubRsp rspM = JsonConvert.DeserializeObject<MFE88_PubRsp>(rstStr);

                string decryptedStr = "";
                if (!string.IsNullOrWhiteSpace(rspM.data))
                {
                    //解密数据
                    decryptedStr = RsaEncryptUtil.RSADecrypt(rspM.data, "UTF-8", txtPrivateKey.Text, "PKCS1");
                    GLog.WLog("data解密后：\r\n" + decryptedStr);
                }

                if (rspM.code == "000000")
                {
                    TermBindRsp rspA = JsonConvert.DeserializeObject<TermBindRsp>(decryptedStr);
                }
                else
                {
                    _code = SissPayReturnCode.FAIL.ToString();
                    _msg = rspM.message;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        OrderQueryRsp RefundQueryCore()
        {
            OrderQueryRsp orderQueryReturnModel = null;

            JsonSerializerSettings jsetting = new JsonSerializerSettings();
            jsetting.DefaultValueHandling = DefaultValueHandling.Ignore;

            string amount = ((int)(decimal.Parse(txtAmt.Text) * 100)).ToString();

            /*
110101101	聚合-微信-B扫C支付
110102201	聚合-支付宝-B扫C支付
110103301	银联二维码-B扫C支付
             */
            string subProductCode = "110101101";
            if (txtAuthCode.Text.StartsWith("2") || txtAuthCode.Text.StartsWith("3"))
            {
                subProductCode = "110102201";
            }
            else if (txtAuthCode.Text.StartsWith("6"))
            {
                subProductCode = "110103301";
            }

            OrderQueryReq req = new OrderQueryReq();
            req.merchantNo = txtMchNo.Text.Trim();
            req.merOrderNo = txtOutRefundNo.Text; //退款申请号
            req.tradeDate = DateTime.Parse(txtRefundTime.Text).ToString("yyyyMMdd");//退款申请时间
            req.amount = amount;
            req.curType = "CNY";
            req.sendupTime = DateTime.Now.ToString("yyyyMMddHHmmss"); // 退款申请日期
            req.version = "1.0";
            req.signType = "3";
            req.subProductCode = subProductCode;

            string dataJson = JsonConvert.SerializeObject(req, jsetting);
            GLog.WLog("未加密：\r\n" + dataJson);
            //RSA加密
            var paramStr = RsaEncryptUtil.RSAEncrypt(dataJson, "UTF-8", txtPubCert.Text);
            //SHA256withRSA
            #region 签名

            //获取私钥对象
            var rsaPri = RsaUtil.LoadPrivateKey(txtPrivateKey.Text, "PKCS1");
            //待签名字符串 转为byte 数组
            byte[] byToSign = Encoding.UTF8.GetBytes(paramStr); // 编码要和其它语言一致，一般是：UTF8 
            byte[] bySigned = rsaPri.SignData(byToSign, "SHA256");//SHA256，对应JAVA，SHA256withRSA,签名结果是 byte 数组
            string strSigned = Convert.ToBase64String(bySigned);//将byte 数组转为字符串，方便传输，一般是base64字符串，其它类型需和对方协商
                                                                //Console.WriteLine("签名值：" + strSigned);
            GLog.WLog("签名值：" + strSigned);

            #endregion
            MFE88_PubReq pubReq = new MFE88_PubReq();
            pubReq.param = paramStr;

            string paramJson = JsonConvert.SerializeObject(pubReq, jsetting);

            #region 准备HEADER

            Dictionary<string, string> dicHeaders = new Dictionary<string, string>();
            dicHeaders.Add("X-Security", "CFCA");
            dicHeaders.Add("X-Agency", txtAgency.Text);
            dicHeaders.Add("X-Sign", strSigned);
            dicHeaders.Add("X-Time", DateTime.Now.ToString("R"));
            dicHeaders.Add("X-Trace", Guid.NewGuid().ToString("N"));
            string headerLogJson = JsonConvert.SerializeObject(dicHeaders, jsetting);
            GLog.WLog("headerLogJson:" + headerLogJson);

            #endregion


            //报文主体

            string bodyJson = JsonConvert.SerializeObject(pubReq, jsetting);

            string suffix = "/trade-connect/api/qrQuery";

            string url = txtMainUrl.Text + suffix;
            GLog.WLog("url:" + url);

            GLog.WLog("请求报文:" + bodyJson);
            Dictionary<string, string> dicRspHeaders = new Dictionary<string, string>();
            var rstStr = HttpUtil.HttpPostJson(url, bodyJson, dicHeaders, 30, dicRspHeaders);
            GLog.WLog("响应报文:" + rstStr);

            txtRst.Text = rstStr;

            string _code = "";
            string _msg = "";


            MFE88_PubRsp rspM = JsonConvert.DeserializeObject<MFE88_PubRsp>(rstStr);

            string decryptedStr = "";
            if (!string.IsNullOrWhiteSpace(rspM.data))
            {
                //解密数据
                decryptedStr = RsaEncryptUtil.RSADecrypt(rspM.data, "UTF-8", txtPrivateKey.Text, "PKCS1");
                GLog.WLog("data解密后：\r\n" + decryptedStr);
                txtRst.Text = decryptedStr;
                orderQueryReturnModel = JsonConvert.DeserializeObject<OrderQueryRsp>(decryptedStr);
            }

            return orderQueryReturnModel;
        }


    }
}
