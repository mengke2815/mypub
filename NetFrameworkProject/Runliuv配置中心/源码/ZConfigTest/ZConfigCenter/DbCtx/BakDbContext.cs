﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using ZConfigCenter.DbModels;

namespace ZConfigCenter.DbCtx
{
    public class BakDbContext : DbContext
    {
        public BakDbContext(): base("ZConfigBak")
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {            
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();//移除复数表名
            base.OnModelCreating(modelBuilder);
        }

        public virtual DbSet<t_config> t_config { get; set; }
    }
}
