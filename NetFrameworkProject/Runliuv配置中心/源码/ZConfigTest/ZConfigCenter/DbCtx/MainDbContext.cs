﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using ZConfigCenter.DbModels;

namespace ZConfigCenter.DbCtx
{
    public class MainDbContext: DbContext
    {
        public MainDbContext(): base("ZConfigMain")
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {            
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();//移除复数表名
            base.OnModelCreating(modelBuilder);
        }

        public virtual DbSet<t_config> t_config { get; set; }

        public virtual DbSet<t_app> t_app { get; set; }

        public virtual DbSet<t_config_history> t_config_history { get; set; }

    }
}
