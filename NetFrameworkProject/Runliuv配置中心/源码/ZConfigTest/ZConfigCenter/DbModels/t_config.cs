﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ZConfigCenter.DbModels
{
    public class t_config
    {
        [Key] //主键 
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]  //设置自增
        public string id { get; set; }

        //public string sync_id { get; set; }

        public string app_id { get; set; }
        public string var_id { get; set; }
        public string var_name { get; set; }
        public string var_memo { get; set; }
        public string var_value { get; set; }
        public DateTime create_time { get; set; }
        public DateTime update_time { get; set; }


    }
}
