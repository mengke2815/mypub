﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ZConfigCenter.DbModels
{
    public class t_config_history
    {
        [Key] //主键 
        public string id { get; set; }

        public string app_id { get; set; }
        public string var_id { get; set; }
         
        public string old_var_value { get; set; }
        public string new_var_value { get; set; }

        /// <summary>
        /// EDIT,编辑；DEL,删除；
        /// </summary>
        public string history_type { get; set; }


        public DateTime create_time { get; set; }
        public DateTime update_time { get; set; }
    }
}
