﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ZConfigCenter.DbModels
{
    public class t_app
    {
        [Key] //主键          
        public string id { get; set; }

        public string app_id { get; set; }
        public string appname { get; set; }
        public string app_memo { get; set; }
        
        public DateTime create_time { get; set; }
        public DateTime update_time { get; set; }
    }
}
