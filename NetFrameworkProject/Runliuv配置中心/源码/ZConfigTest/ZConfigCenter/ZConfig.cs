﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ZConfigCenter.DbCtx;

namespace ZConfigCenter
{
    public static class ZConfig
    {
        static int _ErrSleep = 30000;

        static ZConfig()
        {            
            int workerThreads;//默认“最小工作线程数”
            int completePortsThreads; //默认“最小异步I/O 线程数”
            ThreadPool.GetMinThreads(out workerThreads, out completePortsThreads);
            // wantWork 一般来说 50 - 300 就够用了，根据压力测试情况再加大。
            int wantWork = 60;
#if DEBUG
            ThreadPool.SetMinThreads(2, 2);
#endif
            if (workerThreads < wantWork)
            {
                // 最小 IO线程数 值不要超过 默认“最大 IO线程数”，否则会不生效。如果需要调整的更高，先要调高“最大 IO线程数”。
                //ThreadPool.SetMinThreads(wantWork, wantWork);
            }
        }

        /// <summary>
        /// 只从主库取
        /// </summary>
        /// <param name="vadId"></param>
        /// <returns></returns>
        public static string Get(string vadId)
        {
            //获取appId;
            string appId = ConfigurationManager.AppSettings["ZConfigAppId"].ToString();
            if (string.IsNullOrWhiteSpace(appId))
                throw new Exception("ZConfigAppId 不能为空！");             

            return GetFromMain(vadId, appId);
        }

        /// <summary>
        /// 从指定应用获取参数
        /// </summary>
        /// <param name="vadId"></param>
        /// <returns></returns>
        public static string Get(string vadId, string appId)
        {
            return GetFromMain(vadId, appId);
        }

        /// <summary>
        /// 同时从2个库中取数据，谁先返回就用谁
        /// </summary>
        /// <param name="vadId"></param>
        /// <returns></returns>
        public static string Get2(string vadId)
        {
            //获取appId;
            string appId = ConfigurationManager.AppSettings["ZConfigAppId"].ToString();
            if (string.IsNullOrWhiteSpace(appId))
                throw new Exception("ZConfigAppId 不能为空！");

            string varValue = string.Empty;

            List<Task<string>> lst = new List<Task<string>>();
            var taskMain = new Task<string>(() =>
            {
                return GetFromMain(vadId, appId);
            });
            var taskBak = new Task<string>(() =>
            {
                return GetFromBak(vadId, appId);
            });
            lst.Add(taskMain);
            lst.Add(taskBak);
            taskMain.Start();
            taskBak.Start();
            //Task.WaitAny(lst.ToArray(),TimeSpan.FromSeconds(6));
            Task.WaitAny(lst.ToArray());

            if (taskMain.IsCompleted)
                varValue = taskMain.Result;
            else if (taskBak.IsCompleted)
                varValue = taskBak.Result;
                        
            return varValue;
        }

        static string GetFromMain(string vadId, string appId)
        {
            string varValue = string.Empty;
            //try
            //{
                 

                using (MainDbContext db = new MainDbContext())
                {
                    //db.Database.CommandTimeout = 6;
                    var mod = db.t_config.FirstOrDefault(x => x.app_id == appId && x.var_id == vadId);
                    if (mod != null)
                    {
                        varValue = mod.var_value;
                    }
                }
            //}
            //catch (Exception ex)
            //{
            //    System.Threading.Thread.Sleep(_ErrSleep);

            //}
            return varValue;
        }

        static string GetFromBak(string vadId, string appId)
        {
            string varValue = string.Empty;
            //try
            //{
                 

                using (BakDbContext db = new BakDbContext())
                {
                    //db.Database.CommandTimeout = 6;
                    var mod = db.t_config.FirstOrDefault(x => x.app_id == appId && x.var_id == vadId);
                    if (mod != null)
                    {
                        varValue = mod.var_value;
                    }
                }
            //}
            //catch (Exception ex)
            //{
            //    System.Threading.Thread.Sleep(_ErrSleep);

            //}
            return varValue;
        }


    }
}