﻿namespace ZConfigCenterManager
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvServer = new System.Windows.Forms.DataGridView();
            this.MyCheck = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ColItemId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColVarId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColVarName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColFolerFullName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColWeight = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.eCS主机管理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiMngApp = new System.Windows.Forms.ToolStripMenuItem();
            this.btnRefreshAppL = new System.Windows.Forms.Button();
            this.cbxApps = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblMsg = new System.Windows.Forms.Label();
            this.btnRefreshList = new System.Windows.Forms.Button();
            this.btnAddConfig = new System.Windows.Forms.Button();
            this.btnEditConfig = new System.Windows.Forms.Button();
            this.btnDelConfig = new System.Windows.Forms.Button();
            this.btnViewHistory = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.btnAddPreset = new System.Windows.Forms.Button();
            this.btnViewPreset = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvServer)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvServer
            // 
            this.dgvServer.AllowUserToAddRows = false;
            this.dgvServer.AllowUserToDeleteRows = false;
            this.dgvServer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvServer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvServer.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MyCheck,
            this.ColItemId,
            this.ColVarId,
            this.ColVarName,
            this.ColFolerFullName,
            this.ColWeight,
            this.ColId});
            this.dgvServer.Location = new System.Drawing.Point(1, 164);
            this.dgvServer.Name = "dgvServer";
            this.dgvServer.RowHeadersVisible = false;
            this.dgvServer.RowHeadersWidth = 51;
            this.dgvServer.RowTemplate.Height = 23;
            this.dgvServer.Size = new System.Drawing.Size(1007, 557);
            this.dgvServer.TabIndex = 3;
            // 
            // MyCheck
            // 
            this.MyCheck.FalseValue = "0";
            this.MyCheck.HeaderText = "选择";
            this.MyCheck.MinimumWidth = 6;
            this.MyCheck.Name = "MyCheck";
            this.MyCheck.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.MyCheck.TrueValue = "1";
            this.MyCheck.Width = 60;
            // 
            // ColItemId
            // 
            this.ColItemId.DataPropertyName = "app_id";
            this.ColItemId.HeaderText = "应用ID";
            this.ColItemId.MinimumWidth = 6;
            this.ColItemId.Name = "ColItemId";
            this.ColItemId.ReadOnly = true;
            this.ColItemId.Width = 125;
            // 
            // ColVarId
            // 
            this.ColVarId.DataPropertyName = "var_id";
            this.ColVarId.HeaderText = "参数ID";
            this.ColVarId.MinimumWidth = 6;
            this.ColVarId.Name = "ColVarId";
            this.ColVarId.ReadOnly = true;
            this.ColVarId.Width = 145;
            // 
            // ColVarName
            // 
            this.ColVarName.DataPropertyName = "var_name";
            this.ColVarName.HeaderText = "参数名";
            this.ColVarName.MinimumWidth = 6;
            this.ColVarName.Name = "ColVarName";
            this.ColVarName.ReadOnly = true;
            this.ColVarName.Width = 125;
            // 
            // ColFolerFullName
            // 
            this.ColFolerFullName.DataPropertyName = "var_memo";
            this.ColFolerFullName.HeaderText = "参数备注";
            this.ColFolerFullName.MinimumWidth = 6;
            this.ColFolerFullName.Name = "ColFolerFullName";
            this.ColFolerFullName.ReadOnly = true;
            this.ColFolerFullName.Width = 200;
            // 
            // ColWeight
            // 
            this.ColWeight.DataPropertyName = "var_value";
            this.ColWeight.HeaderText = "参数值";
            this.ColWeight.MinimumWidth = 6;
            this.ColWeight.Name = "ColWeight";
            this.ColWeight.ReadOnly = true;
            this.ColWeight.Width = 300;
            // 
            // ColId
            // 
            this.ColId.DataPropertyName = "id";
            this.ColId.HeaderText = "id";
            this.ColId.Name = "ColId";
            this.ColId.ReadOnly = true;
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.eCS主机管理ToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(4, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(1008, 25);
            this.menuStrip1.TabIndex = 15;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // eCS主机管理ToolStripMenuItem
            // 
            this.eCS主机管理ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiMngApp});
            this.eCS主机管理ToolStripMenuItem.Name = "eCS主机管理ToolStripMenuItem";
            this.eCS主机管理ToolStripMenuItem.Size = new System.Drawing.Size(44, 21);
            this.eCS主机管理ToolStripMenuItem.Text = "菜单";
            // 
            // tsmiMngApp
            // 
            this.tsmiMngApp.Name = "tsmiMngApp";
            this.tsmiMngApp.Size = new System.Drawing.Size(124, 22);
            this.tsmiMngApp.Text = "应用管理";
            this.tsmiMngApp.Click += new System.EventHandler(this.tsmiMngApp_Click);
            // 
            // btnRefreshAppL
            // 
            this.btnRefreshAppL.Location = new System.Drawing.Point(18, 48);
            this.btnRefreshAppL.Name = "btnRefreshAppL";
            this.btnRefreshAppL.Size = new System.Drawing.Size(75, 23);
            this.btnRefreshAppL.TabIndex = 19;
            this.btnRefreshAppL.Text = "刷新应用";
            this.btnRefreshAppL.UseVisualStyleBackColor = true;
            this.btnRefreshAppL.Click += new System.EventHandler(this.btnRefreshAppL_Click);
            // 
            // cbxApps
            // 
            this.cbxApps.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxApps.Font = new System.Drawing.Font("宋体", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cbxApps.FormattingEnabled = true;
            this.cbxApps.Location = new System.Drawing.Point(141, 50);
            this.cbxApps.Name = "cbxApps";
            this.cbxApps.Size = new System.Drawing.Size(289, 23);
            this.cbxApps.TabIndex = 18;
            this.cbxApps.SelectedValueChanged += new System.EventHandler(this.cbxApps_SelectedValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(99, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 17;
            this.label1.Text = "应用：";
            // 
            // lblMsg
            // 
            this.lblMsg.AutoSize = true;
            this.lblMsg.Location = new System.Drawing.Point(728, 25);
            this.lblMsg.Name = "lblMsg";
            this.lblMsg.Size = new System.Drawing.Size(23, 12);
            this.lblMsg.TabIndex = 20;
            this.lblMsg.Text = "msg";
            // 
            // btnRefreshList
            // 
            this.btnRefreshList.Location = new System.Drawing.Point(74, 90);
            this.btnRefreshList.Name = "btnRefreshList";
            this.btnRefreshList.Size = new System.Drawing.Size(43, 23);
            this.btnRefreshList.TabIndex = 21;
            this.btnRefreshList.Text = "刷新";
            this.btnRefreshList.UseVisualStyleBackColor = true;
            this.btnRefreshList.Click += new System.EventHandler(this.btnRefreshList_Click);
            // 
            // btnAddConfig
            // 
            this.btnAddConfig.Location = new System.Drawing.Point(141, 90);
            this.btnAddConfig.Name = "btnAddConfig";
            this.btnAddConfig.Size = new System.Drawing.Size(43, 23);
            this.btnAddConfig.TabIndex = 22;
            this.btnAddConfig.Text = "添加";
            this.btnAddConfig.UseVisualStyleBackColor = true;
            this.btnAddConfig.Click += new System.EventHandler(this.btnAddConfig_Click);
            // 
            // btnEditConfig
            // 
            this.btnEditConfig.Location = new System.Drawing.Point(205, 90);
            this.btnEditConfig.Name = "btnEditConfig";
            this.btnEditConfig.Size = new System.Drawing.Size(43, 23);
            this.btnEditConfig.TabIndex = 23;
            this.btnEditConfig.Text = "编辑";
            this.btnEditConfig.UseVisualStyleBackColor = true;
            this.btnEditConfig.Click += new System.EventHandler(this.btnEditConfig_Click);
            // 
            // btnDelConfig
            // 
            this.btnDelConfig.Location = new System.Drawing.Point(276, 90);
            this.btnDelConfig.Name = "btnDelConfig";
            this.btnDelConfig.Size = new System.Drawing.Size(43, 23);
            this.btnDelConfig.TabIndex = 24;
            this.btnDelConfig.Text = "删除";
            this.btnDelConfig.UseVisualStyleBackColor = true;
            this.btnDelConfig.Click += new System.EventHandler(this.btnDelConfig_Click);
            // 
            // btnViewHistory
            // 
            this.btnViewHistory.Location = new System.Drawing.Point(360, 90);
            this.btnViewHistory.Name = "btnViewHistory";
            this.btnViewHistory.Size = new System.Drawing.Size(70, 23);
            this.btnViewHistory.TabIndex = 25;
            this.btnViewHistory.Text = "查看记录";
            this.btnViewHistory.UseVisualStyleBackColor = true;
            this.btnViewHistory.Click += new System.EventHandler(this.btnViewHistory_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(360, 132);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(43, 23);
            this.btnSearch.TabIndex = 26;
            this.btnSearch.Text = "搜索";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(72, 138);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 12);
            this.label2.TabIndex = 27;
            this.label2.Text = "条件";
            // 
            // txtSearch
            // 
            this.txtSearch.Location = new System.Drawing.Point(141, 133);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(193, 21);
            this.txtSearch.TabIndex = 28;
            this.txtSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSearch_KeyDown);
            // 
            // btnAddPreset
            // 
            this.btnAddPreset.Location = new System.Drawing.Point(452, 132);
            this.btnAddPreset.Name = "btnAddPreset";
            this.btnAddPreset.Size = new System.Drawing.Size(70, 23);
            this.btnAddPreset.TabIndex = 29;
            this.btnAddPreset.Text = "添加预设";
            this.btnAddPreset.UseVisualStyleBackColor = true;
            this.btnAddPreset.Click += new System.EventHandler(this.btnAddPreset_Click);
            // 
            // btnViewPreset
            // 
            this.btnViewPreset.Location = new System.Drawing.Point(540, 132);
            this.btnViewPreset.Name = "btnViewPreset";
            this.btnViewPreset.Size = new System.Drawing.Size(70, 23);
            this.btnViewPreset.TabIndex = 30;
            this.btnViewPreset.Text = "查看预设";
            this.btnViewPreset.UseVisualStyleBackColor = true;
            this.btnViewPreset.Click += new System.EventHandler(this.btnViewPreset_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 730);
            this.Controls.Add(this.btnViewPreset);
            this.Controls.Add(this.btnAddPreset);
            this.Controls.Add(this.txtSearch);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.btnViewHistory);
            this.Controls.Add(this.btnDelConfig);
            this.Controls.Add(this.btnEditConfig);
            this.Controls.Add(this.btnAddConfig);
            this.Controls.Add(this.btnRefreshList);
            this.Controls.Add(this.lblMsg);
            this.Controls.Add(this.btnRefreshAppL);
            this.Controls.Add(this.cbxApps);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.dgvServer);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "配置中心-管理器";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Shown += new System.EventHandler(this.Form1_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.dgvServer)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvServer;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem eCS主机管理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsmiMngApp;
        private System.Windows.Forms.Button btnRefreshAppL;
        private System.Windows.Forms.ComboBox cbxApps;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblMsg;
        private System.Windows.Forms.Button btnRefreshList;
        private System.Windows.Forms.Button btnAddConfig;
        private System.Windows.Forms.Button btnEditConfig;
        private System.Windows.Forms.Button btnDelConfig;
        private System.Windows.Forms.DataGridViewCheckBoxColumn MyCheck;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColItemId;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColVarId;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColVarName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColFolerFullName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColWeight;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColId;
        private System.Windows.Forms.Button btnViewHistory;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.Button btnAddPreset;
        private System.Windows.Forms.Button btnViewPreset;
    }
}

