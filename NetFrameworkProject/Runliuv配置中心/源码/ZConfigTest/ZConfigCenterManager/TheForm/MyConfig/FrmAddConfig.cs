﻿using CommonUtils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using ZConfigCenter.DbCtx;
using ZConfigCenter.DbModels;

namespace ZConfigCenterManager.TheForm.MyConfig
{
    public partial class FrmAddConfig : Form
    {
        string _editId = "0";
        bool _isEdit = false;

        string _appId = "";

        public FrmAddConfig(bool isEdit, string editId, string appId)
        {
            InitializeComponent();

            _isEdit = isEdit;
            _editId = editId;
            _appId = appId;
        }

        private void FrmAddItem_Load(object sender, EventArgs e)
        {
        }

        private void FrmAddConfig_Shown(object sender, EventArgs e)
        {
            try
            {
                MainDbContext db = new MainDbContext();
                if (string.IsNullOrWhiteSpace(_appId))
                {
                    MessageBox.Show("传入APP ID 为空。");
                    this.Close();
                    return;
                }
                var appInfo = db.t_app.FirstOrDefault(x => x.app_id == _appId);
                if (appInfo != null)
                    lblApp.Text = "正在为 " + appInfo.app_id + " " + appInfo.appname + " 配置参数";

                if (!_isEdit)
                {
                    btnEdit.Visible = false;
                }
                else
                {
                    btnAdd.Visible = false;
                    txt_var_id.ReadOnly = true;
                    //edit
                    var m = db.t_config.First(x => x.id == _editId);
                    if (m == null)
                    {
                        return;
                    }
                    txt_var_id.Text = m.var_id;
                    txt_var_name.Text = m.var_name;
                    txt_var_memo.Text = m.var_memo;
                    txt_var_value.Text = m.var_value;
                }

                db.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(txt_var_id.Text))
                {
                    MessageBox.Show("var ID 不能为空！");
                    return;
                }
                if (string.IsNullOrWhiteSpace(txt_var_name.Text))
                {
                    MessageBox.Show("var NAME 不能为空！");
                    return;
                }
                if (string.IsNullOrWhiteSpace(txt_var_value.Text))
                {
                    MessageBox.Show("var value 不能为空！");
                    return;
                }

                MainDbContext db = new MainDbContext();
                if (db.t_config.Any(x => x.var_id == txt_var_id.Text.Trim()))
                {
                    MessageBox.Show("这个 var ID 已存在！");
                    return;
                }
                var t = new t_config();
                t.id = Guid.NewGuid().ToString("N");
                t.app_id = _appId;
                t.var_id = txt_var_id.Text.Trim();
                t.var_name = txt_var_name.Text.Trim();
                t.var_memo = txt_var_memo.Text.Trim();
                t.var_value = txt_var_value.Text.Trim();

                t.create_time = DateTime.Now;
                t.update_time = DateTime.Now;

                db.t_config.Add(t);
                db.SaveChanges();
                db.Dispose();
                MessageBox.Show("添加成功！");
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(MU.GED(ex));
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(txt_var_id.Text))
                {
                    MessageBox.Show("APP ID 不能为空！");
                    return;
                }
                if (string.IsNullOrWhiteSpace(txt_var_name.Text))
                {
                    MessageBox.Show("APP NAME 不能为空！");
                    return;
                }
                if (string.IsNullOrWhiteSpace(txt_var_value.Text))
                {
                    MessageBox.Show("var value 不能为空！");
                    return;
                }

                MainDbContext db = new MainDbContext();
                var t = db.t_config.First(x => x.id == _editId);

                #region 修改历史
                if (txt_var_value.Text.Trim() != t.var_value)
                {
                    t_config_history his = new t_config_history();
                    his.id = Guid.NewGuid().ToString("N");
                    his.app_id = _appId;
                    his.var_id = txt_var_id.Text.Trim();
                    his.old_var_value = t.var_value;
                    his.new_var_value = txt_var_value.Text.Trim();
                    his.history_type = "EDIT";
                    his.create_time = DateTime.Now;
                    his.update_time = DateTime.Now;
                    db.t_config_history.Add(his);
                }
                #endregion

                t.var_id = txt_var_id.Text.Trim();
                t.var_memo = txt_var_memo.Text.Trim();
                t.var_value = txt_var_value.Text.Trim();
                t.update_time = DateTime.Now;

                db.SaveChanges();
                db.Dispose();
                MessageBox.Show("成功！");
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(MU.GED(ex));
            }
        }


    }
}
