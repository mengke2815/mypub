﻿namespace ZConfigCenterManager.TheForm.History
{
    partial class FrmViewHistory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblMsg = new System.Windows.Forms.Label();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.dgvServer = new System.Windows.Forms.DataGridView();
            this.cbxAll = new System.Windows.Forms.CheckBox();
            this.ColId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColServerId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvServer)).BeginInit();
            this.SuspendLayout();
            // 
            // lblMsg
            // 
            this.lblMsg.AutoSize = true;
            this.lblMsg.Location = new System.Drawing.Point(573, 17);
            this.lblMsg.Name = "lblMsg";
            this.lblMsg.Size = new System.Drawing.Size(23, 12);
            this.lblMsg.TabIndex = 20;
            this.lblMsg.Text = "msg";
            // 
            // btnRefresh
            // 
            this.btnRefresh.Location = new System.Drawing.Point(205, 12);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(43, 23);
            this.btnRefresh.TabIndex = 17;
            this.btnRefresh.Text = "刷新";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // dgvServer
            // 
            this.dgvServer.AllowUserToAddRows = false;
            this.dgvServer.AllowUserToDeleteRows = false;
            this.dgvServer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvServer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvServer.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColId,
            this.ColServerId});
            this.dgvServer.Location = new System.Drawing.Point(12, 70);
            this.dgvServer.Name = "dgvServer";
            this.dgvServer.RowTemplate.Height = 23;
            this.dgvServer.Size = new System.Drawing.Size(1145, 663);
            this.dgvServer.TabIndex = 21;
            // 
            // cbxAll
            // 
            this.cbxAll.AutoSize = true;
            this.cbxAll.Location = new System.Drawing.Point(70, 17);
            this.cbxAll.Name = "cbxAll";
            this.cbxAll.Size = new System.Drawing.Size(72, 16);
            this.cbxAll.TabIndex = 22;
            this.cbxAll.Text = "查看所有";
            this.cbxAll.UseVisualStyleBackColor = true;
            // 
            // ColId
            // 
            this.ColId.DataPropertyName = "id";
            this.ColId.HeaderText = "ID";
            this.ColId.Name = "ColId";
            this.ColId.ReadOnly = true;
            // 
            // ColServerId
            // 
            this.ColServerId.DataPropertyName = "app_id";
            this.ColServerId.HeaderText = "应用ID";
            this.ColServerId.Name = "ColServerId";
            this.ColServerId.ReadOnly = true;
            this.ColServerId.Width = 145;
            // 
            // FrmViewHistory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1189, 771);
            this.Controls.Add(this.cbxAll);
            this.Controls.Add(this.dgvServer);
            this.Controls.Add(this.lblMsg);
            this.Controls.Add(this.btnRefresh);
            this.Name = "FrmViewHistory";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "应用管理";
            this.Load += new System.EventHandler(this.FrmMngItem_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvServer)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblMsg;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.DataGridView dgvServer;
        private System.Windows.Forms.CheckBox cbxAll;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColId;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColServerId;
    }
}