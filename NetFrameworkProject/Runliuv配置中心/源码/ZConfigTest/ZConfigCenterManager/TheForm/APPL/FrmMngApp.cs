﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZConfigCenter.DbCtx;
using ZConfigCenter.DbModels;

namespace ZConfigCenterManager.TheForm.APPL
{
    public partial class FrmMngApp : Form
    {
        /// <summary>
        /// checkbox列名:MyCheck
        /// </summary>
        string _selCol = "MyCheck";

        public FrmMngApp()
        {
            InitializeComponent();
        }

        private void FrmMngItem_Load(object sender, EventArgs e)
        {
            LoadList();
        }

        void LoadList()
        {
            try
            {
                lblMsg.Text = "正在加载……";
                Task.Factory.StartNew(() =>
                {
                    List<t_app> lstItems = new List<t_app>();
                    try
                    {
                        MainDbContext db = new MainDbContext();
                        lstItems = db.t_app.OrderByDescending(x => x.create_time).ToList();
                    }
                    catch (Exception ex1)
                    {
                        this.BeginInvoke(new Action(() =>
                        {
                            MessageBox.Show("ex1:" + ex1.Message);
                        }));
                    }
                    //update UI
                    this.BeginInvoke(new Action(() =>
                    {
                        if (lstItems.Count() > 0)
                        {
                            dgvServer.DataSource = lstItems;
                        }
                        lblMsg.Text = "";
                    }));
                });
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            FrmAddApp frm = new FrmAddApp(false, "0");
            frm.ShowDialog();
            LoadList();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                string editId = "0";
                bool anyCheck = false;
                foreach (DataGridViewRow dgvr in dgvServer.Rows)
                {
                    if (dgvr.Cells[_selCol].Value != null && dgvr.Cells[_selCol].Value.ToString() == "1")
                    {
                        anyCheck = true;
                        editId =  dgvr.Cells["ColId"].Value.ToString();
                        break;
                    }
                }
                if (!anyCheck)
                {
                    MessageBox.Show("未选择任何项");
                    return;
                }

                FrmAddApp frm = new FrmAddApp(true, editId);
                frm.ShowDialog();
                LoadList();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnDel_Click(object sender, EventArgs e)
        {
            try
            {
                bool anyCheck = false;
                foreach (DataGridViewRow dgvr in dgvServer.Rows)
                {
                    if (dgvr.Cells[_selCol].Value != null && dgvr.Cells[_selCol].Value.ToString() == "1")
                    {
                        anyCheck = true;
                    }
                }
                if (!anyCheck)
                {
                    MessageBox.Show("未选择任何项");
                    return;
                }

                DialogResult dr = MessageBox.Show(" 确定？  ", "确定", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                if (dr == DialogResult.No)
                {
                    return;
                }

                //UpdaterDbContext db = new UpdaterDbContext();

                //foreach (DataGridViewRow dgvr in dgvServer.Rows)
                //{
                //    if (dgvr.Cells[_selCol].Value != null && dgvr.Cells[_selCol].Value.ToString() == "1")
                //    {
                //        long editId = long.Parse(dgvr.Cells["ColId"].Value.ToString());
                //        db.t_item.Remove(db.t_item.First(x => x.id == editId));
                //    }
                //}
                //db.SaveChanges();
                //LoadList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
