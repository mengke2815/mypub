﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using ZConfigCenter.DbCtx;
using ZConfigCenter.DbModels;

namespace ZConfigCenterManager.TheForm.APPL
{
    public partial class FrmAddApp : Form
    {
        string _editId = "0";
        bool _isEdit = false;

        public FrmAddApp(bool isEdit, string editId)
        {
            InitializeComponent();

            _isEdit = isEdit;
            _editId = editId;
        }

        private void FrmAddItem_Load(object sender, EventArgs e)
        {
            try
            {
                MainDbContext db = new MainDbContext();
                if (!_isEdit)
                {
                    btnEdit.Visible = false;
                }
                else
                {
                    btnAdd.Visible = false;
                    txtAppId.ReadOnly = true;
                    //edit
                    var m = db.t_app.First(x => x.id == _editId);
                    if (m == null)
                    {
                        return;
                    }
                    txtAppId.Text = m.app_id;
                    txtAppName.Text = m.appname;
                    txtAppMemo.Text = m.app_memo;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(txtAppId.Text))
                {
                    MessageBox.Show("APP ID 不能为空！");
                    return;
                }
                if (string.IsNullOrWhiteSpace(txtAppName.Text))
                {
                    MessageBox.Show("APP NAME 不能为空！");
                    return;
                }
                MainDbContext db = new MainDbContext();
                if (db.t_config.Any(x => x.app_id == txtAppId.Text.Trim()))
                {
                    MessageBox.Show("这个 app ID 已存在！");
                    return;
                }
                t_app t = new t_app();
                t.id = Guid.NewGuid().ToString("N");
                t.app_id = txtAppId.Text.Trim();
                t.appname = txtAppName.Text.Trim();
                t.app_memo = txtAppMemo.Text.Trim();
                t.create_time = DateTime.Now;
                t.update_time = DateTime.Now;

                db.t_app.Add(t);
                db.SaveChanges();
                db.Dispose();
                MessageBox.Show("添加成功！");
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(txtAppId.Text))
                {
                    MessageBox.Show("APP ID 不能为空！");
                    return;
                }
                if (string.IsNullOrWhiteSpace(txtAppName.Text))
                {
                    MessageBox.Show("APP NAME 不能为空！");
                    return;
                }

                MainDbContext db = new MainDbContext();
                t_app t = db.t_app.First(x => x.id == _editId);                
                t.appname = txtAppName.Text.Trim();
                t.app_memo = txtAppMemo.Text.Trim();                
                t.update_time = DateTime.Now;                 
                db.SaveChanges();
                db.Dispose();
                MessageBox.Show("成功！");
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
