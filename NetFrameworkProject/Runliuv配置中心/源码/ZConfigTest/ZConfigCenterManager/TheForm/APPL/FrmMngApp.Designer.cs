﻿namespace ZConfigCenterManager.TheForm.APPL
{
    partial class FrmMngApp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.lblMsg = new System.Windows.Forms.Label();
            this.btnDel = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.dgvServer = new System.Windows.Forms.DataGridView();
            this.MyCheck = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ColId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColServerId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColLastAct = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvServer)).BeginInit();
            this.SuspendLayout();
            // 
            // lblMsg
            // 
            this.lblMsg.AutoSize = true;
            this.lblMsg.Location = new System.Drawing.Point(573, 17);
            this.lblMsg.Name = "lblMsg";
            this.lblMsg.Size = new System.Drawing.Size(23, 12);
            this.lblMsg.TabIndex = 20;
            this.lblMsg.Text = "msg";
            // 
            // btnDel
            // 
            this.btnDel.Location = new System.Drawing.Point(358, 12);
            this.btnDel.Name = "btnDel";
            this.btnDel.Size = new System.Drawing.Size(43, 23);
            this.btnDel.TabIndex = 19;
            this.btnDel.Text = "删除";
            this.btnDel.UseVisualStyleBackColor = true;
            this.btnDel.Visible = false;
            this.btnDel.Click += new System.EventHandler(this.btnDel_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.Location = new System.Drawing.Point(285, 12);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(43, 23);
            this.btnEdit.TabIndex = 18;
            this.btnEdit.Text = "编辑";
            this.btnEdit.UseVisualStyleBackColor = true;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(205, 12);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(43, 23);
            this.btnAdd.TabIndex = 17;
            this.btnAdd.Text = "添加";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // dgvServer
            // 
            this.dgvServer.AllowUserToAddRows = false;
            this.dgvServer.AllowUserToDeleteRows = false;
            this.dgvServer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvServer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvServer.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MyCheck,
            this.ColId,
            this.ColServerId,
            this.ColLastAct,
            this.Column1});
            this.dgvServer.Location = new System.Drawing.Point(12, 70);
            this.dgvServer.Name = "dgvServer";
            this.dgvServer.RowTemplate.Height = 23;
            this.dgvServer.Size = new System.Drawing.Size(1145, 663);
            this.dgvServer.TabIndex = 21;
            // 
            // MyCheck
            // 
            this.MyCheck.FalseValue = "0";
            this.MyCheck.HeaderText = "选择";
            this.MyCheck.Name = "MyCheck";
            this.MyCheck.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.MyCheck.TrueValue = "1";
            this.MyCheck.Width = 60;
            // 
            // ColId
            // 
            this.ColId.DataPropertyName = "id";
            this.ColId.HeaderText = "ID";
            this.ColId.Name = "ColId";
            this.ColId.ReadOnly = true;
            // 
            // ColServerId
            // 
            this.ColServerId.DataPropertyName = "app_id";
            this.ColServerId.HeaderText = "应用ID";
            this.ColServerId.Name = "ColServerId";
            this.ColServerId.ReadOnly = true;
            this.ColServerId.Width = 145;
            // 
            // ColLastAct
            // 
            this.ColLastAct.DataPropertyName = "appname";
            dataGridViewCellStyle1.Format = "G";
            this.ColLastAct.DefaultCellStyle = dataGridViewCellStyle1;
            this.ColLastAct.HeaderText = "应用名";
            this.ColLastAct.Name = "ColLastAct";
            this.ColLastAct.ReadOnly = true;
            this.ColLastAct.Width = 120;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "app_memo";
            this.Column1.HeaderText = "应用备注";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // FrmMngApp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1189, 771);
            this.Controls.Add(this.dgvServer);
            this.Controls.Add(this.lblMsg);
            this.Controls.Add(this.btnDel);
            this.Controls.Add(this.btnEdit);
            this.Controls.Add(this.btnAdd);
            this.Name = "FrmMngApp";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "应用管理";
            this.Load += new System.EventHandler(this.FrmMngItem_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvServer)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblMsg;
        private System.Windows.Forms.Button btnDel;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.DataGridView dgvServer;
        private System.Windows.Forms.DataGridViewCheckBoxColumn MyCheck;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColId;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColServerId;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColLastAct;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
    }
}