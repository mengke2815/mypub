﻿using System;

namespace CommonUtils
{
    public static class MU
    {
        /// <summary>
        /// 获取异常消息，包含子异常消息
        /// </summary>
        /// <param name="ex"></param>
        /// <returns></returns>
        public static string GED(Exception ex)
        {
            string msg = "";
            if (ex == null)
                return msg;
            msg = ex.Message;
            if (ex.InnerException != null)
            {
                msg = msg + " 内层错误：" + ex.InnerException.Message;
            }
            return msg;
        }
    }
}
