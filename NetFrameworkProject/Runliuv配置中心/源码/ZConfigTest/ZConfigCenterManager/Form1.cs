﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ZConfigCenterManager.TheForm.APPL;
using CommonUtils;
using System.Threading.Tasks;
using ZConfigCenter.DbModels;
using ZConfigCenter.DbCtx;
using ZConfigCenterManager.TheForm.MyConfig;
using ZConfigCenterManager.TheForm.History;

namespace ZConfigCenterManager
{
    public partial class Form1 : Form
    {
        #region VAR
        /// <summary>
        /// checkbox列名:MyCheck
        /// </summary>
        string _selCol = "MyCheck";
        #endregion

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            try
            {
                this.Text = this.Text + " 版本：" + AppVerUtil.GetAppVer();
                LoadItem();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// 刷新应用Combobox
        /// </summary>
        void LoadItem()
        {
            try
            {
                lblMsg.Text = "正在加载……";
                Task.Factory.StartNew(() =>
                {
                    IEnumerable<t_app> lstItems = new List<t_app>();
                    try
                    {
                        using (MainDbContext db = new MainDbContext())
                        {
                            lstItems = db.t_app.ToList();
                        }
                    }
                    catch (Exception ex1)
                    {
                        this.BeginInvoke(new Action(() =>
                        {
                            MessageBox.Show("ex1:" + ex1.Message);
                        }));
                    }
                    //update UI
                    this.BeginInvoke(new Action(() =>
                    {
                        if (lstItems.Count() > 0)
                        {
                            cbxApps.DisplayMember = "appname";
                            cbxApps.ValueMember = "app_id";
                            cbxApps.DataSource = lstItems;
                        }
                        lblMsg.Text = "";
                    }));
                });
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void tsmiMngApp_Click(object sender, EventArgs e)
        {
            FrmMngApp frm = new FrmMngApp();
            frm.ShowDialog();

            LoadItem();
        }

        private void btnRefreshAppL_Click(object sender, EventArgs e)
        {
            LoadItem();
        }

        /// <summary>
        /// 刷新参数GRID
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbxApps_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (cbxApps.SelectedValue == null)
                    return;

                string value = cbxApps.SelectedValue.ToString();
                MainDbContext db = new MainDbContext();
                IEnumerable<t_config> lstServer = db.t_config.Where(x => x.app_id == value).ToList();
                dgvServer.DataSource = lstServer;
                db.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnRefreshList_Click(object sender, EventArgs e)
        {
            //刷新
            cbxApps_SelectedValueChanged(sender, e);
        }

        private void btnAddConfig_Click(object sender, EventArgs e)
        {
            try
            {
                if (cbxApps.SelectedValue == null)
                {
                    MessageBox.Show("下拉列表未选择。");
                    return;
                }

                string appId = cbxApps.SelectedValue.ToString();

                FrmAddConfig frm = new FrmAddConfig(false, "0", appId);
                frm.ShowDialog();
                //刷新
                cbxApps_SelectedValueChanged(sender, e);
            }
            catch (Exception ex)
            {
                MessageBox.Show(MU.GED(ex));
            }

        }

        private void btnEditConfig_Click(object sender, EventArgs e)
        {
            try
            {
                string editId = "0";
                bool anyCheck = false;
                foreach (DataGridViewRow dgvr in dgvServer.Rows)
                {
                    if (dgvr.Cells[_selCol].Value != null && dgvr.Cells[_selCol].Value.ToString() == "1")
                    {
                        anyCheck = true;
                        editId = dgvr.Cells["ColId"].Value.ToString();
                        break;
                    }
                }
                if (!anyCheck)
                {
                    MessageBox.Show("未选择任何项");
                    return;
                }

                if (cbxApps.SelectedValue == null)
                {
                    MessageBox.Show("下拉列表未选择。");
                    return;
                }

                string appId = cbxApps.SelectedValue.ToString();

                FrmAddConfig frm = new FrmAddConfig(true, editId, appId);
                frm.ShowDialog();
                //刷新
                cbxApps_SelectedValueChanged(sender, e);
            }
            catch (Exception ex)
            {
                MessageBox.Show(MU.GED(ex));
            }
        }

        private void btnDelConfig_Click(object sender, EventArgs e)
        {
            try
            {
                string editId = "0";
                bool anyCheck = false;
                string varId = "";
                string varName = "";
                foreach (DataGridViewRow dgvr in dgvServer.Rows)
                {
                    if (dgvr.Cells[_selCol].Value != null && dgvr.Cells[_selCol].Value.ToString() == "1")
                    {
                        anyCheck = true;
                        editId = dgvr.Cells["ColId"].Value.ToString();
                        varId = dgvr.Cells["ColVarId"].Value.ToString();
                        varName = dgvr.Cells["ColVarName"].Value.ToString();
                        break;
                    }
                }
                if (!anyCheck)
                {
                    MessageBox.Show("未选择任何项");
                    return;
                }

                if (cbxApps.SelectedValue == null)
                {
                    MessageBox.Show("下拉列表未选择。");
                    return;
                }

                string confirmMsg = string.Format("确定删除参数 {0} ？ 一次只能删除一条。", varId + " " + varName);

                DialogResult dr = MessageBox.Show(confirmMsg, "确定"
                    , MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                if (dr == DialogResult.No)
                {
                    return;
                }

                string appId = cbxApps.SelectedValue.ToString();

                MainDbContext db = new MainDbContext();
                var t = db.t_config.First(x => x.id == editId);

                #region 修改历史

                t_config_history his = new t_config_history();
                his.id = Guid.NewGuid().ToString("N");
                his.app_id = appId;
                his.var_id = t.var_id;
                his.old_var_value = t.var_value;
                his.new_var_value = t.var_value;
                his.history_type = "DEL";
                his.create_time = DateTime.Now;
                his.update_time = DateTime.Now;
                db.t_config_history.Add(his);

                #endregion

                db.t_config.Remove(t);

                db.SaveChanges();
                db.Dispose();
                MessageBox.Show("成功！");

                //刷新
                cbxApps_SelectedValueChanged(sender, e);
            }
            catch (Exception ex)
            {
                MessageBox.Show(MU.GED(ex));
            }
        }

        private void btnViewHistory_Click(object sender, EventArgs e)
        {
            if (cbxApps.SelectedValue == null)
            {
                MessageBox.Show("下拉列表未选择。");
                return;
            }
            string appId = cbxApps.SelectedValue.ToString();

            FrmViewHistory frm = new FrmViewHistory(appId);
            frm.ShowDialog();

        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                if (cbxApps.SelectedValue == null)
                {
                    MessageBox.Show("下拉列表选择为空。");
                    return;
                }

                string value = cbxApps.SelectedValue.ToString();
                MainDbContext db = new MainDbContext();
                IEnumerable<t_config> lstServer = db.t_config.Where(x => x.app_id == value).ToList();
                if (!string.IsNullOrWhiteSpace(txtSearch.Text))
                {
                    string s = txtSearch.Text;
                    lstServer = db.t_config.Where(x => x.app_id == value &&
                    (x.var_id.Contains(s) || x.var_name.Contains(s) || x.var_memo.Contains(s) || x.var_value.Contains(s))).ToList();
                }
                dgvServer.DataSource = lstServer;
                db.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(MU.GED(ex));
            }
        }

        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnSearch_Click(sender,e);
            }
        }

        private void btnAddPreset_Click(object sender, EventArgs e)
        {

        }

        private void btnViewPreset_Click(object sender, EventArgs e)
        {

        }
    }
}
