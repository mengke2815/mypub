﻿using System;
using System.Windows.Forms;
using ZConfigCenter;

namespace ZConfigTest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void Form1_Shown(object sender, EventArgs e)
        {

        }

        private void btnTest_Click(object sender, EventArgs e)
        {
            try
            {
                string varValue = string.Empty;
                if (cbxGet2.Checked)
                    varValue = ZConfig.Get2("psz");
                else
                    varValue = ZConfig.Get("psz");

                MessageBox.Show(varValue);

            }
            catch (Exception ex)
            {
                string exMsg = ex.Message;
                if (ex.InnerException != null) exMsg += ex.InnerException.Message;
                MessageBox.Show(ex.Message);
            }
        }
    }
}
