

--应用表
if not exists ( select 1 from sysobjects where id=object_id('t_app') )
CREATE TABLE  t_app(
	id varchar(36) NOT NULL,--  ID	 
	app_id varchar(64) NOT NULL,-- 应用ID	
	appname nvarchar(256) NOT NULL, -- 应用名
	app_memo nvarchar(512)  NULL, -- 应用备注
	create_time datetime NOT NULL constraint df_t_app_create_time default(getdate()),
	update_time datetime NOT NULL constraint df_t_app_update_time default(getdate()),		
 CONSTRAINT pk_t_app_id PRIMARY KEY CLUSTERED 
	(
		Id ASC
	)  
)  
go

--唯一索引 (应用ID)
if not exists (select * from sysindexes where name = 'idx_t_app_unique1') 
begin
	create unique index idx_t_app_unique1 on t_app (app_id)
end
go



--配置表
if not exists ( select 1 from sysobjects where id=object_id('t_config') )
CREATE TABLE  t_config(
	id varchar(36) NOT NULL,--  ID	 
	app_id varchar(64) NOT NULL,-- 应用ID
	var_id varchar(128) NOT NULL,	-- 参数ID
	var_name nvarchar(128) NOT NULL, -- 参数名
	var_memo nvarchar(128)  NULL, -- 参数备注
	var_value varchar(7999) NOT NULL,-- 参数值
	create_time datetime NOT NULL constraint df_t_config_create_time default(getdate()),
	update_time datetime NOT NULL constraint df_t_config_update_time default(getdate()),		
 CONSTRAINT pk_t_config_id PRIMARY KEY CLUSTERED 
	(
		Id ASC
	)  
)  
go

--唯一索引 (应用ID+参数ID)
if not exists (select * from sysindexes where name = 'idx_t_config_unique1') 
begin
	create unique index idx_t_config_unique1 on t_config (app_id,var_id)
end
go



--配置更改历史表
if not exists ( select 1 from sysobjects where id=object_id('t_config_history') )
CREATE TABLE  t_config_history(
	id varchar(36) NOT NULL,--  ID	 
	app_id varchar(64) NOT NULL,-- 应用ID
	var_id varchar(128) NOT NULL,	-- 参数ID	
	old_var_value varchar(7999) NOT NULL,-- 参数值,旧
	new_var_value varchar(7999) NOT NULL,-- 参数值,新
	history_type varchar(36) NOT NULL,--  操作类型 
	create_time datetime NOT NULL constraint df_t_config_history_create_time default(getdate()),
	update_time datetime NOT NULL constraint df_t_config_history_update_time default(getdate()),		
 CONSTRAINT pk_t_config_history_id PRIMARY KEY CLUSTERED 
	(
		Id ASC
	)  
)  
go

--配置预设表
if not exists ( select 1 from sysobjects where id=object_id('t_config_preset') )
CREATE TABLE  t_config_preset(
	id varchar(36) NOT NULL,--  ID	 
	app_id varchar(64) NOT NULL,-- 应用ID
	var_id varchar(128) NOT NULL,	-- 参数ID
	var_name nvarchar(128) NOT NULL, -- 参数名
	var_memo nvarchar(128)  NULL, -- 参数备注
	var_value varchar(7999) NOT NULL,-- 参数值
	create_time datetime NOT NULL constraint df_t_config_preset_create_time default(getdate()),
	update_time datetime NOT NULL constraint df_t_config_preset_update_time default(getdate()),		
 CONSTRAINT pk_t_config_preset_id PRIMARY KEY CLUSTERED 
	(
		Id ASC
	)  
)  
go


