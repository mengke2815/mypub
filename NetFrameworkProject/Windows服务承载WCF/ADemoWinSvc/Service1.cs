﻿using CommonUtils;
using System;
using System.ServiceModel;
using System.ServiceProcess;
using WcfYeah;

namespace ADemoWinSvc
{
    public partial class Service1 : ServiceBase
    {
        WCFServer aw = null;
        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            try            {                if (aw == null)                {                    aw = new WCFServer();                }                if (aw.host.State == CommunicationState.Opening || aw.host.State == CommunicationState.Opened)                {                    GLog.WLog("[1]服务已启动。");                    return;                }

                aw.Start();

                GLog.WLog("[2]服务已启动");            }            catch (Exception ex)            {                GLog.WLog("OnStart ex:" + ex.Message);            }
        }

        protected override void OnStop()
        {
            try            {                aw.Close();                GLog.WLog("服务已停止");            }            catch (Exception ex)            {                GLog.WLog("OnStop ex:" + ex.Message);            }
        }
    }
}
