﻿using System;
using System.ServiceModel;

namespace WcfYeah
{
    // 调整 ServiceBehavior，使其支持并发
    [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.PerCall, UseSynchronizationContext = false)]
    public class Service1 : IService1
    {
        public CompositeType geta(CompositeType composite)
        {
            CompositeType myret = new CompositeType();
            try
            {
                if(composite==null)
                    myret.StringValue = "[0]输入实体为空:" + DateTime.Now.ToString();
                else
                    myret.StringValue = "[1]输入实体不为空:" + DateTime.Now.ToString();
            }
            catch (Exception ex)
            {
                myret.StringValue = "发生异常:" + ex.Message;                 
            }
            return myret;
        }

    }
}
