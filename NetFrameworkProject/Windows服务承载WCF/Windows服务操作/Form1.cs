﻿using CommonUtils;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Windows服务操作
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// windows服务名
        /// </summary>
        static string _winSvcName = "ADemoWinSvc";

        /// <summary>
        /// windows服务对应的exe 名
        /// </summary>
        static string _winSvcExeName = "ADemoWinSvc.exe";

        private void btnSetup_Click(object sender, EventArgs e)
        {
            try
            {
                //是否存在服务
                if (ServiceUtil.ServiceIsExisted(_winSvcName))
                {
                    //已存在，检查是否启动
                    if (ServiceUtil.IsRun(_winSvcName))
                    {
                        //服务是已启动状态
                        lblMsg.Text = "[001]服务是已启动状态";
                    }
                    else
                    {
                        //未启动，则启动
                        ServiceUtil.StarService(_winSvcName);
                        lblMsg.Text = "[002]服务是已启动状态";
                    }
                }
                else
                {
                    //不存在，则安装
                    IDictionary mySavedState = new Hashtable();
                    string curPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().ManifestModule.FullyQualifiedName);
                    string apppath = Path.Combine(curPath, _winSvcExeName);
                    ServiceUtil.InstallService(mySavedState, apppath);

                    lblMsg.Text = "[003]服务是已启动状态";

                    //安装后并不会自动启动。需要启动这个服务
                    ServiceUtil.StarService(_winSvcName);

                    lblMsg.Text = "[004]服务是已启动状态";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnUninstall_Click(object sender, EventArgs e)
        {
            //** 卸载服务最重要的是先停止，否则电脑需要重启或注销 **
            try
            {
                //是否存在服务
                if (!ServiceUtil.ServiceIsExisted(_winSvcName))
                {
                    MessageBox.Show("服务不存在，不需要卸载");
                    return;
                }

                //如果服务正在运行，停止它
                if (ServiceUtil.IsRun(_winSvcName))
                {
                    ServiceUtil.StopService(_winSvcName);
                    //也可以调用 net stop 命令来停止服务。
                }
                //再检查一次是否在运行
                if (ServiceUtil.IsRun(_winSvcName))
                {
                    MessageBox.Show("服务无法停止,请手动停止这个服务");
                    return;
                }
                //卸载
                ServiceUtil.UnInstallServiceByName(_winSvcName);

                lblMsg.Text = "[005]服务已卸载";

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
