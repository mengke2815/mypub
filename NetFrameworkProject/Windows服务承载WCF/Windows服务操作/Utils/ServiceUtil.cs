﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration.Install;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;

namespace CommonUtils
{
    /// <summary>
    /// windows服务操作工具类
    /// </summary>
    public static class ServiceUtil
    {
        /// <summary>
        /// 服务是否正在运行
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static bool IsRun(string name)
        {
            bool IsRun = false;
            try
            {
                if (!ServiceIsExisted(name)) return false;
                var sc = new ServiceController(name);
                if (sc.Status == ServiceControllerStatus.StartPending || sc.Status == ServiceControllerStatus.Running)
                {
                    IsRun = true;
                }
                sc.Close();
            }
            catch
            {
                IsRun = false;
            }
            return IsRun;
        }

        /// <summary>
        /// 启动服务
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static bool StarService(string name)
        {
            try
            {
                var sc = new ServiceController(name);
                if (sc.Status == ServiceControllerStatus.Stopped || sc.Status == ServiceControllerStatus.StopPending)
                {
                    sc.Start();
                    sc.WaitForStatus(ServiceControllerStatus.Running, new TimeSpan(0, 0, 10));
                }
                else
                {

                }
                sc.Close();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 停止服务(有可能超时)
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static bool StopService(string name)
        {
            try
            {
                var sc = new ServiceController(name);
                if (sc.Status == ServiceControllerStatus.Running || sc.Status == ServiceControllerStatus.StartPending)
                {
                    sc.Stop();
                    //停止服务超时时间：56秒。
                    sc.WaitForStatus(ServiceControllerStatus.Stopped, new TimeSpan(0, 0, 56));
                }
                else
                {

                }
                sc.Close();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 是否存在
        /// </summary>
        /// <param name="serviceName"></param>
        /// <returns></returns>
        public static bool ServiceIsExisted(string serviceName)
        {
            ServiceController[] services = ServiceController.GetServices();
            foreach (ServiceController s in services)
                if (s.ServiceName.ToLower() == serviceName.ToLower())
                    return true;
            return false;
        }

        /// <summary>
        /// 安装
        /// </summary>
        /// <param name="stateSaver"></param>
        /// <param name="filepath"></param>
        public static void InstallService(IDictionary stateSaver, string filepath)
        {
            try
            {
                AssemblyInstaller myAssemblyInstaller = new AssemblyInstaller();
                myAssemblyInstaller.UseNewContext = true;
                myAssemblyInstaller.Path = filepath;
                myAssemblyInstaller.Install(stateSaver);
                myAssemblyInstaller.Commit(stateSaver);
                myAssemblyInstaller.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 使用路径卸载(有时候不便于用路径来卸载，则使用SC DELETE 名称来卸载)
        /// </summary>
        /// <param name="filepath"></param>
        public static void UnInstallService(string filepath)
        {
            try
            {
                AssemblyInstaller myAssemblyInstaller = new AssemblyInstaller();
                myAssemblyInstaller.UseNewContext = true;
                myAssemblyInstaller.Path = filepath;
                myAssemblyInstaller.Uninstall(null);
                myAssemblyInstaller.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 使用windows服务名卸载
        /// </summary>
        /// <param name="WinServiceName"></param>
        public static void UnInstallServiceByName(string WinServiceName)
        {
            ProcessStartInfo pStart = new ProcessStartInfo("sc.exe");
            Process pRoc = new Process();

            pStart.Arguments = " delete " + WinServiceName;
            pStart.UseShellExecute = false;
            pStart.CreateNoWindow = false;

            pRoc.StartInfo = pStart;
            pRoc.Start();
            pRoc.WaitForExit();
        }





        

    }
}
