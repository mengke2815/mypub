﻿using CommonUtils;
using FastNetSwitch.Models;
using FastNetSwitch.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

namespace FastNetSwitch
{
    public partial class Form1 : Form
    {
        #region MyRegion

        List<TestSource> _lstTestSource = new List<TestSource>();

        /// <summary>
        /// 上次选中的网卡接口索引
        /// </summary>
        string _lastInterfaceIndex = "-1";

        /// <summary>
        /// 停止标识
        /// </summary>
        bool _stopFlag = false;
        #endregion

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            this.Text = this.Text + " - 版本:" + GetAppVer();

            _lstTestSource.Add(new TestSource { Name = "京东", Url = "https://www.jd.com/", IP = "61.139.244.3" });
            _lstTestSource.Add(new TestSource { Name = "百度", Url = "https://www.baidu.com/", IP = "14.215.177.38" });
            _lstTestSource.Add(new TestSource { Name = "博客园", Url = "https://www.cnblogs.com/", IP = "114.55.205.139" });
            _lstTestSource.Add(new TestSource { Name = "淘宝", Url = "https://www.taobao.com/", IP = "113.105.168.244" });

            //if (string.IsNullOrWhiteSpace(txtTestSpeedUrl.Text))
            //{
            //    txtTestSpeedUrl.Text = "https://www.baidu.com/";
            //}
            //if (string.IsNullOrWhiteSpace(txtTestSpeedIp.Text))
            //{
            //    txtTestSpeedIp.Text = "14.215.177.38";
            //}
        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            cbxTestSource.DataSource = _lstTestSource.Select(x => x.Name).ToList();
            cbxTestSource.SelectedIndex = 0;
        }

        private void btnTest_Click(object sender, EventArgs e)
        {
            try
            {
                //HTTP测试网速
                string testUrl = txtTestSpeedUrl.Text;
                string testIp = txtTestSpeedIp.Text;

                if (string.IsNullOrWhiteSpace(testUrl) || string.IsNullOrWhiteSpace(testIp))
                {
                    MessageBox.Show("测速网址和IP不能为空！");
                    return;
                }

                //URL,IP,SleepTimeInterval,
                StartParam sp = new StartParam();
                if (cbxUseAllTestSource.Checked)
                {
                    sp.LstTestSource = _lstTestSource;
                }
                else
                {
                    sp.LstTestSource = new List<TestSource>();
                    sp.LstTestSource.Add(new TestSource { Url = testUrl, IP = testIp });
                }
                sp.SleepTimeInterval = int.Parse(nudSleepInterval.Value.ToString());

                DoIt(sp);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        void DoIt(StartParam sp)
        {


            if (sp != null && sp.LstTestSource != null && sp.LstTestSource.Count > 0)
            {
                //var aa = sp.LstTestSource[0];
                //string testUrl2 = aa.Url;
                //string testIp2 = aa.IP;


            }
            else
            {
                LogInfo("无测试源传入。");
                return;
            }

            #region 得到待切换网卡

            List<SwitchModel> lstToSwitch = new List<SwitchModel>();
            NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();

            foreach (var adapter in nics)
            {

                #region 有些网卡不在处理范围
                if (!adapter.Supports(NetworkInterfaceComponent.IPv4))
                {
                    LogDebug(adapter.Description + " 不支持IPV4。");
                    continue;
                }
                if (adapter.NetworkInterfaceType == NetworkInterfaceType.Loopback)
                {
                    LogDebug(adapter.Description + " Loopback，跳过。");
                    continue;
                }
                //有些网卡不在处理范围
                if (SkipNet(adapter.Description))
                {
                    LogDebug("跳过的网卡：" + adapter.Description);
                    continue;
                }
                #endregion

                if (adapter.OperationalStatus == OperationalStatus.Down)
                {
                    LogDebug("网卡未连接：" + adapter.Description);
                    continue;
                }

                IPInterfaceProperties properties = adapter.GetIPProperties();
                if (properties.GatewayAddresses.Count == 0)
                {
                    LogDebug("无任何网关：" + adapter.Description);
                    continue;
                }
                //找到网卡接口索引，使用route命令时用到。
                IPv4InterfaceProperties ipv4 = properties.GetIPv4Properties();
                string InterfaceIndex = ipv4.Index.ToString();

                string lineMsg = InterfaceIndex + " || " + adapter.Description;
                LogDebug(lineMsg);

                //找到IPV4网关。
                var ipv4GatewayAddress = properties.GatewayAddresses.FirstOrDefault(x => x.Address.AddressFamily == AddressFamily.InterNetwork);
                if (ipv4GatewayAddress == null)
                {
                    LogDebug("无IPV4网关：" + adapter.Description);
                    continue;
                }
                string GatewayAddress = ipv4GatewayAddress.Address.ToString();

                if (lstToSwitch.Any(x => x.GatewayAddress == GatewayAddress))
                {
                    //LogInfo("重复网关：" + InterfaceIndex + " " + adapter.Description);
                    //continue;
                }

                lstToSwitch.Add(new SwitchModel
                {
                    InterfaceIndex = InterfaceIndex,
                    GatewayAddress = GatewayAddress,
                    Description = adapter.Description,
                    Name = adapter.Name
                });
            }

            #endregion

            //日志
            LogDebug("挑选出的网卡。");
            foreach (var item in lstToSwitch)
            {
                LogDebug(item.InterfaceIndex + " " + item.Description + " " + item.GatewayAddress + " " + item.Name);
            }

            if (lstToSwitch.Count == 1)
            {
                LogInfo("只有一张网卡，不可切换。");
                return;
            }



            int routeSleep = 900;
            int testSleep = 600;
            int httpTimeOut = 6;
#if DEBUG
            routeSleep = 100;
            testSleep = 300;
            httpTimeOut = 6;
#endif

            //循环网卡
            foreach (var item in lstToSwitch)
            {
                //循环测试源
                foreach (var testSource in sp.LstTestSource)
                {
                    string testIp = testSource.IP;
                    string testUrl = testSource.Url;

                    if (_stopFlag)//停止标识为TURE 时，停止循环。
                    {
                        return;
                    }

                    //用 route 命令切换成当前网卡。随后开始测试。
                    //route delete 172.25.100.125
                    //route add 172.25.100.125 mask 255.255.255.255 128.0.100.198
                    if (!RouteDel(testIp))
                    {
                        LogInfo("路由命令 delete 执行失败，可尝试用管理员权限运行本程序。");
                        return;
                    }
                    if (!RouteAdd(testIp, item.GatewayAddress,"1",item.InterfaceIndex))
                    {
                        LogInfo("路由命令 add 执行失败，可尝试用管理员权限运行本程序。");
                        return;
                    }
                    //防止路由不生效，停  
                    System.Threading.Thread.Sleep(routeSleep);

                    for (int i = 0; i < 6; i++)
                    {
                        if (_stopFlag)//停止标识为TURE 时，停止循环。
                        {
                            return;
                        }

                        HttpTestRst rst = HttpTest(testUrl, httpTimeOut);
                        if (rst.HttpEx)
                            item.HttpExCount += 1;
                        item.TotalMs += rst.TotalMs;

                        LogDebug("用于测试网速的网址：" + testUrl + "和IP：" + testIp + "耗时：" + rst.TotalMs.ToString());

                        //防止 频率过高被BAN IP，停  
                        System.Threading.Thread.Sleep(testSleep);
                    }
                }
            }

            //日志
            LogInfo(MU.DN() + " HTTP测试结果。");
            foreach (var item in lstToSwitch)
            {
                LogInfo("网卡接口索引："+item.InterfaceIndex + " 网关：" + item.GatewayAddress
                    + " 错误次数：" + item.HttpExCount.ToString() + " 总耗时：" + (item.TotalMs / 1000M).ToString("F2")
                     + "秒，描述：" + item.Description + " 网卡名：" + item.Name);
            }

            //测试结束，选择最快的网卡
            var priorityAdapter = lstToSwitch.OrderBy(a => a.HttpExCount).ThenBy(a => a.TotalMs).FirstOrDefault();

            LogInfo("决定使用本网卡来上网：网卡接口索引：" + priorityAdapter.InterfaceIndex + 
                " 描述：" + priorityAdapter.Description
                + " 网关：" + priorityAdapter.GatewayAddress + " 网卡名：" + priorityAdapter.Name
                    + " 错误次数：" + priorityAdapter.HttpExCount.ToString() + " 耗时：" + (priorityAdapter.TotalMs / 1000M).ToString("F2")
                     + "秒。");

            if (priorityAdapter.InterfaceIndex == _lastInterfaceIndex)
            {
                LogInfo("与上次首选网上索引一致，不切换。");
                return;
            }

            foreach (var item in lstToSwitch)
            {
                //调高其它网卡跃点数
                //调低“首选网卡”跃点数
                string Metric = "99";
                if (item.InterfaceIndex == priorityAdapter.InterfaceIndex)
                {
                    Metric = "1";
                }
                if (!RouteChange(item.GatewayAddress, Metric, item.InterfaceIndex))
                {
                    LogDebug("路由命令 change 执行失败，可尝试用管理员权限运行本程序。");
                    return;
                }
            }
            //更新上次网上索引
            _lastInterfaceIndex = priorityAdapter.InterfaceIndex;
            LogInfo("调整结束，无异常。");
        }

        HttpTestRst HttpTest(string url, int timeOut)
        {
            HttpTestRst rstModel = new HttpTestRst();

            Stopwatch swt = Stopwatch.StartNew();
            try
            {
                int timeOut2 = timeOut * 1000;
                string rst = "";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "GET";
                request.Timeout = timeOut2;
                request.ReadWriteTimeout = timeOut2;
                request.KeepAlive = false;
                request.ServicePoint.Expect100Continue = false;

                HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                using (Stream stream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(stream);
                    rst = reader.ReadToEnd();
                }
                rstModel.HttpRspBody = rst;
            }
            catch (Exception ex)
            {
                rstModel.HttpEx = true;
            }
            swt.Stop();
            rstModel.TotalMs = swt.ElapsedMilliseconds;
            return rstModel;
        }

        bool RouteDel(string ip)
        {
            if (string.IsNullOrWhiteSpace(ip))
            {
                return false;
            }
            string cmdLine = "delete " + ip;
            ProcessStartInfo si = new ProcessStartInfo();

            si.FileName = "route";
            si.Arguments = cmdLine;
            si.UseShellExecute = false;
            si.RedirectStandardOutput = true;
            si.CreateNoWindow = true;
            Process ps = new Process();
            ps.StartInfo = si;
            ps.Start();
            //获取cmd窗口的输出信息
            string successJudge = ps.StandardOutput.ReadToEnd();
            //List<string> lstOut = new List<string>();
            //using (StreamReader sr = ps.StandardOutput)
            //{
            //    while (sr.Peek() >= 0)
            //    {
            //        lstOut.Add(sr.ReadLine());
            //    }
            //}
            ps.WaitForExit();

            if (!string.IsNullOrWhiteSpace(successJudge) && successJudge.Contains("提升"))
            {
                return false;
            }

            return true;
        }

        bool RouteAdd(string ip, string GatewayAddress, string Metric, string InterfaceIndex)
        {
            if (string.IsNullOrWhiteSpace(ip))
            {
                return false;
            }
            if (string.IsNullOrWhiteSpace(GatewayAddress) || string.IsNullOrWhiteSpace(Metric) || string.IsNullOrWhiteSpace(InterfaceIndex))
            {
                return false;
            }
            //route add 172.25.100.125 mask 255.255.255.255 128.0.100.198
            //string cmdLine = string.Format("add {0} mask 255.255.255.255 {1}", ip, GatewayAddress);
            //route ADD 157.0.0.0 MASK 255.0.0.0  157.55.80.1 METRIC 3 IF 2
            string cmdLine = string.Format("add {0} mask 255.255.255.255 {1} metric {2} if {3}", ip, GatewayAddress, Metric, InterfaceIndex);

            ProcessStartInfo si = new ProcessStartInfo();

            si.FileName = "route";
            si.Arguments = cmdLine;
            si.UseShellExecute = false;
            si.RedirectStandardOutput = true;
            si.CreateNoWindow = true;
            Process ps = new Process();
            ps.StartInfo = si;
            ps.Start();
            //获取cmd窗口的输出信息
            string successJudge = ps.StandardOutput.ReadToEnd();
            
            ps.WaitForExit();

            if (!string.IsNullOrWhiteSpace(successJudge) && successJudge.Contains("提升"))
            {
                return false;
            }

            return true;
        }

        bool RouteChange(string GatewayAddress, string Metric, string InterfaceIndex)
        {
            if (string.IsNullOrWhiteSpace(GatewayAddress) || string.IsNullOrWhiteSpace(Metric) || string.IsNullOrWhiteSpace(InterfaceIndex))
            {
                return false;
            }
            //route change 0.0.0.0 mask 0.0.0.0 192.168.10.2 metric 10 if 19
            //192.168.10.2是网关，10是跃点数，19是route print -4中的接口
            string cmdLine = string.Format("change 0.0.0.0 mask 0.0.0.0 {0} metric {1} if {2}", GatewayAddress, Metric, InterfaceIndex);
            ProcessStartInfo si = new ProcessStartInfo();

            si.FileName = "route";
            si.Arguments = cmdLine;
            si.UseShellExecute = false;
            si.RedirectStandardOutput = true;
            si.CreateNoWindow = true;
            Process ps = new Process();
            ps.StartInfo = si;
            ps.Start();
            //获取cmd窗口的输出信息
            string successJudge = ps.StandardOutput.ReadToEnd();
             
            ps.WaitForExit();

            if (!string.IsNullOrWhiteSpace(successJudge) && successJudge.Contains("提升"))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// 常规日志，只写TXT
        /// </summary>
        /// <param name="msg"></param>
        void LogInfo(string msg)
        {
            //Log4NetUtil.Info(msg);
            Log4NetUtil._log.Info(msg);
            LogListBox(msg);
        }

        /// <summary>
        /// 即写UI，也写TXT
        /// </summary>
        /// <param name="msg"></param>
        void LogDebug(string msg)
        {
            Log4NetUtil._log.Debug(msg);
            //LogListBox(msg);
        }

        void LogListBox(string msg)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new MethodInvoker(() =>
                {
                    if (lbxLog.Items.Count > 1999)
                    {
                        lbxLog.Items.Clear();
                    }
                    lbxLog.Items.Insert(0, msg);
                }));
            }
            else
            {
                if (lbxLog.Items.Count > 1999)
                {
                    lbxLog.Items.Clear();
                }
                lbxLog.Items.Insert(0, msg);
            }
        }


        /// <summary>
        /// 跳过网卡
        /// </summary>
        /// <param name="myDesc"></param>
        /// <returns></returns>
        bool SkipNet(string myDesc)
        {
            if (string.IsNullOrWhiteSpace(myDesc))
                return false;

            if (myDesc.Contains("Software Loopback Interface"))
                return true;

            if (myDesc.Contains("Microsoft Wi-Fi Direct Virtual Adapter"))
                return true;

            if (myDesc.Contains("Bluetooth Device"))
                return true;

            if (myDesc.Contains("Microsoft Teredo Tunneling Adapter"))
                return true;

            return false;
        }

        private void cbxTestSource_SelectedIndexChanged(object sender, EventArgs e)
        {
            //MessageBox.Show(cbxTestSource.Text);
            string myName = cbxTestSource.Text;
            var myModel = _lstTestSource.FirstOrDefault(x => x.Name == myName);
            if (myModel != null)
            {
                txtTestSpeedUrl.Text = myModel.Url;
                txtTestSpeedIp.Text = myModel.IP;
            }
        }

        private void tsmiExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnToTray_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void nfiMain_MouseDoubleClick(object sender, MouseEventArgs e)
        {

        }

        private void nfiMain_DoubleClick(object sender, EventArgs e)
        {
            this.Show();
            this.ShowInTaskbar = true;
        }

        private void bwWork_DoWork(object sender, DoWorkEventArgs e)
        {
            int sleepTime = 90;
            try
            {
                if (e.Argument == null)
                {
                    LogInfo("bwWork_DoWork 无参数传入。");
                    return;
                }
                StartParam sp = e.Argument as StartParam;
                sleepTime = sp.SleepTimeInterval;

                DoIt(sp);
            }
            catch (Exception ex)
            {
                LogInfo("bwWork_DoWork ex:" + ex.Message);
            }
            finally
            {
                int sleepCount = 0;
                while (true)
                {
                    if (_stopFlag)//停止标识为TURE 时，停止循环。
                    {
                        break;
                    }
                    sleepCount += 1;
                    //1秒1次，90秒，90次。
                    System.Threading.Thread.Sleep(1000);
                    if (sleepCount >= sleepTime)
                    {
                        break;
                    }
                }
            }

        }

        private void bwWork_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (!_stopFlag)
            {
                btnAutoStart_Click(null, null);
            }
            else
            {
                btnAutoStart.Enabled = true;
            }
        }

        private void btnAutoStart_Click(object sender, EventArgs e)
        {
            _stopFlag = false;

            //HTTP测试网速
            string testUrl = txtTestSpeedUrl.Text;
            string testIp = txtTestSpeedIp.Text;

            if (string.IsNullOrWhiteSpace(testUrl) || string.IsNullOrWhiteSpace(testIp))
            {
                MessageBox.Show("测速网址和IP不能为空！");
                return;
            }

            //URL,IP,SleepTimeInterval,
            StartParam sp = new StartParam();
            if (cbxUseAllTestSource.Checked)
            {
                sp.LstTestSource = _lstTestSource;
            }
            else
            {
                sp.LstTestSource = new List<TestSource>();
                sp.LstTestSource.Add(new TestSource { Url = testUrl, IP = testIp });
            }
            //sp.URL = testUrl;
            //sp.IP = testIp;
            sp.SleepTimeInterval = int.Parse(nudSleepInterval.Value.ToString());

            bwWork.RunWorkerAsync(sp);

            btnAutoStart.Enabled = false;
        }

        private void btnAutoStop_Click(object sender, EventArgs e)
        {
            _stopFlag = true;
        }

        #region Get Ver

        /// <summary>
        /// 获取当前程序版本.fileversion
        /// </summary>
        /// <returns></returns>
        public static string GetAppVer()
        {
            string currentVer = "1.0.0.0";
            try
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyFileVersionAttribute), false);
                if (attributes.Length > 0)
                {
                    currentVer = ((AssemblyFileVersionAttribute)attributes[0]).Version;
                }
            }
            catch
            { }
            return currentVer;
        }

        #endregion

    }
}
