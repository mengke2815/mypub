﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FastNetSwitch.Models
{
    public class TestSource
    {
        public string Name { get; set; }

        public string Url { get; set; }

        public string IP { get; set; }

    }
}
