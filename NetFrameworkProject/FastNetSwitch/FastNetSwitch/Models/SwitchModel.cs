﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FastNetSwitch.Models
{
    public class SwitchModel
    {
        /// <summary>
        /// 接口列表索引
        /// </summary>
        public string InterfaceIndex { get; set; }

        /// <summary>
        /// 网关
        /// </summary>
        public string GatewayAddress { get; set; }

        public string Description { get; set; }

        public string Name { get; set; }


        public int HttpExCount { get; set; }

        /// <summary>
        /// 耗时,ms
        /// </summary>
        public long TotalMs { get; set; }

    }
}
