﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FastNetSwitch.Models
{
    /// <summary>
    /// 点击开始需要的参数
    /// </summary>
    public class StartParam
    {
        //public string URL { get; set; }

        //public string IP { get; set; }

        public List<TestSource> LstTestSource { get; set; }

        /// <summary>
        /// 秒
        /// </summary>
        public int SleepTimeInterval { get; set; }

    }
}
