﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FastNetSwitch.Models
{
    public class HttpTestRst
    {
        public bool HttpEx { get; set; }

        /// <summary>
        /// 耗时,ms
        /// </summary>
        public long TotalMs { get; set; }

        public string HttpRspBody { get; set; }

    }
}
