﻿using log4net.Config;
using System.IO;
using System.Windows.Forms;

namespace CommonUtils
{
    public static class Log4NetUtil
    {
        public static log4net.ILog _log;

        /// <summary>
        /// 利用静态构造函数，只实例化一次。
        /// </summary>
        static Log4NetUtil()
        {
            //Application.StartupPath 是WINFORM的，可以根据需要，更换为其它
            string appPath = Application.StartupPath;
            // config，注意位置和名称。
            string logCfg = Path.Combine(appPath, "log4net2.config");
            XmlConfigurator.Configure(new FileInfo(logCfg));

            //在 Configure 后，再实例化。 log 名随意
            _log = log4net.LogManager.GetLogger("OH");//获取一个日志记录器
        }

        public static void Info(string msg)
        {
            _log.Info(msg);//写入一条新log
        }

    }
}