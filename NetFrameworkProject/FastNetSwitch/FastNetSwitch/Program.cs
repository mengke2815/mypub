﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Windows.Forms;

namespace FastNetSwitch
{
    internal static class Program
    {
        static System.Threading.Mutex appMutex;

        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            #region MyMutex
            string exeName = "FastNetSwitch";
            string appName = "FastNetSwitch";

            bool createNew;
            appMutex = new System.Threading.Mutex(true, exeName, out createNew);
            if (!createNew)
            {
                appMutex.Close();
                appMutex = null;
                MessageBox.Show(appName + "已开启，进程为" + exeName + "！", "提示");
                return;
            }
            #endregion

            Application.ApplicationExit += new EventHandler(OnApplicationExit);

            #region 优化调整
            //对外连接数，根据实际情况加大
            if (ServicePointManager.DefaultConnectionLimit < 100)
                ServicePointManager.DefaultConnectionLimit = 100;

            int workerThreads;//工作线程数
            int completePortsThreads; //异步I/O 线程数
            ThreadPool.GetMinThreads(out workerThreads, out completePortsThreads);

            int blogCnt = 100;
            if (workerThreads < blogCnt)
            {
                // MinThreads 值不要超过 (max_thread /2  )，否则会不生效。要不然就同时加大max_thread
                ThreadPool.SetMinThreads(blogCnt, blogCnt);
            }
            #endregion

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }


        #region MyRegionRelMutex

        static void OnApplicationExit(object sender, EventArgs e)
        {             
            RelMutex();
        }

        /// <summary>
        /// 释放
        /// </summary>
        public static void RelMutex()
        {
            try
            {
                if (appMutex != null)
                {
                    appMutex.ReleaseMutex();
                    appMutex.Close();
                }
            }
            catch (Exception expMu) { }
        }
        #endregion

    }
}
