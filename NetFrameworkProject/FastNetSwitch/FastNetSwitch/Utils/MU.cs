﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FastNetSwitch.Utils
{
    public static class MU
    {
        /// <summary>
        /// 当前时间 ,yyyy-MM-dd HH:mm:ss
        /// </summary>
        /// <returns></returns>
        public static string DN()
        {
            return DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        }
    }
}
