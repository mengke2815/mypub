﻿namespace FastNetSwitch
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.btnTest = new System.Windows.Forms.Button();
            this.lbxLog = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtTestSpeedUrl = new System.Windows.Forms.TextBox();
            this.txtTestSpeedIp = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cbxTestSource = new System.Windows.Forms.ComboBox();
            this.nfiMain = new System.Windows.Forms.NotifyIcon(this.components);
            this.cmsTray = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmiExit = new System.Windows.Forms.ToolStripMenuItem();
            this.nudSleepInterval = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnAutoStart = new System.Windows.Forms.Button();
            this.btnAutoStop = new System.Windows.Forms.Button();
            this.btnToTray = new System.Windows.Forms.Button();
            this.bwWork = new System.ComponentModel.BackgroundWorker();
            this.cbxUseAllTestSource = new System.Windows.Forms.CheckBox();
            this.cmsTray.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudSleepInterval)).BeginInit();
            this.SuspendLayout();
            // 
            // btnTest
            // 
            this.btnTest.Location = new System.Drawing.Point(21, 153);
            this.btnTest.Name = "btnTest";
            this.btnTest.Size = new System.Drawing.Size(75, 23);
            this.btnTest.TabIndex = 0;
            this.btnTest.Text = "测试";
            this.btnTest.UseVisualStyleBackColor = true;
            this.btnTest.Click += new System.EventHandler(this.btnTest_Click);
            // 
            // lbxLog
            // 
            this.lbxLog.FormattingEnabled = true;
            this.lbxLog.ItemHeight = 12;
            this.lbxLog.Location = new System.Drawing.Point(142, 179);
            this.lbxLog.Name = "lbxLog";
            this.lbxLog.Size = new System.Drawing.Size(854, 532);
            this.lbxLog.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(390, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 2;
            this.label1.Text = "测速网址：";
            // 
            // txtTestSpeedUrl
            // 
            this.txtTestSpeedUrl.Location = new System.Drawing.Point(471, 45);
            this.txtTestSpeedUrl.Name = "txtTestSpeedUrl";
            this.txtTestSpeedUrl.Size = new System.Drawing.Size(416, 21);
            this.txtTestSpeedUrl.TabIndex = 0;
            // 
            // txtTestSpeedIp
            // 
            this.txtTestSpeedIp.Location = new System.Drawing.Point(471, 72);
            this.txtTestSpeedIp.Name = "txtTestSpeedIp";
            this.txtTestSpeedIp.Size = new System.Drawing.Size(242, 21);
            this.txtTestSpeedIp.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(390, 77);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 4;
            this.label2.Text = "测速IP：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(731, 79);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(269, 12);
            this.label3.TabIndex = 6;
            this.label3.Text = "“测速网址”域名对应的IP，需要手动PING出来。";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(140, 54);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 7;
            this.label4.Text = "测速源：";
            // 
            // cbxTestSource
            // 
            this.cbxTestSource.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxTestSource.FormattingEnabled = true;
            this.cbxTestSource.Location = new System.Drawing.Point(199, 51);
            this.cbxTestSource.Name = "cbxTestSource";
            this.cbxTestSource.Size = new System.Drawing.Size(155, 20);
            this.cbxTestSource.TabIndex = 8;
            this.cbxTestSource.SelectedIndexChanged += new System.EventHandler(this.cbxTestSource_SelectedIndexChanged);
            // 
            // nfiMain
            // 
            this.nfiMain.ContextMenuStrip = this.cmsTray;
            this.nfiMain.Icon = ((System.Drawing.Icon)(resources.GetObject("nfiMain.Icon")));
            this.nfiMain.Text = "切换快网";
            this.nfiMain.Visible = true;
            this.nfiMain.DoubleClick += new System.EventHandler(this.nfiMain_DoubleClick);
            this.nfiMain.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.nfiMain_MouseDoubleClick);
            // 
            // cmsTray
            // 
            this.cmsTray.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiExit});
            this.cmsTray.Name = "cmsTray";
            this.cmsTray.Size = new System.Drawing.Size(97, 26);
            // 
            // tsmiExit
            // 
            this.tsmiExit.Name = "tsmiExit";
            this.tsmiExit.Size = new System.Drawing.Size(96, 22);
            this.tsmiExit.Text = "&Exit";
            this.tsmiExit.Click += new System.EventHandler(this.tsmiExit_Click);
            // 
            // nudSleepInterval
            // 
            this.nudSleepInterval.Location = new System.Drawing.Point(199, 95);
            this.nudSleepInterval.Maximum = new decimal(new int[] {
            900,
            0,
            0,
            0});
            this.nudSleepInterval.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudSleepInterval.Name = "nudSleepInterval";
            this.nudSleepInterval.Size = new System.Drawing.Size(60, 21);
            this.nudSleepInterval.TabIndex = 10;
            this.nudSleepInterval.Value = new decimal(new int[] {
            90,
            0,
            0,
            0});
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(276, 97);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 12);
            this.label5.TabIndex = 11;
            this.label5.Text = "秒测速一次";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(140, 97);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 12);
            this.label6.TabIndex = 12;
            this.label6.Text = "间隔：";
            // 
            // btnAutoStart
            // 
            this.btnAutoStart.Location = new System.Drawing.Point(21, 339);
            this.btnAutoStart.Name = "btnAutoStart";
            this.btnAutoStart.Size = new System.Drawing.Size(86, 23);
            this.btnAutoStart.TabIndex = 13;
            this.btnAutoStart.Text = "自动处理(&A)";
            this.btnAutoStart.UseVisualStyleBackColor = true;
            this.btnAutoStart.Click += new System.EventHandler(this.btnAutoStart_Click);
            // 
            // btnAutoStop
            // 
            this.btnAutoStop.Location = new System.Drawing.Point(21, 414);
            this.btnAutoStop.Name = "btnAutoStop";
            this.btnAutoStop.Size = new System.Drawing.Size(86, 23);
            this.btnAutoStop.TabIndex = 14;
            this.btnAutoStop.Text = "停止自动(&S)";
            this.btnAutoStop.UseVisualStyleBackColor = true;
            this.btnAutoStop.Click += new System.EventHandler(this.btnAutoStop_Click);
            // 
            // btnToTray
            // 
            this.btnToTray.Location = new System.Drawing.Point(674, 12);
            this.btnToTray.Name = "btnToTray";
            this.btnToTray.Size = new System.Drawing.Size(116, 23);
            this.btnToTray.TabIndex = 15;
            this.btnToTray.Text = "最小化到托盘(&M)";
            this.btnToTray.UseVisualStyleBackColor = true;
            this.btnToTray.Click += new System.EventHandler(this.btnToTray_Click);
            // 
            // bwWork
            // 
            this.bwWork.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwWork_DoWork);
            this.bwWork.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bwWork_RunWorkerCompleted);
            // 
            // cbxUseAllTestSource
            // 
            this.cbxUseAllTestSource.AutoSize = true;
            this.cbxUseAllTestSource.Checked = true;
            this.cbxUseAllTestSource.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbxUseAllTestSource.Location = new System.Drawing.Point(142, 132);
            this.cbxUseAllTestSource.Name = "cbxUseAllTestSource";
            this.cbxUseAllTestSource.Size = new System.Drawing.Size(108, 16);
            this.cbxUseAllTestSource.TabIndex = 16;
            this.cbxUseAllTestSource.Text = "使用所有测速源";
            this.cbxUseAllTestSource.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 729);
            this.Controls.Add(this.cbxUseAllTestSource);
            this.Controls.Add(this.btnToTray);
            this.Controls.Add(this.btnAutoStop);
            this.Controls.Add(this.btnAutoStart);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.nudSleepInterval);
            this.Controls.Add(this.cbxTestSource);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtTestSpeedIp);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtTestSpeedUrl);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbxLog);
            this.Controls.Add(this.btnTest);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "切换快网";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Shown += new System.EventHandler(this.Form1_Shown);
            this.cmsTray.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.nudSleepInterval)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnTest;
        private System.Windows.Forms.ListBox lbxLog;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtTestSpeedUrl;
        private System.Windows.Forms.TextBox txtTestSpeedIp;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbxTestSource;
        private System.Windows.Forms.NotifyIcon nfiMain;
        private System.Windows.Forms.ContextMenuStrip cmsTray;
        private System.Windows.Forms.ToolStripMenuItem tsmiExit;
        private System.Windows.Forms.NumericUpDown nudSleepInterval;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnAutoStart;
        private System.Windows.Forms.Button btnAutoStop;
        private System.Windows.Forms.Button btnToTray;
        private System.ComponentModel.BackgroundWorker bwWork;
        private System.Windows.Forms.CheckBox cbxUseAllTestSource;
    }
}

