﻿using Microsoft.Win32;
using System;

namespace Windows远程桌面端口修改.Utils
{
    public static class RegeditUtil
    {
        public static void DelayWindowsUpdate(double days)
        {
            //计算机\HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\WindowsUpdate\UX\Settings
            //要在 windows 更新中，手动点击下暂停更新，注册表中才会出现下面三个项
            //PauseFeatureUpdatesEndTime,PauseQualityUpdatesEndTime,PauseUpdatesExpiryTime
            //字符串类型，时间格式 2023-11-07T06:06:58Z

            DateTime dateTime = DateTime.Now.AddDays(days);
            string strDate = dateTime.ToString("yyyy-MM-ddTHH:mm:ssZ");            

            RegistryKey reg;
            reg = Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\WindowsUpdate\\UX\\Settings", true);
            reg.SetValue("PauseFeatureUpdatesEndTime", strDate);
            reg.SetValue("PauseQualityUpdatesEndTime", strDate);
            reg.SetValue("PauseUpdatesExpiryTime", strDate);
            reg.Close();             
        }
    }
}
