﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Windows远程桌面端口修改.Utils;

namespace Windows远程桌面端口修改
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            try
            {
                cbxRemoteFps.SelectedIndex= 0;

                LoadPortToUI();
                LoadRemoteFpsToUI();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        void LoadPortToUI()
        {
            string startOnSysStartRegKey = "PortNumber";
            RegistryKey reg;
            reg = Registry.LocalMachine.OpenSubKey(@"System\CurrentControlSet\Control\Terminal Server\WinStations\RDP-Tcp", true);
            object obSt = reg.GetValue(startOnSysStartRegKey);
            if (obSt != null)
                lblPort1.Text = obSt.ToString();

            reg = Registry.LocalMachine.OpenSubKey(@"SYSTEM\CurrentControlSet\Control\Terminal Server\Wds\rdpwd\Tds\tcp", true);
            obSt = reg.GetValue(startOnSysStartRegKey);
            if (obSt != null)
                lblPort2.Text = obSt.ToString();

            nudPort.Value = int.Parse(lblPort2.Text);
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                int port = 3389;
                if (!int.TryParse(nudPort.Value.ToString(), out port))
                {
                    MessageBox.Show("端口非整数。");
                    return;
                }

                DialogResult dr = MessageBox.Show("是否要修改？", "修改", MessageBoxButtons.YesNo,MessageBoxIcon.Question,MessageBoxDefaultButton.Button2);
                if (dr != DialogResult.Yes)
                    return;

                string startOnSysStartRegKey = "PortNumber";
                RegistryKey reg;
                reg = Registry.LocalMachine.OpenSubKey(@"System\CurrentControlSet\Control\Terminal Server\WinStations\RDP-Tcp", true);
                reg.SetValue(startOnSysStartRegKey, port.ToString(), RegistryValueKind.DWord);


                reg = Registry.LocalMachine.OpenSubKey(@"SYSTEM\CurrentControlSet\Control\Terminal Server\Wds\rdpwd\Tds\tcp", true);
                reg.SetValue(startOnSysStartRegKey, port.ToString(), RegistryValueKind.DWord);

                //端口要添加例外
                //用端口作防火墙规则名
                INetFwManger.NetFwAddPorts(port.ToString(), port, "TCP");

                MessageBox.Show("修改成功后，要重启电脑。");
                LoadPortToUI();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnAddFwPort_Click(object sender, EventArgs e)
        {
            try
            {
                int port = 3389;
                if (!int.TryParse(nudPort2.Value.ToString(), out port))
                {
                    MessageBox.Show("端口非整数。");
                    return;
                }

                //用端口作防火墙规则名
                INetFwManger.NetFwAddPorts(port.ToString(), port,"TCP");

                MessageBox.Show("添加完成。");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnDelFwPort_Click(object sender, EventArgs e)
        {
            try
            {
                int port = 3389;
                if (!int.TryParse(nudPort2.Value.ToString(), out port))
                {
                    MessageBox.Show("端口非整数。");
                    return;
                }

                //用端口作防火墙规则名
                INetFwManger.NetFwDelPort( port, "TCP");

                MessageBox.Show("删除完成。");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnUpdateDelay_Click(object sender, EventArgs e)
        {             
            try
            {
                int days = 365;//nudDelayDays
                if (!int.TryParse(nudDelayDays.Value.ToString(), out days))
                {
                    MessageBox.Show("非整数。");
                    return;
                }
                RegeditUtil.DelayWindowsUpdate(days);
                MessageBox.Show("完成。");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnEditRemoteFps_Click(object sender, EventArgs e)
        {
            try
            {
                int port = 3389;
                if (!int.TryParse(cbxRemoteFps.Text, out port))
                {
                    MessageBox.Show("刷新率非整数。");
                    return;
                }

                port = (int)RemoteFpsIntToDecimal(port);

                DialogResult dr = MessageBox.Show("是否要修改刷新率？", "修改", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                if (dr != DialogResult.Yes)
                    return;

                string startOnSysStartRegKey = "DWMFRAMEINTERVAL";
                RegistryKey reg;
                reg = Registry.LocalMachine.OpenSubKey(@"System\CurrentControlSet\Control\Terminal Server\WinStations", true);
                reg.SetValue(startOnSysStartRegKey, port.ToString(), RegistryValueKind.DWord);              

                MessageBox.Show("修改成功后，要重启电脑。");
                LoadRemoteFpsToUI();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// 远程桌面刷新率
        /// </summary>
        void LoadRemoteFpsToUI()
        {
            string startOnSysStartRegKey = "DWMFRAMEINTERVAL";
            RegistryKey reg;
            reg = Registry.LocalMachine.OpenSubKey(@"System\CurrentControlSet\Control\Terminal Server\WinStations", true);
            object obSt = reg.GetValue(startOnSysStartRegKey);
            if (obSt != null)
                lblRemoteFps.Text = RemoteFpsDecimalToInt(decimal.Parse(obSt.ToString())).ToString();
            else
                lblRemoteFps.Text = "30"; 
        }
        /// <summary>
        /// 帧速率映射（15 decimal → 60 帧）
        /// </summary>
        /// <returns></returns>
        static int RemoteFpsDecimalToInt(decimal dIn)
        {
            /*
             帧速率映射
15 decimal = 60 帧
10 decimal = 40 帧
5 decimal = 20 帧
1 decimal = 4 帧
             */
            if (dIn == 15M) return 60;
            if (dIn == 10M) return 40;
            if (dIn == 5M) return 20;
            if (dIn == 1M) return 4;

            return 30;
        }
        /// <summary>
        /// 帧速率映射（60 帧 → 15 decima）
        /// </summary>
        /// <param name="dIn"></param>
        /// <returns></returns>
        static decimal RemoteFpsIntToDecimal(int dIn)
        {
            /*
             帧速率映射
15 decimal = 60 帧
10 decimal = 40 帧
5 decimal = 20 帧
1 decimal = 4 帧
             */
            if (dIn == 60) return 15M;
            if (dIn == 40) return 10M;
            if (dIn == 20) return 5M;
            if (dIn == 4) return 1M;

            return 15M;
        }

    }
}
