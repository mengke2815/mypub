﻿namespace Windows远程桌面端口修改
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.nudPort = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblPort1 = new System.Windows.Forms.Label();
            this.lblPort2 = new System.Windows.Forms.Label();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnAddFwPort = new System.Windows.Forms.Button();
            this.btnDelFwPort = new System.Windows.Forms.Button();
            this.btnUpdateDelay = new System.Windows.Forms.Button();
            this.nudDelayDays = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.nudPort2 = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.lblRemoteFps = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.btnEditRemoteFps = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.cbxRemoteFps = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.nudPort)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudDelayDays)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPort2)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 158);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "新端口号";
            // 
            // nudPort
            // 
            this.nudPort.Location = new System.Drawing.Point(84, 156);
            this.nudPort.Maximum = new decimal(new int[] {
            65000,
            0,
            0,
            0});
            this.nudPort.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudPort.Name = "nudPort";
            this.nudPort.Size = new System.Drawing.Size(120, 21);
            this.nudPort.TabIndex = 1;
            this.nudPort.Value = new decimal(new int[] {
            3389,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(25, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(527, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "HKEY_LOCAL_MACHINE\\System\\CurrentControlSet\\Control\\Terminal Server\\WinStations\\R" +
    "DP-Tcp";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(25, 93);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(515, 12);
            this.label3.TabIndex = 3;
            this.label3.Text = "HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Control\\Terminal Server\\Wds\\rdpwd\\Tds" +
    "\\tcp";
            // 
            // lblPort1
            // 
            this.lblPort1.AutoSize = true;
            this.lblPort1.Location = new System.Drawing.Point(139, 59);
            this.lblPort1.Name = "lblPort1";
            this.lblPort1.Size = new System.Drawing.Size(41, 12);
            this.lblPort1.TabIndex = 4;
            this.lblPort1.Text = "label4";
            // 
            // lblPort2
            // 
            this.lblPort2.AutoSize = true;
            this.lblPort2.Location = new System.Drawing.Point(139, 118);
            this.lblPort2.Name = "lblPort2";
            this.lblPort2.Size = new System.Drawing.Size(41, 12);
            this.lblPort2.TabIndex = 5;
            this.lblPort2.Text = "label5";
            // 
            // btnEdit
            // 
            this.btnEdit.Location = new System.Drawing.Point(228, 153);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(75, 23);
            this.btnEdit.TabIndex = 6;
            this.btnEdit.Text = "修改端口";
            this.btnEdit.UseVisualStyleBackColor = true;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnAddFwPort
            // 
            this.btnAddFwPort.Location = new System.Drawing.Point(28, 66);
            this.btnAddFwPort.Name = "btnAddFwPort";
            this.btnAddFwPort.Size = new System.Drawing.Size(132, 23);
            this.btnAddFwPort.TabIndex = 7;
            this.btnAddFwPort.Text = "防火墙添加端口例外";
            this.btnAddFwPort.UseVisualStyleBackColor = true;
            this.btnAddFwPort.Click += new System.EventHandler(this.btnAddFwPort_Click);
            // 
            // btnDelFwPort
            // 
            this.btnDelFwPort.Location = new System.Drawing.Point(28, 95);
            this.btnDelFwPort.Name = "btnDelFwPort";
            this.btnDelFwPort.Size = new System.Drawing.Size(132, 23);
            this.btnDelFwPort.TabIndex = 8;
            this.btnDelFwPort.Text = "防火墙删除端口例外";
            this.btnDelFwPort.UseVisualStyleBackColor = true;
            this.btnDelFwPort.Click += new System.EventHandler(this.btnDelFwPort_Click);
            // 
            // btnUpdateDelay
            // 
            this.btnUpdateDelay.Location = new System.Drawing.Point(23, 63);
            this.btnUpdateDelay.Name = "btnUpdateDelay";
            this.btnUpdateDelay.Size = new System.Drawing.Size(132, 23);
            this.btnUpdateDelay.TabIndex = 9;
            this.btnUpdateDelay.Text = "WIN10 11 更新延期";
            this.btnUpdateDelay.UseVisualStyleBackColor = true;
            this.btnUpdateDelay.Click += new System.EventHandler(this.btnUpdateDelay_Click);
            // 
            // nudDelayDays
            // 
            this.nudDelayDays.Location = new System.Drawing.Point(69, 33);
            this.nudDelayDays.Maximum = new decimal(new int[] {
            65000,
            0,
            0,
            0});
            this.nudDelayDays.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudDelayDays.Name = "nudDelayDays";
            this.nudDelayDays.Size = new System.Drawing.Size(120, 21);
            this.nudDelayDays.TabIndex = 11;
            this.nudDelayDays.Value = new decimal(new int[] {
            3650,
            0,
            0,
            0});
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(21, 36);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 12);
            this.label4.TabIndex = 10;
            this.label4.Text = "天数";
            // 
            // nudPort2
            // 
            this.nudPort2.Location = new System.Drawing.Point(73, 30);
            this.nudPort2.Maximum = new decimal(new int[] {
            65000,
            0,
            0,
            0});
            this.nudPort2.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudPort2.Name = "nudPort2";
            this.nudPort2.Size = new System.Drawing.Size(120, 21);
            this.nudPort2.TabIndex = 13;
            this.nudPort2.Value = new decimal(new int[] {
            3389,
            0,
            0,
            0});
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 32);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 12);
            this.label5.TabIndex = 12;
            this.label5.Text = "端口";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnDelFwPort);
            this.groupBox1.Controls.Add(this.nudPort2);
            this.groupBox1.Controls.Add(this.btnAddFwPort);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Location = new System.Drawing.Point(281, 478);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(206, 144);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "防火墙";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnUpdateDelay);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.nudDelayDays);
            this.groupBox2.Location = new System.Drawing.Point(23, 464);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(204, 107);
            this.groupBox2.TabIndex = 15;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "WIN10 11 更新延期";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.cbxRemoteFps);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.btnEditRemoteFps);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.lblRemoteFps);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.lblPort1);
            this.groupBox3.Controls.Add(this.btnEdit);
            this.groupBox3.Controls.Add(this.nudPort);
            this.groupBox3.Controls.Add(this.lblPort2);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Location = new System.Drawing.Point(23, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(581, 338);
            this.groupBox3.TabIndex = 16;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "修改远程桌面端口";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(21, 220);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(113, 12);
            this.label6.TabIndex = 7;
            this.label6.Text = "当前远程桌面刷新率";
            // 
            // lblRemoteFps
            // 
            this.lblRemoteFps.AutoSize = true;
            this.lblRemoteFps.Location = new System.Drawing.Point(153, 220);
            this.lblRemoteFps.Name = "lblRemoteFps";
            this.lblRemoteFps.Size = new System.Drawing.Size(17, 12);
            this.lblRemoteFps.TabIndex = 8;
            this.lblRemoteFps.Text = "60";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(187, 220);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(17, 12);
            this.label8.TabIndex = 9;
            this.label8.Text = "帧";
            // 
            // btnEditRemoteFps
            // 
            this.btnEditRemoteFps.Location = new System.Drawing.Point(200, 246);
            this.btnEditRemoteFps.Name = "btnEditRemoteFps";
            this.btnEditRemoteFps.Size = new System.Drawing.Size(75, 23);
            this.btnEditRemoteFps.TabIndex = 10;
            this.btnEditRemoteFps.Text = "修改刷新率";
            this.btnEditRemoteFps.UseVisualStyleBackColor = true;
            this.btnEditRemoteFps.Click += new System.EventHandler(this.btnEditRemoteFps_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(163, 251);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(17, 12);
            this.label7.TabIndex = 11;
            this.label7.Text = "帧";
            // 
            // cbxRemoteFps
            // 
            this.cbxRemoteFps.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxRemoteFps.FormattingEnabled = true;
            this.cbxRemoteFps.Items.AddRange(new object[] {
            "60",
            "40",
            "20",
            "4"});
            this.cbxRemoteFps.Location = new System.Drawing.Point(27, 251);
            this.cbxRemoteFps.Name = "cbxRemoteFps";
            this.cbxRemoteFps.Size = new System.Drawing.Size(121, 20);
            this.cbxRemoteFps.TabIndex = 12;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(35, 59);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(53, 12);
            this.label9.TabIndex = 13;
            this.label9.Text = "当前端口";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(672, 634);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "修改远程桌面端口";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Shown += new System.EventHandler(this.Form1_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.nudPort)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudDelayDays)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPort2)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown nudPort;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblPort1;
        private System.Windows.Forms.Label lblPort2;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Button btnAddFwPort;
        private System.Windows.Forms.Button btnDelFwPort;
        private System.Windows.Forms.Button btnUpdateDelay;
        private System.Windows.Forms.NumericUpDown nudDelayDays;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown nudPort2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblRemoteFps;
        private System.Windows.Forms.Button btnEditRemoteFps;
        private System.Windows.Forms.ComboBox cbxRemoteFps;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
    }
}

