﻿using System;
using System.IO;

namespace CommonUtils
{
     
    public static class GLog
    {
        static object _lockObj = new object();

        public static void WLog(string content)
        {
            lock (_lockObj)
            {
                string curPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().ManifestModule.FullyQualifiedName);
                string logDir = "Logs";
                string logDirFullName = Path.Combine(curPath, logDir);

                try
                {
                    if (!Directory.Exists(logDirFullName))
                        Directory.CreateDirectory(logDirFullName);
                }
                catch { return; }

                string fileName = "Logs" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt";
                string logFullName = Path.Combine(logDirFullName, fileName);

                try
                {
                    using (FileStream fs = new FileStream(logFullName, FileMode.Append, FileAccess.Write))
                    using (StreamWriter sw = new StreamWriter(fs))
                        sw.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + " " + content);
                }
                catch { return; }

            }
        }
    }
}
