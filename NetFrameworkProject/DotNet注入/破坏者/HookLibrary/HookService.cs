﻿using DotNetDetour;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HookLibrary
{
    public class HookService
    {
        /// <summary>
        /// Hook 初始化
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        public static int Start(string msg)
        {
            try
            {
                MethodHook.Install();
            }
            catch(Exception ex)
            {
                return -1;
            }

            return 1;
        }
    }
}
