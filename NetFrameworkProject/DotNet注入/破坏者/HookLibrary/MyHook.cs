﻿using DotNetDetour;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HookLibrary
{
    public class MyHook : IMethodHook
    {
        [HookMethod("TargetLibrary.HuiShuo", null, null)]
        public string GaoShi()
        {
            return "工作2";
        }

        [OriginalMethod]
        public string GaoShi_Original()
        {
            return null;
        }
    }
}
