﻿using FastWin32.Diagnostics;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Pro1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                string processName = "Target";
                int returnValue2;
                Process[] pros = Process.GetProcessesByName(processName);
                Int32 targetPID = 0;
                if (pros != null && pros.Length > 0 && (targetPID = pros[0].Id) > 0)
                {
                    string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "HookLibrary.dll");
                    Injector.InjectManaged((uint)targetPID, path, "HookLibrary.HookService", "Start", "", out  returnValue2);
                    MessageBox.Show("ok");
                }
                else
                {
                    MessageBox.Show("fail，未找到进程名为："+ processName+"的进程");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("ex：" + ex.Message);
            }
        }
    }
}
