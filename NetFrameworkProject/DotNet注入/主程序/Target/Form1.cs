﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TargetLibrary;

namespace Target
{
    public partial class Form1 : Form
    {
        HuiShuo huiShuo = new HuiShuo();
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            lblMsg.Text = string.Empty;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            lblMsg.Text = huiShuo.GaoShi();
        }
    }
}
