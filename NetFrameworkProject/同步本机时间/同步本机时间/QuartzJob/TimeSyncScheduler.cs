﻿using CommonUtils;
using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 同步本机时间.QuartzJob
{
    public class TimeSyncScheduler
    {
        /// <summary>
        /// 创建计划任务
        /// </summary>
        public static async void Start()
        {

            try
            {
                string thisJob = "TimeSyncJob";

                string groupName = "gp" + thisJob;
                string jobName = "job" + thisJob;
                string triggerName = "trigger" + thisJob;

                // 创建作业调度池
                ISchedulerFactory factory = new StdSchedulerFactory();
                IScheduler scheduler = await factory.GetScheduler();

                // 创建作业
                IJobDetail job = JobBuilder.Create<TimeSyncJob>()
                  .WithIdentity(jobName, groupName)
                  .Build();

                // 创建触发器，每 2m 执行一次
                ITrigger trigger = TriggerBuilder.Create()
                  .WithIdentity(triggerName, groupName)
                  .StartNow()
                  .WithSimpleSchedule(x => x
                    .WithIntervalInMinutes(2)
                    .RepeatForever())
                  .Build();

                // 加入到作业调度池中
                await scheduler.ScheduleJob(job, trigger);

                // 开始运行
                await scheduler.Start();
            }
            catch (Exception ex)
            {
                Log4NetUtil.Info("TimeSyncScheduler start ex:" + ex.Message);
            }

        }

    }
}
