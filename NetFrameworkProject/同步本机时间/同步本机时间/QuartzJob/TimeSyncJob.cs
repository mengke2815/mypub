﻿using CommonUtils;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using 同步本机时间.Structs;
using 同步本机时间.Utils;

namespace 同步本机时间.QuartzJob
{
    [DisallowConcurrentExecution]
    public class TimeSyncJob : IJob
    {
        public Task Execute(IJobExecutionContext context)
        {
            return Task.Run(() =>
            {
                foo();
            });
        }

        public void foo()
        {
            try
            {
                var myTask = Task.Run(() =>
                {
                    return GetNetTimeUtil.GetNetDateTime("https://www.baidu.com");
                });
                var myTask2 = Task.Run(() =>
                {
                    return GetNetTimeUtil.GetNetDateTime("https://cn.bing.com");
                });

                Task.WaitAny(myTask, myTask2);
                DateTime serverTime = DateTime.Now;
                if (myTask.IsCompleted && !string.IsNullOrWhiteSpace(myTask.Result))
                {
                    serverTime = Convert.ToDateTime(myTask.Result);
                    Log4NetUtil.Info("百度:" + serverTime.ToString("yyyy-MM-dd HH:mm:ss"));
                }
                else if (myTask2.IsCompleted && !string.IsNullOrWhiteSpace(myTask2.Result))
                {
                    serverTime = Convert.ToDateTime(myTask2.Result);
                    Log4NetUtil.Info("Bing:" + serverTime.ToString("yyyy-MM-dd HH:mm:ss"));
                }

                Log4NetUtil.Info("同步前，本机时间：" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));

                //服务器时间大于本机时间
                if (serverTime > DateTime.Now)
                {
                    //误差在 1 到 10 分钟。这种情况才同步本机时间
                    TimeSpan ts = serverTime - DateTime.Now;
                    if (ts.TotalMinutes >= 1 && ts.TotalMinutes <= 10)
                    {
                        //转换System.DateTime到 MySystemTime
                        MySystemTime st = new MySystemTime();
                        st.FromDateTime(serverTime);

                        //调用Win32 API设置系统时间  
                        Win32API.SetLocalTime(ref st);

                        Log4NetUtil.Info("同步后，本机时间：" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                    }
                    else
                    {
                        Log4NetUtil.Info("误差不在 1 到 10 分钟。");
                    }
                }
                else
                {
                    Log4NetUtil.Info("本机时间大于服务器时间");
                }
            }
            catch (Exception ex)
            {
                Log4NetUtil.Info("ex:" + ex.Message);
                if (ex.InnerException != null)
                {
                    Log4NetUtil.Info("InnerException ex:" + ex.InnerException.Message);
                }
            }
        }

         

    }
}
