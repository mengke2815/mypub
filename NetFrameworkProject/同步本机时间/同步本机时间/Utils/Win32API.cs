﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using 同步本机时间.Structs;

namespace 同步本机时间.Utils
{
    public class Win32API
    {
        [DllImport("Kernel32.dll")]
        public static extern bool SetLocalTime(ref MySystemTime Time);
        [DllImport("Kernel32.dll")]
        public static extern void GetLocalTime(ref MySystemTime Time);
    }
}
