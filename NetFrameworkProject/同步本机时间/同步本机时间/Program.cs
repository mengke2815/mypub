﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace 同步本机时间
{
    internal static class Program
    {
        static Mutex appMutex;

        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {

            string exeName = "同步本机时间";
            string globalMutexName = @"Global\" + exeName;
            string appName = "同步本机时间";

            bool createNew;
            appMutex = new  Mutex(true, globalMutexName, out createNew);
            if (!createNew)
            {
                appMutex.Close(); appMutex = null;
                MessageBox.Show(appName + "已开启，进程为" + exeName + "！", "提示");
                return;
            }

            Application.ApplicationExit += new EventHandler(OnApplicationExit);


            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }

        static void OnApplicationExit(object sender, EventArgs e)
        {
            RelMutex();
        }

        /// <summary>
        /// 释放
        /// </summary>
        public static void RelMutex()
        {
            try
            {
                if (appMutex != null)
                {
                    appMutex.ReleaseMutex();
                    appMutex.Close();
                }
            }
            catch (Exception expMu) { }
        }

    }
}
