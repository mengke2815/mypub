﻿using CommonUtils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using 同步本机时间.QuartzJob;
using 同步本机时间.Structs;
using 同步本机时间.Utils;

namespace 同步本机时间
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
            
        }

        private void btnSync_Click(object sender, EventArgs e)
        {

            var myTask = Task.Run(() =>
            {
                return GetNetDateTime("https://www.baidu.com");
            });
            var myTask2 = Task.Run(() =>
            {
                return GetNetDateTime("https://cn.bing.com");
            });

            Task.WaitAny(myTask, myTask2);
            DateTime serverTime = DateTime.Now;
            if (myTask.IsCompleted && !string.IsNullOrWhiteSpace(myTask.Result))
            {
                serverTime = Convert.ToDateTime(myTask.Result);
                Log4NetUtil.Info("百度:" + serverTime.ToString("yyyy-MM-dd HH:mm:ss"));
            }
            else if (myTask2.IsCompleted && !string.IsNullOrWhiteSpace(myTask2.Result))
            {
                serverTime = Convert.ToDateTime(myTask2.Result);
                Log4NetUtil.Info("Bing:" + serverTime.ToString("yyyy-MM-dd HH:mm:ss"));
            }

            Log4NetUtil.Info("同步前，本机时间：" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));

            //服务器时间大于本机时间
            if (serverTime > DateTime.Now)
            {
                //误差在 1 到 10 分钟。这种情况才同步本机时间
                TimeSpan ts = serverTime - DateTime.Now;
                if (ts.TotalMinutes >= 1 && ts.TotalMinutes <= 10)
                {
                    //转换System.DateTime到 MySystemTime
                    MySystemTime st = new MySystemTime();
                    st.FromDateTime(serverTime);

                    //调用Win32 API设置系统时间  
                    Win32API.SetLocalTime(ref st);

                    Log4NetUtil.Info("同步后，本机时间：" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                }
                else
                {
                    Log4NetUtil.Info("误差不在 1 到 10 分钟。");
                }
            }
            else
            {
                Log4NetUtil.Info("本机时间大于服务器时间");
            }
        }

        /// <summary>  
        /// 获取网络日期时间  
        /// </summary>  
        /// <returns></returns>  
        public static string GetNetDateTime(string url)
        {
            int timeout = 6000;

            HttpWebRequest request = null;
            WebResponse response = null;
            WebHeaderCollection headerCollection = null;
            string datetime = string.Empty;
            try
            {
                request = WebRequest.Create(url) as HttpWebRequest;
                request.Timeout = timeout;
                request.ReadWriteTimeout = timeout;
                request.Method = "GET";


                //request.Credentials = CredentialCache.DefaultCredentials;
                response = (WebResponse)request.GetResponse();
                headerCollection = response.Headers;
                foreach (var h in headerCollection.AllKeys)
                {
                    if (h == "Date")
                    {
                        datetime = headerCollection[h];
                    }
                }
                return datetime;
            }
            catch (Exception ex) { return datetime; }
            finally
            {
                if (request != null)
                { request.Abort(); }
                if (response != null)
                { response.Close(); }
                if (headerCollection != null)
                { headerCollection.Clear(); }
            }
        }

        private void btnToTray_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void nfiMain_MouseDoubleClick(object sender, MouseEventArgs e)
        {

        }

        private void nfiMain_DoubleClick(object sender, EventArgs e)
        {
            this.Show();
        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            try
            {
                TimeSyncScheduler.Start();

                lblMsg.Text = "正在运行";

                this.Hide();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }
    }
}
