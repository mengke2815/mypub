﻿namespace WindowsForms私钥签名公钥验签
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.label13 = new System.Windows.Forms.Label();
            this.txtPubKey = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtPrivateKey = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtToSignStr = new System.Windows.Forms.TextBox();
            this.btnSign = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtSign = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.lblValidRst = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(12, 204);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(101, 12);
            this.label13.TabIndex = 30;
            this.label13.Text = "公钥（验证签名）";
            // 
            // txtPubKey
            // 
            this.txtPubKey.Location = new System.Drawing.Point(132, 188);
            this.txtPubKey.Multiline = true;
            this.txtPubKey.Name = "txtPubKey";
            this.txtPubKey.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtPubKey.Size = new System.Drawing.Size(546, 90);
            this.txtPubKey.TabIndex = 29;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(12, 61);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(101, 12);
            this.label10.TabIndex = 28;
            this.label10.Text = "私钥(签名,PKCS8)";
            // 
            // txtPrivateKey
            // 
            this.txtPrivateKey.Location = new System.Drawing.Point(132, 58);
            this.txtPrivateKey.Multiline = true;
            this.txtPrivateKey.Name = "txtPrivateKey";
            this.txtPrivateKey.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtPrivateKey.Size = new System.Drawing.Size(546, 106);
            this.txtPrivateKey.TabIndex = 27;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(23, 313);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(89, 12);
            this.label5.TabIndex = 32;
            this.label5.Text = "要签名的字符串";
            // 
            // txtToSignStr
            // 
            this.txtToSignStr.Location = new System.Drawing.Point(132, 299);
            this.txtToSignStr.Multiline = true;
            this.txtToSignStr.Name = "txtToSignStr";
            this.txtToSignStr.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtToSignStr.Size = new System.Drawing.Size(634, 76);
            this.txtToSignStr.TabIndex = 31;
            this.txtToSignStr.Text = "Hello中国";
            // 
            // btnSign
            // 
            this.btnSign.Location = new System.Drawing.Point(131, 381);
            this.btnSign.Name = "btnSign";
            this.btnSign.Size = new System.Drawing.Size(75, 23);
            this.btnSign.TabIndex = 33;
            this.btnSign.Text = "私钥签名";
            this.btnSign.UseVisualStyleBackColor = true;
            this.btnSign.Click += new System.EventHandler(this.btnSign_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 438);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 12);
            this.label1.TabIndex = 35;
            this.label1.Text = "签名";
            // 
            // txtSign
            // 
            this.txtSign.Location = new System.Drawing.Point(12, 453);
            this.txtSign.Multiline = true;
            this.txtSign.Name = "txtSign";
            this.txtSign.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtSign.Size = new System.Drawing.Size(909, 76);
            this.txtSign.TabIndex = 34;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(131, 583);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(94, 23);
            this.button1.TabIndex = 36;
            this.button1.Text = "公钥验证签名";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // lblValidRst
            // 
            this.lblValidRst.AutoSize = true;
            this.lblValidRst.Location = new System.Drawing.Point(256, 588);
            this.lblValidRst.Name = "lblValidRst";
            this.lblValidRst.Size = new System.Drawing.Size(29, 12);
            this.lblValidRst.TabIndex = 37;
            this.lblValidRst.Text = "签11";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("宋体", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(141, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(439, 15);
            this.label2.TabIndex = 38;
            this.label2.Text = "同一组私钥公钥才能验证签名，公私钥不匹配，无法验证签名";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(933, 649);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblValidRst);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtSign);
            this.Controls.Add(this.btnSign);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtToSignStr);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.txtPubKey);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txtPrivateKey);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtPubKey;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtPrivateKey;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtToSignStr;
        private System.Windows.Forms.Button btnSign;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtSign;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label lblValidRst;
        private System.Windows.Forms.Label label2;
    }
}

