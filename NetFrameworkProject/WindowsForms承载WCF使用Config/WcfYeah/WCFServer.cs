﻿using System.Net;
using System.ServiceModel.Web;
using System.Threading;

namespace WcfYeah
{
    public class WCFServer
    {
        public WebServiceHost host = null;

        public WCFServer()
        {
            #region 优化调整
            //对外连接数，根据实际情况加大
            if (ServicePointManager.DefaultConnectionLimit < 100)
                System.Net.ServicePointManager.DefaultConnectionLimit = 100;

            int workerThreads;//工作线程数
            int completePortsThreads; //异步I/O 线程数
            ThreadPool.GetMinThreads(out workerThreads, out completePortsThreads);

            int blogCnt = 100;
            if (workerThreads < blogCnt)
            {
                // MinThreads 值不要超过 (max_thread /2  )，否则会不生效。要不然就同时加大max_thread
                ThreadPool.SetMinThreads(blogCnt, blogCnt);
            }
            #endregion
             
            //注意：这里是实现类，不是接口，否则会报：ServiceHost 仅支持类服务类型。
            host = new WebServiceHost( typeof(WcfYeah.Service1));
        }


        public void Start()
        {
            host.Open();
        }

        public void Close()
        {
            if (host != null)
            {
                host.Close();
            }
        }
    }
}
