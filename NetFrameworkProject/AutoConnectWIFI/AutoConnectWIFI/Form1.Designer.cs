﻿namespace AutoConnectWIFI
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.dtpConnectEnd = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.dtpConnectStart = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.dtpDisConnectEnd = new System.Windows.Forms.DateTimePicker();
            this.label8 = new System.Windows.Forms.Label();
            this.dtpDisConnectStart = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.cbxWifiInterfaceName = new System.Windows.Forms.ComboBox();
            this.btnApply = new System.Windows.Forms.Button();
            this.cbxWifiProfile = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.bwDo = new System.ComponentModel.BackgroundWorker();
            this.lbxLog = new System.Windows.Forms.ListBox();
            this.chkOnSysStart = new System.Windows.Forms.CheckBox();
            this.label10 = new System.Windows.Forms.Label();
            this.nudCheckInterval = new System.Windows.Forms.NumericUpDown();
            this.nfiMain = new System.Windows.Forms.NotifyIcon(this.components);
            this.btnToTray = new System.Windows.Forms.Button();
            this.cmsTray = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmiAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiExit = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudCheckInterval)).BeginInit();
            this.cmsTray.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "自动连接或断开WIFI";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("宋体", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(190, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(299, 15);
            this.label2.TabIndex = 1;
            this.label2.Text = "先手动连接一次WIFI后，再使用本程序。";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.dtpConnectEnd);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.dtpConnectStart);
            this.groupBox1.Location = new System.Drawing.Point(12, 183);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(183, 138);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "在此区间连接WIFI";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 97);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 12);
            this.label6.TabIndex = 9;
            this.label6.Text = "结束时间";
            // 
            // dtpConnectEnd
            // 
            this.dtpConnectEnd.CustomFormat = "yyyy-MM-dd HH:mm:ss";
            this.dtpConnectEnd.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtpConnectEnd.Location = new System.Drawing.Point(75, 93);
            this.dtpConnectEnd.Name = "dtpConnectEnd";
            this.dtpConnectEnd.Size = new System.Drawing.Size(91, 21);
            this.dtpConnectEnd.TabIndex = 8;
            this.dtpConnectEnd.Value = new System.DateTime(2023, 3, 31, 22, 0, 0, 0);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 46);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 12);
            this.label5.TabIndex = 7;
            this.label5.Text = "开始时间";
            // 
            // dtpConnectStart
            // 
            this.dtpConnectStart.CustomFormat = "yyyy-MM-dd HH:mm:ss";
            this.dtpConnectStart.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtpConnectStart.Location = new System.Drawing.Point(75, 42);
            this.dtpConnectStart.Name = "dtpConnectStart";
            this.dtpConnectStart.Size = new System.Drawing.Size(91, 21);
            this.dtpConnectStart.TabIndex = 0;
            this.dtpConnectStart.Value = new System.DateTime(2023, 3, 31, 8, 30, 0, 0);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(89, 40);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 12);
            this.label3.TabIndex = 3;
            this.label3.Text = "要连接的WIFI名";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.dtpDisConnectEnd);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.dtpDisConnectStart);
            this.groupBox2.Location = new System.Drawing.Point(12, 396);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(183, 138);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "在此区间断开WIFI";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 88);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 12);
            this.label7.TabIndex = 13;
            this.label7.Text = "结束时间";
            // 
            // dtpDisConnectEnd
            // 
            this.dtpDisConnectEnd.CustomFormat = "yyyy-MM-dd HH:mm:ss";
            this.dtpDisConnectEnd.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtpDisConnectEnd.Location = new System.Drawing.Point(67, 84);
            this.dtpDisConnectEnd.Name = "dtpDisConnectEnd";
            this.dtpDisConnectEnd.Size = new System.Drawing.Size(85, 21);
            this.dtpDisConnectEnd.TabIndex = 12;
            this.dtpDisConnectEnd.Value = new System.DateTime(2023, 3, 31, 8, 0, 0, 0);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(10, 44);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 12);
            this.label8.TabIndex = 11;
            this.label8.Text = "开始时间";
            // 
            // dtpDisConnectStart
            // 
            this.dtpDisConnectStart.CustomFormat = "yyyy-MM-dd HH:mm:ss";
            this.dtpDisConnectStart.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtpDisConnectStart.Location = new System.Drawing.Point(67, 40);
            this.dtpDisConnectStart.Name = "dtpDisConnectStart";
            this.dtpDisConnectStart.Size = new System.Drawing.Size(85, 21);
            this.dtpDisConnectStart.TabIndex = 10;
            this.dtpDisConnectStart.Value = new System.DateTime(2023, 3, 31, 22, 30, 0, 0);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(89, 84);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 6;
            this.label4.Text = "无线网卡";
            // 
            // cbxWifiInterfaceName
            // 
            this.cbxWifiInterfaceName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxWifiInterfaceName.Font = new System.Drawing.Font("宋体", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cbxWifiInterfaceName.FormattingEnabled = true;
            this.cbxWifiInterfaceName.Location = new System.Drawing.Point(193, 76);
            this.cbxWifiInterfaceName.Name = "cbxWifiInterfaceName";
            this.cbxWifiInterfaceName.Size = new System.Drawing.Size(544, 23);
            this.cbxWifiInterfaceName.TabIndex = 7;
            // 
            // btnApply
            // 
            this.btnApply.Location = new System.Drawing.Point(708, 35);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(75, 23);
            this.btnApply.TabIndex = 8;
            this.btnApply.Text = "应用";
            this.btnApply.UseVisualStyleBackColor = true;
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // cbxWifiProfile
            // 
            this.cbxWifiProfile.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxWifiProfile.Font = new System.Drawing.Font("宋体", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cbxWifiProfile.FormattingEnabled = true;
            this.cbxWifiProfile.Location = new System.Drawing.Point(193, 37);
            this.cbxWifiProfile.Name = "cbxWifiProfile";
            this.cbxWifiProfile.Size = new System.Drawing.Size(350, 23);
            this.cbxWifiProfile.TabIndex = 9;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("宋体", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label9.Location = new System.Drawing.Point(50, 346);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(128, 15);
            this.label9.TabIndex = 10;
            this.label9.Text = "2个区间不要重叠";
            // 
            // bwDo
            // 
            this.bwDo.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwDo_DoWork);
            this.bwDo.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bwDo_RunWorkerCompleted);
            // 
            // lbxLog
            // 
            this.lbxLog.FormattingEnabled = true;
            this.lbxLog.ItemHeight = 12;
            this.lbxLog.Location = new System.Drawing.Point(214, 116);
            this.lbxLog.Name = "lbxLog";
            this.lbxLog.Size = new System.Drawing.Size(782, 532);
            this.lbxLog.TabIndex = 11;
            // 
            // chkOnSysStart
            // 
            this.chkOnSysStart.AutoSize = true;
            this.chkOnSysStart.Location = new System.Drawing.Point(13, 109);
            this.chkOnSysStart.Name = "chkOnSysStart";
            this.chkOnSysStart.Size = new System.Drawing.Size(102, 16);
            this.chkOnSysStart.TabIndex = 12;
            this.chkOnSysStart.Text = "开机时启动(&D)";
            this.chkOnSysStart.UseVisualStyleBackColor = true;
            this.chkOnSysStart.CheckedChanged += new System.EventHandler(this.chkOnSysStart_CheckedChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(11, 143);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(77, 12);
            this.label10.TabIndex = 13;
            this.label10.Text = "检测间隔(秒)";
            // 
            // nudCheckInterval
            // 
            this.nudCheckInterval.Location = new System.Drawing.Point(101, 141);
            this.nudCheckInterval.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.nudCheckInterval.Minimum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.nudCheckInterval.Name = "nudCheckInterval";
            this.nudCheckInterval.Size = new System.Drawing.Size(94, 21);
            this.nudCheckInterval.TabIndex = 20;
            this.nudCheckInterval.Value = new decimal(new int[] {
            600,
            0,
            0,
            0});
            // 
            // nfiMain
            // 
            this.nfiMain.ContextMenuStrip = this.cmsTray;
            this.nfiMain.Icon = ((System.Drawing.Icon)(resources.GetObject("nfiMain.Icon")));
            this.nfiMain.Text = "自动连接或断开WIFI";
            this.nfiMain.Visible = true;
            this.nfiMain.DoubleClick += new System.EventHandler(this.nfiMain_DoubleClick);
            // 
            // btnToTray
            // 
            this.btnToTray.Location = new System.Drawing.Point(837, 35);
            this.btnToTray.Name = "btnToTray";
            this.btnToTray.Size = new System.Drawing.Size(116, 23);
            this.btnToTray.TabIndex = 21;
            this.btnToTray.Text = "最小化到托盘(&M)";
            this.btnToTray.UseVisualStyleBackColor = true;
            this.btnToTray.Click += new System.EventHandler(this.btnToTray_Click);
            // 
            // cmsTray
            // 
            this.cmsTray.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiAbout,
            this.tsmiExit});
            this.cmsTray.Name = "cmsTray";
            this.cmsTray.Size = new System.Drawing.Size(199, 48);
            // 
            // tsmiAbout
            // 
            this.tsmiAbout.Name = "tsmiAbout";
            this.tsmiAbout.Size = new System.Drawing.Size(198, 22);
            this.tsmiAbout.Text = "关于自动连接WIFI程序";
            this.tsmiAbout.Click += new System.EventHandler(this.tsmiAbout_Click);
            // 
            // tsmiExit
            // 
            this.tsmiExit.Name = "tsmiExit";
            this.tsmiExit.Size = new System.Drawing.Size(198, 22);
            this.tsmiExit.Text = "退出(&X)";
            this.tsmiExit.Click += new System.EventHandler(this.tsmiExit_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 729);
            this.Controls.Add(this.btnToTray);
            this.Controls.Add(this.nudCheckInterval);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.chkOnSysStart);
            this.Controls.Add(this.lbxLog);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.cbxWifiProfile);
            this.Controls.Add(this.btnApply);
            this.Controls.Add(this.cbxWifiInterfaceName);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "自动连接或断开WIFI";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Shown += new System.EventHandler(this.Form1_Shown);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudCheckInterval)).EndInit();
            this.cmsTray.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbxWifiInterfaceName;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker dtpConnectStart;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker dtpConnectEnd;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DateTimePicker dtpDisConnectEnd;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DateTimePicker dtpDisConnectStart;
        private System.Windows.Forms.Button btnApply;
        private System.Windows.Forms.ComboBox cbxWifiProfile;
        private System.Windows.Forms.Label label9;
        private System.ComponentModel.BackgroundWorker bwDo;
        private System.Windows.Forms.ListBox lbxLog;
        private System.Windows.Forms.CheckBox chkOnSysStart;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.NumericUpDown nudCheckInterval;
        private System.Windows.Forms.NotifyIcon nfiMain;
        private System.Windows.Forms.Button btnToTray;
        private System.Windows.Forms.ContextMenuStrip cmsTray;
        private System.Windows.Forms.ToolStripMenuItem tsmiAbout;
        private System.Windows.Forms.ToolStripMenuItem tsmiExit;
    }
}

