﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoConnectWIFI.Models
{
    /// <summary>
    /// 网卡的 Interface 相关信息
    /// </summary>
    public class CardInterfaceInfo
    {
        public int InterfaceIndex { get; set; }
        public string InterfaceName { get; set; }

    }
}
