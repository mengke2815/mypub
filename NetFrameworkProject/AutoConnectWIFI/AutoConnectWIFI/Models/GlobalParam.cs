﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoConnectWIFI
{
    /// <summary>
    /// 全局变量
    /// </summary>
    public static class GlobalParam
    {
        public static string WifiProfile = "";

        /// <summary>
        /// Interface名（网卡描述）
        /// </summary>
        public static string WifiInterfaceName = "";

        public static int WifiInterfaceIndex = -1;

        #region 连接
        /// <summary>
        /// 连接-开始时间
        /// </summary>
        public static string StrConnectStart = "";
        /// <summary>
        /// 连接-结束时间
        /// </summary>
        public static string StrConnectEnd = "";
        #endregion

        #region 断开连接
        /// <summary>
        /// 断开-开始时间
        /// </summary>
        public static string StrDisConnectStart = "";
        /// <summary>
        /// 断开-结束时间
        /// </summary>
        public static string StrDisConnectEnd = "";
        #endregion

        /// <summary>
        /// 检测间隔（秒）
        /// </summary>
        public static int CheckInterval = 600;

    }
}
