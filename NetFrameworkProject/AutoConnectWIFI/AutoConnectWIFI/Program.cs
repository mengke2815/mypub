﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows.Forms;

namespace AutoConnectWIFI
{
    static class Program
    {
        static Mutex appMutex;

        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            #region MyMutex
            string exeName = "AutoConnectWIFI";
            string appName = "AutoConnectWIFI";

            bool createNew;
            appMutex = new Mutex(true, exeName, out createNew);
            if (!createNew)
            {
                appMutex.Close();
                appMutex = null;
                MessageBox.Show(appName + "已开启，进程为" + exeName + "！", "提示");
                return;
            }
            #endregion

            Application.ApplicationExit += new EventHandler(OnApplicationExit);

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }

        #region MyRegionRelMutex

        static void OnApplicationExit(object sender, EventArgs e)
        {
            RelMutex();
        }

        /// <summary>
        /// 释放
        /// </summary>
        public static void RelMutex()
        {
            try
            {
                if (appMutex != null)
                {
                    appMutex.ReleaseMutex();
                    appMutex.Close();
                }
            }
            catch (Exception expMu) { }
        }
        #endregion

    }
}
