﻿using System;
using System.Configuration;
using System.Linq;

namespace CommonUtils
{
    /// <summary>
    /// 应用程序配置文件访问辅助类，需要在项目上，右键引用“System.Configuration”。
    /// </summary>
    public class AppConfigHelper
    {
        /// <summary>
        /// 读取string配置项
        /// </summary>
        /// <param name="appSettingKey">AppSetting配置项的Key值</param>
        /// <param name="defaultValue">默认值</param>
        /// <returns>返回对应配置项的string类型value值</returns>
        public static string GetAppSettingValue(string appSettingKey, string defaultValue)
        {
            string result = ConfigurationManager.AppSettings[appSettingKey];
            if (String.IsNullOrEmpty(result))
            {
                return defaultValue;
            }
            return result;
        }

        /// <summary>
        /// 读取bool配置项
        /// </summary>
        /// <param name="appSettingKey">AppSetting配置项的Key值</param>
        /// <param name="defaultValue">默认值</param>
        /// <returns>返回对应配置项的bool类型value值</returns>
        public static bool GetAppSettingValue(string appSettingKey, bool defaultValue)
        {
            string result = ConfigurationManager.AppSettings[appSettingKey];
            if (String.IsNullOrEmpty(result))
            {
                return defaultValue;
            }
            bool boolResult = false;
            if (bool.TryParse(result, out boolResult))
            {
                return boolResult;
            }
            else
            {
                return defaultValue;
            }
        }

        /// <summary>
        /// 读取int配置项
        /// </summary>
        /// <param name="appSettingKey">AppSetting配置项的Key值</param>
        /// <param name="defaultValue">默认值</param>
        /// <returns>返回对应配置项的int类型value值</returns>
        public static int GetAppSettingValue(string appSettingKey, int defaultValue)
        {
            string result = ConfigurationManager.AppSettings[appSettingKey];
            if (String.IsNullOrEmpty(result))
            {
                return defaultValue;
            }
            int intResult = 0;
            if (int.TryParse(result, out intResult))
            {
                return intResult;
            }
            else
            {
                return defaultValue;
            }
        }

        /// <summary>
        /// 读取double类型配置项
        /// </summary>
        /// <param name="appSettingKey">AppSetting配置项的Key值</param>
        /// <param name="defaultValue">默认值</param>
        /// <returns>返回对应配置项的double类型value值</returns>
        public static double GetAppSettingValue(string appSettingKey, double defaultValue)
        {
            string result = ConfigurationManager.AppSettings[appSettingKey];
            if (String.IsNullOrEmpty(result))
            {
                return defaultValue;
            }
            double doubleResult = 0.0;
            if (double.TryParse(result, out doubleResult))
            {
                return doubleResult;
            }
            else
            {
                return defaultValue;
            }
        }

        /// <summary>
        /// 修改App.config中AppSetttings中的配置项
        /// </summary>
        /// <param name="name">要修改的配置项的名称</param>
        /// <param name="value">要修改的配置项的值</param>
        /// <returns></returns>
        public static bool SetAppSettings(string name, string value, bool _ismodify = true)
        {
            try
            {
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);//当前应用程序的配置文件
                if (config != null)
                {
                    AppSettingsSection appSettings = (AppSettingsSection)config.GetSection("appSettings");
                    if (appSettings.Settings.AllKeys.Contains(name))
                    {
                        if (_ismodify)
                            appSettings.Settings[name].Value = value;
                    }
                    else
                    {
                        appSettings.Settings.Add(name, value);
                    }
                    config.Save(); //保存配置文件

                    ConfigurationManager.RefreshSection("appSettings");        //刷新 appSettings 节点 （立即生效）

                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(String.Format("修改app.config配置{0}的值为{1}异常：{2}", name, value, ex.Message));
                return false;
            }
        }
    }
}