﻿using System;
using System.IO;
using System.Windows.Forms;
using Microsoft.Win32;

namespace UsualLib
{
    /// <summary>
    /// 开机启动.14.4.22
    /// </summary>
    public class StartOnSysStart
    {
        public static void RegAutoStart(string startOnSysStartRegKey)
        {
            RegistryKey reg;
            reg = Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
            reg.SetValue(startOnSysStartRegKey, Application.ExecutablePath);
            reg.Close();

            //string shPath = startOnSysStartRegKey;
            //string pExePath = Application.ExecutablePath;
            //string subKey = @"SOFTWARE\Microsoft\Windows\CurrentVersion\Run";
            //CheckRegeditBack(RegistryHive.LocalMachine, RegistryView.Registry32, subKey, shPath, pExePath);
            //CheckRegeditBack(RegistryHive.LocalMachine, RegistryView.Registry64, subKey, shPath, pExePath);
            //CheckRegeditBack(RegistryHive.CurrentUser, RegistryView.Registry32, subKey, shPath, pExePath);
            //CheckRegeditBack(RegistryHive.CurrentUser, RegistryView.Registry64, subKey, shPath, pExePath);
        }

        /// <summary>
        /// 开机自启注册表项
        /// </summary>
        /// <param name="rHive"></param>
        /// <param name="view"></param>
        /// <param name="subKey"></param>
        /// <param name="key"></param>
        /// <param name="value"></param>
        private static void CheckRegeditBack(RegistryHive rHive, RegistryView view, string subKey, string key, string value)
        {
             
                RegistryKey reg = RegistryKey.OpenBaseKey(rHive, view).OpenSubKey(subKey, true);
                if (reg != null)
                {
                    string[] subkeyvalues = reg.GetValueNames();

                    if (subkeyvalues != null && subkeyvalues.Length > 0)
                    {
                        string p = key.Trim().ToLower();
                        string va;
                        object obj;
                        foreach (string item in subkeyvalues)
                        {
                            obj = reg.GetValue(item);
                            if (obj != null)
                            {
                                va = obj.ToString();
                                if (va.ToLower().Trim() == p)
                                {
                                    reg.SetValue(item, value);
                                }
                            }
                        }
                    }
                }
             
        }

        public static void UnRegAutoStart(string startOnSysStartRegKey)
        {
            RegistryKey reg;
            reg = Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
            //如果存在则删除，不存在不处理，否则报错
            object obSt = reg.GetValue(startOnSysStartRegKey);
            if (obSt != null)
            {
                reg.DeleteValue(startOnSysStartRegKey);
            }
            reg.Close();
        }

        /// <summary>
        /// 检查开始启动，从注册表中查询。
        /// </summary>
        public static bool CheckStartWhenWinStart(string startOnSysStartRegKey)
        {
            bool rst = false;
            RegistryKey reg;
            reg = Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
            object obSt = reg.GetValue(startOnSysStartRegKey);
            if (obSt != null)
            {
                rst = true;
                //如果存在，检查路径是否正确。
                if (obSt.ToString().ToLower() != Application.ExecutablePath.ToLower())
                {
                    RegAutoStart(startOnSysStartRegKey);
                }
            }
            reg.Close();

            return rst;
        }
    }
}
