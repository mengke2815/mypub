﻿using AutoConnectWIFI.Models;
using CommonUtils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Net.NetworkInformation;
using System.Reflection;
using System.Threading;
using System.Windows.Forms;
using UsualLib;

namespace AutoConnectWIFI
{
    public partial class Form1 : Form
    {
        #region VAR

        /// <summary>
        /// 程序是否正在加载中
        /// </summary>
        bool _formLoading = false;

        /// <summary>
        /// 注册表开机项名称
        /// </summary>
        private string _regKey = "AutoConnectWIFI";

        #endregion


        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            _formLoading = true;
        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            try
            {
                this.Text = this.Text + " - 版本:" + GetAppVer();


                //加载WIFI用户配置
                var lstWifiName = GetWifiProfile();
                foreach (var ssid in lstWifiName)
                {
                    cbxWifiProfile.Items.Add(ssid);
                }
                if (cbxWifiProfile.Items.Count > 0)
                    cbxWifiProfile.SelectedIndex = 0;

                #region 加载本机的无线网卡 interface name

                List<CardInterfaceInfo> lstWifiCard = GetWifiCard2();
                if (lstWifiCard != null && lstWifiCard.Count > 0)
                {
                    cbxWifiInterfaceName.DisplayMember = "InterfaceName";
                    cbxWifiInterfaceName.ValueMember = "InterfaceIndex";
                    cbxWifiInterfaceName.DataSource = lstWifiCard;
                }

                if (cbxWifiInterfaceName.Items.Count > 0)
                    cbxWifiInterfaceName.SelectedIndex = 0;

                #endregion

                #region 将上次保存的配置，显示在UI

                ReadConfigToGlobalParam();
                //cbxWifiProfile，如果配置列表中包含上次保存的值，则修改界面
                if (!string.IsNullOrWhiteSpace(GlobalParam.WifiProfile) && lstWifiName.Contains(GlobalParam.WifiProfile))
                    cbxWifiProfile.Text = GlobalParam.WifiProfile; // 如果没绑定 DataSource，要用 Text
                //cbxWifiInterfaceName
                if (!string.IsNullOrWhiteSpace(GlobalParam.WifiInterfaceName) &&
                    lstWifiCard!=null && lstWifiCard.Count>0 && lstWifiCard.Any(x=>x.InterfaceName== GlobalParam.WifiInterfaceName))
                    cbxWifiInterfaceName.Text = GlobalParam.WifiInterfaceName;
                if (StartOnSysStart.CheckStartWhenWinStart(_regKey))
                {
                    chkOnSysStart.Checked = true;
                    this.Hide();//如果是“开机自动启动”，则自动隐藏到托盘。
                }
                    
                //nudCheckInterval
                if(GlobalParam.CheckInterval>0)
                    nudCheckInterval.Value = GlobalParam.CheckInterval;

                //dtpConnectStart
                if(!string.IsNullOrWhiteSpace(GlobalParam.StrConnectStart))
                    dtpConnectStart.Text = GlobalParam.StrConnectStart;
                if (!string.IsNullOrWhiteSpace(GlobalParam.StrConnectEnd))
                    dtpConnectEnd.Text = GlobalParam.StrConnectEnd;

                if (!string.IsNullOrWhiteSpace(GlobalParam.StrDisConnectStart))
                    dtpDisConnectStart.Text = GlobalParam.StrDisConnectStart;
                if (!string.IsNullOrWhiteSpace(GlobalParam.StrDisConnectEnd))
                    dtpDisConnectEnd.Text = GlobalParam.StrDisConnectEnd;

                #endregion

                //开始工作
                bwDo.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                _formLoading = false;
            }
        }


        List<string> GetWifiProfile()
        {

            string cmdLine = "wlan show profile";
            ProcessStartInfo si = new ProcessStartInfo();

            si.FileName = "netsh";
            si.Arguments = cmdLine;
            si.UseShellExecute = false;
            si.RedirectStandardOutput = true;
            si.CreateNoWindow = true;
            Process ps = new Process();
            ps.StartInfo = si;
            ps.Start();
            //获取cmd窗口的输出信息
            string cmdOutStr = ps.StandardOutput.ReadToEnd();

            ps.WaitForExit();

            List<string> lstWifiProfile = new List<string>();

            //"所有用户配置文件 : abc233"
            if (cmdOutStr.Contains("所有用户配置文件") && cmdOutStr.Contains(":"))
            {
                string tmp = cmdOutStr.Substring(cmdOutStr.IndexOf("所有用户配置文件"));
                var array1 = tmp.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
                if (array1 != null && array1.Length > 0)
                {
                    foreach (string item in array1)
                    {
                        var array2 = item.Split(new string[] { ":" }, StringSplitOptions.RemoveEmptyEntries);
                        if (array2 != null && array2.Length > 1)
                        {
                            lstWifiProfile.Add(array2[1].Trim());//必须TRIM
                        }
                    }

                }
            }

            return lstWifiProfile;
        }

        /// <summary>
        /// 命令行连接WIFI
        /// </summary>
        string CmdWifiConnect(string profileName)
        {
            //netsh wlan connect name=PROFILE
            //   如果电脑中存在多个网卡那么要制定interface：
            // netsh wlan connect ssid = SSID name = PROFILENAME interface="INTERFACE"

            string cmdLine = "wlan connect name=" + profileName;
            ProcessStartInfo si = new ProcessStartInfo();

            si.FileName = "netsh";
            si.Arguments = cmdLine;
            si.UseShellExecute = false;
            si.RedirectStandardOutput = true;
            si.CreateNoWindow = true;
            Process ps = new Process();
            ps.StartInfo = si;
            ps.Start();
            //获取cmd窗口的输出信息
            string cmdOutStr = ps.StandardOutput.ReadToEnd();

            ps.WaitForExit();

            return cmdOutStr;//返回控制台输出信息
        }

        /// <summary>
        /// 命令行断开WIFI
        /// </summary>
        string CmdWifiDisConnect(string profileName)
        {

            //   断开网络
            // nnetsh wlan disconnect

            string cmdLine = "wlan disconnect";
            ProcessStartInfo si = new ProcessStartInfo();

            si.FileName = "netsh";
            si.Arguments = cmdLine;
            si.UseShellExecute = false;
            si.RedirectStandardOutput = true;
            si.CreateNoWindow = true;
            Process ps = new Process();
            ps.StartInfo = si;
            ps.Start();
            //获取cmd窗口的输出信息
            string cmdOutStr = ps.StandardOutput.ReadToEnd();

            ps.WaitForExit();

            return cmdOutStr;//返回控制台输出信息
        }


        List<string> GetWifiCard()
        {
            List<string> lstWifiCard = new List<string>();

            NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();
            foreach (var adapter in nics)
            {
                //有些网卡不在处理范围
                if (SkipNet(adapter.Description))
                {
                    //LogDebug("跳过的网卡：" + adapter.Description);
                    continue;
                }

                if (adapter.NetworkInterfaceType == NetworkInterfaceType.Wireless80211)
                {
                    string Description = adapter.Description;
                    string Name = adapter.Name;

                    //IPInterfaceProperties properties = adapter.GetIPProperties();
                    //IPv4InterfaceProperties ipv4 = properties.GetIPv4Properties();
                    //string InterfaceIndex = ipv4.Index.ToString();

                    lstWifiCard.Add(Description);
                }
            }

            return lstWifiCard;
        }

        List<CardInterfaceInfo> GetWifiCard2()
        {
            List<CardInterfaceInfo> lstWifiCard = new List<CardInterfaceInfo>();

            NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();
            foreach (var adapter in nics)
            {
                //有些网卡不在处理范围
                if (SkipNet(adapter.Description))
                {
                    //LogDebug("跳过的网卡：" + adapter.Description);
                    continue;
                }

                if (adapter.NetworkInterfaceType == NetworkInterfaceType.Wireless80211)
                {
                    string Description = adapter.Description;
                    string Name = adapter.Name;

                    IPInterfaceProperties properties = adapter.GetIPProperties();
                    IPv4InterfaceProperties ipv4 = properties.GetIPv4Properties();
                    string InterfaceIndex = ipv4.Index.ToString();
                    CardInterfaceInfo cardInfo = new CardInterfaceInfo();
                    cardInfo.InterfaceIndex = ipv4.Index;
                    cardInfo.InterfaceName = Description;
                    lstWifiCard.Add(cardInfo);
                }
            }

            return lstWifiCard;
        }

        /// <summary>
        /// 跳过网卡
        /// </summary>
        /// <param name="myDesc"></param>
        /// <returns></returns>
        bool SkipNet(string myDesc)
        {
            if (string.IsNullOrWhiteSpace(myDesc))
                return false;

            if (myDesc.Contains("Software Loopback Interface"))
                return true;

            if (myDesc.Contains("Microsoft Wi-Fi Direct Virtual Adapter"))
                return true;

            if (myDesc.Contains("Bluetooth Device"))
                return true;

            if (myDesc.Contains("Microsoft Teredo Tunneling Adapter"))
                return true;

            return false;
        }

        /// <summary>
        /// 将UI参数放进全局变量
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnApply_Click(object sender, EventArgs e)
        {
            try
            {
                if (nudCheckInterval.Value < 6)                 
                    nudCheckInterval.Value = 6;                

                //把参数保存到配置文件中

                AppConfigHelper.SetAppSettings("CheckInterval", ((int)nudCheckInterval.Value).ToString());

                AppConfigHelper.SetAppSettings("WifiProfile", cbxWifiProfile.Text);
                AppConfigHelper.SetAppSettings("WifiInterfaceIndex", cbxWifiInterfaceName.SelectedValue.ToString());
                AppConfigHelper.SetAppSettings("WifiInterfaceName", cbxWifiInterfaceName.Text);

                AppConfigHelper.SetAppSettings("StrConnectStart", dtpConnectStart.Text);
                AppConfigHelper.SetAppSettings("StrConnectEnd", dtpConnectEnd.Text);

                AppConfigHelper.SetAppSettings("StrDisConnectStart", dtpDisConnectStart.Text);
                AppConfigHelper.SetAppSettings("StrDisConnectEnd", dtpDisConnectEnd.Text);

                CheckAndDo();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// 读取配置文件到全局参数
        /// </summary>
        void ReadConfigToGlobalParam()
        {
            GlobalParam.CheckInterval = AppConfigHelper.GetAppSettingValue("CheckInterval", 600);

            GlobalParam.WifiProfile = AppConfigHelper.GetAppSettingValue("WifiProfile", "");
            GlobalParam.WifiInterfaceIndex = AppConfigHelper.GetAppSettingValue("WifiInterfaceIndex", 0);
            GlobalParam.WifiInterfaceName = AppConfigHelper.GetAppSettingValue("WifiInterfaceName", "");

            GlobalParam.StrConnectStart = AppConfigHelper.GetAppSettingValue("StrConnectStart", "");
            GlobalParam.StrConnectEnd = AppConfigHelper.GetAppSettingValue("StrConnectEnd", "");

            GlobalParam.StrDisConnectStart = AppConfigHelper.GetAppSettingValue("StrDisConnectStart", "");
            GlobalParam.StrDisConnectEnd = AppConfigHelper.GetAppSettingValue("StrDisConnectEnd", "");
        }

        private void bwDo_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                CheckAndDo();
            }
            finally
            {
                int mySleep = GlobalParam.CheckInterval * 1000;
                Thread.Sleep(mySleep);
            }
        }

        /// <summary>
        /// 检查并执行
        /// </summary>
        void CheckAndDo()
        {
            ReadConfigToGlobalParam();

            if (string.IsNullOrWhiteSpace(GlobalParam.WifiProfile) || string.IsNullOrWhiteSpace(GlobalParam.WifiInterfaceName))
            {
                return;
            }

            //判断时间段，是连接，还是断开。
            bool isIn = false;//是否在时间段内，默认 false
            bool isConnect = true;

            DateTime dtNow = DateTime.Now;
            //连接时间段
            DateTime dt1 = DateTime.Parse(dtNow.ToString("yyyy-MM-dd ") + GlobalParam.StrConnectStart);
            DateTime dt2 = DateTime.Parse(dtNow.ToString("yyyy-MM-dd ") + GlobalParam.StrConnectEnd);

            DateTime dt3 = DateTime.Parse(dtNow.ToString("yyyy-MM-dd ") + GlobalParam.StrDisConnectStart);
            DateTime dt4 = DateTime.Parse(dtNow.ToString("yyyy-MM-dd ") + GlobalParam.StrDisConnectEnd);

            if (dt1 < dtNow && dtNow < dt2)
            {
                LogListBoxWithTime("在连接时间段");
                isIn = true;
                isConnect = true;
            }
            else if (dt3 < dtNow || dtNow < dt4)
            {
                LogListBoxWithTime("在断开时间段");
                isIn = true;
                isConnect = false;
            }
            else
            {
                LogListBoxWithTime("未在任何区间。");
            }

            if (isIn)//在时间段内
            {
                if (isConnect)
                {
                    //在连接时间段内
                    LogListBoxWithTime("在连接时间段内");
                    //检测是否已连接，如果不检测，重复调用连接命令，连接过程中是断网状态。
                    //CheckWifiOn2
                    //if (CheckWifiOn(GlobalParam.WifiInterfaceIndex))
                    if (CheckWifiOn2(GlobalParam.WifiProfile, GlobalParam.WifiInterfaceName))
                    {
                        LogListBoxWithTime("WIFI已是连接状态！");
                    }
                    else
                    {
                        LogListBoxWithTime("WIFI已是断开状态，准备连接！");
                        string connectOut = CmdWifiConnect(GlobalParam.WifiProfile);
                        LogListBoxWithTime(connectOut);
                    }
                }
                else
                {
                    //在断开时间段内
                    LogListBoxWithTime("在断开时间段内");

                    //检测是否已连接，如果不检测，重复调用连接命令，连接过程中是断网状态。
                    if (CheckWifiOn2(GlobalParam.WifiProfile, GlobalParam.WifiInterfaceName))
                    {
                        LogListBoxWithTime("WIFI已是连接状态，准备断开！");
                        string connectOut = CmdWifiDisConnect(GlobalParam.WifiProfile);
                        LogListBoxWithTime(connectOut);
                    }
                    else
                    {
                        LogListBoxWithTime("WIFI已是断开状态！");
                    }
                }
            }
        }

        /// <summary>
        /// 只是检测了网卡是否已连接，未检测WIFI名称是否为指定的。
        /// </summary>
        /// <param name="WifiInterfaceIndex"></param>
        /// <returns></returns>
        bool CheckWifiOn(int WifiInterfaceIndex)
        {
            NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();
            foreach (var adapter in nics)
            {
                //有些网卡不在处理范围
                if (SkipNet(adapter.Description))
                {
                    //LogDebug("跳过的网卡：" + adapter.Description);
                    continue;
                }

                if (adapter.NetworkInterfaceType == NetworkInterfaceType.Wireless80211)
                {
                    string Description = adapter.Description;
                    string Name = adapter.Name;

                    IPInterfaceProperties properties = adapter.GetIPProperties();
                    IPv4InterfaceProperties ipv4 = properties.GetIPv4Properties();
                    //string InterfaceIndex = ipv4.Index.ToString();
                    if (ipv4.Index == WifiInterfaceIndex)
                    {
                        if (adapter.OperationalStatus == OperationalStatus.Up)
                        {
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// 检测WIFI是否连接
        /// </summary>
        /// <param name="WifiInterfaceIndex"></param>
        /// <returns></returns>
        bool CheckWifiOn2(string wifiProfileName,string wifiDesc)
        {
            // netsh wlan show interfaces
            /*
             系统上有 1 个接口:

   名称                   : WLAN
   描述                   : MERCURY Wireless N Adapter
   GUID                   : b820c2c7-f54d-4bcd-8d28-4e5a6f5e160a
   物理地址               : 5c:de:34:16:e9:65
   状态                   : 已连接
   SSID                   : SS-WIFI-Office
   BSSID                  : 40:fe:95:23:ef:60
   网络类型               : 结构
   无线电类型             : 802.11ac
   身份验证               : WPA2 - 个人
   密码                   : CCMP
   连接模式               : 配置文件
   信道                   : 36
   接收速率(Mbps)         : 600
   传输速率 (Mbps)        : 866.5
   信号                   : 98%
   配置文件               : SS-WIFI-Office

   承载网络状态  : 不可用

             */


            /*
             系统上有 1 个接口:

    名称                   : WLAN
    描述                   : MERCURY Wireless N Adapter
    GUID                   : b820c2c7-f54d-4bcd-8d28-4e5a6f5e160a
    物理地址               : 5c:de:34:16:e9:65
    状态                   : 已断开连接
    无线电状态           : 硬件 开
                             软件 开

    承载网络状态  : 不可用
             */

            string cmdLine = "wlan show interfaces";
            ProcessStartInfo si = new ProcessStartInfo();

            si.FileName = "netsh";
            si.Arguments = cmdLine;
            si.UseShellExecute = false;
            si.RedirectStandardOutput = true;
            si.CreateNoWindow = true;
            Process ps = new Process();
            ps.StartInfo = si;
            ps.Start();
            //获取cmd窗口的输出信息
            string cmdOutStr = ps.StandardOutput.ReadToEnd();

            ps.WaitForExit();

            if (cmdOutStr.Contains(wifiProfileName) && cmdOutStr.Contains(wifiDesc)
                && cmdOutStr.Contains("已连接"))
            {
                return true;
            }
            else {
                return false;
            }

            
        }

        private void bwDo_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //循环
            bwDo.RunWorkerAsync();
        }

        /// <summary>
        /// UI日志，有时间前缀。
        /// </summary>
        /// <param name="msg"></param>
        void LogListBoxWithTime(string msg)
        {
            LogListBox(DateTime.Now.ToString() + " " + msg);
        }

        void LogListBox(string msg)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new MethodInvoker(() =>
                {
                    if (lbxLog.Items.Count > 1999)
                    {
                        lbxLog.Items.Clear();
                    }
                    lbxLog.Items.Insert(0, msg);
                }));
            }
            else
            {
                if (lbxLog.Items.Count > 1999)
                {
                    lbxLog.Items.Clear();
                }
                lbxLog.Items.Insert(0, msg);
            }
        }

        private void chkOnSysStart_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (!_formLoading)
                {
                    // 获取全局 开始 文件夹位置
                    var pathAll = Environment.GetFolderPath(Environment.SpecialFolder.CommonStartup);
                    // 获取当前登录用户的 开始 文件夹位置
                    var pathCur = Environment.GetFolderPath(Environment.SpecialFolder.Startup);

                    if (chkOnSysStart.Checked)
                    {
                        StartOnSysStart.RegAutoStart(_regKey);
                        //ShortcutUtil.Create(pathCur, regKey, Application.ExecutablePath, null, null);
                    }
                    else
                    {
                        StartOnSysStart.UnRegAutoStart(_regKey);
                        //ShortcutUtil.Del(pathCur, regKey);
                    }
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show("chkOnSysStart_CheckedChanged ex:" + ex.Message);
            }

        }

        #region Get Ver

        /// <summary>
        /// 获取当前程序版本.fileversion
        /// </summary>
        /// <returns></returns>
        public static string GetAppVer()
        {
            string currentVer = "1.0.0.0";
            try
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyFileVersionAttribute), false);
                if (attributes.Length > 0)
                {
                    currentVer = ((AssemblyFileVersionAttribute)attributes[0]).Version;
                }
            }
            catch
            { }
            return currentVer;
        }


        #endregion

        private void nfiMain_DoubleClick(object sender, EventArgs e)
        {
            this.Show();
            this.ShowInTaskbar = true;
        }

        private void btnToTray_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void tsmiExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void tsmiAbout_Click(object sender, EventArgs e)
        {
            MessageBox.Show("OK");
        }
    }
}
