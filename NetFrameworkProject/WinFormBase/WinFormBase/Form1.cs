﻿using CommonUtils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinFormBase
{
    public partial class Form1 : Form
    {
        System.Timers.Timer _tmrRemeber;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.Text = this.Text + " [" + AppVerUtil .GetAppVer()+ "]";
            //将配置文件里的值，加载到界面 
            RemForm.ReadRememberForm(this, null);

            //开启记忆功能
            _tmrRemeber = new System.Timers.Timer();
            _tmrRemeber.Elapsed += new System.Timers.ElapsedEventHandler(_tmrRemeber_Elapsed);
            _tmrRemeber.Interval = 10 * 1000;
            _tmrRemeber.Start();
        }

        void _tmrRemeber_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                RemForm.RememberForm(this, null);
            }
            catch (Exception ex)
            {
                //MessageBox.Show("_tmrRemeber_Elapsed" + ex.Message);
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult dr = MessageBox.Show("确定要退出吗？", "确定",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
            if (dr == DialogResult.No)
            {
                e.Cancel = true;
                return;
            }
        }

        private void tsmiExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
