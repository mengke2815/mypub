﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CommonUtils
{
   public static class AppVerUtil
    {
        /// <summary>
        /// 读取AssemblyInfo.cs-AssemblyFileVersion 作为程序版本
        /// </summary>
        /// <returns></returns>
        public static string GetAppVer()
        {
            string currentVer = "1.0.0.0";
            try
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyFileVersionAttribute), false);
                if (attributes.Length > 0)
                {
                    currentVer = ((AssemblyFileVersionAttribute)attributes[0]).Version;
                }
            }
            catch
            { }
            return currentVer;
        }
    }
}
