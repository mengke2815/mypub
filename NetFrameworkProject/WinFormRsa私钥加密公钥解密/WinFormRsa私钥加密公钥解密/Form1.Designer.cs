﻿
namespace WinFormRsa私钥加密公钥解密
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.btnPrivateKeyEncrypt = new System.Windows.Forms.Button();
            this.txtPrivateKey = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtPubKey = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtMingWen = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtJiaMiHou = new System.Windows.Forms.TextBox();
            this.btnPubKeyDecrypt = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.txtJieMiHou = new System.Windows.Forms.TextBox();
            this.cbxPrivateKeyFormat = new System.Windows.Forms.ComboBox();
            this.btnPubKeyEncrypt = new System.Windows.Forms.Button();
            this.btnPrivateKeyDecrypt = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnPrivateKeyEncrypt
            // 
            this.btnPrivateKeyEncrypt.Location = new System.Drawing.Point(1037, 195);
            this.btnPrivateKeyEncrypt.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnPrivateKeyEncrypt.Name = "btnPrivateKeyEncrypt";
            this.btnPrivateKeyEncrypt.Size = new System.Drawing.Size(100, 29);
            this.btnPrivateKeyEncrypt.TabIndex = 0;
            this.btnPrivateKeyEncrypt.Text = "私钥加密";
            this.btnPrivateKeyEncrypt.UseVisualStyleBackColor = true;
            this.btnPrivateKeyEncrypt.Click += new System.EventHandler(this.btnPrivateKeyEncrypt_Click);
            // 
            // txtPrivateKey
            // 
            this.txtPrivateKey.Location = new System.Drawing.Point(168, 32);
            this.txtPrivateKey.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtPrivateKey.Multiline = true;
            this.txtPrivateKey.Name = "txtPrivateKey";
            this.txtPrivateKey.Size = new System.Drawing.Size(831, 54);
            this.txtPrivateKey.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(76, 36);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 15);
            this.label1.TabIndex = 2;
            this.label1.Text = "私钥";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(76, 99);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 15);
            this.label2.TabIndex = 4;
            this.label2.Text = "公钥";
            // 
            // txtPubKey
            // 
            this.txtPubKey.Location = new System.Drawing.Point(168, 95);
            this.txtPubKey.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtPubKey.Multiline = true;
            this.txtPubKey.Name = "txtPubKey";
            this.txtPubKey.Size = new System.Drawing.Size(831, 54);
            this.txtPubKey.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(76, 189);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 15);
            this.label3.TabIndex = 6;
            this.label3.Text = "明文";
            // 
            // txtMingWen
            // 
            this.txtMingWen.Location = new System.Drawing.Point(168, 185);
            this.txtMingWen.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtMingWen.Multiline = true;
            this.txtMingWen.Name = "txtMingWen";
            this.txtMingWen.Size = new System.Drawing.Size(831, 54);
            this.txtMingWen.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(76, 254);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 15);
            this.label4.TabIndex = 8;
            this.label4.Text = "加密后";
            // 
            // txtJiaMiHou
            // 
            this.txtJiaMiHou.Location = new System.Drawing.Point(168, 250);
            this.txtJiaMiHou.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtJiaMiHou.Multiline = true;
            this.txtJiaMiHou.Name = "txtJiaMiHou";
            this.txtJiaMiHou.Size = new System.Drawing.Size(831, 54);
            this.txtJiaMiHou.TabIndex = 7;
            // 
            // btnPubKeyDecrypt
            // 
            this.btnPubKeyDecrypt.Location = new System.Drawing.Point(1037, 265);
            this.btnPubKeyDecrypt.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnPubKeyDecrypt.Name = "btnPubKeyDecrypt";
            this.btnPubKeyDecrypt.Size = new System.Drawing.Size(100, 29);
            this.btnPubKeyDecrypt.TabIndex = 9;
            this.btnPubKeyDecrypt.Text = "公钥解密";
            this.btnPubKeyDecrypt.UseVisualStyleBackColor = true;
            this.btnPubKeyDecrypt.Click += new System.EventHandler(this.btnPubKeyDecrypt_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(76, 320);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 15);
            this.label5.TabIndex = 11;
            this.label5.Text = "解密后";
            // 
            // txtJieMiHou
            // 
            this.txtJieMiHou.Location = new System.Drawing.Point(168, 316);
            this.txtJieMiHou.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtJieMiHou.Multiline = true;
            this.txtJieMiHou.Name = "txtJieMiHou";
            this.txtJieMiHou.Size = new System.Drawing.Size(831, 54);
            this.txtJieMiHou.TabIndex = 10;
            // 
            // cbxPrivateKeyFormat
            // 
            this.cbxPrivateKeyFormat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxPrivateKeyFormat.FormattingEnabled = true;
            this.cbxPrivateKeyFormat.Items.AddRange(new object[] {
            "PKCS1",
            "PKCS8"});
            this.cbxPrivateKeyFormat.Location = new System.Drawing.Point(1037, 36);
            this.cbxPrivateKeyFormat.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbxPrivateKeyFormat.Name = "cbxPrivateKeyFormat";
            this.cbxPrivateKeyFormat.Size = new System.Drawing.Size(160, 23);
            this.cbxPrivateKeyFormat.TabIndex = 12;
            // 
            // btnPubKeyEncrypt
            // 
            this.btnPubKeyEncrypt.Location = new System.Drawing.Point(1195, 195);
            this.btnPubKeyEncrypt.Margin = new System.Windows.Forms.Padding(4);
            this.btnPubKeyEncrypt.Name = "btnPubKeyEncrypt";
            this.btnPubKeyEncrypt.Size = new System.Drawing.Size(100, 29);
            this.btnPubKeyEncrypt.TabIndex = 13;
            this.btnPubKeyEncrypt.Text = "公钥加密";
            this.btnPubKeyEncrypt.UseVisualStyleBackColor = true;
            this.btnPubKeyEncrypt.Click += new System.EventHandler(this.btnPubKeyEncrypt_Click);
            // 
            // btnPrivateKeyDecrypt
            // 
            this.btnPrivateKeyDecrypt.Location = new System.Drawing.Point(1195, 265);
            this.btnPrivateKeyDecrypt.Margin = new System.Windows.Forms.Padding(4);
            this.btnPrivateKeyDecrypt.Name = "btnPrivateKeyDecrypt";
            this.btnPrivateKeyDecrypt.Size = new System.Drawing.Size(100, 29);
            this.btnPrivateKeyDecrypt.TabIndex = 14;
            this.btnPrivateKeyDecrypt.Text = "私钥解密";
            this.btnPrivateKeyDecrypt.UseVisualStyleBackColor = true;
            this.btnPrivateKeyDecrypt.Click += new System.EventHandler(this.btnPrivateKeyDecrypt_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1383, 562);
            this.Controls.Add(this.btnPrivateKeyDecrypt);
            this.Controls.Add(this.btnPubKeyEncrypt);
            this.Controls.Add(this.cbxPrivateKeyFormat);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtJieMiHou);
            this.Controls.Add(this.btnPubKeyDecrypt);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtJiaMiHou);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtMingWen);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtPubKey);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtPrivateKey);
            this.Controls.Add(this.btnPrivateKeyEncrypt);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnPrivateKeyEncrypt;
        private System.Windows.Forms.TextBox txtPrivateKey;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtPubKey;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtMingWen;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtJiaMiHou;
        private System.Windows.Forms.Button btnPubKeyDecrypt;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtJieMiHou;
        private System.Windows.Forms.ComboBox cbxPrivateKeyFormat;
        private System.Windows.Forms.Button btnPubKeyEncrypt;
        private System.Windows.Forms.Button btnPrivateKeyDecrypt;
    }
}

