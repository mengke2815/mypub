﻿using CommonUtils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WinFormRsa私钥加密公钥解密
{
    public partial class Form1 : Form
    {
        System.Timers.Timer _tmrRemeber;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            RemForm.ReadRememberForm(this, null);

            _tmrRemeber = new System.Timers.Timer();
            _tmrRemeber.Elapsed += new System.Timers.ElapsedEventHandler(_tmrRemeber_Elapsed);
            _tmrRemeber.Interval = 10 * 1000;
            _tmrRemeber.Start();

            cbxPrivateKeyFormat.SelectedIndex = 1;
        }

        void _tmrRemeber_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                RemForm.RememberForm(this, null);
            }
            catch (Exception ex)
            {
                //MessageBox.Show("_tmrRemeber_Elapsed" + ex.Message);
            }
        }

        private void btnPrivateKeyEncrypt_Click(object sender, EventArgs e)
        {
            try
            {
                //byte[] rst = RsaEncryptUtil.encryptByPrivateKey(txtMingWen.Text, txtPrivateKey.Text);
                byte[] rst = RsaEncryptUtil.encryptByPrivateKey(txtMingWen.Text, txtPrivateKey.Text, cbxPrivateKeyFormat.Text);

                //加密后一般转Base64String ,Base64FormattingOptions.InsertLineBreaks.
                string base64str = Convert.ToBase64String(rst, Base64FormattingOptions.InsertLineBreaks);

                txtJiaMiHou.Text = base64str;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnPubKeyDecrypt_Click(object sender, EventArgs e)
        {
            try
            {
                byte[] rst = RsaEncryptUtil.decryptByPublicKey(txtJiaMiHou.Text, txtPubKey.Text);

                string strRst = Encoding.UTF8.GetString(rst);
                txtJieMiHou.Text = strRst;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnPubKeyEncrypt_Click(object sender, EventArgs e)
        {
            try
            {
                string rst = RsaEncryptUtil.RSAEncrypt(txtMingWen.Text, "UTF-8", txtPubKey.Text);
                txtJiaMiHou.Text = rst;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnPrivateKeyDecrypt_Click(object sender, EventArgs e)
        {
            try
            {
                string strRst = RsaEncryptUtil.RSADecrypt(txtJiaMiHou.Text, "UTF-8", txtPrivateKey.Text, cbxPrivateKeyFormat.Text);
                txtJieMiHou.Text = strRst;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
