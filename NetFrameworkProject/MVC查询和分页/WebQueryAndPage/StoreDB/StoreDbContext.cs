﻿using StoreDB.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace StoreDB
{
    public class StoreDbContext: DbContext
    {
        public StoreDbContext()
            : base("name=StoreDbContext")
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //移除复数表名
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            base.OnModelCreating(modelBuilder);
        }

        public virtual DbSet<t_bd_item_info> t_bd_item_info { get; set; }
    }
}
