﻿using System.ComponentModel.DataAnnotations;

namespace StoreDB.Models
{
    public class t_bd_item_info
    {
        [Key] //主键 
        public string item_no { get; set; }

        public string item_name { get; set; }

        public decimal price { get; set; }

        //public DateTime? build_date { get; set; }
    }
}
