﻿using LinqKit;
using PagedList;
using StoreDB;
using StoreDB.Models;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc;

namespace WebQueryAndPage.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return Redirect("/Home/SearchIndex");
            //return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult SearchIndex(string ItemNo, string ItemName, int page = 1)
        {
            try
            {
                //为true 默认加载所有
                Expression<Func<t_bd_item_info, bool>> where = PredicateBuilder.New<t_bd_item_info>(true);

                //动态拼接查询条件
                if (!string.IsNullOrWhiteSpace(ItemNo))
                {
                    where = where.And(x => x.item_no == ItemNo);
                }
                if (!string.IsNullOrWhiteSpace(ItemName))
                {
                    where = where.And(x => x.item_name.Contains(ItemName));
                }
                StoreDbContext dbContext = new StoreDbContext();

                //直接ToPagedList，不要使用ToList
                var mylist = dbContext.t_bd_item_info.Where(where).OrderBy(x => x.item_no).ToPagedList(page, 10);

                return View(mylist);
            }
            catch (Exception ex)
            {
                return Content(ex.Message);
            }
        }
    }
}