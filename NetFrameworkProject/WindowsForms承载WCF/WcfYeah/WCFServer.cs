﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceModel.Web;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WcfYeah
{
    public class WCFServer
    {
        public WebServiceHost host = null;

        public WCFServer()
        {
            /* 硬编码形式配置WCF服务
             * WebServiceHost需要在项目中右键引用System.ServiceModel.Web.dll程序集
             *  */

            #region 优化调整
            //对外连接数，根据实际情况加大
            if (ServicePointManager.DefaultConnectionLimit < 100)
                System.Net.ServicePointManager.DefaultConnectionLimit = 100;

            int workerThreads;//工作线程数
            int completePortsThreads; //异步I/O 线程数
            ThreadPool.GetMinThreads(out workerThreads, out completePortsThreads);

            int blogCnt = 100;
            if (workerThreads < blogCnt)
            {
                // MinThreads 值不要超过 (max_thread /2  )，否则会不生效。要不然就同时加大max_thread
                ThreadPool.SetMinThreads(blogCnt, blogCnt);
            }
            #endregion           

            string port = "16110";

            Uri baseURI = new Uri("http://localhost:" + port.ToString() + "/myapi");
            //注意：这里是实现类，不是接口，否则会报：ServiceHost 仅支持类服务类型。
            host = new WebServiceHost(typeof(Service1), baseURI);

            WebHttpBinding binding = new WebHttpBinding();
            // 这里不需要安全验证
            binding.Security.Mode = WebHttpSecurityMode.None;
            host.AddServiceEndpoint(typeof(IService1), binding, "");
            binding.MaxReceivedMessageSize = 2147483647;
            binding.MaxBufferSize = 2147483647;
            binding.MaxBufferPoolSize = 2147483647;

            binding.OpenTimeout = new TimeSpan(0, 10, 0);
            binding.CloseTimeout = new TimeSpan(0, 10, 0);
            binding.SendTimeout = new TimeSpan(0, 10, 0);
            binding.ReceiveTimeout = new TimeSpan(0, 10, 0);

            //项目需要引用System.Runtime.Serialization.dll
            binding.ReaderQuotas.MaxDepth = 2147483647;
            binding.ReaderQuotas.MaxStringContentLength = 2147483647;
            binding.ReaderQuotas.MaxArrayLength = 2147483647;
            binding.ReaderQuotas.MaxBytesPerRead = 2147483647;
            binding.ReaderQuotas.MaxNameTableCharCount = 2147483647;

            ServiceThrottlingBehavior mdBehavior = new ServiceThrottlingBehavior()
            {
                MaxConcurrentCalls = 96, //16 * CPU核心数
                MaxConcurrentSessions = 600,//100 * CPU核心数
                MaxConcurrentInstances = 696,//MaxConcurrentCalls+MaxConcurrentSession
            };
            host.Description.Behaviors.Add(mdBehavior);
        }


        public void Start()
        {
            host.Open();
        }

        public void Close()
        {
            if (host != null)
            {
                host.Close();
            }
        }

    }
}
