﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;

namespace WcfYeah
{
    // 调整 ServiceBehavior，使其支持并发
    [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.PerCall, UseSynchronizationContext = false)]
    public class Service1 : IService1
    {
        public CompositeType geta(CompositeType composite)
        {
            CompositeType myret = new CompositeType();
            try
            {
                string ip = GetClientIpWcf();
                if (composite == null)
                    myret.StringValue = "输入实体为空:" + ip + " " + DateTime.Now.ToString();
                else
                    myret.StringValue = "输入实体不为空:" + ip + " " + DateTime.Now.ToString();
            }
            catch (Exception ex)
            {
                myret.StringValue = "发生异常:" + ex.Message;
            }
            return myret;
        }

        /// <summary>
        /// WCF 获取客户端IP，必须是WebServiceHost
        /// </summary>
        /// <returns></returns>
        public static string GetClientIpWcf()
        {
            OperationContext context = OperationContext.Current;
            MessageProperties messageProperties = context.IncomingMessageProperties;
            RemoteEndpointMessageProperty endpointProperty = messageProperties[RemoteEndpointMessageProperty.Name] as RemoteEndpointMessageProperty;
            HttpRequestMessageProperty requestProperty = messageProperties[HttpRequestMessageProperty.Name] as HttpRequestMessageProperty;
            string myip = !string.IsNullOrEmpty(requestProperty.Headers["X-Real-IP"]) ? requestProperty.Headers["X-Real-IP"] : endpointProperty.Address;

            return myip;
        }

    }
}
