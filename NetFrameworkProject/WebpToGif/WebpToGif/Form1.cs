﻿using ImageMagick;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WebpToGif
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(txtWebpPath.Text))
                {
                    MessageBox.Show("路径不能为空。");
                    return;
                }
                if (!File.Exists(txtWebpPath.Text))
                {
                    MessageBox.Show("文件不存在。");
                    return;
                }

                string webpFullName = txtWebpPath.Text;
                //截取目标目录和目标文件名
                string targetDir = Path.GetDirectoryName(webpFullName);
                string targetName = Path.GetFileNameWithoutExtension(webpFullName);
                targetName = targetName + ".gif";
                string gifFullName = Path.Combine(targetDir, targetName);
                WebpToGif(webpFullName, gifFullName);
                MessageBox.Show("转换完成："+ gifFullName);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void WebpToGif(string webpFullName, string gifFullName)
        {
            //webp 转 GIF
            using (var animatedWebP = new MagickImageCollection(webpFullName))
            {
                animatedWebP.Write(gifFullName, MagickFormat.Gif);
            }
            ////GIF  转 webp
            //using (var animatedGif = new MagickImageCollection("animated.gif"))
            //{
            //    animatedGif.Write("animated-generated.webp", MagickFormat.WebP);
            //}
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            if (ofdWebp.ShowDialog() == DialogResult.OK)
            {
                txtWebpPath.Text = ofdWebp.FileName;
            }
        }
    }
}
