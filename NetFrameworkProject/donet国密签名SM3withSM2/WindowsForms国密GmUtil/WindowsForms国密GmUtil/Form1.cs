﻿using CommonUtils;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Math;
using Org.BouncyCastle.Utilities.Encoders;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsForms国密GmUtil
{
    public partial class Form1 : CommonUtils.FormBase
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        //签名
        private void btnSign_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(txtPrivateKey.Text) || string.IsNullOrWhiteSpace(txtUserId.Text) || string.IsNullOrWhiteSpace(txtToSignStr.Text))
                {
                    MessageBox.Show("私钥,USERID,签名字符串不能为空。");
                    return;
                }

                // 由d生成私钥 ---------------------
                BigInteger d = new BigInteger(txtPrivateKey.Text, 16);
                ECPrivateKeyParameters bcecPrivateKey = CommonUtils.GmUtil.GetPrivatekeyFromD(d);
                byte[] userId = Encoding.UTF8.GetBytes(txtUserId.Text);

                byte[] msg = System.Text.Encoding.UTF8.GetBytes(txtToSignStr.Text);
                byte[] sig = GmUtil.SignSm3WithSm2(msg, userId, bcecPrivateKey);
                
                //byte[] bytTmp = Hex.Decode(txtSign.Text);
                txtSign.Text = Convert.ToBase64String(sig);

                string sign16 = Hex.ToHexString(sig);
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        //验签
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(txtPubKey.Text) || string.IsNullOrWhiteSpace(txtUserId.Text) || string.IsNullOrWhiteSpace(txtToSignStr.Text)
                    || string.IsNullOrWhiteSpace(txtSign.Text))
                {
                    MessageBox.Show("公钥,USERID,签名字符串，签名值不能为空。");
                    return;
                }
                //公钥128位，拆成2个64位。
                string pa = txtPubKey.Text.Substring(0, 64);
                string pb = txtPubKey.Text.Substring(64, 64);

                AsymmetricKeyParameter publicKey1 = GmUtil.GetPublickeyFromXY(new BigInteger(pa, 16), new BigInteger(pb, 16));

                byte[] userId = Encoding.UTF8.GetBytes(txtUserId.Text);
                byte[] msg = System.Text.Encoding.UTF8.GetBytes(txtToSignStr.Text);
                //byte[] bytSig = Hex.Decode(txtSign.Text);
                byte[] bytSig = Convert.FromBase64String(txtSign.Text);
                bool bRst = GmUtil.VerifySm3WithSm2(msg, userId, bytSig, publicKey1);
                if (bRst)
                {
                    lblValidRst.Text = "验证：通过 "+DateTime.Now.ToString();
                }
                else
                {
                    lblValidRst.Text = "验证：不通过 " + DateTime.Now.ToString();
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void btnSM3Hash_Click(object sender, EventArgs e)
        {
            try
            {
                if ( string.IsNullOrWhiteSpace(txtToSignStr.Text)
                     )
                {
                    MessageBox.Show(" 签名字符串 不能为空。");
                    return;
                }
                 
                byte[] msg = System.Text.Encoding.UTF8.GetBytes(txtToSignStr.Text);
                byte[] bytRst= GmUtil.Sm3(msg);
                string strRst = Hex.ToHexString(bytRst).ToUpper();
                txtSign.Text = strRst;
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void btnGen_Click(object sender, EventArgs e)
        {
            try
            {
                AsymmetricCipherKeyPair kp = GmUtil.GenerateKeyPair();                

                txtPrivateKey.Text=  Hex.ToHexString( ((ECPrivateKeyParameters)kp.Private).D.ToByteArray());
                txtPubKey.Text =   Hex.ToHexString( ((ECPublicKeyParameters)kp.Public).Q.GetEncoded(false));
                //生成的公钥是130位，如果是签名，公钥要去除开头的“04”，为128位。
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            

            try
            {
                //读.sm2文件
                String sm2 = "MIIDHQIBATBHBgoqgRzPVQYBBAIBBgcqgRzPVQFoBDDW5/I9kZhObxXE9Vh1CzHdZhIhxn+3byBU\nUrzmGRKbDRMgI3hJKdvpqWkM5G4LNcIwggLNBgoqgRzPVQYBBAIBBIICvTCCArkwggJdoAMCAQIC\nBRA2QSlgMAwGCCqBHM9VAYN1BQAwXDELMAkGA1UEBhMCQ04xMDAuBgNVBAoMJ0NoaW5hIEZpbmFu\nY2lhbCBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTEbMBkGA1UEAwwSQ0ZDQSBURVNUIFNNMiBPQ0Ex\nMB4XDTE4MTEyNjEwMTQxNVoXDTIwMTEyNjEwMTQxNVowcjELMAkGA1UEBhMCY24xEjAQBgNVBAoM\nCUNGQ0EgT0NBMTEOMAwGA1UECwwFQ1VQUkExFDASBgNVBAsMC0VudGVycHJpc2VzMSkwJwYDVQQD\nDCAwNDFAWnRlc3RAMDAwMTAwMDA6U0lHTkAwMDAwMDAwMTBZMBMGByqGSM49AgEGCCqBHM9VAYIt\nA0IABDRNKhvnjaMUShsM4MJ330WhyOwpZEHoAGfqxFGX+rcL9x069dyrmiF3+2ezwSNh1/6YqfFZ\nX9koM9zE5RG4USmjgfMwgfAwHwYDVR0jBBgwFoAUa/4Y2o9COqa4bbMuiIM6NKLBMOEwSAYDVR0g\nBEEwPzA9BghggRyG7yoBATAxMC8GCCsGAQUFBwIBFiNodHRwOi8vd3d3LmNmY2EuY29tLmNuL3Vz\nL3VzLTE0Lmh0bTA4BgNVHR8EMTAvMC2gK6AphidodHRwOi8vdWNybC5jZmNhLmNvbS5jbi9TTTIv\nY3JsNDI4NS5jcmwwCwYDVR0PBAQDAgPoMB0GA1UdDgQWBBREhx9VlDdMIdIbhAxKnGhPx8FcHDAd\nBgNVHSUEFjAUBggrBgEFBQcDAgYIKwYBBQUHAwQwDAYIKoEcz1UBg3UFAANIADBFAiEAgWvQi3h6\niW4jgF4huuXfhWInJmTTYr2EIAdG8V4M8fYCIBixygdmfPL9szcK2pzCYmIb6CBzo5SMv50Odycc\nVfY6";
               var   bs = Convert.FromBase64String(sm2);
                String pwd = "cfca1234";
                GmUtil.Sm2Cert sm2Cert = GmUtil.readSm2File(bs, pwd);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
