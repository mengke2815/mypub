﻿namespace WindowsForms国密GmUtil
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.lblValidRst = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtSign = new System.Windows.Forms.TextBox();
            this.btnSign = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.txtToSignStr = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtPubKey = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtPrivateKey = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtUserId = new System.Windows.Forms.TextBox();
            this.btnSM3Hash = new System.Windows.Forms.Button();
            this.btnGen = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("宋体", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(188, 11);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(549, 19);
            this.label2.TabIndex = 50;
            this.label2.Text = "同一组私钥公钥才能验证签名，公私钥不匹配，无法验证签名";
            // 
            // lblValidRst
            // 
            this.lblValidRst.AutoSize = true;
            this.lblValidRst.Location = new System.Drawing.Point(341, 735);
            this.lblValidRst.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblValidRst.Name = "lblValidRst";
            this.lblValidRst.Size = new System.Drawing.Size(38, 15);
            this.lblValidRst.TabIndex = 49;
            this.lblValidRst.Text = "签11";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(175, 729);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(125, 29);
            this.button1.TabIndex = 48;
            this.button1.Text = "公钥验证签名";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(31, 548);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 15);
            this.label1.TabIndex = 47;
            this.label1.Text = "签名值";
            // 
            // txtSign
            // 
            this.txtSign.Location = new System.Drawing.Point(16, 566);
            this.txtSign.Margin = new System.Windows.Forms.Padding(4);
            this.txtSign.Multiline = true;
            this.txtSign.Name = "txtSign";
            this.txtSign.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtSign.Size = new System.Drawing.Size(1211, 94);
            this.txtSign.TabIndex = 46;
            // 
            // btnSign
            // 
            this.btnSign.Location = new System.Drawing.Point(175, 476);
            this.btnSign.Margin = new System.Windows.Forms.Padding(4);
            this.btnSign.Name = "btnSign";
            this.btnSign.Size = new System.Drawing.Size(100, 29);
            this.btnSign.TabIndex = 45;
            this.btnSign.Text = "私钥签名";
            this.btnSign.UseVisualStyleBackColor = true;
            this.btnSign.Click += new System.EventHandler(this.btnSign_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(31, 391);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(112, 15);
            this.label5.TabIndex = 44;
            this.label5.Text = "要签名的字符串";
            // 
            // txtToSignStr
            // 
            this.txtToSignStr.Location = new System.Drawing.Point(176, 374);
            this.txtToSignStr.Margin = new System.Windows.Forms.Padding(4);
            this.txtToSignStr.Multiline = true;
            this.txtToSignStr.Name = "txtToSignStr";
            this.txtToSignStr.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtToSignStr.Size = new System.Drawing.Size(844, 94);
            this.txtToSignStr.TabIndex = 43;
            this.txtToSignStr.Text = "Hello中国";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(16, 255);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(127, 15);
            this.label13.TabIndex = 42;
            this.label13.Text = "公钥（验证签名）";
            // 
            // txtPubKey
            // 
            this.txtPubKey.Location = new System.Drawing.Point(176, 235);
            this.txtPubKey.Margin = new System.Windows.Forms.Padding(4);
            this.txtPubKey.Multiline = true;
            this.txtPubKey.Name = "txtPubKey";
            this.txtPubKey.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtPubKey.Size = new System.Drawing.Size(727, 112);
            this.txtPubKey.TabIndex = 41;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(16, 76);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(83, 15);
            this.label10.TabIndex = 40;
            this.label10.Text = "私钥(签名)";
            // 
            // txtPrivateKey
            // 
            this.txtPrivateKey.Location = new System.Drawing.Point(176, 72);
            this.txtPrivateKey.Margin = new System.Windows.Forms.Padding(4);
            this.txtPrivateKey.Multiline = true;
            this.txtPrivateKey.Name = "txtPrivateKey";
            this.txtPrivateKey.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtPrivateKey.Size = new System.Drawing.Size(727, 53);
            this.txtPrivateKey.TabIndex = 39;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 151);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 15);
            this.label3.TabIndex = 52;
            this.label3.Text = "userId";
            // 
            // txtUserId
            // 
            this.txtUserId.Location = new System.Drawing.Point(175, 148);
            this.txtUserId.Margin = new System.Windows.Forms.Padding(4);
            this.txtUserId.Multiline = true;
            this.txtUserId.Name = "txtUserId";
            this.txtUserId.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtUserId.Size = new System.Drawing.Size(727, 53);
            this.txtUserId.TabIndex = 51;
            // 
            // btnSM3Hash
            // 
            this.btnSM3Hash.Location = new System.Drawing.Point(412, 476);
            this.btnSM3Hash.Margin = new System.Windows.Forms.Padding(4);
            this.btnSM3Hash.Name = "btnSM3Hash";
            this.btnSM3Hash.Size = new System.Drawing.Size(100, 29);
            this.btnSM3Hash.TabIndex = 53;
            this.btnSM3Hash.Text = "SM3 HASH";
            this.btnSM3Hash.UseVisualStyleBackColor = true;
            this.btnSM3Hash.Click += new System.EventHandler(this.btnSM3Hash_Click);
            // 
            // btnGen
            // 
            this.btnGen.Location = new System.Drawing.Point(996, 98);
            this.btnGen.Margin = new System.Windows.Forms.Padding(4);
            this.btnGen.Name = "btnGen";
            this.btnGen.Size = new System.Drawing.Size(100, 29);
            this.btnGen.TabIndex = 54;
            this.btnGen.Text = "生成私钥";
            this.btnGen.UseVisualStyleBackColor = true;
            this.btnGen.Click += new System.EventHandler(this.btnGen_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(1147, 264);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(94, 48);
            this.button2.TabIndex = 55;
            this.button2.Text = "button2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1408, 921);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.btnGen);
            this.Controls.Add(this.btnSM3Hash);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtUserId);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblValidRst);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtSign);
            this.Controls.Add(this.btnSign);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtToSignStr);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.txtPubKey);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txtPrivateKey);
            this.Margin = new System.Windows.Forms.Padding(5);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblValidRst;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtSign;
        private System.Windows.Forms.Button btnSign;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtToSignStr;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtPubKey;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtPrivateKey;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtUserId;
        private System.Windows.Forms.Button btnSM3Hash;
        private System.Windows.Forms.Button btnGen;
        private System.Windows.Forms.Button button2;
    }
}

