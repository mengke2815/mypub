package org.lw;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class Main {

    public static void main(String[] args) {

        //国密 私钥
        String strPrivateKey = "022A88CD5B5B3F56500615EF380C245CE81F32E980BB0CC708D01F9BD8558FEF";
        //国密 userId
        String strUserId = "123456789";
        //待签名内容
        String strBody = "签名报文字符串123456";

        try {
            //签名值
            String strSign = SmUtil.signSm3WithSm2(strBody, strUserId, strPrivateKey);
            System.out.println(getNow() + " 签名结果：" + strSign);

            //C#.NET 签名值
            strSign="uc4WZqLjj0mziBK9sLpOukPzRAlocXNAtEVO/RwkiPVzGtVmIr2cAT27m7J17jzx81Pekj4R1C1IjF3riEMYjw==";
            System.out.println(getNow() + " C#.NET 签名值：" + strSign);

            //国密 公钥
            String strPubKey = "3D3AA4732B56DFCC643CF1B0ABAF75EF9EC16A756C18967090E8250E0A49915EEFDD5CBE16BB34CC93B20D3EFB4C842FFCE13887FE211DAE33DFD2AD025265D6";
            //验证签名
            boolean bRst = SmUtil.verifySm3WithSm2(strBody, strUserId, strSign, strPubKey);
            System.out.println(getNow() + " 验证签名：" + bRst);

            //SM3 HASH
            String strSM3Hash=SmUtil.generateSM3HASH(strBody);
            System.out.println(getNow() + " "+strBody+" 的SM3 HASH:" + strSM3Hash);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    public static String getNow() {
        String formatStr = "yyyy-MM-dd HH:mm:ss.SSS";
        LocalDateTime lnow = LocalDateTime.now();
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(formatStr);
        String nowFormat = lnow.format(dateTimeFormatter);
        return nowFormat;
    }

}
