﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VIPSPT.Models
{
    public class MicroPayRsp
    {



        public string third_order_id { get; set; }
        public string out_order_id { get; set; }
        public string leshua_order_id { get; set; }
        public string amount { get; set; }

        /// <summary>
        /// 支付订单状态（0：支付中；2：支付成功；6：订单关闭；7：退款中；8：交易结束；11：转入退款；其他：支付失败；）
        /// </summary>
        public string status { get; set; }
        public string merchant_id { get; set; }
        public string enterpriseReg { get; set; }

        /// <summary>
        /// 2022-03-30 11:45:15
        /// </summary>
        public string dctime { get; set; }
        public string pay_way { get; set; }
        public string sSignature { get; set; }




    }
}
