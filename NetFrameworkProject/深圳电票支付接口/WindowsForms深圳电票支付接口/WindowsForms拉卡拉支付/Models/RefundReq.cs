﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VIPSPT.Models
{
    public  class RefundReq
    {
        public string merchant_id { get; set; }
        public string enterpriseReg { get; set; } 
       

        public string third_order_id { get; set; }
        public string refund_amount { get; set; }

        public string refundMsg { get; set; }

    }
}
