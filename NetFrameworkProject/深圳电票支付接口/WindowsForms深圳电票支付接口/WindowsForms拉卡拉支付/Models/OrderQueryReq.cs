﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VIPSPT.Models
{
    public class OrderQueryReq
    {

        public string enterpriseReg { get; set; }
        public string merchant_id { get; set; }

        public string out_order_id { get; set; }

    }
}
