﻿namespace WindowsForms拉卡拉支付
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.btnReadCrt = new System.Windows.Forms.Button();
            this.ofdCrt = new System.Windows.Forms.OpenFileDialog();
            this.txtCertSN = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtAuthCode = new System.Windows.Forms.TextBox();
            this.btnMicroPay = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.txtAmt = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtOutTradeNo = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtMchNo = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtTermNo = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtenterpriseReg = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtPrivateKey = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtMainUrl = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtRst = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtPubCert = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.lblPayStatus = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.txtTradeNo = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtThirdTradeNo = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtPayTime = new System.Windows.Forms.TextBox();
            this.btnOrderQuery = new System.Windows.Forms.Button();
            this.btnRefund = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.txtOutRefundNo = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtRefundAmt = new System.Windows.Forms.TextBox();
            this.lblRefundStatus = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.btnRefundQuery = new System.Windows.Forms.Button();
            this.label19 = new System.Windows.Forms.Label();
            this.txtAPPSECRET = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtAppId = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnReadCrt
            // 
            this.btnReadCrt.Location = new System.Drawing.Point(53, 31);
            this.btnReadCrt.Margin = new System.Windows.Forms.Padding(4);
            this.btnReadCrt.Name = "btnReadCrt";
            this.btnReadCrt.Size = new System.Drawing.Size(119, 29);
            this.btnReadCrt.TabIndex = 1;
            this.btnReadCrt.Text = "读取CRT证书";
            this.btnReadCrt.UseVisualStyleBackColor = true;
            this.btnReadCrt.Click += new System.EventHandler(this.btnReadCrt_Click);
            // 
            // ofdCrt
            // 
            this.ofdCrt.Filter = "crt cert|*.crt";
            // 
            // txtCertSN
            // 
            this.txtCertSN.Location = new System.Drawing.Point(287, 34);
            this.txtCertSN.Margin = new System.Windows.Forms.Padding(4);
            this.txtCertSN.Name = "txtCertSN";
            this.txtCertSN.Size = new System.Drawing.Size(340, 25);
            this.txtCertSN.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(192, 38);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 15);
            this.label1.TabIndex = 2;
            this.label1.Text = "证书序列号";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(61, 239);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 15);
            this.label3.TabIndex = 6;
            this.label3.Text = "付款码";
            // 
            // txtAuthCode
            // 
            this.txtAuthCode.Location = new System.Drawing.Point(156, 235);
            this.txtAuthCode.Margin = new System.Windows.Forms.Padding(4);
            this.txtAuthCode.Name = "txtAuthCode";
            this.txtAuthCode.Size = new System.Drawing.Size(291, 25);
            this.txtAuthCode.TabIndex = 0;
            this.txtAuthCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtAuthCode_KeyDown);
            // 
            // btnMicroPay
            // 
            this.btnMicroPay.Location = new System.Drawing.Point(497, 232);
            this.btnMicroPay.Margin = new System.Windows.Forms.Padding(4);
            this.btnMicroPay.Name = "btnMicroPay";
            this.btnMicroPay.Size = new System.Drawing.Size(100, 29);
            this.btnMicroPay.TabIndex = 7;
            this.btnMicroPay.Text = "条码支付";
            this.btnMicroPay.UseVisualStyleBackColor = true;
            this.btnMicroPay.Click += new System.EventHandler(this.btnMicroPay_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(61, 205);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 15);
            this.label4.TabIndex = 9;
            this.label4.Text = "支付金额";
            // 
            // txtAmt
            // 
            this.txtAmt.Location = new System.Drawing.Point(156, 201);
            this.txtAmt.Margin = new System.Windows.Forms.Padding(4);
            this.txtAmt.Name = "txtAmt";
            this.txtAmt.Size = new System.Drawing.Size(168, 25);
            this.txtAmt.TabIndex = 8;
            this.txtAmt.Text = "0.01";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(61, 272);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(67, 15);
            this.label5.TabIndex = 11;
            this.label5.Text = "商户单号";
            // 
            // txtOutTradeNo
            // 
            this.txtOutTradeNo.Location = new System.Drawing.Point(156, 269);
            this.txtOutTradeNo.Margin = new System.Windows.Forms.Padding(4);
            this.txtOutTradeNo.Name = "txtOutTradeNo";
            this.txtOutTradeNo.Size = new System.Drawing.Size(291, 25);
            this.txtOutTradeNo.TabIndex = 10;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(61, 124);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 15);
            this.label6.TabIndex = 13;
            this.label6.Text = "商户号";
            // 
            // txtMchNo
            // 
            this.txtMchNo.Location = new System.Drawing.Point(156, 120);
            this.txtMchNo.Margin = new System.Windows.Forms.Padding(4);
            this.txtMchNo.Name = "txtMchNo";
            this.txtMchNo.Size = new System.Drawing.Size(291, 25);
            this.txtMchNo.TabIndex = 12;
            this.txtMchNo.Text = "822290070111135";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(61, 158);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 15);
            this.label7.TabIndex = 15;
            this.label7.Text = "终端号";
            // 
            // txtTermNo
            // 
            this.txtTermNo.Location = new System.Drawing.Point(156, 154);
            this.txtTermNo.Margin = new System.Windows.Forms.Padding(4);
            this.txtTermNo.Name = "txtTermNo";
            this.txtTermNo.Size = new System.Drawing.Size(291, 25);
            this.txtTermNo.TabIndex = 14;
            this.txtTermNo.Text = "29034705";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(357, 205);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(22, 15);
            this.label8.TabIndex = 16;
            this.label8.Text = "元";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(29, 89);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(111, 15);
            this.label9.TabIndex = 18;
            this.label9.Text = "enterpriseReg";
            // 
            // txtenterpriseReg
            // 
            this.txtenterpriseReg.Location = new System.Drawing.Point(156, 86);
            this.txtenterpriseReg.Margin = new System.Windows.Forms.Padding(4);
            this.txtenterpriseReg.Name = "txtenterpriseReg";
            this.txtenterpriseReg.Size = new System.Drawing.Size(291, 25);
            this.txtenterpriseReg.TabIndex = 17;
            this.txtenterpriseReg.Text = "800000010334001";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(689, 132);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(128, 15);
            this.label10.TabIndex = 20;
            this.label10.Text = "接入方私钥(签名)";
            // 
            // txtPrivateKey
            // 
            this.txtPrivateKey.Location = new System.Drawing.Point(688, 160);
            this.txtPrivateKey.Margin = new System.Windows.Forms.Padding(4);
            this.txtPrivateKey.Multiline = true;
            this.txtPrivateKey.Name = "txtPrivateKey";
            this.txtPrivateKey.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtPrivateKey.Size = new System.Drawing.Size(727, 132);
            this.txtPrivateKey.TabIndex = 19;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(693, 35);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(63, 15);
            this.label11.TabIndex = 22;
            this.label11.Text = "mainUrl";
            // 
            // txtMainUrl
            // 
            this.txtMainUrl.Location = new System.Drawing.Point(792, 31);
            this.txtMainUrl.Margin = new System.Windows.Forms.Padding(4);
            this.txtMainUrl.Name = "txtMainUrl";
            this.txtMainUrl.Size = new System.Drawing.Size(328, 25);
            this.txtMainUrl.TabIndex = 21;
            this.txtMainUrl.Text = "https://test.wsmsd.cn/sit/labs/txn";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(47, 659);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(67, 15);
            this.label12.TabIndex = 24;
            this.label12.Text = "响应报文";
            // 
            // txtRst
            // 
            this.txtRst.Location = new System.Drawing.Point(124, 655);
            this.txtRst.Margin = new System.Windows.Forms.Padding(4);
            this.txtRst.Multiline = true;
            this.txtRst.Name = "txtRst";
            this.txtRst.Size = new System.Drawing.Size(727, 148);
            this.txtRst.TabIndex = 23;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(685, 304);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(187, 15);
            this.label13.TabIndex = 26;
            this.label13.Text = "平台公钥证书（验证签名）";
            // 
            // txtPubCert
            // 
            this.txtPubCert.Location = new System.Drawing.Point(688, 322);
            this.txtPubCert.Margin = new System.Windows.Forms.Padding(4);
            this.txtPubCert.Multiline = true;
            this.txtPubCert.Name = "txtPubCert";
            this.txtPubCert.Size = new System.Drawing.Size(727, 112);
            this.txtPubCert.TabIndex = 25;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(52, 311);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(67, 15);
            this.label14.TabIndex = 27;
            this.label14.Text = "支付状态";
            // 
            // lblPayStatus
            // 
            this.lblPayStatus.AutoSize = true;
            this.lblPayStatus.Location = new System.Drawing.Point(165, 311);
            this.lblPayStatus.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPayStatus.Name = "lblPayStatus";
            this.lblPayStatus.Size = new System.Drawing.Size(37, 15);
            this.lblPayStatus.TabIndex = 28;
            this.lblPayStatus.Text = "初始";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(61, 359);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(67, 15);
            this.label15.TabIndex = 30;
            this.label15.Text = "交易单号";
            // 
            // txtTradeNo
            // 
            this.txtTradeNo.Location = new System.Drawing.Point(156, 355);
            this.txtTradeNo.Margin = new System.Windows.Forms.Padding(4);
            this.txtTradeNo.Name = "txtTradeNo";
            this.txtTradeNo.Size = new System.Drawing.Size(291, 25);
            this.txtTradeNo.TabIndex = 29;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(45, 392);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(97, 15);
            this.label16.TabIndex = 32;
            this.label16.Text = "三方交易单号";
            // 
            // txtThirdTradeNo
            // 
            this.txtThirdTradeNo.Location = new System.Drawing.Point(156, 389);
            this.txtThirdTradeNo.Margin = new System.Windows.Forms.Padding(4);
            this.txtThirdTradeNo.Name = "txtThirdTradeNo";
            this.txtThirdTradeNo.Size = new System.Drawing.Size(291, 25);
            this.txtThirdTradeNo.TabIndex = 31;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(45, 426);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(67, 15);
            this.label17.TabIndex = 34;
            this.label17.Text = "支付时间";
            // 
            // txtPayTime
            // 
            this.txtPayTime.Location = new System.Drawing.Point(156, 422);
            this.txtPayTime.Margin = new System.Windows.Forms.Padding(4);
            this.txtPayTime.Name = "txtPayTime";
            this.txtPayTime.Size = new System.Drawing.Size(291, 25);
            this.txtPayTime.TabIndex = 33;
            // 
            // btnOrderQuery
            // 
            this.btnOrderQuery.Location = new System.Drawing.Point(497, 272);
            this.btnOrderQuery.Margin = new System.Windows.Forms.Padding(4);
            this.btnOrderQuery.Name = "btnOrderQuery";
            this.btnOrderQuery.Size = new System.Drawing.Size(100, 29);
            this.btnOrderQuery.TabIndex = 35;
            this.btnOrderQuery.Text = "交易查询";
            this.btnOrderQuery.UseVisualStyleBackColor = true;
            this.btnOrderQuery.Click += new System.EventHandler(this.btnOrderQuery_Click);
            // 
            // btnRefund
            // 
            this.btnRefund.Location = new System.Drawing.Point(512, 461);
            this.btnRefund.Margin = new System.Windows.Forms.Padding(4);
            this.btnRefund.Name = "btnRefund";
            this.btnRefund.Size = new System.Drawing.Size(100, 29);
            this.btnRefund.TabIndex = 36;
            this.btnRefund.Text = "退款";
            this.btnRefund.UseVisualStyleBackColor = true;
            this.btnRefund.Click += new System.EventHandler(this.btnRefund_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(61, 509);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 15);
            this.label2.TabIndex = 38;
            this.label2.Text = "退款单号";
            // 
            // txtOutRefundNo
            // 
            this.txtOutRefundNo.Location = new System.Drawing.Point(156, 505);
            this.txtOutRefundNo.Margin = new System.Windows.Forms.Padding(4);
            this.txtOutRefundNo.Name = "txtOutRefundNo";
            this.txtOutRefundNo.Size = new System.Drawing.Size(291, 25);
            this.txtOutRefundNo.TabIndex = 37;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(61, 475);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(67, 15);
            this.label18.TabIndex = 40;
            this.label18.Text = "退款金额";
            // 
            // txtRefundAmt
            // 
            this.txtRefundAmt.Location = new System.Drawing.Point(156, 471);
            this.txtRefundAmt.Margin = new System.Windows.Forms.Padding(4);
            this.txtRefundAmt.Name = "txtRefundAmt";
            this.txtRefundAmt.Size = new System.Drawing.Size(168, 25);
            this.txtRefundAmt.TabIndex = 39;
            this.txtRefundAmt.Text = "0.01";
            // 
            // lblRefundStatus
            // 
            this.lblRefundStatus.AutoSize = true;
            this.lblRefundStatus.Location = new System.Drawing.Point(192, 541);
            this.lblRefundStatus.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblRefundStatus.Name = "lblRefundStatus";
            this.lblRefundStatus.Size = new System.Drawing.Size(37, 15);
            this.lblRefundStatus.TabIndex = 42;
            this.lblRefundStatus.Text = "初始";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(29, 541);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(97, 15);
            this.label20.TabIndex = 41;
            this.label20.Text = "退款申请状态";
            // 
            // btnRefundQuery
            // 
            this.btnRefundQuery.Location = new System.Drawing.Point(512, 509);
            this.btnRefundQuery.Margin = new System.Windows.Forms.Padding(4);
            this.btnRefundQuery.Name = "btnRefundQuery";
            this.btnRefundQuery.Size = new System.Drawing.Size(100, 29);
            this.btnRefundQuery.TabIndex = 43;
            this.btnRefundQuery.Text = "退款查询";
            this.btnRefundQuery.UseVisualStyleBackColor = true;
            this.btnRefundQuery.Click += new System.EventHandler(this.btnRefundQuery_Click);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(746, 479);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(79, 15);
            this.label19.TabIndex = 45;
            this.label19.Text = "APPSECRET";
            // 
            // txtAPPSECRET
            // 
            this.txtAPPSECRET.Location = new System.Drawing.Point(841, 475);
            this.txtAPPSECRET.Margin = new System.Windows.Forms.Padding(4);
            this.txtAPPSECRET.Name = "txtAPPSECRET";
            this.txtAPPSECRET.Size = new System.Drawing.Size(291, 25);
            this.txtAPPSECRET.TabIndex = 44;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(746, 517);
            this.label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(47, 15);
            this.label21.TabIndex = 47;
            this.label21.Text = "appid";
            // 
            // txtAppId
            // 
            this.txtAppId.Location = new System.Drawing.Point(841, 513);
            this.txtAppId.Margin = new System.Windows.Forms.Padding(4);
            this.txtAppId.Name = "txtAppId";
            this.txtAppId.Size = new System.Drawing.Size(291, 25);
            this.txtAppId.TabIndex = 46;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1432, 834);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.txtAppId);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.txtAPPSECRET);
            this.Controls.Add(this.btnRefundQuery);
            this.Controls.Add(this.lblRefundStatus);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.txtRefundAmt);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtOutRefundNo);
            this.Controls.Add(this.btnRefund);
            this.Controls.Add(this.btnOrderQuery);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.txtPayTime);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.txtThirdTradeNo);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.txtTradeNo);
            this.Controls.Add(this.lblPayStatus);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.txtPubCert);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.txtRst);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtMainUrl);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txtPrivateKey);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtenterpriseReg);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtTermNo);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtMchNo);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtOutTradeNo);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtAmt);
            this.Controls.Add(this.btnMicroPay);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtAuthCode);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtCertSN);
            this.Controls.Add(this.btnReadCrt);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnReadCrt;
        private System.Windows.Forms.OpenFileDialog ofdCrt;
        private System.Windows.Forms.TextBox txtCertSN;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtAuthCode;
        private System.Windows.Forms.Button btnMicroPay;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtAmt;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtOutTradeNo;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtMchNo;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtTermNo;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtenterpriseReg;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtPrivateKey;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtMainUrl;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtRst;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtPubCert;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label lblPayStatus;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtTradeNo;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtThirdTradeNo;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtPayTime;
        private System.Windows.Forms.Button btnOrderQuery;
        private System.Windows.Forms.Button btnRefund;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtOutRefundNo;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtRefundAmt;
        private System.Windows.Forms.Label lblRefundStatus;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button btnRefundQuery;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtAPPSECRET;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtAppId;
    }
}

