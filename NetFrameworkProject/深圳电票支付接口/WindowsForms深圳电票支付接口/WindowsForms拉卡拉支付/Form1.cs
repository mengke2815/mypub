﻿using CommonUtils;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VIPSPT.Models;
using Aop.Api.Util;
using System.Net.Security;
using System.Net;
using Common.Models2;
using System.Security.Cryptography;

namespace WindowsForms拉卡拉支付
{
    public partial class Form1 : Form
    {
        System.Timers.Timer _tmrRemeber;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            RemForm.ReadRememberForm(this, null);

            _tmrRemeber = new System.Timers.Timer();
            _tmrRemeber.Elapsed += new System.Timers.ElapsedEventHandler(_tmrRemeber_Elapsed);
            _tmrRemeber.Interval = 10 * 1000;
            _tmrRemeber.Start();

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
            System.Net.ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(CheckValidationResult);

        }


        private static bool CheckValidationResult(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors errors)
        {
            return true;
        }

        void _tmrRemeber_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                RemForm.RememberForm(this, null);
            }
            catch (Exception ex)
            {
                //MessageBox.Show("_tmrRemeber_Elapsed" + ex.Message);
            }
        }

        private void btnReadCrt_Click(object sender, EventArgs e)
        {
            try
            {
                if (ofdCrt.ShowDialog() == DialogResult.OK)
                {
                    X509Certificate2 cert = new X509Certificate2(ofdCrt.FileName);
                    txtCertSN.Text = cert.SerialNumber;
                    //txtCertSN10.Text = cert.SerialNumber.ToLower();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        private void btnMicroPay_Click(object sender, EventArgs e)
        {
            try
            {
                JsonSerializerSettings jsetting = new JsonSerializerSettings();
                jsetting.DefaultValueHandling = DefaultValueHandling.Ignore;

                if (decimal.Parse(txtAmt.Text) >= 1M)
                {
                    DialogResult dr = MessageBox.Show("金额过大，是否继续？", "OO", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dr == DialogResult.No)
                    {
                        return;
                    }
                }
                if (string.IsNullOrWhiteSpace(txtAuthCode.Text))
                {
                    MessageBox.Show("付款码不能为空！");
                    return;
                }
                //初始化付款完成时间。
                txtPayTime.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                int total_fee = VString.TryInt((VString.TryDec(txtAmt.Text.Trim()) * 100));
                txtOutTradeNo.Text = "S000000000000" + DateTime.Now.ToString("yyyyMMddHHmmssfff") ;

                string pay_way = "WXZF";
                if (txtAuthCode.Text.StartsWith("2") || txtAuthCode.Text.StartsWith("3"))
                {
                    pay_way = "ZFBZF";
                }
                else if (txtAuthCode.Text.StartsWith("6"))
                {
                    pay_way = "UPSMZF";
                }

                MicroPayReq req = new MicroPayReq();
                req.merchant_id = txtMchNo.Text.Trim();
                req.enterpriseReg = txtenterpriseReg.Text;
                req.sMchtIp = "116.30.109.119";
                req.sAuthCode = txtAuthCode.Text.Trim();
                req.pay_way = pay_way;
                req.amount = txtAmt.Text;
                req.out_order_id = txtOutTradeNo.Text;
                req.date_time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                string dataJson = JsonConvert.SerializeObject(req, jsetting);

                var pubReq = new VIPSPT_PubReq<MicroPayReq>();
                pubReq.appid = txtAppId.Text;
                pubReq.timeStamp = TimeStampUtil.ConvertDateTimeToInt(DateTime.Now).ToString();
                pubReq.data = req;

                var dic1 = HashUtil.ModelToDic(req);
                var dic2 = HashUtil.AsciiDictionary(dic1);
                var queryStr = HashUtil.BuildQueryString(dic2);
                var signStr = queryStr + txtAPPSECRET.Text;

                SHA256CryptoServiceProvider mySHA256 = new SHA256CryptoServiceProvider();
                byte[] byHash = mySHA256.ComputeHash(Encoding.UTF8.GetBytes(signStr));
                string mySign = BitConverter.ToString(byHash).Replace("-", "").ToUpper();

                pubReq.sign = mySign;

                //报文主体

                string bodyJson = JsonConvert.SerializeObject(pubReq, jsetting);

                string suffix = "/bToC";

                string url = txtMainUrl.Text + suffix;
                GLog.WLog("url:" + url);

                GLog.WLog("请求报文:" + bodyJson);

                var rstStr = HttpUtil.HttpPostJson(url, bodyJson, 30);
                GLog.WLog("响应报文:" + rstStr);

                txtRst.Text = rstStr;

                string _code = "";
                string _msg = "";

                //com.fh.resolver.ReturnViewException:
                string fixExp = "com.fh.resolver.ReturnViewException:";
                if (!string.IsNullOrWhiteSpace(rstStr) && rstStr.Contains(fixExp))
                {
                    rstStr = rstStr.Substring(rstStr.IndexOf(fixExp) + fixExp.Length);
                    rstStr = rstStr.Substring(0, rstStr.IndexOf("</div>"));
                    _code = SissPayReturnCode.FAIL.ToString();
                    _msg = rstStr;
                    lblPayStatus.Text = _msg;
                    return;
                }

                var rspM = JsonConvert.DeserializeObject<VIPSPT_PubRsp<MicroPayRsp>>(rstStr);

                if (rspM.ret == 0)
                {
                    var rspA = rspM.data;

                    if (rspA.status == "2")
                    {
                        _code = SissPayReturnCode.SUCCESS.ToString();
                        lblPayStatus.Text = "成功";
                    }
                    else if (rspA.status == "0")
                    {
                        _code = SissPayReturnCode.WAIT_BUYER_PAY.ToString();
                        lblPayStatus.Text = "支付中";
                    }
                    else
                    {
                        _code = SissPayReturnCode.FAIL.ToString();
                        lblPayStatus.Text = "失败";
                    }

                    if (!string.IsNullOrWhiteSpace(rspA.third_order_id))
                    {
                        string trade_no = rspA.third_order_id;
                    }
                }
                else
                {
                    _code = SissPayReturnCode.FAIL.ToString();
                    _msg = rspM.msg;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private static byte[] strToToHexByte(string hexString)
        {
            hexString = hexString.Replace(" ", "");
            if ((hexString.Length % 2) != 0)
                hexString += " ";
            byte[] returnBytes = new byte[hexString.Length / 2];
            for (int i = 0; i < returnBytes.Length; i++)
                returnBytes[i] = Convert.ToByte(hexString.Substring(i * 2, 2), 16);
            return returnBytes;
        }

        public static string AesEncryptCBC(string encryptStr, byte[] byteKEY, byte[] byteIV)
        {


            //byte[] byteIV = Encoding.UTF8.GetBytes(iv);

            //byte[] byteKEY = Encoding.UTF8.GetBytes(key);

            byte[] byteContent = Encoding.UTF8.GetBytes(encryptStr);

            var _aes = new RijndaelManaged();


            _aes.Padding = PaddingMode.PKCS7;
            _aes.Mode = CipherMode.CBC;

            var _crypto = _aes.CreateEncryptor(byteKEY, byteIV);
            byte[] encrypted = _crypto.TransformFinalBlock(byteContent, 0, byteContent.Length);

            _crypto.Dispose();

            return System.Convert.ToBase64String(encrypted);
        }

        public static string AesDecryptCBC(string decryptStr, byte[] byteKEY, byte[] byteIV)
        {
            //byte[] byteKEY = Encoding.UTF8.GetBytes(key);

            //byte[] byteIV = Encoding.UTF8.GetBytes(iv);

            byte[] byteDecrypt = System.Convert.FromBase64String(decryptStr);

            var _aes = new RijndaelManaged();

            _aes.Padding = PaddingMode.PKCS7;
            _aes.Mode = CipherMode.CBC;

            var _crypto = _aes.CreateDecryptor(byteKEY, byteIV);
            byte[] decrypted = _crypto.TransformFinalBlock(
                byteDecrypt, 0, byteDecrypt.Length);

            _crypto.Dispose();

            return Encoding.UTF8.GetString(decrypted);
        }

        //public string GetSign(BOCOM_PubReq cont, string suffix, string PrivateKey, string PrivateKeyForm)
        //{
        //    var dic1 = HashUtil.ModelToDic(cont);
        //    var dic2 = HashUtil.AsciiDictionary(dic1);//排序
        //    var toSignStr1 = HashUtil.BuildQueryString(dic2);//拼接

        //    string toSignStr2 = suffix + "?" + toSignStr1;//拼上URL后缀
        //    byte[] byToSign = Encoding.UTF8.GetBytes(toSignStr2);
        //    var rsa = RsaUtil.LoadPrivateKey(PrivateKey, PrivateKeyForm);
        //    byte[] byFnl = rsa.SignData(byToSign, "SHA256");
        //    string sign = Convert.ToBase64String(byFnl);

        //    return sign;
        //}

        /// <summary>
        /// 验证签名
        /// </summary>
        /// <param name="rspHeader">HTTP响应头</param>
        /// <param name="rspBody">响应报文</param>
        /// <param name="pubCert">公钥证书</param>
        /// <returns></returns>
        bool ValidSign(IDictionary<string, string> rspHeader, string rspBody, string pubCert)
        {
            //var dic = JsonConvert.DeserializeObject<BOCOM_PubRspSign>(rspBody);
            //string fnstr = rspBody.Substring(0, rspBody.LastIndexOf(",\"sign"));
            //fnstr = fnstr.Substring(fnstr.IndexOf("rsp_biz_content") + 17);
            //string rspSign = dic.sign;

            //byte[] byteCon = Encoding.UTF8.GetBytes(fnstr);

            //byte[] byteRspSign = Convert.FromBase64String(rspSign);
            //var rsaPub = RsaUtil.LoadPublicKey(pubCert);

            //bool bRst = rsaPub.VerifyData(byteCon, "SHA256", byteRspSign);

            return true;

        }

        private void txtAuthCode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnMicroPay_Click(sender, e);
            }
        }

        private void btnOrderQuery_Click(object sender, EventArgs e)
        {
            try
            {
                JsonSerializerSettings jsetting = new JsonSerializerSettings();
                jsetting.DefaultValueHandling = DefaultValueHandling.Ignore;

                var req = new OrderQueryReq();
                req.merchant_id = txtMchNo.Text;
                req.enterpriseReg = txtenterpriseReg.Text;
                req.out_order_id = txtOutTradeNo.Text;

                string dataJson = JsonConvert.SerializeObject(req, jsetting);

                var pubReq = new VIPSPT_PubReq<OrderQueryReq>();
                pubReq.appid = txtAppId.Text;
                pubReq.timeStamp = TimeStampUtil.ConvertDateTimeToInt(DateTime.Now).ToString();
                pubReq.data = req;

                var dic1 = HashUtil.ModelToDic(req);
                var dic2 = HashUtil.AsciiDictionary(dic1);
                var queryStr = HashUtil.BuildQueryString(dic2);
                var signStr = queryStr + txtAPPSECRET.Text;

                SHA256CryptoServiceProvider mySHA256 = new SHA256CryptoServiceProvider();
                byte[] byHash = mySHA256.ComputeHash(Encoding.UTF8.GetBytes(signStr));
                string mySign = BitConverter.ToString(byHash).Replace("-", "").ToUpper();

                pubReq.sign = mySign;

                //报文主体

                string bodyJson = JsonConvert.SerializeObject(pubReq, jsetting);

                string suffix = "/query.do";

                string url = txtMainUrl.Text + suffix;
                GLog.WLog("url:" + url);

                GLog.WLog("请求报文:" + bodyJson);

                var rstStr = HttpUtil.HttpPostJson(url, bodyJson, 30);
                GLog.WLog("响应报文:" + rstStr);

                txtRst.Text = rstStr;

                string _code = "";
                string _msg = "";
                string _trade_status = "";

                //com.fh.resolver.ReturnViewException:
                string fixExp = "com.fh.resolver.ReturnViewException:";
                if (!string.IsNullOrWhiteSpace(rstStr) && rstStr.Contains(fixExp))
                {
                    rstStr = rstStr.Substring(rstStr.IndexOf(fixExp) + fixExp.Length);
                    rstStr = rstStr.Substring(0, rstStr.IndexOf("</div>"));
                    _code = SissPayReturnCode.FAIL.ToString();
                    _msg = rstStr;
                    lblPayStatus.Text = _msg;
                    return;
                }

                var rspM = JsonConvert.DeserializeObject<VIPSPT_PubRsp<OrderQueryRsp>>(rstStr);

                if (rspM.ret == 0)
                {
                    var rspA = rspM.data;
                    _code = SissPayReturnCode.SUCCESS.ToString();
                    if (rspA.status == "2")
                    {
                        _trade_status = SissPayTradeStatus.TRADE_SUCCESS.ToString();
                        lblPayStatus.Text = "成功";
                    }
                    else if (rspA.status == "0")
                    {
                        _trade_status = SissPayTradeStatus.WAIT_BUYER_PAY.ToString();
                        lblPayStatus.Text = "支付中";
                    }
                    else
                    {
                        _trade_status = SissPayTradeStatus.TRADE_FAIL.ToString();
                        lblPayStatus.Text = "失败";
                    }

                    if (!string.IsNullOrWhiteSpace(rspA.third_order_id))
                    {
                        string trade_no = rspA.third_order_id;
                    }

                    if (!string.IsNullOrEmpty(rspA.amount))
                    {
                        string total_amount = rspA.amount;
                    }

                    if (!string.IsNullOrEmpty(rspA.dctime))
                    {
                        string trade_time = rspA.dctime;
                    }
                }
                else
                {
                    _code = SissPayReturnCode.FAIL.ToString();
                    _msg = rspM.msg;
                }


                //#region 转义
                //string _code = "";
                //string _msg = "";
                //string _trade_status = "";

                //var rspMA = JsonConvert.DeserializeObject<BOCOM_PubRsp<OrderQueryRsp>>(rstStr);
                //var rspHead = rspMA.rsp_biz_content.rsp_head;
                //var rspBody = rspMA.rsp_biz_content.rsp_body;

                //_code = SissPayReturnCode.SUCCESS.ToString();

                ////response_status,交易状态 P-处理中  F-失败  S-成功
                //if (rspHead.response_status == "S")
                //{
                //    _code = SissPayReturnCode.SUCCESS.ToString();
                //}
                //else
                //{
                //    _code = SissPayReturnCode.FAIL.ToString();
                //    _msg = rspHead.response_msg;
                //}


                //if (rspBody != null)
                //{
                //    //商户单号
                //    if (!string.IsNullOrWhiteSpace(rspBody.pay_mer_tran_no))
                //        txtOutTradeNo.Text = rspBody.pay_mer_tran_no;

                //    //交易单号
                //    if (rspBody.require_values!=null&&  !string.IsNullOrWhiteSpace(rspBody.require_values.bank_tran_no))
                //        txtTradeNo.Text = rspBody.require_values.bank_tran_no;

                //    //三方交易号
                //    if (rspBody.require_values != null && !string.IsNullOrWhiteSpace(rspBody.require_values.third_party_tran_no))
                //        txtThirdTradeNo.Text = rspBody.require_values.third_party_tran_no;

                //    if (rspBody.order_status == "PAIED")
                //    {
                //        lblPayStatus.Text = "支付成功";
                //        _trade_status = SissPayTradeStatus.TRADE_SUCCESS.ToString();
                //    }
                //    else if (rspBody.order_status == "WAITPAY")
                //    {
                //        lblPayStatus.Text = "支付中";
                //        _trade_status = SissPayTradeStatus.WAIT_BUYER_PAY.ToString();

                //    }
                //    else
                //    {
                //        lblPayStatus.Text = "支付失败";
                //        _trade_status = SissPayTradeStatus.TRADE_FAIL.ToString();

                //    }

                //    if (!string.IsNullOrEmpty(rspBody.total_amount))
                //    {
                //        string total_amount = rspBody.total_amount;
                //    }
                //}

                //#endregion
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }



        OrderQueryRsp QuerCore()
        {
            JsonSerializerSettings jsetting = new JsonSerializerSettings();
            jsetting.DefaultValueHandling = DefaultValueHandling.Ignore;

            var req = new OrderQueryReq();
            req.merchant_id = txtMchNo.Text;
            req.enterpriseReg = txtenterpriseReg.Text;
            req.out_order_id = txtOutTradeNo.Text;

            string dataJson = JsonConvert.SerializeObject(req, jsetting);

            var pubReq = new VIPSPT_PubReq<OrderQueryReq>();
            pubReq.appid = txtAppId.Text;
            pubReq.timeStamp = TimeStampUtil.ConvertDateTimeToInt(DateTime.Now).ToString();
            pubReq.data = req;

            var dic1 = HashUtil.ModelToDic(req);
            var dic2 = HashUtil.AsciiDictionary(dic1);
            var queryStr = HashUtil.BuildQueryString(dic2);
            var signStr = queryStr + txtAPPSECRET.Text;

            SHA256CryptoServiceProvider mySHA256 = new SHA256CryptoServiceProvider();
            byte[] byHash = mySHA256.ComputeHash(Encoding.UTF8.GetBytes(signStr));
            string mySign = BitConverter.ToString(byHash).Replace("-", "").ToUpper();

            pubReq.sign = mySign;

            //报文主体

            string bodyJson = JsonConvert.SerializeObject(pubReq, jsetting);

            string suffix = "/query.do";

            string url = txtMainUrl.Text + suffix;
            GLog.WLog("url:" + url);

            GLog.WLog("请求报文:" + bodyJson);

            var rstStr = HttpUtil.HttpPostJson(url, bodyJson, 30);
            GLog.WLog("响应报文:" + rstStr);

            txtRst.Text = rstStr;

            string _code = "";
            string _msg = "";
            string _trade_status = "";

            //com.fh.resolver.ReturnViewException:
            string fixExp = "com.fh.resolver.ReturnViewException:";
            if (!string.IsNullOrWhiteSpace(rstStr) && rstStr.Contains(fixExp))
            {
                rstStr = rstStr.Substring(rstStr.IndexOf(fixExp) + fixExp.Length);
                rstStr = rstStr.Substring(0, rstStr.IndexOf("</div>"));
                _code = SissPayReturnCode.FAIL.ToString();
                _msg = rstStr;
                lblPayStatus.Text = _msg;
                return null;
            }

            var rspM = JsonConvert.DeserializeObject<VIPSPT_PubRsp<OrderQueryRsp>>(rstStr);
            return rspM.data;
        }

        private void btnRefund_Click(object sender, EventArgs e)
        {
            try
            {

                //查找原交易单号
                OrderQueryRsp  queryModel= QuerCore();
                string trade_no = "";
                if (queryModel != null)
                {
                    trade_no = queryModel.third_order_id;
                }

                JsonSerializerSettings jsetting = new JsonSerializerSettings();
                jsetting.DefaultValueHandling = DefaultValueHandling.Ignore;

                int refund_fee = VString.TryInt((VString.TryDec(txtRefundAmt.Text) * 100));
                txtOutRefundNo.Text = "R0000000000000" + DateTime.Now.ToString("yyyyMMddHHmmssfff");

                var req = new RefundReq();
                req.merchant_id = txtMchNo.Text;
                req.enterpriseReg = txtenterpriseReg.Text;
                req.third_order_id = trade_no;
                req.refund_amount = txtRefundAmt.Text;
                req.refundMsg = "正常退货";

                string dataJson = JsonConvert.SerializeObject(req, jsetting);

                var pubReq = new VIPSPT_PubReq<RefundReq>();
                pubReq.appid = txtAppId.Text;
                pubReq.timeStamp = TimeStampUtil.ConvertDateTimeToInt(DateTime.Now).ToString();
                pubReq.data = req;

                var dic1 = HashUtil.ModelToDic(req);
                var dic2 = HashUtil.AsciiDictionary(dic1);
                var queryStr = HashUtil.BuildQueryString(dic2);
                var signStr = queryStr + txtAPPSECRET.Text;

                SHA256CryptoServiceProvider mySHA256 = new SHA256CryptoServiceProvider();
                byte[] byHash = mySHA256.ComputeHash(Encoding.UTF8.GetBytes(signStr));
                string mySign = BitConverter.ToString(byHash).Replace("-", "").ToUpper();

                pubReq.sign = mySign;

                //报文主体

                string bodyJson = JsonConvert.SerializeObject(pubReq, jsetting);

                string suffix = "/refund.do";

                string url = txtMainUrl.Text + suffix;
                GLog.WLog("url:" + url);

                GLog.WLog("请求报文:" + bodyJson);

                var rstStr = HttpUtil.HttpPostJson(url, bodyJson, 30);
                GLog.WLog("响应报文:" + rstStr);

                txtRst.Text = rstStr;

                string _code = "";
                string _msg = "";
                string _trade_status = "";

                //com.fh.resolver.ReturnViewException:
                string fixExp = "com.fh.resolver.ReturnViewException:";
                if (!string.IsNullOrWhiteSpace(rstStr) && rstStr.Contains(fixExp))
                {
                    rstStr = rstStr.Substring(rstStr.IndexOf(fixExp) + fixExp.Length);
                    rstStr = rstStr.Substring(0, rstStr.IndexOf("</div>"));
                    _code = SissPayReturnCode.FAIL.ToString();
                    _msg = rstStr;
                    lblPayStatus.Text = _msg;
                    return;
                }

                var rspM = JsonConvert.DeserializeObject<VIPSPT_PubRsp<RefundRsp>>(rstStr);

                if (rspM.ret == 0)
                {
                    var rspA = rspM.data;

                    if (rspA.status == "11" || rspA.status == "7")
                    {
                        _code = SissPayReturnCode.SUCCESS.ToString();
                        lblRefundStatus.Text = "成功";
                    }                    
                    else
                    {
                        _code = SissPayReturnCode.FAIL.ToString();
                        lblRefundStatus.Text = "失败";
                    }
                }
                else
                {
                    _code = SissPayReturnCode.FAIL.ToString();
                    _msg = rspM.msg;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnRefundQuery_Click(object sender, EventArgs e)
        {
            try
            {
                //                JsonSerializerSettings jsetting = new JsonSerializerSettings();
                //                jsetting.DefaultValueHandling = DefaultValueHandling.Ignore;

                //                var req = new RefundQueryReq();
                //                req.mer_ptc_id = txtMchNo.Text.Trim();//商户号

                //                req.tran_scene = "B2C-API-SCANCODE";
                //                req.mer_refund_date = DateTime.Now.ToString("yyyyMMdd");//退款发起日期

                //                req.refund_mer_tran_no = txtOutRefundNo.Text;//退款申请单号

                //                var pub = new BOCOM_PubBizReq<RefundQueryReq>();
                //                pub.req_body = req;

                //                //报文主体

                //                string bodyJson = JsonConvert.SerializeObject(pub, jsetting);

                //                string suffix = "/api/pmssMpng/MPNG020703/v1";

                //                #region 生成签名
                //                BOCOM_PubReq cont = new BOCOM_PubReq();
                //                cont.devId = txtAppId.Text;
                //                cont.content = bodyJson;

                //                var dic1 = HashUtil.ModelToDic(cont);

                //                string sign = GetSign(cont, suffix, txtPrivateKey.Text, "PKCS8");

                //                dic1.Add("sign", sign);


                //                #endregion

                //                WebUtils wu = new WebUtils();

                //                string mainUrl = txtMainUrl.Text;
                //                string url = mainUrl + suffix;
                //                GLog.WLog("url:" + url);

                //                GLog.WLog("请求报文:" + bodyJson);
                //                Dictionary<string, string> rspHeader = new Dictionary<string, string>();

                //                var rstStr = wu.DoPost(url, dic1, "UTF-8");
                //                GLog.WLog("响应报文:" + rstStr);
                //                txtRst.Text = rstStr;
                //                if (!ValidSign(rspHeader, rstStr, txtPubCert.Text))
                //                {
                //                    MessageBox.Show("签名验证失败");
                //                }

                //                #region 转义
                //                string _code = "";
                //                string _msg = "";
                //                string _refund_status = "";

                //                var rspMA = JsonConvert.DeserializeObject<BOCOM_PubRsp<OrderQueryRsp>>(rstStr);
                //                var rspHead = rspMA.rsp_biz_content.rsp_head;
                //                var rspBody = rspMA.rsp_biz_content.rsp_body;

                //                _code = SissPayReturnCode.SUCCESS.ToString();

                //                //response_status,交易状态 P-处理中  F-失败  S-成功
                //                if (rspHead.response_status == "S")
                //                {                    
                //                    _code = SissPayReturnCode.SUCCESS.ToString();
                //                }                 
                //                else
                //                {                    
                //                    _code = SissPayReturnCode.FAIL.ToString();
                //                    _msg = rspHead.response_msg;
                //                }

                //                if (rspBody != null)
                //                {
                //                    /*
                //                    INITIAL 初始化
                //PAIED 交易成功
                //WAITPAY 等待支付
                //REFUNDED 部分退款
                //REFUNDALL 全部退款
                //CLOSED 订单关闭 
                //                    */

                //                    if (rspBody.order_status == "REFUNDALL" || rspBody.order_status == "REFUNDED")
                //                    {
                //                        lblRefundStatus.Text = "退款成功";
                //                        _refund_status = SissPayTradeStatus.TRADE_SUCCESS.ToString();
                //                    }
                //                    else
                //                    {
                //                        lblRefundStatus.Text = "退款失败";
                //                        _refund_status = SissPayTradeStatus.TRADE_FAIL.ToString();
                //                    }
                //                }

                //                #endregion


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
