{
	"req_head": {
		"trans_time": "20210913132611",
		"version": "V-1.0"
	},
	"req_body": {
		"valid_period": "20210913172911",
		"scan_code_text": "134672238038762018",
		"terminal_info": "",
		"partner_id": "ISV202103304293",
		"require_fields": [
			{
				"require_field": "open_id"
			},
			{
				"require_field": "sub_openid"
			}
		],
		"currency": "CNY",
		"tran_scene": "B2C-API-SCANCODE",
		"ip": "182.119.117.128",
		"addi_trade_data": {
			"method": "",
			"value": {}
		},
		"mer_ptc_id": "131058140005888",
		"mer_trade_time": "132911",
		"mer_trade_date": "20210913",
		"pay_mer_tran_no": "121882266543778670086",
		"total_amount": "0.01",
		"location": "OFFLINE"
	}
}