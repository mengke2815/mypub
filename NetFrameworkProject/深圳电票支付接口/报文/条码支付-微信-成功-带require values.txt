{
	"rsp_biz_content": {
		"biz_state": "S",
		"rsp_code": null,
		"rsp_msg": null,
		"rsp_body": {
			"pay_mer_tran_no": "9000000601220316092846067",
			"total_amount": "0.01",
			"trd_dsct_amount": "",
			"require_values": {
				"bank_tran_no": "010120220316093014009200094Y",
				"open_id": null,
				"refund_info": null,
				"payment_info": null,
				"third_party": "01",
				"third_party_tran_no": "4200001376202203169473178709",
				"sub_openid": null
			},
			"currency": null,
			"detail": null,
			"buyer_pay_amount": "0.01",
			"sys_order_no": "010120220316093014009200094Y",
			"pay_dsct_amount": null
		},
		"rsp_head": {
			"trans_code": "MPNG210002",
			"response_code": "CIPP0004PY0000",
			"response_status": "S",
			"response_time": "20220316093016",
			"response_msg": "SUCCESS"
		}
	},
	"sign": "DuQefMBQoiX2sXmz/CHECNeUEx/mumpcnUarEv+x56U+YPS/L4Z5NXJ3JJy80uPv+EHYtycF/vrKuVlMn2v+aVSdZJn4JTHCuZ9AACbv+ORB6oSnSwbOMfMfpMCWxgCaYecDB/JVA2F1yKQTK0K7lkF6yrYyXNcid7wAuIBc2KJpH8HD2ic/OHuQpVM95LkknMiqv5/pbBk5oE4c4eICei+0CGl3IVh+1ZaNXUoZFwhIAgt+D1skVw0Q6ErQD/DLwnDV791TVbC36++xvumkUdsCqNDKBNlFjOce4qumD6+Zhmpmx1qTa6ajsdoBesMLSq5SPbr126NBPe0MXPPGiQ=="
}