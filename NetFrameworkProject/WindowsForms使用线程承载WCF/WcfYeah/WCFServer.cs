﻿using CommonUtils;
using System;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceModel.Web;
using System.Threading;

namespace WcfYeah
{
    public class WCFServer
    {
        public WebServiceHost host = null;

        /// <summary>
        /// 线程承载时sleep时间
        /// </summary>
        const int SleepTime = 100;

        private Thread _thread;

        /// <summary>
        /// WCF是否在运行
        /// </summary>
        private bool _isRunning;

        /// <summary>
        /// win server 2008以下用同步承载，以上用线程承载
        /// </summary>
        bool _useThreadHost = true;

        public WCFServer()
        {
            /* 硬编码形式配置WCF服务
             * WebServiceHost需要在项目中右键引用System.ServiceModel.Web.dll程序集
             *  */

            #region 优化调整
            //对外连接数，根据实际情况加大
            if (ServicePointManager.DefaultConnectionLimit < 100)
                System.Net.ServicePointManager.DefaultConnectionLimit = 100;

            int workerThreads;//工作线程数
            int completePortsThreads; //异步I/O 线程数
            ThreadPool.GetMinThreads(out workerThreads, out completePortsThreads);

            int blogCnt = 100;
            if (workerThreads < blogCnt)
            {
                // MinThreads 值不要超过 (max_thread /2  )，否则会不生效。要不然就同时加大max_thread
                ThreadPool.SetMinThreads(blogCnt, blogCnt);
            }
            #endregion

            string port = "16110";

            Uri baseURI = new Uri("http://localhost:" + port.ToString() + "/myapi");
            //注意：这里是实现类，不是接口，否则会报：ServiceHost 仅支持类服务类型。
            host = new WebServiceHost(typeof(Service1), baseURI);

            WebHttpBinding binding = new WebHttpBinding();
            // 这里不需要安全验证
            binding.Security.Mode = WebHttpSecurityMode.None;
            host.AddServiceEndpoint(typeof(IService1), binding, "");
            binding.MaxReceivedMessageSize = 2147483647;
            binding.MaxBufferSize = 2147483647;
            binding.MaxBufferPoolSize = 2147483647;

            binding.OpenTimeout = new TimeSpan(0, 10, 0);
            binding.CloseTimeout = new TimeSpan(0, 10, 0);
            binding.SendTimeout = new TimeSpan(0, 10, 0);
            binding.ReceiveTimeout = new TimeSpan(0, 10, 0);

            //项目需要引用System.Runtime.Serialization.dll
            binding.ReaderQuotas.MaxDepth = 2147483647;
            binding.ReaderQuotas.MaxStringContentLength = 2147483647;
            binding.ReaderQuotas.MaxArrayLength = 2147483647;
            binding.ReaderQuotas.MaxBytesPerRead = 2147483647;
            binding.ReaderQuotas.MaxNameTableCharCount = 2147483647;

            ServiceThrottlingBehavior mdBehavior = new ServiceThrottlingBehavior()
            {
                MaxConcurrentCalls = 96, //16 * CPU核心数
                MaxConcurrentSessions = 600,//100 * CPU核心数
                MaxConcurrentInstances = 696,//MaxConcurrentCalls+MaxConcurrentSession
            };
            host.Description.Behaviors.Add(mdBehavior);


            #region 注册和初始化变量
            //注册
            _thread = new Thread(RunService);

            Version v2008 = new Version("6.0.0.0");
            Version osver = System.Environment.OSVersion.Version;
            GLog.WLog("当前系统版本是：" + osver.ToString());
            if (osver.CompareTo(v2008) >= 0)
            {
                _useThreadHost = true;
                GLog.WLog("系统是2008或以上");
            }
            else
            {
                _useThreadHost = false;
                GLog.WLog("系统是2008以下");
            }
            #endregion

        }


        public void Start()
        {
            if (_useThreadHost)
            {
                GLog.WLog("Start() 线程承载,_isRunning:" + _isRunning);
                if (!_isRunning)
                {
                    GLog.WLog("Start() _thread.Start()");
                    _thread.Start();
                }
            }
            else
            {
                #region 同步承载
                GLog.WLog("Start() 同步承载 ");
                try
                {
                    host.Open();
                }
                catch (Exception ex)
                {                    
                    GLog.WLog("服务启动失败:" + ex.Message);
                    throw ex;
                }
                #endregion
            }
        }

        public void Close()
        {
            GLog.WLog("wcfservice Close");
            if (_useThreadHost)
            {
                GLog.WLog("wcfservice Close 线程上停止WCF");
                lock (this)
                {
                    _isRunning = false;
                }
            }
            else
            {
                #region 同步
                GLog.WLog("wcfservice Close 同步停止WCF");
                try
                {
                    if (host != null)
                    {
                        host.Close();
                    }
                    host = null;
                }
                catch (Exception ex)
                {
                    GLog.WLog("wcfservice Close host.Close();时异常：" + ex.Message);
                }
                #endregion
            }
        }

        /// <summary>
        /// 线程承载WCF
        /// </summary>
        void RunService()
        {
            try
            {
                _isRunning = true;

                host.Open();
                GLog.WLog("RunService host.Open();");
                while (_isRunning)
                {
                    Thread.Sleep(SleepTime);
                }
                host.Close();
                GLog.WLog("RunService host.Close();");
                ((IDisposable)host).Dispose();
                GLog.WLog("RunService host Dispose;");
            }
            catch (Exception ex)
            {
                GLog.WLog("RunService 异常;" + ex.Message);

                try
                {
                    if (host != null)
                        host.Close();
                }
                catch (Exception exClose)
                {
                    GLog.WLog("host.Close();异常" + exClose.Message);
                }
            }
        }


    }
}
