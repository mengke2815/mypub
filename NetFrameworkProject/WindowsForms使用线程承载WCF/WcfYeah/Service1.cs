﻿using System;
using System.ServiceModel;

namespace WcfYeah
{
    // 调整 ServiceBehavior，使其支持并发
    [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.PerCall, UseSynchronizationContext = false)]
    public class Service1 : IService1
    {
        public CompositeType geta(CompositeType composite)
        {
            CompositeType myret = new CompositeType();
            try
            {
                //System.Threading.Thread.Sleep(200);
                if(composite==null)
                    myret.StringValue = "输入实体为空:" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");
                else
                    myret.StringValue = "输入实体不为空:" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");
            }
            catch (Exception ex)
            {
                myret.StringValue = "发生异常:" + ex.Message;                 
            }
            return myret;
        }

    }
}
