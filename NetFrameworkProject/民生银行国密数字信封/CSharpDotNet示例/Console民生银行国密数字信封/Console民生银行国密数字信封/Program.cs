﻿using CommonUtils;
using Org.BouncyCastle.Asn1;
using Org.BouncyCastle.Asn1.Cms;
using Org.BouncyCastle.Asn1.X509;
using Org.BouncyCastle.Cms;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Math;
using Org.BouncyCastle.Utilities.Encoders;
using System;
using System.IO;
using System.Text;

namespace Console民生银行国密数字信封
{
    internal class Program
    {
        static void Main(string[] args)
        {
            try
            {
                //国密公钥证书
                string txtIsvPubCert = "MIICuzCCAl6gAwIBAgIFMAU2UREwDAYIKoEcz1UBg3UFADArMQswCQYDVQQGEwJDTjEcMBoGA1UECgwTQ0ZDQSBTTTIgVEVTVCBPQ0EyMTAeFw0yMDA1MTQxMDQ5MjNaFw0yMTA1MTQxMDQ5MjNaMHExCzAJBgNVBAYTAkNOMQ0wCwYDVQQKDARDTUJDMRIwEAYDVQQLDAlDTUJDX0RDTVMxGTAXBgNVBAsMEE9yZ2FuaXphdGlvbmFsLTExJDAiBgNVBAMMGzAzMDVAWjEyMzMyMjMxQDYzODA1MTQwMDFAMTBZMBMGByqGSM49AgEGCCqBHM9VAYItA0IABEnvYQ89A2eFUsEvhfik13ehN1vNivpfk3UTCOPp209UbwLlatSu1aBFerNeNB6FQ9dgSSaXCiSMUfgKKL9wpBWjggElMIIBITAfBgNVHSMEGDAWgBTifrYQu5TrFeau0RUK/+jXoFc5nTBIBgNVHSAEQTA/MD0GCGCBHIbvKgICMDEwLwYIKwYBBQUHAgEWI2h0dHA6Ly93d3cuY2ZjYS5jb20uY24vdXMvdXMtMTMuaHRtMGkGA1UdHwRiMGAwLqAsoCqGKGh0dHA6Ly8yMTAuNzQuNDIuMy9PQ0EyMS9TTTIvY3JsMzE1Ni5jcmwwLqAsoCqGKGh0dHA6Ly8yMTAuNzQuNDIuMy9PQ0EyMS9TTTIvY3JsMzE1Ni5jcmwwCwYDVR0PBAQDAgP4MB0GA1UdDgQWBBRXXvBVVWTiR6ARshCGukqaGyUKfzAdBgNVHSUEFjAUBggrBgEFBQcDAgYIKwYBBQUHAwQwDAYIKoEcz1UBg3UFAANJADBGAiEA7O5rAx4+Alx2eIq0CHxTrNrQIOmphxShur4JDMwLiEYCIQDpJD/z85jWcwUeRAOOcstm4Ppl5zKu6pPohnbKm7gqvg==";

                //国密私钥证书
                string gmPrivateKeyCert = @"MIIDHwIBATBHBgoqgRzPVQYBBAIBBgcqgRzPVQFoBDBgkpL4SzxKcmNmrzvEtrl5lncqsy/fvW/6
uKSe2Wa5x0NeIcRuohTMdQIYd6rXsOAwggLPBgoqgRzPVQYBBAIBBIICvzCCArswggJeoAMCAQIC
BTAFNlERMAwGCCqBHM9VAYN1BQAwKzELMAkGA1UEBhMCQ04xHDAaBgNVBAoME0NGQ0EgU00yIFRF
U1QgT0NBMjEwHhcNMjAwNTE0MTA0OTIzWhcNMjEwNTE0MTA0OTIzWjBxMQswCQYDVQQGEwJDTjEN
MAsGA1UECgwEQ01CQzESMBAGA1UECwwJQ01CQ19EQ01TMRkwFwYDVQQLDBBPcmdhbml6YXRpb25h
bC0xMSQwIgYDVQQDDBswMzA1QFoxMjMzMjIzMUA2MzgwNTE0MDAxQDEwWTATBgcqhkjOPQIBBggq
gRzPVQGCLQNCAARJ72EPPQNnhVLBL4X4pNd3oTdbzYr6X5N1Ewjj6dtPVG8C5WrUrtWgRXqzXjQe
hUPXYEkmlwokjFH4Cii/cKQVo4IBJTCCASEwHwYDVR0jBBgwFoAU4n62ELuU6xXmrtEVCv/o16BX
OZ0wSAYDVR0gBEEwPzA9BghggRyG7yoCAjAxMC8GCCsGAQUFBwIBFiNodHRwOi8vd3d3LmNmY2Eu
Y29tLmNuL3VzL3VzLTEzLmh0bTBpBgNVHR8EYjBgMC6gLKAqhihodHRwOi8vMjEwLjc0LjQyLjMv
T0NBMjEvU00yL2NybDMxNTYuY3JsMC6gLKAqhihodHRwOi8vMjEwLjc0LjQyLjMvT0NBMjEvU00y
L2NybDMxNTYuY3JsMAsGA1UdDwQEAwID+DAdBgNVHQ4EFgQUV17wVVVk4kegEbIQhrpKmhslCn8w
HQYDVR0lBBYwFAYIKwYBBQUHAwIGCCsGAQUFBwMEMAwGCCqBHM9VAYN1BQADSQAwRgIhAOzuawMe
PgJcdniKtAh8U6za0CDpqYcUobq+CQzMC4hGAiEA6SQ/8/OY1nMFHkQDjnLLZuD6ZecyruqT6IZ2
ypu4Kr4=";
                //国密私钥证书密码
                string gmPrivateKeyCertPwd = "123abc";

                string inputStr = "oh yeah 123。";
                Console.WriteLine("数字信封-待加密数据:" + inputStr);
                var aa = CmsEncrypt(inputStr, txtIsvPubCert);
                Console.WriteLine("数字信封-加密数据:" + aa);

                //测试JAVA程序加密结果
                //  aa = "MIIBdgYKKoEcz1UGAQQCA6CCAWYwggFiAgECMYGdMIGaAgECgBRXXvBVVWTiR6ARshCGukqaGyUKfzANBgkqgRzPVQGCLQMFAARwV3Je52JbokQWyl7uD2GDuWtkyzIzn8nsE2uX2XthHTwnoB+WtLkemKjs+iUJl5EShTgUI2g5N6Uw3/4QN9/LHETixsQ/Emv3hW9W3V2ERNJiu9Hd/raPN+zIyWhraEU6oXTvpwfFgNoVjbp9U9UbHjCBvAYKKoEcz1UGAQQCATAbBgcqgRzPVQFoBBArCB1Z2eO/rLl1Xdn9MMIOgIGQKNpS0jslyLvneyGs2xI2Rz4dLiEznOeXt8JjuEo08vKNf0JOpxFIK9q8X8oCE/NCBjwSmZ38ZwauUydQE+3zQUiInqIOPV8+w+1ejmi4LyWTeDuWtYUESw7qfmG7oxd6hvIM3pswNXXxOEjfmH/7cS8iHCzRg2fli0smDOREil8lPjUM68lTZ9xgcsq4a8+2";                
                // Console.WriteLine("数字信封-加密数据:" + aa);

                var bb = CmsDecrypt(aa, gmPrivateKeyCert, gmPrivateKeyCertPwd);
                Console.WriteLine("数字信封-解密数据:" + bb);
            }
            catch (Exception ex)
            {
                Console.WriteLine("ex:" + ex.Message);
            }
            Console.ReadKey();
        }

        /// <summary>
        /// 数字信封-加密
        /// </summary>
        static string CmsEncrypt(string inputStr, string pubkeyCert)
        {
            //SM2,SM4 加密数据从 string 转 byte  数组时，使用UTF8。
            Encoding enc = Encoding.UTF8;
            //调试时可以写死KEY和IV
            //string sm4key = "1234567890123456";
            //string sm4iv = "1234567890123456";
            //随机生成KEY和IV，都是16字符长度
            string sm4key = Guid.NewGuid().ToString("N").Substring(0, 16);
            string sm4iv = Guid.NewGuid().ToString("N").Substring(0, 16);

            //如果是 NoPadding, SM4 加密的数据（byte[] plain）得是16的倍数
            string algo = "SM4/CBC/PKCS7Padding";
            byte[] keyBytes = Encoding.UTF8.GetBytes(sm4key);
            byte[] ivBytes = Encoding.UTF8.GetBytes(sm4iv);

            #region 使用SM2加密SM4 KEY

            AsymmetricKeyParameter bankPubCert2 = GmUtil.GetPublickeyFromX509String(pubkeyCert);
            byte[] bytencryptedData = enc.GetBytes(sm4key);
            byte[] bysm4keyEncrypted = GmUtil.Sm2EncryptOld(bytencryptedData, bankPubCert2);//使用SM2加密SM4 KEY

            //BC库SM2加密结果会带04，删除加密结果前的04，否则JAVA（民生银行） 那边解密不了。Invalid point encoding
            string newCipherText = Hex.ToHexString(bysm4keyEncrypted);
            if (newCipherText.StartsWith("04"))
            {
                newCipherText = newCipherText.Substring(2);
            }
            bysm4keyEncrypted = Hex.Decode(newCipherText);

            // 主题密钥标识符 (SKI)。 JAVA BC 库写法：SubjectKeyIdentifier sid = cert.getSubjectKeyIdentifier();             
            SubjectKeyIdentifier sid = new Org.BouncyCastle.X509.Extension.SubjectKeyIdentifierStructure(bankPubCert2);

            #endregion

            Asn1OctetString encKey = new DerOctetString(bysm4keyEncrypted);
            AlgorithmIdentifier keyEncAlg = new AlgorithmIdentifier(new DerObjectIdentifier("1.2.156.10197.1.301.3")); //sm2  公钥加密算法ID
            KeyTransRecipientInfo ktr = new KeyTransRecipientInfo(new RecipientIdentifier(new DerOctetString(sid.GetKeyIdentifier())), keyEncAlg, encKey);
            RecipientInfo ri = new RecipientInfo(ktr); //SM4 key 使用SM2加密后，放入RecipientInfo。

            byte[] sourceData = enc.GetBytes(inputStr);
            byte[] encryptedData = GmUtil.Sm4EncryptCBC(keyBytes, sourceData, ivBytes, algo); ;//待SM4 加密，加密后的数据

            DerObjectIdentifier sm2DataID = new DerObjectIdentifier("1.2.156.10197.6.1.4.2.1");//SM2数据类型data ID
            DerOctetString doct = new DerOctetString(ivBytes);
            DerObjectIdentifier tOID = new DerObjectIdentifier("1.2.156.10197.1.104"); // SM4_CBC ,ID 
            AlgorithmIdentifier contentEncryptionAlgId = new AlgorithmIdentifier(tOID, doct);//用 AlgorithmIdentifier携带 IV 数据

            Asn1EncodableVector recipientInfos = new Asn1EncodableVector();
            recipientInfos.Add(ri);//SM4 key 使用SM2加密后，放入RecipientInfo。
            Asn1Set aSet1 = null;

            Asn1OctetString encryptOctet = new BerOctetString(encryptedData);//SM4加密后的数据
            EncryptedContentInfo eci = new EncryptedContentInfo(sm2DataID, contentEncryptionAlgId, encryptOctet);// SM4-IV 和 SM4加密后的数据
            EnvelopedData enData = new EnvelopedData(null, new DerSet(recipientInfos), eci, aSet1);// SM4-KEY,SM4-IV 和 SM4加密后的数据

            DerObjectIdentifier sm2EnvelopedDataID = new DerObjectIdentifier("1.2.156.10197.6.1.4.2.3");//数字信封数据类型envelopedData ID
            ContentInfo contentInfo = new ContentInfo(sm2EnvelopedDataID, enData);

            string base64CmsEncryptData = string.Empty;

            using (MemoryStream bos = new MemoryStream())
            {
                using (DerOutputStream dos = new DerOutputStream(bos))
                {
                    dos.WriteObject((new CmsEnvelopedData(contentInfo)).ContentInfo);
                    byte[] byteArray = bos.ToArray();
                    base64CmsEncryptData = Convert.ToBase64String(byteArray);
                }
            }

            return base64CmsEncryptData;
        }

        /// <summary>
        /// 数字信封-解密
        /// </summary>
        static string CmsDecrypt(string cmsEnvelopedData, string gmPrivateKeyCert, string gmPrivateKeyCertPwd)
        {

            //获取私钥对象 AsymmetricKeyParameter privateKey
            var pemPrivateKey = Convert.FromBase64String(gmPrivateKeyCert);
            var tempPrivateKey = GmUtil.readSm2File(pemPrivateKey, gmPrivateKeyCertPwd);
            AsymmetricKeyParameter privateKey = tempPrivateKey.privateKey;

            byte[] bEnvelop = Convert.FromBase64String(cmsEnvelopedData);
            CmsEnvelopedData cmsEnData = new CmsEnvelopedData(bEnvelop);
            ContentInfo info = cmsEnData.ContentInfo;
            EnvelopedData enData = EnvelopedData.GetInstance(info.Content);
            Asn1Set receivers = enData.RecipientInfos;

            #region 使用SM2 私钥解密出SM4 KEY 

            //没适配多receivers的情况，强取第1个
            RecipientInfo recipientInfo = RecipientInfo.GetInstance(receivers[0]);
            KeyTransRecipientInfo keyTransRecipientInfo = KeyTransRecipientInfo.GetInstance(recipientInfo.Info);
            Asn1OctetString encryptKey = keyTransRecipientInfo.EncryptedKey;
            AlgorithmIdentifier algId = keyTransRecipientInfo.KeyEncryptionAlgorithm;
            byte[] byEncryptKey = encryptKey.GetOctets();
            string hexEncryptKey = Hex.ToHexString(byEncryptKey);
            //BC库解密，密文前要加 “04”，否则会报 Invalid point encoding XX
            if (!hexEncryptKey.StartsWith("04"))
            {
                hexEncryptKey = "04" + hexEncryptKey;
            }
            byEncryptKey = Hex.Decode(hexEncryptKey);
            byte[] symmetricKey = GmUtil.Sm2DecryptOld(byEncryptKey, privateKey);
            #endregion

            #region 拿到 SM4 IV
            //拿到 SM4 IV
            EncryptedContentInfo data = enData.EncryptedContentInfo;
            AlgorithmIdentifier symmetricAlgId = data.ContentEncryptionAlgorithm;
            DerOctetString doct = (DerOctetString)symmetricAlgId.Parameters;
            byte[] bySm4Iv = doct.GetOctets();

            #endregion

            //取出被SM4加密的数据
            string algo = "SM4/CBC/PKCS7Padding";
            byte[] encryptedBytes = data.EncryptedContent.GetOctets();
            byte[] sourceData = GmUtil.Sm4DecryptCBC(symmetricKey, encryptedBytes, bySm4Iv, algo);
            string sm4DecryptedStr = Encoding.UTF8.GetString(sourceData);//偷懒，写死了UTF8。
            return sm4DecryptedStr;
        }

    }
}