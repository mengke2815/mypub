﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinFormBase
{
    public partial class FrmSelectName : Form
    {

        #region VAR
        /// <summary>
        /// 周目
        /// </summary>
        public int ZM { get; set; }
        /// <summary>
        /// 等级
        /// </summary>
        public int DJ { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Memo { get; set; }

        #endregion

        public FrmSelectName()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtMemo.Text))
            {
                MessageBox.Show("存档备注内容不能为空！");
                return;
            }
            int tZM = 1;
            int.TryParse(nudZM.Value.ToString(),out tZM);
            int tDJ = 1;
            int.TryParse(nudDJ.Value.ToString(), out tDJ);
            string cfMsg = string.Format("确定周目：{0}，等级：{1}，备注：{2}？", tZM.ToString(), tDJ.ToString(),txtMemo.Text.Trim());
            DialogResult dr1 = MessageBox.Show(cfMsg, "确定", MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
            if (dr1 == DialogResult.Cancel)
            {
                return;
            }

            ZM = tZM;
            DJ= tDJ;
            Memo = txtMemo.Text.Trim();

            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
