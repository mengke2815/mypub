﻿using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace WinFormBase.Utils
{
    public class FolderDialog : FolderNameEditor
    {

        FolderNameEditor.FolderBrowser fDialog = new FolderBrowser();

        public FolderDialog()
        {
        }

        /// <summary>
        /// 弹出
        /// </summary>
        /// <returns></returns>
        public DialogResult DisplayDialog()
        {
            return DisplayDialog("请选择一个文件夹");
        }

        /// <summary>
        /// 弹出
        /// </summary>
        /// <param name="description"></param>
        /// <returns></returns>
        public DialogResult DisplayDialog(string description)
        {
            fDialog.Description = description;
            fDialog.Style = FolderBrowserStyles.ShowTextBox;
            return fDialog.ShowDialog();
        }

        /// <summary>
        /// 选择的文件夹路径
        /// </summary>
        public string Path
        {
            get
            {
                return fDialog.DirectoryPath;
            }
        }


    }
}
