﻿using CommonUtils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WinFormBase.Utils;

namespace WinFormBase
{
    public partial class Form1 : Form
    {
        System.Timers.Timer _tmrRemeber;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.Text = this.Text + " [" + AppVerUtil.GetAppVer() + "]";
            //将配置文件里的值，加载到界面 
            RemForm.ReadRememberForm(this, null);

            //开启记忆功能
            _tmrRemeber = new System.Timers.Timer();
            _tmrRemeber.Elapsed += new System.Timers.ElapsedEventHandler(_tmrRemeber_Elapsed);
            _tmrRemeber.Interval = 10 * 1000;
            _tmrRemeber.Start();
        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            //启动时加载左边列表
            if (!string.IsNullOrWhiteSpace(txtGameSavePath.Text) && Directory.Exists(txtGameSavePath.Text))
            {
                btnOrgLoad_Click(sender, e);
                DisplayRight();
            }
        }

        void _tmrRemeber_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                RemForm.RememberForm(this, null);
            }
            catch (Exception ex)
            {
                //MessageBox.Show("_tmrRemeber_Elapsed" + ex.Message);
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult dr = MessageBox.Show("确定要退出吗？", "确定",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
            if (dr == DialogResult.No)
            {
                e.Cancel = true;
                return;
            }
        }

        private void tsmiExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOrgBrowse_Click(object sender, EventArgs e)
        {
            FolderDialog fdSource = new FolderDialog();
            if (fdSource.DisplayDialog() == DialogResult.OK)
            {
                txtGameSavePath.Text = fdSource.Path;
                btnOrgLoad_Click(sender, e);
            }
        }

        private void btnOrgLoad_Click(object sender, EventArgs e)
        {
            if (!Directory.Exists(txtGameSavePath.Text))
            {
                return;
            }
            dgvLeft.Rows.Clear();

            var myfiles = Directory.GetFiles(txtGameSavePath.Text);
            foreach (var myf in myfiles)
            {
                //ArchiveSaveFile.1.sav,ArchiveSaveFile.2.sav
                FileInfo fi = new FileInfo(myf);
                if (fi.Name.StartsWith("ArchiveSaveFile") && fi.Name.EndsWith(".sav"))
                {
                    dgvLeft.Rows.Add(fi.Name, fi.LastWriteTime.ToString("yyyy-MM-dd HH:mm:ss"));
                }
            }

        }

        

        /// <summary>
        /// 设置存档备份文件夹
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnBackupBrowse_Click(object sender, EventArgs e)
        {
            FolderDialog fdSource = new FolderDialog();
            if (fdSource.DisplayDialog() == DialogResult.OK)
            {
                txtSaveBackupDir.Text = fdSource.Path;
            }
        }

        private void btnBackup_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(txtGameSavePath.Text))
                {
                    MessageBox.Show("游戏存档目录 不能为空！");
                    return;
                }
                if (!Directory.Exists(txtGameSavePath.Text))
                {
                    MessageBox.Show("游戏存档目录 不存在！");
                    return;
                }
                if (dgvLeft.Rows.Count == 0)
                {
                    MessageBox.Show("没有任何游戏存档文件！");
                    return;
                }
                //确定退出到游戏主界面了（或退出整个游戏）
                string cfMsg = string.Format("确定退出到游戏主界面了（或退出整个游戏）？若未退出，可能造成存档损坏！");
                DialogResult dr1 = MessageBox.Show(cfMsg, "确定", MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                if (dr1 == DialogResult.Cancel)
                {
                    return;
                }

                //左边当前行，存档文件名。
                string fileName = dgvLeft.SelectedRows[0].Cells["存档文件名"].Value.ToString();
                 cfMsg = "确定要备份：" + fileName + "，这个存档？";
                 dr1 = MessageBox.Show(cfMsg, "确定", MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                if (dr1 == DialogResult.Cancel)
                {
                    return;
                }
                //是否退出到游戏主界面（或退出整个游戏）

                FrmSelectName frm2 = new FrmSelectName();
                DialogResult dr2 = frm2.ShowDialog();
                if (dr2 == DialogResult.Cancel)
                {
                    return;
                }
                //原文件
                string sourceFileFullName = Path.Combine(txtGameSavePath.Text, fileName);
                //目标文件
                string bakFileName = string.Format("{0}周目-{1}级-{2}.sav", frm2.ZM.ToString(), frm2.DJ.ToString(), frm2.Memo);
                //ArchiveSaveFile.2.sav - ArchiveSaveFile2sav

                string targetDir = fileName.Replace(".", ""); //ArchiveSaveFile2sav
                //备份目录\ArchiveSaveFile2sav\
                string backupFullFileName = Path.Combine(txtSaveBackupDir.Text, targetDir);
                if (!Directory.Exists(backupFullFileName))
                {
                    Directory.CreateDirectory(backupFullFileName);
                }
                //备份目录\ArchiveSaveFile2sav\1周目-41级-打虎先锋前.sav
                backupFullFileName = Path.Combine(backupFullFileName, bakFileName);
                if (File.Exists(backupFullFileName))
                {
                    //目标文件已存在，是否要覆盖
                    cfMsg = "目标文件已存在：" + backupFullFileName + "，是否要覆盖？";
                    dr1 = MessageBox.Show(cfMsg, "确定", MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                    if (dr1 == DialogResult.Cancel)
                    {
                        return;
                    }
                }

                File.Copy(sourceFileFullName, backupFullFileName, true);
                MessageBox.Show("备份成功：" + backupFullFileName);
                DisplayRight();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// 根据左边选择，显示右边内容
        /// </summary>
        void DisplayRight()
        {
            try
            {
                if (dgvLeft.Rows.Count == 0)
                {
                    return;
                }
                if (dgvLeft.SelectedRows.Count == 0)
                {
                    return;
                }
                dgvRight.Rows.Clear();

                //左边当前行，存档文件名。
                string fileName = dgvLeft.SelectedRows[0].Cells["存档文件名"].Value.ToString();
                string targetDir = fileName.Replace(".", ""); //ArchiveSaveFile2sav
                string backupDirFullName = Path.Combine(txtSaveBackupDir.Text, targetDir);
                if (!Directory.Exists(backupDirFullName))
                {
                    return;
                }

                var myfiles = Directory.GetFiles(backupDirFullName);
                foreach (var myf in myfiles)
                {
                    FileInfo fi = new FileInfo(myf);
                    dgvRight.Rows.Add(fi.Name, fi.LastWriteTime.ToString("yyyy-MM-dd HH:mm:ss"));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("DisplayRight() ex:" + ex.Message);
            }

        }

        private void dgvLeft_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            DisplayRight();
        }

        private void btnRestore_Click(object sender, EventArgs e)
        {
            try
            {
                //右边不能为空
                if (dgvRight.Rows.Count == 0)
                    return;
                if (dgvRight.SelectedRows.Count == 0)
                    return;
                //左边不能为空
                if (dgvLeft.Rows.Count == 0)
                    return;
                if (dgvLeft.SelectedRows.Count == 0)
                    return;

                string leftName = dgvLeft.SelectedRows[0].Cells["存档文件名"].Value.ToString();
                string rightName = dgvRight.SelectedRows[0].Cells["rFileName"].Value.ToString();
                string leftNameReplaced = leftName.Replace(".", "");
                string rightDir = Path.Combine(txtSaveBackupDir.Text, leftNameReplaced);
                string rightFullName = Path.Combine(rightDir, rightName);
                string leftFullName = Path.Combine(txtGameSavePath.Text, leftName);

                string cfMsg = string.Format("确定要将文件:{0},还原到:{1}？", rightFullName, leftFullName);
                DialogResult dr1 = MessageBox.Show(cfMsg, "确定", MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                if (dr1 == DialogResult.Cancel)
                {
                    return;
                }
                //确定退出到游戏主界面了（或退出整个游戏）
                cfMsg = string.Format("确定退出到游戏主界面了（或退出整个游戏）？若未退出，可能造成存档损坏！");
                dr1 = MessageBox.Show(cfMsg, "确定", MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                if (dr1 == DialogResult.Cancel)
                {
                    return;
                }
                //将备份，还原到游戏存档目录。
                File.Copy(rightFullName, leftFullName,true);
                MessageBox.Show("还原成功！"  );

            }
            catch (Exception ex)
            {

                MessageBox.Show("还原失败 ex:" + ex.Message);
            }
        }

        private void nfiMain_MouseDoubleClick(object sender, MouseEventArgs e)
        {
             
        }
    }
}
