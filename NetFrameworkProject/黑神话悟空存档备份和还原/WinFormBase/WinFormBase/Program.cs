﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinFormBase
{
    static class Program
    {
        static System.Threading.Mutex appMutex;

        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            string exeName = "黑神话悟空存档备份和还原";
            string globalMutexName = @"Global\" + exeName;
            string appName = "黑神话悟空存档备份和还原";

            bool createNew;
            appMutex = new System.Threading.Mutex(true, globalMutexName, out createNew);
            if (!createNew)
            {
                appMutex.Close();
                appMutex = null;
                MessageBox.Show(appName + " 已开启，进程为：‘" + exeName + "’！", "提示");
                return;
            }

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
