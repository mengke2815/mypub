﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// 有关程序集的一般信息由以下
[assembly: AssemblyTitle("黑神话悟空存档备份和还原")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("黑神话悟空存档备份和还原")]
[assembly: AssemblyCopyright("Copyright © runliuv  2024")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

//将 ComVisible 设置为 false 将使此程序集中的类型
[assembly: ComVisible(false)]

// 如果此项目向 COM 公开，则下列 GUID 用于类型库的 ID
[assembly: Guid("07cfa4f3-f3b4-4d14-bad3-1031146d0abe")]

//可以指定所有这些值，也可以使用“生成号”和“修订号”的默认值
[assembly: AssemblyVersion("24.09.21.16")]
[assembly: AssemblyFileVersion("24.09.21.16")]
