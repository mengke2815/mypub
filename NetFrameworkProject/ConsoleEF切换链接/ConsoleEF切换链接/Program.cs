﻿using ConsoleEF切换链接.Models;
using ConsoleEF切换链接.MyDbContext;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace ConsoleEF切换链接
{
    internal class Program
    {
        static void Main(string[] args)
        {
            try
            {
                using (Db1DbContext db1 = new Db1DbContext())
                {
                    Table1 t1 = new Table1();
                    t1.bb = "bb" + DateTime.Now.ToString();
                    t1.cc = "c" + DateTime.Now.ToString();

                    db1.Table1s.Add(t1);
                    db1.SaveChanges();

                }

                Console.WriteLine("完成 1 !");

                var str = ConfigurationManager.ConnectionStrings["Db2Context"].ConnectionString;                 
                SqlConnection db2conn = new SqlConnection(str);
                var db2Ctx = new Db1DbContext(db2conn);
                Table1 t2 = new Table1();
                t2.bb = "bb" + DateTime.Now.ToString();
                t2.cc = "c" + DateTime.Now.ToString();

                db2Ctx.Table1s.Add(t2);
                db2Ctx.SaveChanges();


                Console.WriteLine("完成 2 !");

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            Console.WriteLine("press any key to exit!");

            Console.ReadKey();
        }
    }
}
