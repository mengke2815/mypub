﻿using ConsoleEF切换链接.Models;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;

namespace ConsoleEF切换链接.MyDbContext
{
    public class Db1DbContext : DbContext
    {
        public Db1DbContext()
            : base("name=Db1Context")
        {
        }

        public Db1DbContext(DbConnection conn)
            : base(conn,true)
        {
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //移除复数表名
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            base.OnModelCreating(modelBuilder);
        }

        public virtual DbSet<Table1> Table1s { get; set; }
    }
}
