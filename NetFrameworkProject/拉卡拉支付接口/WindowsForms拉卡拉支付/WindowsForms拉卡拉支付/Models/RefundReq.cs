﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LAKALA.Models
{
    public  class RefundReq
    {
        public string mercId { get; set; }
        public string termNo { get; set; }


        public string refundOrderId { get; set; }

        /// <summary>
        /// 退款金额（单位分）
        /// </summary>
        public string amount { get; set; }
        /// <summary>
        /// 商户系统唯一
        /// </summary>
        public string ornOrderId { get; set; }
         

    }
}
