﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LAKALA.Models
{
    public  class MicroPayReq
    {
        public string mercId { get; set; }
        public string termNo { get; set; }
        public string authCode { get; set; }

        /// <summary>
        /// 交易金额（单位分）
        /// </summary>
        public string amount { get; set; }
        /// <summary>
        /// 商户系统唯一
        /// </summary>
        public string orderId { get; set; }
        /// <summary>
        /// 订单包含的商品列表信息，Json格式。payMode为WECHAT，ALIPAY时，可选填此字段
        /// </summary>
        public string detail { get; set; }

        /// <summary>
        /// 标题
        /// </summary>
        public string subject { get; set; }

    }
}
