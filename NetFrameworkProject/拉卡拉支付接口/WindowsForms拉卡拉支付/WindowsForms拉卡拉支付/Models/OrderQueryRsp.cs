﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LAKALA.Models
{
    public class OrderQueryRsp
    {
        public string openId { get; set; }
        public string payMode { get; set; }


        /// <summary>
        /// 交易金额（单位分）
        /// </summary>
        public string amount { get; set; }

        public string lklOrderNo { get; set; }

        /// <summary>
        /// 商户系统唯一
        /// </summary>
        public string orderId { get; set; }

        public string weOrderNo { get; set; }

        /// <summary>
        /// 20210723140155
        /// </summary>
        public string tradeTime { get; set; }

        public string tradeState { get; set; }


    }
}
