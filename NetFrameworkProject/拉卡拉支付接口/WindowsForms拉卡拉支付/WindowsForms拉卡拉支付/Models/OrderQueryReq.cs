﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LAKALA.Models
{
    public class OrderQueryReq
    {
        public string mercId { get; set; }
        public string termNo { get; set; }

        /// <summary>
        /// 原商户订单号
        /// </summary>
        public string ornOrderId { get; set; }


        /// <summary>
        /// 拉卡拉商户订单号
        /// </summary>
        public string lklOrderNo { get; set; }

    }
}
