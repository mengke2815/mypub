﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LAKALA.Models
{
    public  class LKL_PubReq<T>
    {
        public LKL_PubReq()
        {
             ver = "1.0.0";
        }
        /// <summary>
        /// Unix时间戳。13位
        /// </summary>
        public long timestamp { get; set; }

        public string rnd { get; set; }
        public string ver { get; set; }
        public string reqId { get; set; }
        
        public T reqData { get; set; }

        public TermExtInfo termExtInfo { get; set; }
    }
}
